package amazonservices

import (
	"context"
	"github.com/spf13/viper"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/amazon"
	"net/http"
	//"time"
	"bitbucket.org/jyotiswarp/ppc/utils/math"
	//"time"
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationdao"
	"github.com/prometheus/common/log"
	"time"
)

type AmazonConfig struct {
	config *oauth2.Config
}

func NewAmzConfig() *AmazonConfig {
	return &AmazonConfig{
		config: &oauth2.Config{
			RedirectURL:  "https://app.sellerprime.com/seller/dashboard",
			ClientID:     viper.GetString("client_id"),
			ClientSecret: viper.GetString("client_secret"),
			/*Scopes: []string{"https://www.googleapis.com/auth/userinfo.profile",
			"https://www.googleapis.com/auth/userinfo.sellerId"},*/
			Endpoint: oauth2.Endpoint{AuthURL: amazon.Endpoint.AuthURL, TokenURL: amazon.Endpoint.TokenURL},
		},
	}
}

func (amzConfig *AmazonConfig) GetConfig() *oauth2.Config {

	return amzConfig.config

}

var (
	// Some random string, random for each request
	oauthStateString = math.Random(10)
)

func (amzConfig *AmazonConfig) GetAmzClient(email string) *http.Client {
	token := new(oauth2.Token)
	err, authData := authenticationdao.GetAuthDetails(email)
	if err != nil {
		log.Error()
	}
	token.AccessToken = authData.AccessToken   //viper.GetString("access_token")   // todo get it from DB
	token.RefreshToken = authData.RefreshToken //viper.GetString("refresh_token") //todo get it from DB
	token.Expiry = time.Now()                  //todo change
	token.TokenType = "Bearer"
	ctx := context.Background()
	client := amzConfig.config.Client(ctx, token)
	client.Timeout = time.Minute * 1
	return client
}
