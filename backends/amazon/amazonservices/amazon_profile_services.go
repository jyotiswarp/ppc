package amazonservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonmodel"
	"bitbucket.org/jyotiswarp/ppc/models"
	"encoding/json"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"strings"
	"errors"
)

func FetchUserProfiles(sellerId, email, geo string) (result []models.Profile, err error) {
	log.Println("Fetching profile list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")

	req, err := http.NewRequest("GET", endpoint+"/profiles", nil)
	if err != nil {
		return
	}

	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	var profile []amazonmodel.Profile
	marshalErr := json.Unmarshal(body, &profile)
	if marshalErr != nil {
		log.Error(marshalErr)
		return
	}

	result = convertProfileToStandardModel(profile, email)
	return
}

func convertProfileToStandardModel(details []amazonmodel.Profile, email string) (result []models.Profile) {
	for _, data := range details {

		tem := models.Profile{
			ProfileId:           data.ProfileId,
			MarketplaceStringId: data.AccountInfo.MarketplaceStringId,
			SellerStringId:      data.AccountInfo.SellerStringId,
			DailyBudget:         data.DailyBudget,
			CurrencyCode:        data.CurrencyCode,
			TimeZone:            data.TimeZone,
			CountryCode:         data.CountryCode,
			Email:               email,
		}
		result = append(result, tem)
	}
	return result
}
