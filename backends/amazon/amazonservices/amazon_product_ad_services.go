package amazonservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonmodel"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/productad/productadmodels"
	"encoding/json"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
	"errors"
)

func FetchProductAdListExtended(sellerId, email, geo string, profileId int64, form productadmodels.ProductAdForm) (result []models.ProductAd, err error) {

	log.Println("Fetching productAd list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	queryParams := setProductAdQueryParams(form)
	url := endpoint + "/productAds/extended?" + queryParams

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	var details []amazonmodel.ProductAdEx
	marshalErr := json.Unmarshal(body, &details)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	result = convertToStandardProductAdModel(details)
	return

}

func convertToStandardProductAdModel(details []amazonmodel.ProductAdEx) (result []models.ProductAd) {
	for _, data := range details {

		tem := models.ProductAd{}
		created := time.Unix(data.CreationDate/1000, 0)

		last := time.Unix(data.LastUpdatedDate/1000, 0)
		tem = models.ProductAd{
			AdId:            data.AdId,
			AdGroupId:       data.AdGroupId,
			CampaignId:      data.CampaignId,
			State:           data.State,
			LastUpdatedDate: &last,
			CreationDate:    &created,
			ServingStatus:   data.ServingStatus,
			Asin:            data.Asin,
			Sku:             data.Sku,
		}
		result = append(result, tem)
	}
	return result
}
func setProductAdQueryParams(formData productadmodels.ProductAdForm) (queryParams string) {

	if formData.Count != "" {
		queryParams = queryParams + "count=" + formData.Count + "&"
	}
	if formData.CampaignType != "" {
		queryParams = queryParams + "campaignType=" + formData.CampaignType + "&"
	}
	if len(formData.CampaignIdFilter) > 0 {
		idList := ""
		for _, id := range formData.CampaignIdFilter {
			idList = idList + strconv.Itoa(int(id)) + ","
		}
		if len(idList) > 0 {
			idList = idList[0 : len(idList)-1]
		}
		queryParams = queryParams + "campaignIdFilter=" + idList + "&"
	}
	if formData.StartIndex != "" {
		queryParams = queryParams + "startIndex=" + formData.StartIndex + "&"
	}

	if formData.StateFilter != "" {
		queryParams = queryParams + "stateFilter=" + formData.StateFilter + "&"
	}
	if len(formData.AdGroupIdFilter) > 0 {
		idList := ""
		for _, id := range formData.AdGroupIdFilter {
			idList = idList + strconv.Itoa(int(id)) + ","
		}
		if len(idList) > 0 {
			idList = idList[0 : len(idList)-1]
		}
		queryParams = queryParams + "adGroupIdFilter=" + idList + "&"
	}

	return queryParams

}
