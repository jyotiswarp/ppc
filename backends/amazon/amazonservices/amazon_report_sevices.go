package amazonservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonmodel"
	"bytes"
	"encoding/json"
	"errors"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"strings"
	//"bitbucket.org/jyotiswarp/ppc/utils/httpservices"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetcherconstants"
	"bitbucket.org/jyotiswarp/ppc/utils/google_storage"
	"bitbucket.org/jyotiswarp/ppc/utils/math"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"strconv"
	"time"
)

func RequestAmzPPCReport(formRequest models.RequestReport, sellerId, email, reportType, geo string, profileId int64) (result models.ReqReportResponse, err error) {
	log.Println("Requesting ppc report , reportType :", reportType)
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)
	endpoint := viper.GetString("amazon_"+strings.ToUpper(geo)+"_url") + "/" + ppcdatafetcherconstants.UrlMapping[reportType] + "/report"
	byteBody, err := json.Marshal(&formRequest)
	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(byteBody))
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //)viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	//req.Header.Set("Authorization",viper.GetString("access_token"))
	if err != nil {
		return
	}

	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	if response.StatusCode != 202 {
		log.Error(err)
		err = errors.New(string(body))
		return
	}
	//	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	err = json.Unmarshal(body, &result)
	if err != nil {
		log.Error(err)
		return
	}

	return
}

func GetAmzPPCReport(requestId, sellerId, email, geo, reportType string, profileId int64) (err error, fileName string, result interface{}) {
	log.Println("getting ppc report , reportType :", reportType)
	amzConfig := NewAmzConfig()

	status, location, err := CheckReportStatus(requestId, sellerId, email, geo, amzConfig, profileId)

	if err != nil {
		err = errors.New(status)
		return
	}
	if status != "SUCCESS" {
		err = errors.New(status)
		return
	}
	downloadLink := ""

	downloadLink, err = GetS3DownloadLocation(location, sellerId, email, amzConfig, profileId)

	if err != nil {
		// retry 5 times
		for i := 0; i < 5; i++ {
			downloadLink, err = GetS3DownloadLocation(location, sellerId, email, amzConfig, profileId)

			if err == nil {
				break
			}

			time.Sleep(5 * time.Second)
			i++
		}
	}

	if downloadLink == "" {
		err = errors.New(status)
		return
	}

	var report amazonmodel.PPCReportDetails

	report, fileName, err = GetPPCData(downloadLink, sellerId, reportType, geo)
	if err != nil {
		return
	}

	err, result = GetPPCModel(reportType, report)

	return
}

func CheckReportStatus(requestId, sellerId, email, geo string, amzConfig *AmazonConfig, profileId int64) (status, location string, err error) {
	log.Println("checking report status")
	client := amzConfig.GetAmzClient(email)
	endpoint := viper.GetString("amazon_"+strings.ToUpper(geo)+"_url") + "/reports/" + requestId
	req, err := http.NewRequest("GET", endpoint, nil)
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	if err != nil {
		return "", "", err
	}

	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return "", "", err
	}
	if response == nil {
		return "", "", err
	}
	if response.StatusCode != 200 {
		return "", "", err
	}
	body, _ := ioutil.ReadAll(response.Body)

	defer response.Body.Close()

	var result amazonmodel.ReportLocation
	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error(marshalErr)
		return "", "", err
	}

	return result.Status, result.Location, err
}

func GetS3DownloadLocation(location, sellerId, email string, amzConfig *AmazonConfig, profileId int64) (result string, err error) {
	log.Println("connecting to s3 location")
	client := amzConfig.GetAmzClient(email)

	req, err := http.NewRequest("GET", location, nil)
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	if err != nil {
		return result, err
	}
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}

	response, err := client.Do(req) /*
		body, _ := ioutil.ReadAll(response.Body)*/

	if err != nil {
		log.Error(err)
		return result, err
	}
	if response == nil {
		return "", errors.New("no response went wrong")
	}
	if response.StatusCode != 307 || response.Header.Get("Location") == "" {
		return "", errors.New("Please try again")
	}

	return response.Header.Get("Location"), nil

}

func GetPPCData(downloadLink, sellerId, reportType, geo string) (result amazonmodel.PPCReportDetails, fileName string, err error) {

	response, err := http.Get(downloadLink)
	if response == nil {
		return
	}
	epochTime := strconv.Itoa(int(time.Now().UnixNano()))
	tmpFile := geo + "_" + sellerId + "_" + reportType + "_" + epochTime + ".json.gz"
	fileName = "./" + tmpFile
	out, err := os.Create(fileName)
	_, err = io.Copy(out, response.Body)

	UploadFileToGoogleCloud(fileName, tmpFile)
	result = ReadGzipFile(fileName)
	defer out.Close()

	defer DeleteFile(fileName)
	return
}

func DeleteFile(fileName string) {
	log.Println("Deleting file :", fileName)
	err := os.Remove("./" + fileName)
	if err != nil {
		log.Error(err)
	}
}

func UploadFileToGoogleCloud(filename, tempFileName string) {
	log.Println("Uploading report to cloud")
	uploadFile, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, os.ModePerm)

	if err != nil {
		log.Error("update :", err)
		return
	}
	defer uploadFile.Close()
	custErr := google_storage.UploadToCloudPPCReport(uploadFile, tempFileName, "application/csv")
	if custErr != nil {
		log.Error("upload error:", custErr.Error())
	}

}
func ReadGzipFile(fileName string) (result amazonmodel.PPCReportDetails) {

	file, err := os.Open(fileName)
	if err != nil {
		log.Println(err)
		return
	}

	gz, err := gzip.NewReader(file)
	if err != nil {
		log.Println(err)
		return
	}

	body, err := ioutil.ReadAll(gz)
	//fmt.Println("From SS3 :",string(body))
	marshalErr := json.Unmarshal(body, &result.Data)
	if marshalErr != nil {
		log.Error(marshalErr)
		//return "", "", err
	}
	return result
}

func GetPPCModel(reportType string, data amazonmodel.PPCReportDetails) (err error, result interface{}) {

	switch reportType {

	case "campaigns":
		return GetCampaignModel(data.Data)
	case "adGroups":
		return GetAdGroupModel(data.Data)
	case "keywords":
		return GetKeywordModel(data.Data)
	case "productAds":
		return GetProductAdModel(data.Data)
	case "asins":
		return GetAsinModel(data.Data)

	}
	return
}

func GetCampaignModel(data []amazonmodel.ReportResponseData) (err error, result []models.CampaignReportData) {

	for _, row := range data {

		var attrs []models.AttributedTimeData
		attr := models.AttributedTimeData{
			NumberOfDays:                 1,
			AttributedSales:              row.AttributedSales1d,
			AttributedConversions:        row.AttributedConversions1d,
			AttributedSalesSameSKU:       row.AttributedSales1dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions1dSameSKU}
		attrs = append(attrs, attr)

		attr = models.AttributedTimeData{
			NumberOfDays:                 7,
			AttributedSales:              row.AttributedSales7d,
			AttributedConversions:        row.AttributedConversions7d,
			AttributedSalesSameSKU:       row.AttributedSales7dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions7dSameSKU}
		attrs = append(attrs, attr)

		/*sales14dSameSku, _ := strconv.ParseFloat(row.AttributedSales14dSameSKU,64)
		sales14d, _ := strconv.ParseFloat(row.AttributedSales14d,64)
		conversions14d, _ := strconv.Atoi(row.AttributedConversions14d)
		convertion14dSameSku, _ := strconv.Atoi(row.AttributedConversions14dSameSKU)*/

		attr = models.AttributedTimeData{
			NumberOfDays:                 14,
			AttributedSales:              row.AttributedSales14d,
			AttributedConversions:        row.AttributedConversions14d,
			AttributedSalesSameSKU:       row.AttributedSales14dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions14dSameSKU}
		/*AttributedSales:              sales14d,
		AttributedConversions:        conversions14d,
		AttributedSalesSameSKU:      sales14dSameSku,
		AttributedConversionsSameSKU: convertion14dSameSku,}*/
		attrs = append(attrs, attr)

		attr = models.AttributedTimeData{
			NumberOfDays:                 30,
			AttributedSales:              row.AttributedSales30d,
			AttributedConversions:        row.AttributedConversions30d,
			AttributedSalesSameSKU:       row.AttributedSales30dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions30dSameSKU}
		attrs = append(attrs, attr)

		acos := 0.0
		cpc := 0.0
		ctr := 0.0
		if row.AttributedSales1d > 0 {
			acos, _ = math.RoundFloat((row.Cost/row.AttributedSales1d)*100, 2)
		}
		if row.Clicks > 0 {
			cpc, _ = math.RoundFloat((row.Cost / float64(row.Clicks)), 2)
		}
		if row.Impressions > 0 {
			ctr, _ = math.RoundFloat((float64(row.Clicks)/float64(row.Impressions))*100, 2)
		}

		campaign := models.CampaignReportData{
			Acos:             acos,
			Cost:             row.Cost,
			Clicks:           row.Clicks,
			Impressions:      row.Impressions,
			CampaignId:       row.CampaignId,
			Cpc:              cpc,
			Ctr:              ctr,
			Sales:            row.AttributedSales1d,
			Orders:           row.AttributedConversions1d,
			AttributedValues: attrs,
		}
		result = append(result, campaign)
	}

	return
}
func GetAdGroupModel(data []amazonmodel.ReportResponseData) (err error, result []models.AdGroupReportData) {

	for _, row := range data {

		var attrs []models.AttributedTimeData
		attr := models.AttributedTimeData{
			NumberOfDays:                 1,
			AttributedSales:              row.AttributedSales1d,
			AttributedConversions:        row.AttributedConversions1d,
			AttributedSalesSameSKU:       row.AttributedSales1dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions1dSameSKU}
		attrs = append(attrs, attr)

		attr = models.AttributedTimeData{
			NumberOfDays:                 7,
			AttributedSales:              row.AttributedSales7d,
			AttributedConversions:        row.AttributedConversions7d,
			AttributedSalesSameSKU:       row.AttributedSales7dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions7dSameSKU}
		attrs = append(attrs, attr)

		/*sales14dSameSku, _ := strconv.ParseFloat(row.AttributedSales14dSameSKU,64)
		sales14d, _ := strconv.ParseFloat(row.AttributedSales14d,64)
		conversions14d, _ := strconv.Atoi(row.AttributedConversions14d)
		convertion14dSameSku, _ := strconv.Atoi(row.AttributedConversions14dSameSKU)*/

		attr = models.AttributedTimeData{
			NumberOfDays:                 14,
			AttributedSales:              row.AttributedSales14d,
			AttributedConversions:        row.AttributedConversions14d,
			AttributedSalesSameSKU:       row.AttributedSales14dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions14dSameSKU}
		/*AttributedSales:              sales14d,
		AttributedConversions:        conversions14d,
		AttributedSalesSameSKU:      sales14dSameSku,
		AttributedConversionsSameSKU: convertion14dSameSku,}*/
		attrs = append(attrs, attr)
		attr = models.AttributedTimeData{
			NumberOfDays:                 30,
			AttributedSales:              row.AttributedSales30d,
			AttributedConversions:        row.AttributedConversions30d,
			AttributedSalesSameSKU:       row.AttributedSales30dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions30dSameSKU}
		attrs = append(attrs, attr)

		acos := 0.0
		cpc := 0.0
		ctr := 0.0
		if row.AttributedSales1d > 0 {
			acos, _ = math.RoundFloat((row.Cost/row.AttributedSales1d)*100, 2)
		}
		if row.Clicks > 0 {
			cpc, _ = math.RoundFloat((row.Cost / float64(row.Clicks)), 2)
		}
		if row.Impressions > 0 {
			ctr, _ = math.RoundFloat((float64(row.Clicks)/float64(row.Impressions))*100, 2)
		}
		adgroup := models.AdGroupReportData{
			AdGroupId:        row.AdGroupId,
			Acos:             acos,
			Cost:             row.Cost,
			Clicks:           row.Clicks,
			Impressions:      row.Impressions,
			CampaignId:       row.CampaignId,
			Cpc:              cpc,
			Ctr:              ctr,
			Sales:            row.AttributedSales1d,
			Orders:           row.AttributedConversions1d,
			AttributedValues: attrs,
		}
		result = append(result, adgroup)
	}

	return
}
func GetKeywordModel(data []amazonmodel.ReportResponseData) (err error, result []models.KeywordReportData) {

	//fmt.Println(" GetKeywordModel data:",data)
	for _, row := range data {

		var attrs []models.AttributedTimeData
		attr := models.AttributedTimeData{
			NumberOfDays:                 1,
			AttributedSales:              row.AttributedSales1d,
			AttributedConversions:        row.AttributedConversions1d,
			AttributedSalesSameSKU:       row.AttributedSales1dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions1dSameSKU}
		attrs = append(attrs, attr)

		attr = models.AttributedTimeData{
			NumberOfDays:                 7,
			AttributedSales:              row.AttributedSales7d,
			AttributedConversions:        row.AttributedConversions7d,
			AttributedSalesSameSKU:       row.AttributedSales7dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions7dSameSKU}
		attrs = append(attrs, attr)

		/*sales14dSameSku, _ := strconv.ParseFloat(row.AttributedSales14dSameSKU,64)
		sales14d, _ := strconv.ParseFloat(row.AttributedSales14d,64)
		conversions14d, _ := strconv.Atoi(row.AttributedConversions14d)
		convertion14dSameSku, _ := strconv.Atoi(row.AttributedConversions14dSameSKU)*/

		attr = models.AttributedTimeData{
			NumberOfDays:                 14,
			AttributedSales:              row.AttributedSales14d,
			AttributedConversions:        row.AttributedConversions14d,
			AttributedSalesSameSKU:       row.AttributedSales14dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions14dSameSKU}
		/*AttributedSales:              sales14d,
		AttributedConversions:        conversions14d,
		AttributedSalesSameSKU:      sales14dSameSku,
		AttributedConversionsSameSKU: convertion14dSameSku,}*/
		attrs = append(attrs, attr)

		attr = models.AttributedTimeData{
			NumberOfDays:                 30,
			AttributedSales:              row.AttributedSales30d,
			AttributedConversions:        row.AttributedConversions30d,
			AttributedSalesSameSKU:       row.AttributedSales30dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions30dSameSKU}
		attrs = append(attrs, attr)

		acos := 0.0
		cpc := 0.0
		ctr := 0.0
		if row.AttributedSales1d > 0 {
			acos, _ = math.RoundFloat((row.Cost/row.AttributedSales1d)*100, 2)
		}
		if row.Clicks > 0 {
			cpc, _ = math.RoundFloat((row.Cost / float64(row.Clicks)), 2)
		}
		if row.Impressions > 0 {
			ctr, _ = math.RoundFloat((float64(row.Clicks)/float64(row.Impressions))*100, 2)
		}
		keyword := models.KeywordReportData{
			AdGroupId:        row.AdGroupId,
			KeywordId:        row.KeywordId,
			Query:            row.Query,
			Acos:             acos,
			Cost:             row.Cost,
			Clicks:           row.Clicks,
			Impressions:      row.Impressions,
			CampaignId:       row.CampaignId,
			Cpc:              cpc,
			Ctr:              ctr,
			Sales:            row.AttributedSales1d,
			Orders:           row.AttributedConversions1d,
			AttributedValues: attrs,
		}
		result = append(result, keyword)
	}

	return
}
func GetProductAdModel(data []amazonmodel.ReportResponseData) (err error, result []models.ProductAdsReportData) {

	for _, row := range data {

		var attrs []models.AttributedTimeData
		attr := models.AttributedTimeData{
			NumberOfDays:                 1,
			AttributedSales:              row.AttributedSales1d,
			AttributedConversions:        row.AttributedConversions1d,
			AttributedSalesSameSKU:       row.AttributedSales1dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions1dSameSKU}
		attrs = append(attrs, attr)

		attr = models.AttributedTimeData{
			NumberOfDays:                 7,
			AttributedSales:              row.AttributedSales7d,
			AttributedConversions:        row.AttributedConversions7d,
			AttributedSalesSameSKU:       row.AttributedSales7dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions7dSameSKU}
		attrs = append(attrs, attr)

		/*sales14dSameSku, _ := strconv.ParseFloat(row.AttributedSales14dSameSKU,64)
		sales14d, _ := strconv.ParseFloat(row.AttributedSales14d,64)
		conversions14d, _ := strconv.Atoi(row.AttributedConversions14d)
		convertion14dSameSku, _ := strconv.Atoi(row.AttributedConversions14dSameSKU)*/

		attr = models.AttributedTimeData{
			NumberOfDays:                 14,
			AttributedSales:              row.AttributedSales14d,
			AttributedConversions:        row.AttributedConversions14d,
			AttributedSalesSameSKU:       row.AttributedSales14dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions14dSameSKU}
		/*AttributedSales:              sales14d,
		AttributedConversions:        conversions14d,
		AttributedSalesSameSKU:      sales14dSameSku,
		AttributedConversionsSameSKU: convertion14dSameSku,}*/
		attrs = append(attrs, attr)

		attr = models.AttributedTimeData{
			NumberOfDays:                 30,
			AttributedSales:              row.AttributedSales30d,
			AttributedConversions:        row.AttributedConversions30d,
			AttributedSalesSameSKU:       row.AttributedSales30dSameSKU,
			AttributedConversionsSameSKU: row.AttributedConversions30dSameSKU}
		attrs = append(attrs, attr)

		acos := 0.0
		cpc := 0.0
		ctr := 0.0
		if row.AttributedSales1d > 0 {
			acos, _ = math.RoundFloat((row.Cost/row.AttributedSales1d)*100, 2)
		}
		if row.Clicks > 0 {
			cpc, _ = math.RoundFloat((row.Cost / float64(row.Clicks)), 2)
		}
		if row.Impressions > 0 {
			ctr, _ = math.RoundFloat((float64(row.Clicks)/float64(row.Impressions))*100, 2)
		}
		ad := models.ProductAdsReportData{
			AdId:             row.AdId,
			AdGroupId:        row.AdGroupId,
			Acos:             acos,
			Cost:             row.Cost,
			Clicks:           row.Clicks,
			Impressions:      row.Impressions,
			CampaignId:       row.CampaignId,
			Cpc:              cpc,
			Ctr:              ctr,
			Sales:            row.AttributedSales1d,
			Orders:           row.AttributedConversions1d,
			AttributedValues: attrs,
		}
		result = append(result, ad)
	}

	return
}

func GetAsinModel(data []amazonmodel.ReportResponseData) (err error, result []models.AsinReportData) {

	for _, row := range data {

		ad := models.AsinReportData{
			AdGroupId:     row.AdGroupId,
			Sku:           row.Sku,
			Asin:          row.Asin,
			KeywordText:   row.KeywordText,
			MatchType:     row.MatchType,
			CampaignName:  row.CampaignName,
			AdGroupName:   row.AdGroupName,
			Currency:      row.Currency,
			OtherAsin:     row.OtherAsin,
			CampaignId:    row.CampaignId,
			OtherSkuOrder: row.AttributedUnitsOrdered1dOtherSKU,
			OtherSkuSales: row.AttributedSales1dOtherSKU,
			//Order:row.AttributedUnitsOrdered1d,
		}
		result = append(result, ad)
	}

	return
}

func RequestAmzPPCSnapshot(sellerId, email, reportType, geo string, profileId int64) (*models.SnapshotFirstReq, error) {
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)
	endpoint := viper.GetString("amazon_"+strings.ToUpper(geo)+"_url") + "/" + reportType + "/snapshot"
	type Payload struct {
		CampaignType string `json:"campaignType"`
		StateFilter  string `json:"stateFilter"`
	}
	payload := Payload{CampaignType: "sponsoredProducts", StateFilter: "enabled,paused,archived"}
	byteBody, err := json.Marshal(&payload)
	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(byteBody))
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //)viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	//req.Header.Set("Authorization",viper.GetString("access_token"))
	if err != nil {
		return nil, err
	}

	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	body, _ := ioutil.ReadAll(response.Body)
	if response.StatusCode != 200 {
		log.Error(err)
		//log2.Fatal(err)
		return nil, errors.New(string(body))
	}
	//	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	fmt.Println(string(body))
	data := models.SnapshotFirstReq{}
	marshalErr := json.Unmarshal(body, &data)
	if marshalErr != nil {
		log.Error(marshalErr)
		return nil, err
	}
	return &data, nil
}

func CheckSnapshotStatus(requestId, sellerId, email, geo string, profileId int64) (*models.SnapShotResponse, error) {
	amzConfig := NewAmzConfig()
	log.Println("checking report status")
	client := amzConfig.GetAmzClient(email)
	endpoint := viper.GetString("amazon_"+strings.ToUpper(geo)+"_url") + "/snapshots/" + requestId
	req, err := http.NewRequest("GET", endpoint, nil)
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	if err != nil {
		return nil, err
	}

	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	if response == nil {
		return nil, err
	}
	if response.StatusCode != 200 {
		return nil, err
	}
	body, _ := ioutil.ReadAll(response.Body)

	defer response.Body.Close()

	fmt.Println(string(body))
	data := models.SnapShotResponse{}
	marshalErr := json.Unmarshal(body, &data)
	if marshalErr != nil {
		log.Error(marshalErr)
		return nil, err
	}

	downloadLink := ""

	downloadLink, err = GetS3DownloadLocation(data.Location, sellerId, email, amzConfig, profileId)

	if err != nil {
		// retry 5 times
		for i := 0; i < 5; i++ {
			downloadLink, err = GetS3DownloadLocation(data.Location, sellerId, email, amzConfig, profileId)

			if err == nil {
				break
			}

			time.Sleep(5 * time.Second)
			i++
		}
	}

	fmt.Println(downloadLink)

	return &data, nil
}
