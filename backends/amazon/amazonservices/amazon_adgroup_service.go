package amazonservices

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupmodels"
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonmodel"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bytes"
	"encoding/json"
	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func FetchAdGroupList(sellerId, email, geo string, profileId int64) (result []models.AdGroup, err error) {

	log.Println("Fetching adgroup list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/adGroups?" //+queryParams

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Error(err)
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	var data []amazonmodel.AdGroupData
	marshalErr := json.Unmarshal(body, &data)
	if marshalErr != nil {
		log.Error(marshalErr)
		return
	}

	result = convertToStandardAdGroupModel(data)

	return

}

func FetchAdGroupListExtended(sellerId, email, geo string, profileId int64, formData adgroupmodels.AdGroupForm) (result []models.AdGroup, err error) {

	log.Println("Fetching adGroupExtended list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")

	queryParams := setAdQueryParams(formData)
	url := endpoint + "/adGroups/extended?" + queryParams

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Error(err)
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	var data []amazonmodel.AdGroupData
	marshalErr := json.Unmarshal(body, &data)
	if marshalErr != nil {
		log.Error(marshalErr)
		return
	}

	result = convertToStandardAdGroupModel(data)
	return

}

func UpdateAdGroup(sellerId, email, geo string, profileId int64, formRequest *[]adgroupmodels.AdGroupUpdateForm) (result []adgroupmodels.AdGroupResponse, err error) {

	log.Println("Fetching update  list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/adGroups" //+queryParams

	byteBody, err := json.Marshal(&formRequest)
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(byteBody))
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	//result = convertToStandardNegativeKeywordModel(details)
	return

}

func setAdQueryParams(formData adgroupmodels.AdGroupForm) (queryParams string) {

	if formData.Name != "" {
		queryParams = "name=" + formData.Name + "&"
	}

	if formData.Count != "" {
		queryParams = queryParams + "count=" + formData.Count + "&"
	}
	if formData.CampaignType != "" {
		queryParams = queryParams + "campaignType=" + formData.CampaignType + "&"
	}
	if len(formData.CampaignIdFilter) > 0 {
		idList := ""
		for _, id := range formData.CampaignIdFilter {
			idList = idList + strconv.Itoa(int(id)) + ","
		}
		if len(idList) > 0 {
			idList = idList[0 : len(idList)-1]
		}
		queryParams = queryParams + "campaignIdFilter=" + idList + "&"
	}
	if formData.StartIndex != "" {
		queryParams = queryParams + "startIndex=" + formData.StartIndex + "&"
	}

	if formData.StateFilter != "" {
		queryParams = queryParams + "stateFilter=" + formData.StateFilter + "&"
	}
	if len(formData.AdGroupIdFilter) > 0 {
		idList := ""
		for _, id := range formData.AdGroupIdFilter {
			idList = idList + strconv.Itoa(int(id)) + ","
		}
		if len(idList) > 0 {
			idList = idList[0 : len(idList)-1]
		}
		queryParams = queryParams + "adGroupIdFilter=" + idList + "&"
	}

	return queryParams
}

func convertToStandardAdGroupModel(details []amazonmodel.AdGroupData) (result []models.AdGroup) {
	for _, data := range details {
		tem := models.AdGroup{}
		created := time.Unix(data.CreationDate/1000, 0)
		last := time.Unix(data.LastUpdatedDate/1000, 0)
		tem = models.AdGroup{
			LastUpdatedDate: &last,
			CampaignId:      data.CampaignId,
			State:           data.State,
			ServingStatus:   data.ServingStatus,
			CreationDate:    &created,
			Name:            data.Name,
			AdGroupId:       data.AdGroupId,
			DefaultBid:      data.DefaultBid,
		}
		result = append(result, tem)
	}
	return result
}

func GetAdGroupBidRecommendation(sellerId, email, geo string, profileId int64, adGroupID string) (result adgroupmodels.AdGroupBidRecommendation, err error) {
	log.Println("Fetching bid recommendation list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)
	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/adGroups/" + adGroupID + "/bidRecommendations" //+queryParams
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	defer response.Body.Close()

	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	return

}
