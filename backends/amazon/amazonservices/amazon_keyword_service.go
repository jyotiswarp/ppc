package amazonservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonmodel"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func FetchKeywordList(sellerId, email, geo string, profileId int64) (result []models.Keyword, err error) {
	log.Println("Fetching keyword list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")

	url := endpoint + "/keywords?" //+queryParams

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)

	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error(marshalErr)
		return
	}

	return

}

func FetchKeywordListExtended(sellerId, email, geo string, profileId int64, keywordForm keywordmodel.KeywordDetailsForm) (result []models.Keyword, err error) {
	log.Println("Fetching keywordExtended list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	queryParams := setKeywordQueryParams(keywordForm)
	url := endpoint + "/keywords/extended?" + queryParams

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}

	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	var details []amazonmodel.KeywordData
	marshalErr := json.Unmarshal(body, &details)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	result = convertToStandardKeywordModel(details)
	return

}

func FetchAdGroupNegativeKeywordList(sellerId, email, geo string, profileId int64, form keywordmodel.KeywordDetailsForm) (result []models.NegativeKeyword, err error) {
	log.Println("Fetching negative keywords list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	queryParams := setKeywordQueryParams(form)
	url := endpoint + "/negativeKeywords/extended?" + queryParams

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	var details []amazonmodel.NegativeKeywordData
	log.Println("Negative Body : ", string(body))
	marshalErr := json.Unmarshal(body, &details)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	result = convertToStandardNegativeKeywordModel(details)
	return

}

func GetKeywordSuggestionsByAsin(sellerId, email, geo string, profileId int64, form *keywordmodel.AsinSuggestionForm) (result []keywordmodel.SuggestedKeywords, err error) {
	log.Println("Fetching keyword susstions list from amazon , ", form)
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/asins/suggested/keywords" //+queryParams
	/*var keywordId []int64
	for _,data := range form.KeywordId{
		keywordId = append(keywordId,data)
	}*/
	byteBody, err := json.Marshal(&form)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(byteBody))
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}

	//var details []models.KeywordBidRecomendations
	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	//result = convertToStandardNegativeKeywordModel(details)
	return

}

func GetKeywordSuggestionsByAdgroup(sellerId, email, geo string, profileId int64, form *keywordmodel.AdGroupKeywordSuggestionForm) (result []keywordmodel.SuggestedKeywords, err error) {
	log.Println("Fetching keyword susstions list from amazon , ", form)
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)
	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/adGroups/" + fmt.Sprint(form.AdGroupId) + "/suggested/keywords" //+queryParams
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}

	//var details []models.KeywordBidRecomendations
	var suggestionsResp keywordmodel.AdGroupKeywordSuggestion
	marshalErr := json.Unmarshal(body, &suggestionsResp)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}
	result = suggestionsResp.SuggestedKeywords

	return

}

func GetBidRecommendation(sellerId, email, geo string, profileId int64, form *keywordmodel.BidRecommendationForm) (result keywordmodel.KeywordBidRecomendations, err error) {
	log.Println("Fetching bid recommendation list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/keywords/bidRecommendations" //+queryParams
	/*var keywordId []int64
	for _,data := range form.KeywordId{
		keywordId = append(keywordId,data)
	}*/
	byteBody, err := json.Marshal(&form)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(byteBody))
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	defer response.Body.Close()

	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	//var details []models.KeywordBidRecomendations
	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	//result = convertToStandardNegativeKeywordModel(details)
	return

}

func AddAdGroupNegativeKeyword(sellerId, email, geo string, profileId int64, formRequest *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {

	log.Println("add negative keyword list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/negativeKeywords" //+queryParams

	byteBody, err := json.Marshal(&formRequest)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(byteBody))
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)

	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	//var details []amazonmodel.KeywordResponse
	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	//result = convertToStandardNegativeKeywordModel(details)
	return

}

func AddAdGroupKeyword(sellerId, email, geo string, profileId int64, formRequest *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {

	log.Println("add negative keyword list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/keywords" //+queryParams

	byteBody, err := json.Marshal(&formRequest)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(byteBody))
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)

	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	//var details []amazonmodel.KeywordResponse
	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	//result = convertToStandardNegativeKeywordModel(details)
	return

}
func UpdateAdGroupNegativeKeyword(sellerId, email, geo string, profileId int64, formRequest *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {

	log.Println("Fetching update negative list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/negativeKeywords" //+queryParams

	byteBody, err := json.Marshal(&formRequest)
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(byteBody))
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	//result = convertToStandardNegativeKeywordModel(details)
	return

}

func UpdateAdGroupKeyword(sellerId, email, geo string, profileId int64, formRequest *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {

	log.Println("Fetching update  list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/keywords" //+queryParams

	byteBody, err := json.Marshal(&formRequest)
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(byteBody))
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	//result = convertToStandardNegativeKeywordModel(details)
	return

}

func DeleteKeyword(sellerId, email, geo string, profileId int64, keywordId string) (result keywordmodel.KeywordResponse, err error) {

	log.Println("deleting keyword from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/keywords/" + keywordId //+queryParams

	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	log.Println("Body :", string(body))
	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	//result = convertToStandardNegativeKeywordModel(details)
	return

}

func convertToStandardNegativeKeywordModel(details []amazonmodel.NegativeKeywordData) (result []models.NegativeKeyword) {
	for _, data := range details {

		tem := models.NegativeKeyword{}
		created := time.Unix(data.CreationDate/1000, 0)

		last := time.Unix(data.LastUpdatedDate/1000, 0)
		tem = models.NegativeKeyword{
			KeywordId:       data.KeywordId,
			MatchType:       data.MatchType,
			Bid:             data.Bid,
			KeywordText:     data.KeywordText,
			CampaignId:      data.CampaignId,
			State:           data.State,
			ServingStatus:   data.ServingStatus,
			LastUpdatedDate: &last,
			CreationDate:    &created,
			AdGroupId:       data.AdGroupId,
		}
		result = append(result, tem)
	}
	return result
}

func setKeywordQueryParams(formData keywordmodel.KeywordDetailsForm) (queryParams string) {

	if formData.Name != "" {
		queryParams = "name=" + formData.Name + "&"
	}

	if formData.Count != "" {
		queryParams = queryParams + "count=" + formData.Count + "&"
	}
	if formData.CampaignType != "" {
		queryParams = queryParams + "campaignType=" + formData.CampaignType + "&"
	}
	if len(formData.CampaignIdFilter) > 0 {
		idList := ""
		for _, id := range formData.CampaignIdFilter {
			idList = idList + strconv.Itoa(int(id)) + ","
		}
		if len(idList) > 0 {
			idList = idList[0 : len(idList)-1]
		}
		queryParams = queryParams + "campaignIdFilter=" + idList + "&"
	}
	if formData.StartIndex != "" {
		queryParams = queryParams + "startIndex=" + formData.StartIndex + "&"
	}

	if formData.StateFilter != "" {
		queryParams = queryParams + "stateFilter=" + formData.StateFilter + "&"
	}
	if len(formData.AdGroupIdFilter) > 0 {
		idList := ""
		for _, id := range formData.AdGroupIdFilter {
			idList = idList + strconv.Itoa(int(id)) + ","
		}
		if len(idList) > 0 {
			idList = idList[0 : len(idList)-1]
		}
		queryParams = queryParams + "adGroupIdFilter=" + idList + "&"
	}

	return queryParams

}

func convertToStandardKeywordModel(details []amazonmodel.KeywordData) (result []models.Keyword) {
	for _, data := range details {

		tem := models.Keyword{}
		created := time.Unix(data.CreationDate/1000, 0)

		last := time.Unix(data.LastUpdatedDate/1000, 0)
		tem = models.Keyword{
			KeywordId:       data.KeywordId,
			MatchType:       data.MatchType,
			Bid:             data.Bid,
			KeywordText:     data.KeywordText,
			CampaignId:      data.CampaignId,
			State:           data.State,
			ServingStatus:   data.ServingStatus,
			LastUpdatedDate: &last,
			CreationDate:    &created,
			AdGroupId:       data.AdGroupId,
		}
		result = append(result, tem)
	}
	return result
}

func GetExtendedKeywordByID(email, geo string, profileId int64, keywordID int64) (result models.Keyword, err error) {
	log.Println("Fetching keyword list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")

	keywordIDText := fmt.Sprint(keywordID)
	url := endpoint + "/keywords/extended/" + keywordIDText //+queryParams

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)

	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	var rawResult amazonmodel.KeywordData
	marshalErr := json.Unmarshal(body, &rawResult)
	if marshalErr != nil {
		log.Error(marshalErr)
		return
	}

	result = convertSingleKeyToStandardKeywordModel(rawResult)

	return

}

func convertSingleKeyToStandardKeywordModel(data amazonmodel.KeywordData) (result models.Keyword) {
	created := time.Unix(data.CreationDate/1000, 0)
	last := time.Unix(data.LastUpdatedDate/1000, 0)
	result = models.Keyword{
		KeywordId:       data.KeywordId,
		MatchType:       data.MatchType,
		Bid:             data.Bid,
		KeywordText:     data.KeywordText,
		CampaignId:      data.CampaignId,
		State:           data.State,
		ServingStatus:   data.ServingStatus,
		LastUpdatedDate: &last,
		CreationDate:    &created,
		AdGroupId:       data.AdGroupId,
	}
	return
}

func GetExtendedNegativeKeywordByID(email, geo string, profileId int64, keywordID int64) (result models.NegativeKeyword, err error) {
	log.Println("Fetching keyword list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")

	keywordIDText := fmt.Sprint(keywordID)
	url := endpoint + "/negativeKeywords/extended/" + keywordIDText //+queryParams

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)

	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error(marshalErr)
		return
	}

	return

}
