package amazonservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonmodel"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignmodels"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func FetchCampaignsList(sellerId, email, geo string, profileId int64) (result []models.Campaign, err error) {

	log.Println("Fetching campaign list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")
	url := endpoint + "/campaigns?" //+ queryParams

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return
	}
	if response == nil {
		err = errors.New("no response found")
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	var details []amazonmodel.CampaignData
	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshalerrr :", marshalErr)
		return
	}
	result = convertToStandardModel(details)

	return result, nil

}

func FetchCampaignsListExtended(sellerId, email, geo string, profileId int64, formData campaignmodels.CampaignForm) (result []models.Campaign, err error) {
	log.Println("Fetching campaignExtended list from amazon")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")

	queryParams := setQueryParams(formData)

	fmt.Println("campaign Query : ", queryParams)
	url := endpoint + "/campaigns/extended?" + queryParams
	fmt.Println("campaign Url : ", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}

	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil || response.StatusCode != 200 {
		log.Error(err)
		err = errors.New("no response found")
		return
	}
	if response == nil {
		return
	}
	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	var details []amazonmodel.CampaignData
	marshalErr := json.Unmarshal(body, &details)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}

	result = convertToStandardModel(details)
	return

}

func convertToStandardModel(details []amazonmodel.CampaignData) (result []models.Campaign) {
	for _, data := range details {

		tem := models.Campaign{}
		created := time.Unix(data.CreationDate/1000, 0)
		if data.EndDate != "" {
			end, _ := time.Parse("20060102", data.EndDate)
			tem.EndDate = &end
		}
		last := time.Unix(data.LastUpdatedDate/1000, 0)
		start, _ := time.Parse("20060102", data.StartDate)
		tem = models.Campaign{
			DailyBudget:          data.DailyBudget,
			CampaignType:         data.CampaignType,
			Name:                 data.Name,
			CampaignId:           data.CampaignId,
			CreationDate:         &created, //time.Unix(data.CreationDate,0),
			LastUpdatedDate:      &last,
			PremiumBidAdjustment: data.PremiumBidAdjustment,
			ServingStatus:        data.ServingStatus,
			StartDate:            &start,
			State:                data.State,
			TargetingType:        data.TargetingType,
		}
		result = append(result, tem)
	}
	return result
}
func setQueryParams(formData campaignmodels.CampaignForm) (queryParams string) {

	if formData.Name != "" {
		queryParams = "name=" + formData.Name + "&"
	}

	if formData.Count != "" {
		queryParams = queryParams + "count=" + formData.Count + "&"
	}
	if formData.CampaignType != "" {
		queryParams = queryParams + "campaignType=" + formData.CampaignType + "&"
	}
	if len(formData.CampaignIdFilter) > 0 {
		idList := ""
		for _, id := range formData.CampaignIdFilter {
			idList = idList + strconv.Itoa(int(id)) + ","
		}
		if len(idList) > 0 {
			idList = idList[0 : len(idList)-1]
		}
		queryParams = queryParams + "campaignIdFilter=" + idList + "&"
	}
	if formData.StartIndex != "" {
		queryParams = queryParams + "startIndex=" + formData.StartIndex + "&"
	}

	if formData.StateFilter != "" {
		queryParams = queryParams + "stateFilter=" + formData.StateFilter + "&"
	}

	return queryParams

}

func UpdateAmazonCampaign(geo, sellerId, email string, profileId int64, form *[]campaignmodels.CampaignUpdateForm) (result []models.CampaignUpdateResponse, err error) {

	log.Println("Updating campaign")
	amzConfig := NewAmzConfig()
	client := amzConfig.GetAmzClient(email)

	endpoint := viper.GetString("amazon_" + strings.ToUpper(geo) + "_url")

	url := endpoint + "/campaigns/" // + queryParams

	jsonStr, _ := json.Marshal(&form)
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		return
	}
	req.Header.Set("Amazon-Advertising-API-Scope", strconv.Itoa(int(profileId))) //viper.GetString("user_scope")) //take it from Db. For testing took from config
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)

	if err != nil || response.StatusCode != 207 {
		log.Error(err)
		return
	}
	if response == nil {
		return
	}

	body, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	log.Println("Body :", string(body))
	marshalErr := json.Unmarshal(body, &result)
	if marshalErr != nil {
		log.Error("marshal:", marshalErr)
		return
	}
	return

}
