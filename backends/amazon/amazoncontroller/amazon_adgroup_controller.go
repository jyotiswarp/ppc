package amazoncontroller

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupmodels"
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonservices"
	"bitbucket.org/jyotiswarp/ppc/models"
	"errors"
	"github.com/getsentry/raven-go"
)

func (marketPlace *Amazon) GetAdGroupList() (result []models.AdGroup, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.FetchAdGroupList(sellerId, email, geo, profileId)
	if err != nil {
		raven.CaptureError(err, map[string]string{email: "adGroup List Report"})
	}

	return result, err
}

func (marketPlace *Amazon) GetAdGroupListExtended(formData adgroupmodels.AdGroupForm) (result []models.AdGroup, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.FetchAdGroupListExtended(sellerId, email, geo, profileId, formData)
	if err != nil {
		raven.CaptureError(err, map[string]string{email: "adGroup List Extended"})
	}

	return result, err
}
func (marketPlace *Amazon) UpdateAdGroupList(form *[]adgroupmodels.AdGroupUpdateForm) (result []adgroupmodels.AdGroupResponse, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.UpdateAdGroup(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, map[string]string{email: "Update AdGroup List "})
	}

	return result, err
}

func (marketPlace *Amazon) GetAdGroupBidRecommendation(adGroupID string) (result adgroupmodels.AdGroupBidRecommendation, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.GetAdGroupBidRecommendation(sellerId, email, geo, profileId, adGroupID)
	if err != nil {
		raven.CaptureError(err, map[string]string{email: "Bid Recommendation by AdGroup:" + adGroupID})
	}

	return result, err
}
