package amazoncontroller

type Amazon struct {
	market    string
	geo       string
	profileId int64
	sellerID  string
	emailId   string
}

func InitializeClient(market, geo string) *Amazon {
	return &Amazon{market: market, geo: geo}
}

func (marketPlace *Amazon) SetSellerId(sellerId string) {
	marketPlace.sellerID = sellerId
}

func (marketPlace *Amazon) SetProfileId(profileId int64) {
	marketPlace.profileId = profileId
}

func (marketPlace *Amazon) SetEmail(email string) {
	marketPlace.emailId = email
}

func (marketPlace *Amazon) GetProfileId() int64 {
	return marketPlace.profileId
}
func (marketPlace *Amazon) GetSellerId() string {
	return marketPlace.sellerID
}
func (marketPlace *Amazon) GetMarket() string {
	return marketPlace.market
}
func (marketPlace *Amazon) GetGeo() string {
	return marketPlace.geo
}
func (marketPlace *Amazon) GetEmail() string {
	return marketPlace.emailId
}
