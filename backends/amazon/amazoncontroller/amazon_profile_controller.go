package amazoncontroller

import "errors"

import (
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonservices"

	"bitbucket.org/jyotiswarp/ppc/models"
	"fmt"
	"github.com/getsentry/raven-go"
)

func (marketPlace *Amazon) GetListOfProfiles() (result []models.Profile, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	email := marketPlace.GetEmail()
	fmt.Println("sellerId:", sellerId)
	fmt.Println("geo:", geo)
	fmt.Println("email:", email)
	if sellerId == "" || geo == "" || email == "" {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.FetchUserProfiles(sellerId, email, geo)
	if err != nil {
		raven.CaptureError(err, nil)
	}

	return result, err
}
