package amazoncontroller

import (
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonservices"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/utils/stringutils"
	"errors"
	"github.com/getsentry/raven-go"
)

func (marketPlace *Amazon) RequestPPCReport(formRequest models.RequestReport, reportType string) (result models.ReqReportResponse, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.RequestAmzPPCReport(formRequest, sellerId, email, reportType, geo, profileId)
	if err != nil {
		raven.CaptureError(err, map[string]string{email: stringutils.GetStringFromStruct(formRequest)})
		return
	}

	return result, err
}

func (marketPlace *Amazon) GetCampaignReport(requestId string) (result []models.CampaignReportData, fileName string, err error) {

	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		err = errors.New("invalid configuration")
		return
	}
	err, fileName, data := amazonservices.GetAmzPPCReport(requestId, sellerId, email, geo, "campaigns", profileId)
	if err != nil {
		raven.CaptureError(err, map[string]string{email: "requestId"})
		return
	}
	result = data.([]models.CampaignReportData)
	return
}

func (marketPlace *Amazon) GetAdGroupReport(requestId string) (result []models.AdGroupReportData, fileName string, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		err = errors.New("invalid configuration")
		return
	}
	err, fileName, data := amazonservices.GetAmzPPCReport(requestId, sellerId, email, geo, "adGroups", profileId)
	if err != nil || data == nil {
		raven.CaptureError(err, map[string]string{email: requestId})
		return
	}
	result = data.([]models.AdGroupReportData)

	return
}

func (marketPlace *Amazon) GetKeywordReport(requestId string) (result []models.KeywordReportData, fileName string, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		err = errors.New("invalid configuration")
		return
	}
	err, fileName, data := amazonservices.GetAmzPPCReport(requestId, sellerId, email, geo, "keywords", profileId)
	if err != nil || data == nil {
		raven.CaptureError(err, map[string]string{email: requestId})
		return
	}
	result = data.([]models.KeywordReportData)
	return
}

func (marketPlace *Amazon) GetProductAdReport(requestId string) (result []models.ProductAdsReportData, fileName string, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		err = errors.New("invalid configuration")
		return
	}
	err, fileName, data := amazonservices.GetAmzPPCReport(requestId, sellerId, email, geo, "productAds", profileId)
	if err != nil || data == nil {
		raven.CaptureError(err, map[string]string{email: requestId})
		return
	}
	result = data.([]models.ProductAdsReportData)
	return
}

func (marketPlace *Amazon) GetAsinReport(requestId string) (result []models.AsinReportData, fileName string, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		err = errors.New("invalid configuration")
		return
	}
	err, fileName, data := amazonservices.GetAmzPPCReport(requestId, sellerId, email, geo, "asins", profileId)
	if err != nil || data == nil {
		raven.CaptureError(err, map[string]string{email: requestId})
		return
	}
	result = data.([]models.AsinReportData)
	return
}
