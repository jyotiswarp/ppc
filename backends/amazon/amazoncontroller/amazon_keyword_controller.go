package amazoncontroller

import (
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonservices"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
	"bitbucket.org/jyotiswarp/ppc/models"
	"errors"
	"github.com/getsentry/raven-go"
)

func (marketPlace *Amazon) GetKeywordList() (result []models.Keyword, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.FetchKeywordList(sellerId, email, geo, profileId)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}

func (marketPlace *Amazon) GetKeywordListExtended(keywordForm keywordmodel.KeywordDetailsForm) (result []models.Keyword, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.FetchKeywordListExtended(sellerId, email, geo, profileId, keywordForm)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}
func (marketPlace *Amazon) GetAdGroupNegativeKeywordListExtended(form keywordmodel.KeywordDetailsForm) (result []models.NegativeKeyword, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.FetchAdGroupNegativeKeywordList(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}

func (marketPlace *Amazon) CreateAdGroupNegativeKeywordList(form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.AddAdGroupNegativeKeyword(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}

func (marketPlace *Amazon) CreateAdGroupKeywordList(form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.AddAdGroupKeyword(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}

func (marketPlace *Amazon) UpdateAdGroupNegativeKeywordList(form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.UpdateAdGroupNegativeKeyword(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}

func (marketPlace *Amazon) UpdateAdGroupKeywordList(form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.UpdateAdGroupKeyword(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}

func (marketPlace *Amazon) DeleteKeyword(keywordId string) (result keywordmodel.KeywordResponse, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.DeleteKeyword(sellerId, email, geo, profileId, keywordId)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}
func (marketPlace *Amazon) GetBiddableRecommendation(form *keywordmodel.BidRecommendationForm) (result keywordmodel.KeywordBidRecomendations, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.GetBidRecommendation(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}

func (marketPlace *Amazon) GetKeywordSuggestionsByAsin(form *keywordmodel.AsinSuggestionForm) (result []keywordmodel.SuggestedKeywords, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.GetKeywordSuggestionsByAsin(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}

func (marketPlace *Amazon) GetKeywordSuggestionsByAdGroup(form *keywordmodel.AdGroupKeywordSuggestionForm) (result []keywordmodel.SuggestedKeywords, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.GetKeywordSuggestionsByAdgroup(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}

func (marketPlace *Amazon) GetExtendedKeywordByID(keywordId int64) (result models.Keyword, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.GetExtendedKeywordByID(email, geo, profileId,keywordId)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}

func (marketPlace *Amazon) GetExtendedNegativeKeywordByID(keywordId int64) (result models.NegativeKeyword, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.GetExtendedNegativeKeywordByID(email, geo, profileId,keywordId)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}
