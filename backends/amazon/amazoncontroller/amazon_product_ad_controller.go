package amazoncontroller

import (
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonservices"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/productad/productadmodels"
	"errors"
	"github.com/getsentry/raven-go"
)

func (marketPlace *Amazon) GetProductAdListExtended(form productadmodels.ProductAdForm) (result []models.ProductAd, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.FetchProductAdListExtended(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}


	return result, err
}
