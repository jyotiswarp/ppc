package amazoncontroller

import (
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonservices"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignmodels"
	"bitbucket.org/jyotiswarp/ppc/models"
	"errors"
	"github.com/getsentry/raven-go"
)

func (marketPlace *Amazon) GetCampaignsList() (result []models.Campaign, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.FetchCampaignsList(sellerId, email, geo, profileId)
	if err != nil {
		raven.CaptureError(err, nil)
	}

	return result, err
}

func (marketPlace *Amazon) GetCampaignsListExtended(form campaignmodels.CampaignForm) (result []models.Campaign, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.FetchCampaignsListExtended(sellerId, email, geo, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}

	return result, err
}

func (marketPlace *Amazon) UpdateCampaign(form *[]campaignmodels.CampaignUpdateForm) (result []models.CampaignUpdateResponse, err error) {
	sellerId := marketPlace.GetSellerId()
	geo := marketPlace.GetGeo()
	profileId := marketPlace.GetProfileId()
	email := marketPlace.GetEmail()
	if sellerId == "" || geo == "" || email == "" || profileId == 0 {
		return result, errors.New("invalid configuration")
	}
	result, err = amazonservices.UpdateAmazonCampaign(geo, sellerId, email, profileId, form)
	if err != nil {
		raven.CaptureError(err, nil)
	}

	return result, err
}
