package amazonmodel

type CampaignData struct {
	ProfileId            int64   `json:"profileId,omitempty"`
	CampaignId           int64   `json:"campaignId,omitempty"`
	CampaignType         string  `json:"campaignType,omitempty"`
	DailyBudget          float64 `json:"dailyBudget,omitempty"`
	EndDate              string  `json:"endDate,omitempty"`
	Name                 string  `json:"name,omitempty"`
	PremiumBidAdjustment bool    `json:"premiumBidAdjustment,omitempty"`
	StartDate            string  `json:"startDate,omitempty"`
	State                string  `json:"state,omitempty"`
	TargetingType        string  `json:"targetingType,omitempty"`
	ServingStatus        string  `json:"servingStatus"`
	CreationDate         int64   `json:"creationDate"`
	LastUpdatedDate      int64   `json:"lastUpdatedDate"`
}
