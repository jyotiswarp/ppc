package amazonmodel

import (
	"time"
)

type KeywordForm struct {
	StartIndex       string `json:"startIndex,omitempty"`
	Count            string `json:"count,omitempty"`
	CampaignType     string `json:"campaignType,omitempty"`
	StateFilter      string `json:"stateFilter,omitempty"`
	Name             string `json:"name,omitempty"`
	CampaignIdFilter string `json:"campaignIdFilter,omitempty"`
	AdGroupIdFilter  string `json:"adGroupIdFilter,omitempty"`
	KeywordText      string `json:"keywordText,omitempty"`
}

type KeywordEx struct {
	AdGroupId       int64      `json:"adGroupId,omitempty"`
	Bid             float64    `json:"bid,omitempty"`
	CampaignId      int64      `json:"campaignId,omitempty"`
	CreationDate    *time.Time `json:"creationDate,omitempty"` //  "epoch time in milliseconds"
	KeywordId       int64      `json:"keywordId,omitempty"`
	KeywordText     string     `json:"keywordText,omitempty"`
	LastUpdatedDate *time.Time `json:"lastUpdatedDate,omitempty"`
	MatchType       string     `json:"matchType,omitempty"`
	ServingStatus   string     `json:"servingStatus,omitempty"`
	//  ServingStatus oneOf : ["TARGETING_CLAUSE_ARCHIVED", "TARGETING_CLAUSE_PAUSED","TARGETING_CLAUSE_STATUS_LIVE", "TARGETING_CLAUSE_POLICING_SUSPENDED", "CAMPAIGN_OUT_OF_BUDGET","AD_GROUP_PAUSED", "AD_GROUP_ARCHIVED", "CAMPAIGN_PAUSED", "CAMPAIGN_ARCHIVED", "ACCOUNT_OUT_OF_BUDGET"]
	State string `json:"state,omitempty"`
}
type KeywordData struct {
	AdGroupId       int64   `json:"adGroupId,omitempty"`
	Bid             float64 `json:"bid,omitempty"`
	CampaignId      int64   `json:"campaignId,omitempty"`
	CreationDate    int64   `json:"creationDate,omitempty"` //  "epoch time in milliseconds"
	KeywordId       int64   `json:"keywordId,omitempty"`
	KeywordText     string  `json:"keywordText,omitempty"`
	LastUpdatedDate int64   `json:"lastUpdatedDate,omitempty"`
	MatchType       string  `json:"matchType,omitempty"`
	ServingStatus   string  `json:"servingStatus,omitempty"`
	//  ServingStatus oneOf : ["TARGETING_CLAUSE_ARCHIVED", "TARGETING_CLAUSE_PAUSED","TARGETING_CLAUSE_STATUS_LIVE", "TARGETING_CLAUSE_POLICING_SUSPENDED", "CAMPAIGN_OUT_OF_BUDGET","AD_GROUP_PAUSED", "AD_GROUP_ARCHIVED", "CAMPAIGN_PAUSED", "CAMPAIGN_ARCHIVED", "ACCOUNT_OUT_OF_BUDGET"]
	State string `json:"state,omitempty"`
}

// NegativeKeywordEx
type NegativeKeywordData struct {
	AdGroupId       int64   `json:"adGroupId,omitempty"`
	CampaignId      int64   `json:"campaignId,omitempty"`
	CreationDate    int64   `json:"creationDate,omitempty"`
	KeywordId       int64   `json:"keywordId,omitempty"`
	KeywordText     string  `json:"keywordText,omitempty"`
	LastUpdatedDate int64   `json:"lastUpdatedDate,omitempty"`
	MatchType       string  `json:"matchType,omitempty"`
	ServingStatus   string  `json:"servingStatus,omitempty"`
	Bid             float64 `json:"bid,omitempty"`
	// oneOf : ["TARGETING_CLAUSE_ARCHIVED", "TARGETING_CLAUSE_STATUS_LIVE"]
	State string `json:"state,omitempty"`
}
