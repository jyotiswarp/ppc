package amazonmodel

type RequestReport struct {
	ReportDate   int64  `json:"reportDate,omitempty"`
	CampaignType string `json:"campaignType,omitempty"`
	Metrics      string `json:"metrics,omitempty"`
	Segment      string `json:"segment"`
}

type ReportLocation struct {
	ReportId      string `json:"reportId,omitempty"`
	Location      string `json:"location,omitempty"`
	Status        string `json:"status,omitempty"`
	StatusDetails string `json:"statusDetails,omitempty"`
	FileSize      int    `json:"file_size,omitempty"`
}

type PPCReportDetails struct {
	Status string `json:"status"`
	Data   []ReportResponseData
}

type ReportResponseData struct {
	AdId                            int64   `json:"adId,omitempty"`
	Query                           string  `json:"query,omitempty"`
	CampaignId                      int64   `json:"campaignId,omitempty"`
	AdGroupId                       int64   `json:"adGroupId,omitempty"`
	KeywordId                       int64   `json:"keywordId,omitempty"`
	Impressions                     int     `json:"impressions,omitempty"`
	Clicks                          int     `json:"clicks,omitempty"`
	Cost                            float64 `json:"cost,omitempty"`
	CampaignName                    string  `json:"campaignName,omitempty"`
	AdGroupName                     string  `json:"adGroupName,omitempty"`
	Sku                             string  `json:"sku,omitempty"`
	Asin                            string  `json:"asin,omitempty"`
	Currency                        string  `json:"currency,omitempty"`
	KeywordText                     string  `json:"keywordText,omitempty"`
	MatchType                       string  `json:"matchType,omitempty"`
	OtherAsin                       string  `json:"otherAsin,omitempty"`

	AttributedConversions1dSameSKU  int     `json:"attributedConversions1dSameSKU,omitempty"`
	AttributedConversions1d         int     `json:"attributedConversions1d,omitempty"`
	AttributedSales1dSameSKU        float64 `json:"attributedSales1dSameSKU,omitempty"`
	AttributedSales1d               float64 `json:"attributedSales1d,omitempty"`
	AttributedConversions7dSameSKU  int     `json:"attributedConversions7dSameSKU,omitempty"`
	AttributedConversions7d         int     `json:"attributedConversions7d,omitempty"`
	AttributedSales7dSameSKU        float64 `json:"attributedSales7dSameSKU,omitempty"`
	AttributedSales7d               float64 `json:"attributedSales7d,omitempty"`
	AttributedConversions14dSameSKU int     `json:"attributedConversions14dSameSKU,omitempty"`
	AttributedConversions14d        int     `json:"attributedConversions14d,omitempty"`
	AttributedSales14dSameSKU       float64 `json:"attributedSales14dSameSKU,omitempty"`
	AttributedSales14d              float64 `json:"attributedSales14d,omitempty"`
	AttributedConversions30dSameSKU int     `json:"attributedConversions30dSameSKU,omitempty"`
	AttributedConversions30d        int     `json:"attributedConversions30d,omitempty"`
	AttributedSales30dSameSKU       float64 `json:"attributedSales30dSameSKU,omitempty"`
	AttributedSales30d              float64 `json:"attributedSales30d,omitempty"`
	AttributedUnitsOrdered1dOtherSKU        int     `json:"attributedUnitsOrdered1dOtherSKU,omitempty"`
	AttributedSales1dOtherSKU       float64 `json:"attributedSales1dOtherSKU,omitempty"`
	AttributedUnitsOrdered1d        int `json:"attributedUnitsOrdered1d,omitempty"`
}
