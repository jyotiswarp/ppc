package amazonmodel

type ProductAdEx struct {
	AdGroupId       int64  `json:"adGroupId,omitempty"`
	AdId            int64  `json:"adId,omitempty"`
	Asin            string `json:"asin,omitempty"`
	CampaignId      int64  `json:"campaignId,omitempty"`
	CreationDate    int64  `json:"creationDate,omitempty"`
	LastUpdatedDate int64  `json:"lastUpdatedDate,omitempty"`
	ServingStatus   string `json:"servingStatus,omitempty"`
	Sku             string `json:"sku,omitempty"`
	State           string `json:"state,omitempty"`
}

type ProductAdsReportData struct {
	AdId             string  `json:"adId,omitempty"`
	Impressions      int     `json:"impressions,omitempty"`
	Clicks           int     `json:"clicks,omitempty"`
	Cost             float64 `json:"cost,omitempty"`
	Cpc              float64 `json:"cpc"`
	Ctr              float64 `json:"ctr"`
	AttributedValues []AttributedTimeData
}
