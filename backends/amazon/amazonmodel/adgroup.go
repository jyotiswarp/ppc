package amazonmodel

type AdGroupForm struct {
	StartIndex       string  `json:"startIndex,omitempty"`
	Count            string  `json:"count,omitempty"`
	CampaignType     string  `json:"campaignType,omitempty"`
	StateFilter      string  `json:"stateFilter,omitempty"`
	Name             string  `json:"name,omitempty"`
	CampaignIdFilter []int64 `json:"campaignIdFilter,omitempty"`
	AdGroupIdFilter  []int64 `json:"adGroupIdFilter,omitempty"`
}

type AdGroupData struct {
	AdGroupId       int64   `json:"adGroupId,omitempty"`
	CampaignId      int64   `json:"campaignId,omitempty"`
	CreationDate    int64   `json:"creationDate,omitempty"` //  "epoch time in milliseconds"
	DefaultBid      float64 `json:"defaultBid,omitempty"`
	LastUpdatedDate int64   `json:"lastUpdatedDate,omitempty"` //  "epoch time in milliseconds"
	Name            string  `json:"name,omitempty"`
	ServingStatus   string  `json:"servingStatus,omitempty"`
	State           string  `json:"state,omitempty"`
}
