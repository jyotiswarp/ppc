package amazonmodel

// AccountInfo
type AccountInfo struct {
	MarketplaceStringId string `json:"marketplaceStringId,omitempty"`
	SellerStringId      string `json:"sellerStringId,omitempty"`
}

// Profile
type Profile struct {
	AccountInfo  *AccountInfo `json:"accountInfo,omitempty"`
	CountryCode  string       `json:"countryCode,omitempty"`
	CurrencyCode string       `json:"currencyCode,omitempty"`
	DailyBudget  float64      `json:"dailyBudget,omitempty"`
	ProfileId    int64        `json:"profileId,omitempty"`
	TimeZone     string       `json:"timeZone,omitempty"`
}
