package amazonmodel

type AttributedTimeData struct {
	Duration                     int     `json:"duration,omitempty"`
	Acos                         float64 `json:"acos"`
	AttributedConversionsSameSKU int     `json:"attributedConversionsSameSKU,omitempty"`
	AttributedConversions        int     `json:"attributedConversions,omitempty"`
	AttributedSalesSameSKU       float64 `json:"attributedSalesSameSKU,omitempty"`
	AttributedSales              int     `json:"attributedSales,omitempty"`
}
