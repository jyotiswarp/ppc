package backends

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupmodels"
	"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazoncontroller"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignmodels"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/productad/productadmodels"
	"fmt"
	"strings"
)

var (
	Market map[string]MarketClient
)

//all the ralated apis
type MarketClient interface {
	//GetPPCReport(requestId, sellerId, geo string) (result amazonmodel.PPCReportDetails, err error)

	SetSellerId(sellerId string)
	SetProfileId(profileId int64)
	SetEmail(email string)
	//GetProfileId() (int64)
	/*GetSellerId() (string)
	GetMarket() (string)
	GetGeo() (string)*/

	GetListOfProfiles() ([]models.Profile, error)
	GetCampaignsListExtended(form campaignmodels.CampaignForm) (result []models.Campaign, err error)
	GetAdGroupListExtended(formData adgroupmodels.AdGroupForm) (result []models.AdGroup, err error)
	GetKeywordListExtended(keywordForm keywordmodel.KeywordDetailsForm) (result []models.Keyword, err error)
	GetExtendedKeywordByID(keywordId int64) (result models.Keyword, err error)
	GetExtendedNegativeKeywordByID(keywordId int64) (result models.NegativeKeyword, err error)
	GetProductAdListExtended(form productadmodels.ProductAdForm) (result []models.ProductAd, err error)
	GetAdGroupNegativeKeywordListExtended(form keywordmodel.KeywordDetailsForm) (result []models.NegativeKeyword, err error)

	RequestPPCReport(model models.RequestReport, reportType string) (models.ReqReportResponse, error)
	GetCampaignReport(requestId string) (result []models.CampaignReportData, fileName string, err error)
	GetAdGroupReport(requestId string) (result []models.AdGroupReportData, fileName string, err error)
	GetKeywordReport(requestId string) (result []models.KeywordReportData, fileName string, err error)
	GetProductAdReport(requestId string) (result []models.ProductAdsReportData, fileName string, err error)
	GetAsinReport(requestId string) (result []models.AsinReportData, fileName string, err error)

	UpdateCampaign(form *[]campaignmodels.CampaignUpdateForm) (result []models.CampaignUpdateResponse, err error)
	UpdateAdGroupList(form *[]adgroupmodels.AdGroupUpdateForm) (result []adgroupmodels.AdGroupResponse, err error)
	CreateAdGroupKeywordList(form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error)
	UpdateAdGroupKeywordList(form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error)
	DeleteKeyword(keywordId string) (result keywordmodel.KeywordResponse, err error)
	CreateAdGroupNegativeKeywordList(form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error)
	UpdateAdGroupNegativeKeywordList(form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error)
	GetBiddableRecommendation(form *keywordmodel.BidRecommendationForm) (result keywordmodel.KeywordBidRecomendations, err error)
	GetKeywordSuggestionsByAsin(form *keywordmodel.AsinSuggestionForm) (result []keywordmodel.SuggestedKeywords, err error)
	GetKeywordSuggestionsByAdGroup(form *keywordmodel.AdGroupKeywordSuggestionForm) (result []keywordmodel.SuggestedKeywords, err error)
	GetAdGroupBidRecommendation(adGroupID string) (result adgroupmodels.AdGroupBidRecommendation, err error)

	//GetCampaignsList(sellerId, geo string, profileId int64) (result []models.Campaign, err error)
	//GetAdGroupList(sellerId, geo string, profileId int64) (result []models.AdGroupEx, err error)
	//GetKeywordList(sellerId, geo string, profileId int64) (result []models.KeywordEx, err error)
}

func Initialize() {

	Market = make(map[string]MarketClient)
	Market["amazon_us"] = amazoncontroller.InitializeClient("amazon", "us")
	Market["amazon_sandbox"] = amazoncontroller.InitializeClient("amazon", "sandbox")
	fmt.Println("market Place initialised")
}

func GetMarketPlace(market, geo string) MarketClient {
	marketP := strings.ToLower(market)
	geoP := strings.ToLower(geo)
	fmt.Println("market_geo :", marketP+"_"+geoP)
	return Market[marketP+"_"+geoP]
}
