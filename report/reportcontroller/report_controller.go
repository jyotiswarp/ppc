package reportcontroller

import (
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/report/reportdao"
	"bitbucket.org/jyotiswarp/ppc/report/reportmodels"
	"bitbucket.org/jyotiswarp/ppc/report/reportservices"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
)

func GetReportByDate(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.GetReportByDateForm)

	result, err := reportservices.GetReportByDate(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetAsinList(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	result, err := reportservices.GetAsinDetails(ppcManagement)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetTrendingData(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.TrendingReportForm)

	result, err := reportservices.GetTrendingData(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetPPCSummary(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.TrendingReportForm)

	result, err := reportservices.GetPPCSummary(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetKeywordSuggestions(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.TrendingReportForm)

	result, err := reportservices.GetKeywordSuggestions(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetNegativeExactFromReport(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.TrendingReportForm)

	result, err := reportservices.GetNegativeExactKeyword(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetReportByAsin(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.GetReportByAsinForm)

	result, err := reportservices.GetReportByAsin(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}
func Test(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	result, err := reportdao.GetFullReportByDuration(ppcManagement.ProfileID, ppcManagement.SellerID, ppcManagement.Profile.Email, ppcManagement.Marketplace, ppcManagement.Geo, 14)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

/*func GetReport(c *gin.Context) {
	sellerId, _ := c.GetQuery("sellerId")
	geo := c.GetHeader("geo")
	requestId, _ := c.GetQuery("requestId")
	marketPlace := c.GetHeader("market_place")
	err,scope := marketprofileservices.GetProfileId(marketPlace,geo,sellerId)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "invalid profile",
		})
		return
	}
	if sellerId == "" || requestId == "" {
		c.JSON(400, gin.H{"error":"sellerId or requestId is missing"})
		return
	}

	if marketPlace == "" || geo == "" || scope == "" {
		c.JSON(400, gin.H{"error":"market_place or geo  is missing"})
		return
	}
	profileId, err := strconv.ParseInt(scope, 10, 64)

	result, err := reportservices.GetPPCReport(marketPlace, geo, requestId, sellerId, profileId)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}*/

func GetCampaignReport(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.GetCampaignReportForm)
	pagination := c.Keys["pagination"].(ppcmanagementcommons.Pagintion)
	result, err := reportservices.FetchCampaignReport(ppcManagement, *formRequest, pagination)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetAdGroupReport(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.GetAdGroupReportForm)
	pagination := c.Keys["pagination"].(ppcmanagementcommons.Pagintion)
	result, err := reportservices.FetchAdGroupReport(ppcManagement, *formRequest, pagination)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetKeywordReport(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.GetKeywordReportForm)
	pagination := c.Keys["pagination"].(ppcmanagementcommons.Pagintion)
	result, err := reportservices.FetchKeywordReport(ppcManagement, *formRequest, pagination)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetAllKeywordReport(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.GetFullKeywordReportForm)
	result, err := reportservices.FetchFullKeywordReport(ppcManagement, *formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}
func GetProductAdReport(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.GetProductAdReportForm)
	result, err := reportservices.FetchProductAdReport(ppcManagement, *formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}
func GetAsinTrendingData(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.AsinTrendingReportForm)

	result, err := reportservices.GetAsinTrendingData(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetAsinSuggestion(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	result, err := reportservices.GetCompetitiveAsin(ppcManagement)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func CheckActiveCampaignForAsin(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.AsinTrendingReportForm)

	result, err := reportservices.GetActiveDatesOfCampaignForProduct(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetFilteredKeywords(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.KeywordFilterForm)
	result, err := reportservices.FetchFilteredKeywords(ppcManagement, *formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err,
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetKeywordSummary(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*reportmodels.TrendingReportForm)
	result, err := reportservices.GetKeywordsSummary(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

//
//func TopContributingKeywords(c *gin.Context) {
//	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
//	formRequest := c.Keys["form_data"].(*ppcmanagementform.TimeRangeForm)
//}
