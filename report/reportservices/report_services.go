package reportservices

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupmodels"
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupservices"
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignmodels"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignservices"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordservices"
	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofileservices"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetcherconstants"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetchermodels"
	"bitbucket.org/jyotiswarp/ppc/productad/productadmodels"
	"bitbucket.org/jyotiswarp/ppc/productad/productadservices"
	"bitbucket.org/jyotiswarp/ppc/report/reportdao"
	"bitbucket.org/jyotiswarp/ppc/report/reportmodels"
	"bitbucket.org/jyotiswarp/ppc/report/reportmodels/reportuimodels"
	"bitbucket.org/jyotiswarp/ppc/utils/amazonutils"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"time"
)

func UpdateBasicDetails(profile models.Profile, skipProfileUpdate bool) (err error) {
	ppcCommon := ppcmanagementcommons.CreatePPCCommons(profile)
	marketPlace := ppcCommon.Marketplace
	geo := ppcCommon.Geo
	sellerId := ppcCommon.SellerID
	email := ppcCommon.Profile.Email

	fmt.Println("marketPlce : ?, geo = ?", marketPlace, geo)
	if skipProfileUpdate {
		_, err = marketprofileservices.GetProfileList(marketPlace, geo, sellerId, email)
		if err != nil {
			log.Error(err)
			return
		}
	}
	campaignservices.GetAllCampaignsList(ppcCommon, true)
	adgroupservices.GetAdGroupList(ppcCommon, true)
	keywordservices.GetKeywordList(ppcCommon, true)
	keywordservices.GetNegativeKeywordList(ppcCommon, true)
	productadservices.GetProductAdList(ppcCommon, true)
	return
}

func UpdateBasicDetailsByCampaignId(ppcCommon ppcmanagementcommons.PPCManagementCommon, campaignId int64) (err error) {
	fmt.Println("marketPlce : ?, geo = ?", ppcCommon.Marketplace, ppcCommon.Geo)

	//todo write api to get campain details and corresoponding adgroup,keyword, productad
	campaignForm := campaignmodels.CampaignForm{}
	campaignForm.CampaignIdFilter = []int64{campaignId}
	err = campaignservices.UpdateCampaignDataModel(ppcCommon, campaignForm)
	if err != nil {
		log.Error(err)
		return
	}

	adGroupForm := adgroupmodels.AdGroupForm{}
	adGroupForm.CampaignIdFilter = []int64{campaignId}

	err = adgroupservices.UpdateAdGroupDataModel(ppcCommon, adGroupForm)
	if err != nil {
		log.Error(err)
		return
	}
	keywordForm := keywordmodel.KeywordDetailsForm{}
	keywordForm.CampaignIdFilter = []int64{campaignId}
	err = keywordservices.UpdateKeywordDataModel(ppcCommon, keywordForm)
	if err != nil {
		log.Error(err)
		return
	}
	err = keywordservices.UpdateNegativeKeywordDataModel(ppcCommon, keywordForm)
	if err != nil {
		log.Error(err)
		return
	}

	productForm := productadmodels.ProductAdForm{}
	productForm.CampaignIdFilter = []int64{campaignId}
	err = productadservices.UpdateProductAdDataModel(ppcCommon, productForm)
	if err != nil {
		log.Error(err)
		return
	}

	return
}

func RequestBackend(ppcCommon ppcmanagementcommons.PPCManagementCommon, request ppcdatafetchermodels.ReportPubSubData, formRequest models.RequestReport) (result models.ReqReportResponse, err error) {

	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)

	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.RequestPPCReport(formRequest, request.PPCJob.RequestType)

		if err != nil {
			job := request.PPCJob
			job.Status = ppcdatafetcherconstants.Failed
			dbErr := mysql.DB.Save(&job).Error
			if dbErr != nil {
				log.Error(dbErr)
			}
			return
		}
		return
	}
	return result, errors.New("invalid market")
}

func FetchCampaignReportData(request ppcdatafetchermodels.ReportPubSubData, formRequest models.RequestReport, reportDate time.Time) (string, error) {
	ppcCommon := ppcmanagementcommons.CreatePPCCommons(request.Profile)
	result, err := RequestBackend(ppcCommon, request, formRequest)
	if err != nil {
		log.Error(err)
		return "", err
	}
	data, fileName, err := GetCampaignReport(ppcCommon, result.ReportId)
	err = reportdao.UpdateCampaignDb(data, request.PPCJob.ID, reportDate)
	if err != nil {
		log.Error()
		return "", err
	}
	return fileName, nil
}

func FetchAdGroupReportData(request ppcdatafetchermodels.ReportPubSubData, formRequest models.RequestReport, reportDate time.Time) (string, error) {
	ppcCommon := ppcmanagementcommons.CreatePPCCommons(request.Profile)
	result, err := RequestBackend(ppcCommon, request, formRequest)
	if err != nil {
		log.Error(err)
		return "", err
	}
	data, fileName, err := GetAdGroupReport(ppcCommon, result.ReportId)
	err = reportdao.UpdateAdGroupDb(data, request.PPCJob.ID, reportDate)
	if err != nil {
		log.Error()
		return "", err
	}
	return fileName, nil
}

func FetchKeywordReportData(request ppcdatafetchermodels.ReportPubSubData, formRequest models.RequestReport, reportDate time.Time) (string, error) {
	ppcCommon := ppcmanagementcommons.CreatePPCCommons(request.Profile)
	result, err := RequestBackend(ppcCommon, request, formRequest)
	if err != nil {
		log.Error(err)
		return "", err
	}
	data, fileName, err := GetKeywordReport(ppcCommon, result.ReportId)
	err = reportdao.UpdateKeywordDb(data, request.PPCJob.ID, reportDate)
	if err != nil {
		log.Error()
		return "", err
	}
	return fileName, nil
}

func FetchProductAdReportData(request ppcdatafetchermodels.ReportPubSubData, formRequest models.RequestReport, reportDate time.Time) (string, error) {
	ppcCommon := ppcmanagementcommons.CreatePPCCommons(request.Profile)
	result, err := RequestBackend(ppcCommon, request, formRequest)
	if err != nil {
		log.Error(err)
		return "", err
	}
	data, fileName, err := GetProductAdReport(ppcCommon, result.ReportId)
	err = reportdao.UpdateProductAdDb(data, request.PPCJob.ID, reportDate)
	if err != nil {
		log.Error()
		return "", err
	}
	return fileName, nil
}

func FetchAsinReportData(request ppcdatafetchermodels.ReportPubSubData, formRequest models.RequestReport, reportDate time.Time) (string, error) {
	ppcCommon := ppcmanagementcommons.CreatePPCCommons(request.Profile)
	result, err := RequestBackend(ppcCommon, request, formRequest)
	if err != nil {
		log.Error(err)
		return "", err
	}
	data, fileName, err := GetAsinReport(ppcCommon, result.ReportId)
	err = reportdao.UpdateAsinReportDb(data, request.PPCJob.ID, reportDate)
	if err != nil {
		log.Error()
		return "", err
	}
	return fileName, nil
}

func GetCampaignReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, reportId string) (result []models.CampaignReportData, fileName string, err error) {

	timer := time.Tick(10 * time.Second)
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)

	if market != nil {
		counter := 0
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		for {
			select {
			case <-timer:
				counter++
				result, fileName, err = market.GetCampaignReport(reportId)

				fmt.Println("GetCampaignReport : ", result)
				if err == nil {
					return
				}
				log.Error("GetCampaignReport: ", err)
				if counter == 30 {
					err = errors.New("something went wrong in campaign report fetching. try again later")
					return
				}

			}
		}

		return
	}
	err = errors.New("invalid market")
	return

}
func GetAdGroupReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, reportId string) (result []models.AdGroupReportData, fileName string, err error) {

	timer := time.Tick(10 * time.Second)
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		counter := 0
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		for {
			select {
			case <-timer:
				counter++
				result, fileName, err = market.GetAdGroupReport(reportId)
				fmt.Println("AdGroup : ", result, "-----", err)
				if err == nil {
					return
				}
				log.Error("GetAdGroupReport: ", err)
				if counter == 30 {
					err = errors.New("something went wrong in adgroup report fetching. try again later")
					return
				}
			}
		}

		return
	}
	err = errors.New("invalid market")
	return
}
func GetKeywordReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, reportId string) (result []models.KeywordReportData, fileName string, err error) {

	timer := time.Tick(10 * time.Second)
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		counter := 0
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		for {
			select {
			case <-timer:
				counter++
				result, fileName, err = market.GetKeywordReport(reportId)
				if err == nil {
					return
				}
				log.Error("GetKeywordReport: ", err)
				if counter == 30 {
					err = errors.New("something went wrong in keyword report fetching. try again later")
					return
				}
			}
		}

		return
	}
	err = errors.New("invalid market")
	return
}
func GetProductAdReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, reportId string) (result []models.ProductAdsReportData, fileName string, err error) {

	timer := time.Tick(10 * time.Second)
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		counter := 0
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		for {
			select {
			case <-timer:
				counter++
				result, fileName, err = market.GetProductAdReport(reportId)
				if err == nil {
					return
				}
				log.Error("GetproductAdReport: ", err)
				if counter == 30 {
					err = errors.New("something went wrong in productAd report fetching. try again later")
					return
				}
			}
		}

		return
	}
	err = errors.New("invalid market")
	return
}

func GetAsinReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, reportId string) (result []models.AsinReportData, fileName string, err error) {

	timer := time.Tick(10 * time.Second)
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		counter := 0
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		for {
			select {
			case <-timer:
				counter++
				result, fileName, err = market.GetAsinReport(reportId)
				if err == nil {
					return
				}
				log.Error("GetAsinReport: ", err)
				if counter == 30 {
					err = errors.New("something went wrong in productAd report fetching. try again later")
					return
				}
			}
		}

		return
	}
	err = errors.New("invalid market")
	return
}

func GetReportByDate(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.GetReportByDateForm) (result interface{}, err error) {

	//campaignData, _ := reportdao.FetchReportBydate(ppcCommon.Profile.ProfileId, ppcCommon.SellerID, ppcCommon.Marketplace, ppcCommon.Geo)

	switch form.ReportType {
	case "campaign":
		result, err = reportdao.FetchCampaignReportBydate(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.ProfileIDStr, form)
	case "adgroup":
		result, err = reportdao.FetchAdGroupReportBydate(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.ProfileIDStr, form)
	case "keyword":
		result, err = reportdao.FetchKeywordReportBydate(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.ProfileIDStr, form)
	case "productAd":
		result, err = reportdao.FetchProductAdReportBydate(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.ProfileIDStr, form)

	}

	return
}

func GetTrendingData(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.TrendingReportForm) (result []reportmodels.TrendingInfo, err error) {

	//campaignData, _ := reportdao.FetchReportBydate(ppcCommon.Profile.ProfileId, ppcCommon.SellerID, ppcCommon.Marketplace, ppcCommon.Geo)

	result, err = reportdao.FetchTrendingData(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.ProfileIDStr, form)

	return
}
func GetAsinTrendingData(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.AsinTrendingReportForm) (result []reportmodels.TrendingInfo, err error) {

	result, err = reportdao.FetchAsinTrendingData(ppcCommon, form)

	return
}

func GetPPCSummary(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.TrendingReportForm) (result reportmodels.TrendingInfo, err error) {

	//campaignData, _ := reportdao.FetchReportBydate(ppcCommon.Profile.ProfileId, ppcCommon.SellerID, ppcCommon.Marketplace, ppcCommon.Geo)

	result, err = reportdao.FetchSummaryData(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.ProfileIDStr, form)

	return
}

func GetKeywordSuggestions(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.TrendingReportForm) (result []models.KeywordReportData, err error) {

	result, err = reportdao.FetchKeywordDetailsNew(ppcCommon, form, SUGGESTED_KEYWORD)

	result = RemoveAsin(result)
	return
}

func GetNegativeExactKeyword(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.TrendingReportForm) (result []models.KeywordReportData, err error) {

	result, err = reportdao.FetchKeywordDetailsNew(ppcCommon, form, NEGATIVE_EXACT)

	result = RemoveAsin(result)
	return
}
func RemoveAsin(list []models.KeywordReportData) []models.KeywordReportData {
	for i := len(list) - 1; i >= 0; i-- {
		if amazonutils.IsAsin(list[i].Query) {

			list = append(list[:i], list[i+1:]...)
		}
	}
	return list

}
func GetAsinDetails(ppcCommon ppcmanagementcommons.PPCManagementCommon) (result interface{}, err error) {

	result, err = reportdao.FetchAsinDetails(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.ProfileIDStr)

	return
}

func GetReportByAsin(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.GetReportByAsinForm) (result []models.ProductAdsReportData, err error) {

	result, err = reportdao.FetchProductAdReportByAsin(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.ProfileIDStr, form)

	return
}

func FetchCampaignReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, form reportmodels.GetCampaignReportForm, pagination ppcmanagementcommons.Pagintion) ([]reportuimodels.CampaignUIListData, error) {

	result, err := reportdao.GetCampaignReport(ppcCommon, form, pagination)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func FetchAdGroupReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, formRequest reportmodels.GetAdGroupReportForm, pagintion ppcmanagementcommons.Pagintion) (result []reportuimodels.AdGroupUIListData, err error) {

	result, err = reportdao.GetAdGroupReport(ppcCommon, formRequest, pagintion)
	if err != nil {
		return
	}
	return
}
func FetchKeywordReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, formRequest reportmodels.GetKeywordReportForm, pagintion ppcmanagementcommons.Pagintion) (result []reportuimodels.KeywordReportData, err error) {
	var copyResult []reportuimodels.KeywordReportData
	result, err = reportdao.GetKeywordReportNew(ppcCommon, formRequest, pagintion)
	if err != nil {
		log.Error(err)
		return
	}
	bidRequest := keywordmodel.BidRecommendationForm{AdGroupId: formRequest.AdGroupId, Keywords: createKeywordDetailsMap(result)}
	bidRecom, err := GetBiddableRecommendation(ppcCommon, &bidRequest)
	if err != nil {
		log.Error(err)
		return
	}
	recMap := createBidRecommendationMap(bidRecom)
	for _, res := range result {
		copyRes := res
		copyRes.SuggestedBid = recMap[copyRes.KeywordName]
		copyResult = append(copyResult, copyRes)
	}
	result = copyResult
	return
}

func createKeywordDetailsMap(result []reportuimodels.KeywordReportData) []keywordmodel.KeywordDetails {
	var keywordDetails []keywordmodel.KeywordDetails
	for _, res := range result {
		keywordDetails = append(keywordDetails, keywordmodel.KeywordDetails{Keyword: res.KeywordName, MatchType: res.MatchType})
	}
	return keywordDetails
}

func createBidRecommendationMap(result keywordmodel.KeywordBidRecomendations) map[string]keywordmodel.KeywordBid {

	recMap := make(map[string]keywordmodel.KeywordBid, len(result.Recommendations))
	for _, res := range result.Recommendations {
		recMap[res.Keyword] = res
	}
	return recMap
}

func GetBiddableRecommendation(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *keywordmodel.BidRecommendationForm) (result keywordmodel.KeywordBidRecomendations, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.GetBiddableRecommendation(form)
		if err != nil {
			log.Error(err)
			return
		}
		return
	}

	return result, errors.New("invalid market")

}

func FetchProductAdReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, form reportmodels.GetProductAdReportForm) (result []models.ProductAdsReportData, err error) {

	result, err = reportdao.GetProductAdReportBySeller(ppcCommon.ProfileID, ppcCommon.SellerID, ppcCommon.Marketplace, ppcCommon.Geo, form)

	if err != nil {
		return
	}
	return
}
func FetchFullKeywordReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, formRequest reportmodels.GetFullKeywordReportForm) (result []models.KeywordReportData, err error) {

	result, err = reportdao.GetFullKeyword(ppcCommon.ProfileID, ppcCommon.SellerID, ppcCommon.Marketplace, ppcCommon.Geo, formRequest)

	if err != nil {
		return
	}
	return
}

func GetCompetitiveAsin(ppcCommon ppcmanagementcommons.PPCManagementCommon) (result []string, err error) {

	//campaignData, _ := reportdao.FetchReportBydate(ppcCommon.ProfileIDStr, ppcCommon.SellerID, ppcCommon.Marketplace, ppcCommon.Geo)

	queryList, err := reportdao.FetchAsinList(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.ProfileIDStr)
	if err != nil {
		log.Error(err)
		return
	}

	for _, query := range queryList {
		if amazonutils.IsAsin(query.Query) {
			result = append(result, query.Query)
		}
	}

	return
}

func GetActiveDatesOfCampaignForProduct(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.AsinTrendingReportForm) (campainDateMap map[time.Time]bool, err error) {

	//campaignData, _ := reportdao.FetchReportBydate(ppcCommon.ProfileIDStr, ppcCommon.SellerID, ppcCommon.Marketplace, ppcCommon.Geo)

	asinDateInfo, err := reportdao.FetchActiveDatesOfCampaignForProduct(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.ProfileIDStr, form)
	if err != nil {
		log.Error(err)
		return
	}

	campainDateMap = make(map[time.Time]bool)

	for _, asin := range asinDateInfo {
		campainDateMap[*asin.Time] = true
	}

	return
}

func FetchFilteredKeywords(ppcCommon ppcmanagementcommons.PPCManagementCommon, form reportmodels.KeywordFilterForm) (result []models.KeywordReportData, err error) {

	attrCondition := "( " + GetAtrributeLimit("impressions", form.MinImpressions, form.MaxImpressions) + " ) "
	attrCondition += " and " + "( " + GetAtrributeLimit("clicks", form.MinClicks, form.MaxClicks) + " ) "
	attrCondition += " and " + "( " + GetAtrributeLimit("ctr", form.MinCtr, form.MaxCtr) + " ) "
	attrCondition += " and " + "( " + GetAtrributeLimit("acos", form.MinAcos, form.MaxAcos) + " ) "
	attrCondition += " and " + "( " + GetAtrributeLimit("sales", form.MinSales, form.MaxSales) + " ) "
	attrCondition += " and " + "( " + GetAtrributeLimit("cost", form.MinSpend, form.MaxSpend) + " ) "
	//if form.MatchType != "" {
	//	attrCondition += " and " + "( keywords.match_type = '" + form.MatchType + "' )"
	//}

	result, err = reportdao.FilteredKeywordsData(ppcCommon, form, attrCondition)

	return
}

func GetAtrributeLimit(atttribute string, attrLow string, attrHigh string) string {
	attrCondition := ""
	if attrLow != "" && attrHigh != "" {
		attrCondition = atttribute + " between " + attrLow + " and " + attrHigh
	} else if attrLow != "" && attrHigh == "" {
		attrCondition = atttribute + " > " + attrLow

	} else if attrLow == "" && attrHigh != "" {
		attrCondition = atttribute + " < " + attrHigh

	} else {
		attrCondition = " 1 = 1 "

	}
	return attrCondition
}

func GetKeywordsSummary(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.TrendingReportForm) (reportuimodels.KeywordsSummary, error) {

	var summary reportuimodels.KeywordsSummary
	suggestedChan := make(chan int)
	negativeChan := make(chan int)
	totalChan := make(chan int)
	go func() {
		suggestedCount, err := reportdao.FetchKeywordDetailsCount(ppcCommon, form, SUGGESTED_KEYWORD)
		if err != nil {
			log.Error(err)
		}
		suggestedChan <- suggestedCount
	}()

	go func() {
		negativeCount, err := reportdao.FetchKeywordDetailsCount(ppcCommon, form, NEGATIVE_EXACT)
		if err != nil {
			log.Error(err)
		}
		negativeChan <- negativeCount
	}()

	go func() {
		totalCount, err := reportdao.TotalKeywordsCount(ppcCommon, "enabled")
		if err != nil {
			log.Error(err)
		}
		totalChan <- totalCount
	}()

	summary.Total = <-totalChan
	summary.Positive = <-suggestedChan
	summary.Negative = <-negativeChan

	return summary, nil
}

//func GetTopContributingKeywords(ppcCommons ppcmanagementcommons.PPCManagementCommon, timeRange ppcmanagementform.TimeRangeForm) (result []models.KeywordReportData, err error) {
//
//}
