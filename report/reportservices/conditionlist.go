package reportservices

const SUGGESTED_KEYWORD = "orders > 0 and acos < 10"
const NEGATIVE_EXACT = "orders < 1  and clicks>1"
const SEARCH_LEVEL_PERFORMING = "  keyword_report_data.conversion_rate_within_one_week_click >5 AND a_cos < (IF(average_acos = 0, 30, average_acos)) AND ppc_sea.ctr  >.5 AND ( ppc_sea.impressions >1000 OR ppc_sea.campaign_duration < 30) "
const TopContributingKeywords = ""
