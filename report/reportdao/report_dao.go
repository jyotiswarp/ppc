package reportdao

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupdao"
	"bitbucket.org/jyotiswarp/ppc/keyword/keyworddao"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/productad/productaddao"
	"bitbucket.org/jyotiswarp/ppc/report/reportmodels"
	"bitbucket.org/jyotiswarp/ppc/report/reportmodels/reportuimodels"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	"fmt"
	"github.com/pkg/errors"
	"github.com/prometheus/common/log"
	"time"
)

func UpdateCampaignDb(data []models.CampaignReportData, jobId uint, reportDate time.Time) (err error) {
	//_, jobDetails := GetJobDetails(strconv.Itoa(int(jobId)))
	for _, row := range data {
		//var report models.CampaignReportData

		tmp := row
		//
		fmt.Println("Updating campaign dao")
		err = mysql.DB.Model(&row).Where("campaign_id = ? and report_date = ?", row.CampaignId, reportDate).FirstOrCreate(&row).Error
		if err != nil {
			log.Error(err)
			continue
		}

		//id := row.ID
		row.ReportDate = &reportDate
		if row.ReportDate.IsZero() {
			log.Error("invalid report date")
			return errors.New("invalid report date")
		}
		row.Acos = tmp.Acos
		row.Cost = tmp.Cost
		row.Clicks = tmp.Clicks
		row.Impressions = tmp.Impressions
		row.Cpc = tmp.Cpc
		row.Ctr = tmp.Ctr

		//row.ID = id
		err = mysql.DB.Model(&row).Updates(&row).Error
		if err != nil {
			log.Error(err)
			continue
		}
		reportId := row.ID
		for i := 0; i < len(row.AttributedValues); i++ {
			row.AttributedValues[i].ReportId = reportId
			row.AttributedValues[i].ReportType = "campaign"
			UpdateTimeAttributeData(row.AttributedValues[i])

		}

	}
	return
}

func UpdateAdGroupDb(data []models.AdGroupReportData, jobId uint, reportDate time.Time) (err error) {
	//go AddEntryToAdGroupReportDataHistory(data)
	//_, jobDetails := GetJobDetails(strconv.Itoa(int(jobId)))
	for _, row := range data {
		err, adGroup := adgroupdao.GetAdGroupDetails(int64(row.AdGroupId))
		if err != nil {
			log.Error(err)
			continue
		}
		tmp := row
		//var report models.CampaignReportData
		row.CampaignId = adGroup.CampaignId

		err = mysql.DB.Model(&row).Where("campaign_id = ? and ad_group_id = ? and report_date = ?", row.CampaignId, row.AdGroupId, reportDate).FirstOrCreate(&row).Error
		if err != nil {
			log.Error(err)
			continue
		}
		/*row = models.AdGroupReportData{Acos: tmp.Acos, Cost: tmp.Cost, Clicks: tmp.Clicks, Impressions: tmp.Impressions, Cpc: tmp.Cpc, Ctr: tmp.Ctr,}*/
		row.ReportDate = &reportDate
		if row.ReportDate.IsZero() {
			log.Error("report date invalid")
		}
		row.Acos = tmp.Acos
		row.Cost = tmp.Cost
		row.Clicks = tmp.Clicks
		row.Impressions = tmp.Impressions
		row.Cpc = tmp.Cpc
		row.Ctr = tmp.Ctr
		err = mysql.DB.Model(&row).Updates(&row).Error
		if err != nil {
			log.Error(err)
			continue
		}
		reportId := row.ID
		for i := 0; i < len(row.AttributedValues); i++ {
			row.AttributedValues[i].ReportId = reportId
			row.AttributedValues[i].ReportType = "adgroup"
			UpdateTimeAttributeData(row.AttributedValues[i])

		}

	}
	return
}
func UpdateKeywordDb(data []models.KeywordReportData, jobId uint, reportDate time.Time) (err error) {
	//go AddEntryToKeywordReportDataHistory(data)
	//_, jobDetails := GetJobDetails(strconv.Itoa(int(jobId)))
	for _, row := range data {
		//var report models.CampaignReportData
		tmp := row
		err, keyword := keyworddao.GetKeywordDetails(int64(row.KeywordId))
		if err != nil {
			log.Error(err)
			continue
		}

		row.CampaignId = keyword.CampaignId
		row.AdGroupId = keyword.AdGroupId
		err = mysql.DB.Model(&row).Where("campaign_id = ? and ad_group_id = ? and keyword_id = ? and report_date = ? and query = ?", row.CampaignId, row.AdGroupId, row.KeywordId, reportDate, row.Query).FirstOrCreate(&row).Error
		if err != nil {
			log.Error(err)
			continue
		}
		/*row = models.KeywordReportData{Acos: tmp.Acos, Cost: tmp.Cost, Clicks: tmp.Clicks, Impressions: tmp.Impressions, Cpc: tmp.Cpc, Ctr: tmp.Ctr,}*/
		row.ReportDate = &reportDate
		if row.ReportDate.IsZero() {
			log.Error("report date invalid")
		}
		row.Acos = tmp.Acos
		row.Cost = tmp.Cost
		row.Clicks = tmp.Clicks
		row.Impressions = tmp.Impressions
		row.Cpc = tmp.Cpc
		row.Ctr = tmp.Ctr
		err = mysql.DB.Model(&row).Updates(&row).Error
		if err != nil {
			log.Error(err)
			continue
		}
		reportId := row.ID
		for i := 0; i < len(row.AttributedValues); i++ {
			row.AttributedValues[i].ReportId = reportId
			row.AttributedValues[i].ReportType = "keyword"
			UpdateTimeAttributeData(row.AttributedValues[i])

		}

	}
	return
}

func UpdateTimeAttributeData(data models.AttributedTimeData) (err error) {
	tmp := data
	err = mysql.DB.Model(&data).Where("report_id = ? and number_of_days = ? and report_type = ? ", data.ReportId, data.NumberOfDays, data.ReportType).FirstOrCreate(&data).Error
	if err != nil {
		log.Error(err)
		return
	}
	data.AttributedSales = tmp.AttributedSales
	data.AttributedConversions = tmp.AttributedConversions
	data.AttributedSalesSameSKU = tmp.AttributedSalesSameSKU
	data.AttributedConversionsSameSKU = tmp.AttributedConversionsSameSKU
	err = mysql.DB.Model(&data).Updates(&data).Error
	if err != nil {
		log.Error(err)
		return
	}
	return
}
func UpdateProductAdDb(data []models.ProductAdsReportData, jobId uint, reportDate time.Time) (err error) {
	//go AddEntryToProductAdReportDataHistory(data)

	//_, jobDetails := GetJobDetails(strconv.Itoa(int(jobId)))
	for _, row := range data {
		tmp := row
		err, productAd := productaddao.GetProductAdDetails(row.AdId)
		if err != nil {
			log.Error(err)
			continue
		}
		row.CampaignId = productAd.CampaignId
		row.AdGroupId = productAd.AdGroupId
		//row.ReportDate = jobDetails.ReportDate
		//var report models.CampaignReportData
		err = mysql.DB.Model(&row).Where("campaign_id = ? and ad_group_id = ? and ad_id =? and report_date = ? ", row.CampaignId, row.AdGroupId, row.AdId, reportDate).FirstOrCreate(&row).Error
		if err != nil {
			log.Error(err)
			continue
		}
		/*row = models.ProductAdsReportData{Acos: tmp.Acos, Cost: tmp.Cost, Clicks: tmp.Clicks, Impressions: tmp.Impressions, Cpc: tmp.Cpc, Ctr: tmp.Ctr,}*/
		row.ReportDate = &reportDate
		if row.ReportDate.IsZero() {
			log.Error("report date invalid")
		}
		row.Acos = tmp.Acos
		row.Cost = tmp.Cost
		row.Clicks = tmp.Clicks
		row.Impressions = tmp.Impressions
		row.Cpc = tmp.Cpc
		row.Ctr = tmp.Ctr

		err = mysql.DB.Model(&row).Updates(&row).Error
		if err != nil {
			log.Error(err)
			continue
		}
		reportId := row.ID
		for i := 0; i < len(row.AttributedValues); i++ {
			row.AttributedValues[i].ReportId = reportId
			row.AttributedValues[i].ReportType = "productAd"
			UpdateTimeAttributeData(row.AttributedValues[i])

		}

	}
	return
}

func UpdateAsinReportDb(data []models.AsinReportData, jobId uint, reportDate time.Time) (err error) {
	//go AddEntryToProductAdReportDataHistory(data)

	//_, jobDetails := GetJobDetails(strconv.Itoa(int(jobId)))
	for _, row := range data {

		/*asinReport := models.AsinReportData{}*/
		/*err, productAd := productaddao.GetProductAdDetails(row.AdId)
		if err != nil {
			log.Error(err)
			continue
		}*/
		//row.ReportDate = jobDetails.ReportDate
		//var report models.CampaignReportData
		row.ReportDate = &reportDate
		if row.ReportDate.IsZero() {
			log.Error("report date invalid")
			return
		}
		err = mysql.DB.Model(&row).Where("campaign_id = ? and ad_group_id = ? and asin =? and keyword_text = ? and report_date = ? and match_type = ?", row.CampaignId, row.AdGroupId, row.Asin, row.KeywordText, reportDate, row.MatchType).FirstOrCreate(&row).Error
		if err != nil {
			log.Error(err)
			continue
		}
		/*row = models.ProductAdsReportData{Acos: tmp.Acos, Cost: tmp.Cost, Clicks: tmp.Clicks, Impressions: tmp.Impressions, Cpc: tmp.Cpc, Ctr: tmp.Ctr,}*/

	}
	return
}

/*func GetFullReport(profileId int64, sellerId, marketPlace, geo string) (result []models.CampaignReportData, err error) {

	result, err = GetCampaignReport(profileId, sellerId, marketPlace, geo)
	if err != nil {
		log.Error(err)
		return
	}
	for i := 0; i < len(result); i++ {

		GetAdgroupReport(&result[i])

		for j := 0; j < len(result[i].AdGroupReport); j++ {
			result[i].AdGroupReport[j].KeywordReport, _ = GetKeywordReport(&result[i].AdGroupReport[j])
			result[i].AdGroupReport[j].ProductAdReport, _ = GetProductAdReport(&result[i].AdGroupReport[j])
		}
	}

	return
}*/

func GetLastNReportsByCampaignId(campaignId int64, noDays int) (result models.CampaignReportData, err error) {

	result, err = GetLastNDaysCampaignReportByCampaignId(noDays, campaignId)
	if err != nil {
		log.Error(err)
		return
	}
	//for i := 0; i < len(result); i++ {

	result.AdGroupReport, err = GetLastNAdGroupReport(result, noDays)
	if err != nil {
		log.Error(err)
		return
	}
	for j := 0; j < len(result.AdGroupReport); j++ {
		result.AdGroupReport[j].KeywordReport, err = GetLastNKeywordReport(&result.AdGroupReport[j], noDays)
		if err != nil {
			log.Error(err)
			return
		}
		result.AdGroupReport[j].ProductAdReport, err = GetLastNProductAdReport(&result.AdGroupReport[j], noDays)
		if err != nil {
			log.Error(err)
			return
		}
	}
	return
}

func GetReportsByCampaignIdAndDateRange(campaignId int64, fromDate, endDate time.Time) (result models.CampaignReportData, err error) {

	result, err = GetCampaignReportsByCampaignIdAndDateRange(campaignId, fromDate, endDate)
	if err != nil {
		log.Error(err)
		return
	}
	//for i := 0; i < len(result); i++ {

	result.AdGroupReport, err = GetAdGroupReportByDate(result, fromDate, endDate)
	if err != nil {
		log.Error(err)
		return
	}
	for j := 0; j < len(result.AdGroupReport); j++ {
		result.AdGroupReport[j].KeywordReport, err = GetKeywordReportByDate(&result.AdGroupReport[j], fromDate, endDate)
		if err != nil {
			log.Error(err)
			return
		}
		result.AdGroupReport[j].ProductAdReport, err = GetProductAdReportByDate(&result.AdGroupReport[j], fromDate, endDate)
		if err != nil {
			log.Error(err)
			return
		}
	}
	return
}

func GetFullReportByDuration(profileId int64, sellerId, email, marketPlace, geo string, noDays int) (result []models.CampaignReportData, err error) {

	result, err = GetLastNDaysCampaignReport(profileId, sellerId, marketPlace, geo, noDays)
	if err != nil {
		log.Error(err)
		return
	}
	for i := 0; i < len(result); i++ {

		result[i].AdGroupReport, err = GetLastNAdGroupReport(result[i], noDays)
		if err != nil {
			log.Error(err)
			return
		}
		for j := 0; j < len(result[i].AdGroupReport); j++ {
			result[i].AdGroupReport[j].KeywordReport, err = GetLastNKeywordReport(&result[i].AdGroupReport[j], noDays)
			if err != nil {
				log.Error(err)
				return
			}
			result[i].AdGroupReport[j].ProductAdReport, err = GetLastNProductAdReport(&result[i].AdGroupReport[j], noDays)
			if err != nil {
				log.Error(err)
				return
			}
		}
	}

	return
}

/*func GetLastNReports(profileId int64, sellerId, marketPlace, geo string, number int) (result []models.CampaignReportData, err error) {

	result, err = GetCampaignReport(profileId, sellerId, marketPlace, geo)
	if err != nil {
		log.Error(err)
		return
	}
	for i := 0; i < len(result); i++ {

		GetAdgroupReport(&result[i])

		for j := 0; j < len(result[i].AdGroupReport); j++ {
			result[i].AdGroupReport[j].KeywordReport, _ = GetKeywordReport(&result[i].AdGroupReport[j])
			result[i].AdGroupReport[j].ProductAdReport, _ = GetProductAdReport(&result[i].AdGroupReport[j])
		}
	}

	return
}*/

func FetchTrendingData(marketPlace, geo, sellerId, profileId string, form *reportmodels.TrendingReportForm) (result []reportmodels.TrendingInfo, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("campaign_report_data.report_date time,"+
			" round(sum(`campaign_report_data`.`cost`),2) cost, "+
			" round(SUM(campaign_report_data.sales),2) sales,"+
			" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) acos,"+
			" SUM(campaign_report_data.`impressions`) impressions,"+
			" SUM(campaign_report_data.clicks) clicks,SUM(`campaign_report_data`.orders) orders,"+
			" round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc,"+
			" round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("report_date between ? and ?", form.FromDate, form.ToDate).
		Group("campaign_report_data.report_date ").
		Find(&result).Error

	return
}

func FetchActiveDatesOfCampaignForProduct(marketPlace, geo, sellerId, profileId string, form *reportmodels.AsinTrendingReportForm) (result []reportmodels.TrendingInfo, err error) {

	err = mysql.DB.Table("product_ads_report_data").
		Select("campaigns.campaign_id, product_ads_report_data.report_date time").
		Joins("join campaigns on campaigns.campaign_id = product_ads_report_data.campaign_id").
		Joins("join product_ads on product_ads.ad_id = product_ads_report_data.ad_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("report_date between ? and ? and product_ads.asin = ?", form.FromDate, form.ToDate, form.Asin).
		Where("product_ads_report_data.impressions != 0").
		//Group("product_ads_report_data.report_date ").
		Find(&result).Error

	return
}

func FetchCampaignReportBydate(marketPlace, geo, sellerId, profileId string, form *reportmodels.GetReportByDateForm) (result []models.CampaignReportData, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("campaign_report_data.`campaign_id` campaign_id,"+
			" campaigns.name campaign_name, "+
			" round(sum(`campaign_report_data`.`cost`),2) cost,"+
			" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) acos,"+
			" SUM(campaign_report_data.`impressions`) impressions,"+
			" SUM(campaign_report_data.clicks) clicks,"+
			" SUM(`campaign_report_data`.orders) orders,"+
			" round(SUM(campaign_report_data.sales),2) sales, "+
			" round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc,"+
			" round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("report_date between ? and ?", form.FromDate, form.ToDate).
		Group("campaign_report_data.`campaign_id`,campaigns.name ").
		Find(&result).Error

	return
}
func FetchAdGroupReportBydate(marketPlace, geo, sellerId, profileId string, form *reportmodels.GetReportByDateForm) (result []models.AdGroupReportData, err error) {

	//for i:=0;i<len(campaigns);i++{
	//	var
	err = mysql.DB.Table("ad_group_report_data").
		Select("ad_group_report_data.`campaign_id`,"+
			" campaigns.name campaign_name, "+
			" ad_group_report_data.ad_group_id, "+
			" adg.name ad_group_name ,"+
			" round(sum(`ad_group_report_data`.`cost`),2) cost,"+
			" round(SUM(ad_group_report_data.sales),2) sales,"+
			" round((sum(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.sales))*100 ,2) acos,"+
			" SUM(ad_group_report_data.`impressions`) impressions,"+
			" SUM(ad_group_report_data.clicks) clicks,"+
			" SUM(`ad_group_report_data`.orders) orders, "+
			" round((SUM(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.clicks)),2) cpc,"+
			" round((SUM(ad_group_report_data.clicks)/SUM(ad_group_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = ad_group_report_data.campaign_id").
		Joins("join ad_groups adg on adg.ad_group_id = ad_group_report_data.ad_group_id ").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		//Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("ad_group_report_data.report_date between ? and ?", form.FromDate, form.ToDate).
		Group("ad_group_report_data.`campaign_id`,campaigns.name,ad_group_report_data.ad_group_id,adg.name").
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}

	return
}

func FetchKeywordReportBydate(marketPlace, geo, sellerId, profileId string, form *reportmodels.GetReportByDateForm) (keyword []models.KeywordReportData, err error) {

	//for i:=0;i<len(campaigns);i++{
	//	var
	err = mysql.DB.Table("keyword_report_data").
		Select("keywords.`campaign_id` campaign_id,"+
			" campaigns.name campaign_name,"+
			" keywords.ad_group_id ad_group_id,"+
			" adg.name ad_group_name,"+
			" keywords.keyword_id keyword_id,"+
			" keywords.keyword_text keyword_name, "+
			" round(sum(`keyword_report_data`.`cost`),2) cost, "+
			" round((sum(`keyword_report_data`.`cost`)/SUM(keyword_report_data.sales) )*100,2) acos,"+
			" SUM(keyword_report_data.`impressions`) impressions,"+
			" SUM(keyword_report_data.clicks) clicks,"+
			" SUM(`keyword_report_data`.orders) orders,"+
			" round(SUM(keyword_report_data.sales),2) sales, "+
			" round((SUM(`keyword_report_data`.`cost`)/SUM(keyword_report_data.clicks)),2) cpc,"+
			" round((SUM(keyword_report_data.clicks)/SUM(keyword_report_data.`impressions`))*100,2) ctr  ").
		Joins("join ad_groups adg on adg.ad_group_id = keyword_report_data.ad_group_id ").
		Joins("join keywords on keywords.keyword_id = keyword_report_data.keyword_id").
		Joins("join campaigns on campaigns.campaign_id = keywords.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("keyword_report_data.report_date between  ? and ?", form.FromDate, form.ToDate).
		Group("keywords.`campaign_id`,campaigns.name ,keywords.ad_group_id,adg.name,keywords.keyword_id,keywords.keyword_text").
		Find(&keyword).Error
	if err != nil {
		log.Error(err)
		return
	}

	return
}

func FetchProductAdReportBydate(marketPlace, geo, sellerId, profileId string, form *reportmodels.GetReportByDateForm) (productAd []models.ProductAdsReportData, err error) {

	//for i:=0;i<len(campaigns);i++{
	//	var
	err = mysql.DB.Table("product_ads_report_data").
		Select("product_ads_report_data.`campaign_id`,"+
			" campaigns.name campaign_name,"+
			" product_ads_report_data.ad_group_id ad_group_id,"+
			" adg.name ad_group_name,"+
			" product_ads_report_data.ad_id ad_id,asin,"+
			" round(sum(`product_ads_report_data`.`cost`),2) cost, "+
			" round(SUM(product_ads_report_data.sales),2) sales,"+
			" round((sum(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.sales) )*100,2) acos,"+
			" SUM(product_ads_report_data.`impressions`) impressions,"+
			" SUM(product_ads_report_data.clicks) clicks,"+
			" SUM(`product_ads_report_data`.orders) orders, "+
			" round((SUM(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.clicks)),2) cpc,"+
			" round((SUM(product_ads_report_data.clicks)/SUM(product_ads_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = product_ads_report_data.campaign_id").
		Joins("join ad_groups adg on adg.ad_group_id = product_ads_report_data.ad_group_id ").
		Joins("join product_ads on product_ads.ad_id = product_ads_report_data.ad_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("product_ads_report_data.report_date between ? and ? ", form.FromDate, form.ToDate).
		Group("product_ads_report_data.`campaign_id`,campaigns.name,product_ads_report_data.ad_group_id,adg.name," +
			"product_ads_report_data.ad_id,asin").
		Find(&productAd).Error
	if err != nil {
		log.Error(err)
		return
	}

	return
}

func FetchProductAdReportByAsin(marketPlace, geo, sellerId, profileId string, form *reportmodels.GetReportByAsinForm) (productAd []models.ProductAdsReportData, err error) {

	//for i:=0;i<len(campaigns);i++{
	//	var
	err = mysql.DB.Table("product_ads_report_data").
		Select("product_ads_report_data.`campaign_id`,"+
			" campaigns.name campaign_name,"+
			" product_ads_report_data.ad_group_id,"+
			" adg.name ad_group_name,"+
			" ad.ad_id ad_id, ad.asin asin , "+
			" round(sum(`product_ads_report_data`.`cost`),2) cost, "+
			" round(SUM(product_ads_report_data.sales),2) sales,"+
			" round((sum(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.sales) )*100,2) acos,"+
			" SUM(product_ads_report_data.`impressions`) impressions,"+
			" SUM(product_ads_report_data.clicks) clicks,"+
			" SUM(`product_ads_report_data`.orders) orders, "+
			" round((SUM(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.clicks)),2) cpc,"+
			" round((SUM(product_ads_report_data.clicks)/SUM(product_ads_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = product_ads_report_data.campaign_id").
		Joins("join ad_groups adg on adg.ad_group_id = product_ads_report_data.ad_group_id ").
		Joins("join product_ads ad on product_ads_report_data.ad_id = ad.ad_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("ad.asin = ? and product_ads_report_data.report_date between ? and ? ", form.Asin, form.FromDate, form.ToDate).
		Group("product_ads_report_data.`campaign_id`,campaigns.name,product_ads_report_data.ad_group_id,adg.name,ad.ad_id,ad.asin ").
		Find(&productAd).Error
	if err != nil {
		log.Error(err)
		return
	}

	return
}

func FetchSummaryData(marketPlace, geo, sellerId, profileId string, form *reportmodels.TrendingReportForm) (result reportmodels.TrendingInfo, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("round(sum(`campaign_report_data`.`cost`),2) cost, "+
			" round(SUM(campaign_report_data.sales),2) sales,"+
			" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) acos,"+
			" SUM(campaign_report_data.`impressions`) impressions,"+
			" SUM(campaign_report_data.clicks) clicks,"+
			" SUM(`campaign_report_data`.orders) orders,"+
			" round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc,"+
			" round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("report_date between ? and ?", form.FromDate, form.ToDate).
		//Group("campaign_report_data.report_date ").
		Find(&result).Error

	return
}
func FetchAsinDetails(marketPlace, geo, sellerId, profileId string) (productAd []models.ProductAd, err error) {

	//for i:=0;i<len(campaigns);i++{
	//	var
	err = mysql.DB.Table("product_ads").
		Select(" product_ads.* ").
		Joins("join campaigns on campaigns.campaign_id = product_ads.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("asin !=''").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Find(&productAd).Error
	if err != nil {
		log.Error(err)
		return
	}

	return
}
func GetReportByCampaign(profileId, campaignId int64, sellerId, marketPlace, geo string) (result models.CampaignReportData, err error) {

	result, err = GetCampaignReportById(profileId, campaignId, sellerId, marketPlace, geo)
	if err != nil {
		log.Error(err)
		return
	}
	//for i := 0; i < len(result); i++ {

	GetAdgroupReport(&result)

	for j := 0; j < len(result.AdGroupReport); j++ {
		result.AdGroupReport[j].KeywordReport, _ = GetKeywordReport(&result.AdGroupReport[j])
		result.AdGroupReport[j].ProductAdReport, _ = GetProductAdReport(&result.AdGroupReport[j])
	}
	//}

	return
}

func GetCampaignReportById(profileId, campaignId int64, sellerId, marketPlace, geo string) (result models.CampaignReportData, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("campaign_report_data.*").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=? and campaign_report_data.campaign_id = ?", marketPlace, geo, sellerId, profileId, campaignId).
		Find(&result).Error

	//for i := 0; i < len(result); i++ {
	result.AttributedValues = GetAttributeTimeData(result)
	//}

	return
}
func GetCampaignReportOld(ppcCommon ppcmanagementcommons.PPCManagementCommon, form reportmodels.GetCampaignReportForm, pagination ppcmanagementcommons.Pagintion) (result []reportuimodels.CampaignUIListData, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("campaigns.`campaign_id`,"+
			" campaigns.name campaign_name, "+
			" round(sum(`campaign_report_data`.`cost`),2) cost,"+
			" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) acos,"+
			" SUM(campaign_report_data.`impressions`) impressions,"+
			" SUM(campaign_report_data.clicks) clicks,"+
			" SUM(`campaign_report_data`.orders) orders,"+
			" round(SUM(campaign_report_data.sales),2) sales, "+
			" round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc,"+
			" round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr,"+
			" campaigns.targeting_type,"+
			" campaigns.state state,"+
			" campaigns.serving_status, "+
			" campaigns.premium_bid_adjustment,"+
			" campaigns.daily_budget,"+
			" campaigns.start_date,"+
			" campaigns.end_date").
		Joins("right join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Where("campaigns.profile_id=? AND report_date between ? and ?", ppcCommon.ProfileID, form.FromDate, form.ToDate).
		Group("campaign_report_data.`campaign_id`,campaigns.name,campaigns.targeting_type," +
			"campaigns.state,campaigns.serving_status,campaigns.premium_bid_adjustment,campaigns.daily_budget,campaigns.start_date,campaigns.end_date ").
		Limit(pagination.Limit).Offset(pagination.Limit * pagination.Page).
		Scan(&result).Error

	return
}

func GetCampaignReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, form reportmodels.GetCampaignReportForm, pagination ppcmanagementcommons.Pagintion) (result []reportuimodels.CampaignUIListData, err error) {
	rawQuery := "select " +
		" campaigns.`campaign_id`,campaigns.name campaign_name," +
		" campaigns.premium_bid_adjustment,campaigns.daily_budget,campaigns.targeting_type,campaigns.state state,campaigns.serving_status," +
		" campaigns.start_date,campaigns.end_date,cost,acos,impressions,clicks,orders,sales,cpc,ctr" +
		" from campaigns " +
		"left join " +
		"(SELECT " +
		" campaign_report_data.`campaign_id`," +
		" round(sum(`campaign_report_data`.`cost`),2) cost," +
		" round(avg(campaign_report_data.acos) ,2) acos," +
		" SUM(campaign_report_data.`impressions`) impressions," +
		" SUM(campaign_report_data.clicks) clicks," +
		" SUM(`campaign_report_data`.orders) orders," +
		" round(SUM(campaign_report_data.sales),2) sales," +
		" round(AVG(campaign_report_data.cpc),2) cpc," +
		" round(AVG(campaign_report_data.ctr),2) ctr" +
		" FROM `campaigns`" +
		" left outer join campaign_report_data on campaigns.campaign_id = campaign_report_data.campaign_id" +
		" WHERE (campaigns.profile_id=? AND campaign_report_data.report_date between ? and ?)" +
		" GROUP BY campaign_report_data.`campaign_id`) " +
		" temp on campaigns.campaign_id=temp.campaign_id" +
		" where campaigns.profile_id=?"
	err = mysql.DB.Raw(rawQuery, ppcCommon.ProfileID, form.FromDate, form.ToDate, ppcCommon.ProfileID).Order("campaigns.start_date desc").Limit(pagination.Limit).Offset(pagination.Limit * pagination.Page).Scan(&result).Error
	return
}

func GetLastNDaysCampaignReportByCampaignId(numberOfDays int, campaignId int64) (result models.CampaignReportData, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("campaign_report_data.`campaign_id` campaign_id, round(sum(`campaign_report_data`.`cost`),2) cost, round(SUM(campaign_report_data.sales),2) sales,"+
			" round(sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales) ,2) acos,SUM(campaign_report_data.`impressions`) impressions,"+
			"SUM(campaign_report_data.clicks) clicks,SUM(`campaign_report_data`.orders) orders, "+
			"round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc,"+
			"round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Where("campaign_report_data.campaign_id =?", campaignId).
		Where("report_date between CURDATE() - INTERVAL ? DAY and CURDATE()", numberOfDays).
		Group("campaign_report_data.`campaign_id`").
		Find(&result).Error

	/*for i := 0; i < len(result); i++ {
		GetLastNAdGroupReport(&result[i], numberOfDays)
	}*/

	return
}

func GetCampaignReportsByCampaignIdAndDateRange(campaignId int64, fromDate, endDate time.Time) (result models.CampaignReportData, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("campaign_report_data.`campaign_id` campaign_id, round(sum(`campaign_report_data`.`cost`),2) cost, round(SUM(campaign_report_data.sales),2) sales,"+
			" round(sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales) ,2) acos,SUM(campaign_report_data.`impressions`) impressions,"+
			"SUM(campaign_report_data.clicks) clicks,SUM(`campaign_report_data`.orders) orders, "+
			"round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc,"+
			"round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Where("campaign_report_data.campaign_id =?", campaignId).
		Where("report_date between ? and ?", fromDate, endDate).
		Group("campaign_report_data.`campaign_id`").
		Find(&result).Error

	/*for i := 0; i < len(result); i++ {
		GetLastNAdGroupReport(&result[i], numberOfDays)
	}*/

	return
}

func GetLastNDaysCampaignReport(profileId int64, sellerId, marketPlace, geo string, numberOfDays int) (result []models.CampaignReportData, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("campaign_report_data.`campaign_id` campaign_id, round(sum(`campaign_report_data`.`cost`),2) cost, "+
			" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) acos,SUM(campaign_report_data.`impressions`) impressions,"+
			"SUM(campaign_report_data.clicks) clicks,SUM(`campaign_report_data`.orders) orders,round(SUM(campaign_report_data.sales),2) sales, "+
			"round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc,"+
			"round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("report_date between CURDATE() - INTERVAL ? DAY and CURDATE()", numberOfDays).
		Group("campaign_report_data.`campaign_id`").
		Find(&result).Error

	/*for i := 0; i < len(result); i++ {
		GetLastNAdGroupReport(&result[i], numberOfDays)
	}*/

	return
}

func GetAdgroupReport(campaign *models.CampaignReportData) (err error) {

	//for i:=0;i<len(campaigns);i++{
	var adGroups []models.AdGroupReportData
	err = mysql.DB.Where("campaign_id = ?", campaign.CampaignId).Find(&adGroups).Error
	if err != nil {
		log.Error(err)
		return
	}
	//GetKeywordReport(adGroups)
	for i := 0; i < len(adGroups); i++ {

		adGroups[i].AttributedValues = GetAttributeTimeData(adGroups[i])
		//result
	}
	campaign.AdGroupReport = adGroups
	return
}

func GetLastNAdGroupReport(campaign models.CampaignReportData, numberOfDays int) (adGroups []models.AdGroupReportData, err error) {

	//for i:=0;i<len(campaigns);i++{
	//	var
	err = mysql.DB.Table("ad_group_report_data").
		Select("ad_group_report_data.`campaign_id` campaign_id,ad_group_report_data.ad_group_id ad_group_id, "+
			"round(sum(`ad_group_report_data`.`cost`),2) cost, round(SUM(ad_group_report_data.sales),2) sales,"+
			" round((sum(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.sales))*100 ,2) acos,"+
			"SUM(ad_group_report_data.`impressions`) impressions,"+
			"SUM(ad_group_report_data.clicks) clicks,SUM(`ad_group_report_data`.orders) orders, "+
			"round((SUM(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.clicks)),2) cpc,"+
			"round((SUM(ad_group_report_data.clicks)/SUM(ad_group_report_data.`impressions`))*100,2) ctr  ").
		//Joins("join campaigns on campaigns.campaign_id = ad_group_report_data.campaign_id").
		//Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		//Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("ad_group_report_data.report_date between CURDATE() - INTERVAL ? DAY and CURDATE() and ad_group_report_data.campaign_id =?", numberOfDays, campaign.CampaignId).
		Group("ad_group_report_data.`campaign_id`,ad_group_report_data.ad_group_id").
		Find(&adGroups).Error
	if err != nil {
		log.Error(err)
		return
	}
	//campaign.AdGroupReport = adGroups

	return
}

func GetLastNKeywordReport(adgroup *models.AdGroupReportData, noDays int) (keyword []models.KeywordReportData, err error) {

	//for i:=0;i<len(campaigns);i++{
	//var keyword []models.KeywordReportData
	err = mysql.DB.Table("keyword_report_data").
		Select("keyword_report_data.`campaign_id` campaign_id,keyword_report_data.ad_group_id ad_group_id,"+
			"keyword_report_data.keyword_id keyword_id, "+
			"round(sum(`keyword_report_data`.`cost`),2) cost, round(SUM(keyword_report_data.sales),2) sales,"+
			" round((sum(`keyword_report_data`.`cost`)/SUM(keyword_report_data.sales) )*100,2) acos,"+
			"SUM(keyword_report_data.`impressions`) impressions,"+
			"SUM(keyword_report_data.clicks) clicks,SUM(`keyword_report_data`.orders) orders, "+
			"round((SUM(`keyword_report_data`.`cost`)/SUM(keyword_report_data.clicks)),2) cpc,"+
			"round((SUM(keyword_report_data.clicks)/SUM(keyword_report_data.`impressions`))*100,2) ctr  ").
		//Joins("join campaigns on campaigns.campaign_id = ad_group_report_data.campaign_id").
		//Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		//Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("keyword_report_data.report_date between CURDATE() - INTERVAL ? DAY and CURDATE() and keyword_report_data.campaign_id =? and keyword_report_data.ad_group_id=?", noDays, adgroup.CampaignId, adgroup.AdGroupId).
		Group("keyword_report_data.`campaign_id`,keyword_report_data.ad_group_id,keyword_report_data.keyword_id").
		Find(&keyword).Error
	if err != nil {
		log.Error(err)
		return
	}
	//GetKeywordReport(adGroups)
	/*for i := 0; i < len(keyword); i++ {

		keyword[i].AttributedValues = GetAttributeTimeData(keyword[i])
	}*/
	//adgroup.KeywordReport = keyword
	return
}
func GetLastNProductAdReport(adgroup *models.AdGroupReportData, noDays int) (productAd []models.ProductAdsReportData, err error) {

	//for i:=0;i<len(campaigns);i++{

	err = mysql.DB.Table("product_ads_report_data").
		Select("product_ads_report_data.`campaign_id` campaign_id,product_ads_report_data.ad_group_id ad_group_id,"+
			"product_ads_report_data.ad_id ad_id, "+
			"round(sum(`product_ads_report_data`.`cost`),2) cost, round(SUM(product_ads_report_data.sales),2) sales,"+
			" round((sum(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.sales) )*100,2) acos,"+
			"SUM(product_ads_report_data.`impressions`) impressions,"+
			"SUM(product_ads_report_data.clicks) clicks,SUM(`product_ads_report_data`.orders) orders, "+
			"round((SUM(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.clicks)),2) cpc,"+
			"round((SUM(product_ads_report_data.clicks)/SUM(product_ads_report_data.`impressions`))*100,2) ctr  ").
		//Joins("join campaigns on campaigns.campaign_id = ad_group_report_data.campaign_id").
		//Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		//Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("product_ads_report_data.report_date between CURDATE() - INTERVAL ? DAY and CURDATE() and product_ads_report_data.campaign_id =? and product_ads_report_data.ad_group_id =?", noDays, adgroup.CampaignId, adgroup.AdGroupId).
		Group("product_ads_report_data.`campaign_id`,product_ads_report_data.ad_group_id,product_ads_report_data.ad_id").
		Find(&productAd).Error
	if err != nil {
		log.Error(err)
		return
	}
	//GetKeywordReport(adGroups)

	//adgroup.ProductAdReport = productAd
	return
}
func GetKeywordReport(adgroup *models.AdGroupReportData) (keyword []models.KeywordReportData, err error) {

	//for i:=0;i<len(campaigns);i++{
	//var keyword []models.KeywordReportData
	err = mysql.DB.Where("campaign_id = ? and ad_group_id = ?", adgroup.CampaignId, adgroup.AdGroupId).Find(&keyword).Error
	if err != nil {
		log.Error(err)
		return
	}
	//GetKeywordReport(adGroups)
	for i := 0; i < len(keyword); i++ {

		keyword[i].AttributedValues = GetAttributeTimeData(keyword[i])
	}
	//adgroup.KeywordReport = keyword
	return
}
func GetProductAdReport(adgroup *models.AdGroupReportData) (productAd []models.ProductAdsReportData, err error) {

	//for i:=0;i<len(campaigns);i++{

	err = mysql.DB.Where("campaign_id = ? and ad_group_id = ?", adgroup.CampaignId, adgroup.AdGroupId).Find(&productAd).Error
	if err != nil {
		log.Error(err)
		return
	}
	//GetKeywordReport(adGroups)
	for i := 0; i < len(productAd); i++ {

		productAd[i].AttributedValues = GetAttributeTimeData(productAd[i])
	}
	//adgroup.ProductAdReport = productAd
	return
}

func GetAttributeTimeData(data interface{}) (result []models.AttributedTimeData) {

	var err error
	//var result interface{}
	switch a := data.(type) {
	case models.CampaignReportData:
		row := data.(models.CampaignReportData)
		err = mysql.DB.Model(&models.AttributedTimeData{}).Where("report_id=? and report_type = 'campaign'", row.ID).Find(&result).Error
		//result = row
	case models.AdGroupReportData:
		row := data.(models.AdGroupReportData)
		err = mysql.DB.Model(&models.AttributedTimeData{}).Where("report_id=? and report_type = 'adgroup'", row.ID).Find(&result).Error

	case models.KeywordReportData:
		row := data.(models.KeywordReportData)
		err = mysql.DB.Model(&models.AttributedTimeData{}).Where("report_id=? and report_type = 'keyword'", row.ID).Find(&result).Error

	case models.ProductAdsReportData:
		row := data.(models.ProductAdsReportData)
		err = mysql.DB.Model(&models.AttributedTimeData{}).Where("report_id=? and report_type = 'productAd' ", row.ID).Find(&result).Error

	default:
		_ = a
	}

	if err != nil {
		//log.Error(err)
	}
	return result
}

func GetAdGroupReportBySeller(ppcCommon ppcmanagementcommons.PPCManagementCommon, formRequest reportmodels.GetAdGroupReportForm, pagination ppcmanagementcommons.Pagintion) (result []reportuimodels.AdGroupUIListData, err error) {

	err = mysql.DB.Table("ad_group_report_data").
		Select("ad_group_report_data.`campaign_id`,"+
			" campaigns.name campaign_name, "+
			" ad_group_report_data.ad_group_id, "+
			" adg.name ad_group_name ,"+
			" round(sum(`ad_group_report_data`.`cost`),2) cost, "+
			" round(SUM(ad_group_report_data.sales),2) sales,"+
			" round((sum(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.sales))*100 ,2) acos,"+
			" SUM(ad_group_report_data.`impressions`) impressions,"+
			" SUM(ad_group_report_data.clicks) clicks,"+
			" SUM(`ad_group_report_data`.orders) orders, "+
			" round((SUM(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.clicks)),2) cpc,"+
			" round((SUM(ad_group_report_data.clicks)/SUM(ad_group_report_data.`impressions`))*100,2) ctr,"+
			" adg.state state,adg.serving_status,"+
			" adg.default_bid").
		Joins("right join campaigns on campaigns.campaign_id = ad_group_report_data.campaign_id").
		Joins("right join ad_groups adg on adg.ad_group_id = ad_group_report_data.ad_group_id ").
		Where("campaigns.profile_id = ? AND ad_group_report_data.campaign_id = ? and ad_group_report_data.report_date between ? and ?", ppcCommon.ProfileID, formRequest.CampaignId, formRequest.FromDate, formRequest.ToDate).
		Group("ad_group_report_data.`campaign_id`,campaigns.name,ad_group_report_data.ad_group_id,adg.name,adg.state,adg.serving_status,adg.default_bid").
		Limit(pagination.Limit).Offset(pagination.Limit * pagination.Page).
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}
	return
}

func GetAdGroupReport(ppcCommon ppcmanagementcommons.PPCManagementCommon, formRequest reportmodels.GetAdGroupReportForm, pagination ppcmanagementcommons.Pagintion) (result []reportuimodels.AdGroupUIListData, err error) {
	rawQuery := "select " +
		" campaigns.name campaign_name,campaigns.campaign_id," +
		" cost,sales,acos,impressions,clicks,cpc,ctr," +
		" ad_groups.ad_group_id,ad_groups.name ad_group_name," +
		" ad_groups.state,ad_groups.serving_status,ad_groups.default_bid" +
		" from ad_groups " +
		"left join (" +
		" SELECT  ad_group_report_data.ad_group_id ," +
		" round(sum(`ad_group_report_data`.`cost`),2) cost," +
		" round(SUM(ad_group_report_data.sales),2) sales," +
		" round((sum(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.sales))*100 ,2) acos," +
		" SUM(ad_group_report_data.`impressions`) impressions," +
		" SUM(ad_group_report_data.clicks) clicks," +
		" SUM(`ad_group_report_data`.orders) orders," +
		" round((SUM(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.clicks)),2) cpc," +
		" round((SUM(ad_group_report_data.clicks)/SUM(ad_group_report_data.`impressions`))*100,2) ctr" +
		" FROM `ad_group_report_data`" +
		" join campaigns on campaigns.campaign_id = ad_group_report_data.campaign_id" +
		" join ad_groups adg on adg.ad_group_id = ad_group_report_data.ad_group_id" +
		" WHERE (campaigns.profile_id = ?" +
		" AND ad_group_report_data.campaign_id = ?" +
		" and ad_group_report_data.report_date between ? and ?)" +
		" GROUP BY ad_group_report_data.`campaign_id`,ad_group_report_data.ad_group_id) " +
		" temp on temp.ad_group_id=ad_groups.ad_group_id" +
		" join campaigns on campaigns.campaign_id = ad_groups.campaign_id" +
		" where ad_groups.campaign_id=?"

	err = mysql.DB.Raw(rawQuery, ppcCommon.ProfileID, formRequest.CampaignId, formRequest.FromDate, formRequest.ToDate, formRequest.CampaignId).
		Limit(pagination.Limit).Offset(pagination.Limit * pagination.Page).
		Scan(&result).Error
	return
}

func GetKeywordReportNew(ppcCommon ppcmanagementcommons.PPCManagementCommon, formRequest reportmodels.GetKeywordReportForm, pagination ppcmanagementcommons.Pagintion) (result []reportuimodels.KeywordReportData, err error) {
	rawQuery := "  select keywords.campaign_id," +
		" keywords.ad_group_id," +
		" keywords.keyword_id," +
		" keywords.keyword_text keyword_name," +
		" match_type," +
		" bid," +
		" creation_date," +
		" last_updated_date," +
		" serving_status," +
		" state," +
		" cost,acos,impressions,clicks,orders,sales,cpc,ctr" +
		" from keywords" +
		" left join (" +
		"	SELECT" +
		" round(sum(`keyword_report_data`.`cost`),2) cost," +
		" round((sum(`keyword_report_data`.`cost`)/SUM(keyword_report_data.sales))*100 ,2) acos," +
		" SUM(keyword_report_data.`impressions`) impressions," +
		" SUM(keyword_report_data.clicks) clicks," +
		" SUM(`keyword_report_data`.orders) orders," +
		" round(SUM(keyword_report_data.sales),2) sales," +
		" round((SUM(`keyword_report_data`.`cost`)/SUM(keyword_report_data.clicks)),2) cpc," +
		" round((SUM(keyword_report_data.clicks)/SUM(keyword_report_data.`impressions`))*100,2) ctr," +
		" keyword_report_data.campaign_id," +
		" keyword_report_data.ad_group_id," +
		" keyword_report_data.keyword_id" +
		" FROM `keyword_report_data`" +
		" join ad_groups adg on adg.ad_group_id = keyword_report_data.ad_group_id" +
		" join keywords on keywords.keyword_id = keyword_report_data.keyword_id" +
		" join campaigns on campaigns.campaign_id = keywords.campaign_id" +
		" WHERE (campaigns.profile_id = ?" +
		" AND keyword_report_data.campaign_id = ? " +
		" and keyword_report_data.ad_group_id = ? " +
		" and keyword_report_data.report_date between ? and ? )" +
		" GROUP BY keyword_report_data.campaign_id,keyword_report_data.ad_group_id,keyword_report_data.keyword_id) temp" +
		" on temp.keyword_id=keywords.keyword_id" +
		" where keywords.campaign_id=?" +
		" and keywords.ad_group_id=?"
	err = mysql.DB.Raw(rawQuery, ppcCommon.ProfileID, formRequest.CampaignId, formRequest.AdGroupId, formRequest.FromDate, formRequest.ToDate, formRequest.CampaignId, formRequest.AdGroupId).
		Limit(pagination.Limit).Order("last_updated_date desc").Offset(pagination.Limit * pagination.Page).Scan(&result).Error
	return
}

func GetKeywordReportBySeller(ppcCommon ppcmanagementcommons.PPCManagementCommon, formRequest reportmodels.GetKeywordReportForm, pagination ppcmanagementcommons.Pagintion) (result []reportuimodels.KeywordReportData, err error) {

	err = mysql.DB.Table("keyword_report_data").
		Select("keywords.bid,keywords.creation_date,"+
			"keywords.last_updated_date,keywords.serving_status,keywords.state,keywords.match_type,keywords.`campaign_id` campaign_id,campaigns.name campaign_name,"+
			" keywords.ad_group_id ad_group_id,adg.name ad_group_name,"+
			"keywords.keyword_id keyword_id,keywords.keyword_text keyword_name, "+
			"round(sum(`keyword_report_data`.`cost`),2) cost, "+
			" round((sum(`keyword_report_data`.`cost`)/SUM(keyword_report_data.sales) )*100,2) acos,"+
			"SUM(keyword_report_data.`impressions`) impressions,"+
			"SUM(keyword_report_data.clicks) clicks,SUM(`keyword_report_data`.orders) orders,round(SUM(keyword_report_data.sales),2) sales, "+
			"round((SUM(`keyword_report_data`.`cost`)/SUM(keyword_report_data.clicks)),2) cpc,"+
			"round((SUM(keyword_report_data.clicks)/SUM(keyword_report_data.`impressions`))*100,2) ctr  ").
		Joins("join ad_groups adg on adg.ad_group_id = keyword_report_data.ad_group_id ").
		Joins("join keywords on keywords.keyword_id = keyword_report_data.keyword_id").
		Joins("join campaigns on campaigns.campaign_id = keywords.campaign_id").
		Where("campaigns.profile_id = ? AND keyword_report_data.campaign_id =? and keyword_report_data.ad_group_id = ? and keyword_report_data.report_date between  ? and ?", ppcCommon.ProfileID, formRequest.CampaignId, formRequest.AdGroupId, formRequest.FromDate, formRequest.ToDate).
		Group("keywords.`campaign_id`,campaigns.name ,keywords.ad_group_id,adg.name,keywords.keyword_id,keywords.keyword_text,keywords.bid,keywords.creation_date," +
			"keywords.last_updated_date,keywords.serving_status,keywords.state,keywords.match_type").
		Limit(pagination.Limit).Offset(pagination.Limit * pagination.Page).
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}

	return
}
func GetProductAdReportBySeller(profileId int64, sellerId, marketPlace, geo string, form reportmodels.GetProductAdReportForm) (result []models.ProductAdsReportData, err error) {

	err = mysql.DB.Table("product_ads_report_data").
		Select("product_ads_report_data.`campaign_id` campaign_id,campaigns.name campaign_name,"+
			"product_ads_report_data.ad_group_id ad_group_id,adg.name ad_group_name,"+
			"product_ads_report_data.ad_id ad_id,asin,  "+
			"round(sum(`product_ads_report_data`.`cost`),2) cost, round(SUM(product_ads_report_data.sales),2) sales,"+
			" round((sum(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.sales) )*100,2) acos,"+
			"SUM(product_ads_report_data.`impressions`) impressions,"+
			"SUM(product_ads_report_data.clicks) clicks,SUM(`product_ads_report_data`.orders) orders, "+
			"round((SUM(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.clicks)),2) cpc,"+
			"round((SUM(product_ads_report_data.clicks)/SUM(product_ads_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = product_ads_report_data.campaign_id").
		Joins("join ad_groups adg on adg.ad_group_id = product_ads_report_data.ad_group_id ").
		Joins("join product_ads on product_ads.ad_id = product_ads_report_data.ad_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("product_ads_report_data.campaign_id =? and product_ads_report_data.ad_group_id = ? and product_ads_report_data.report_date between ? and ? ", form.CampaignId, form.AdGroupId, form.FromDate, form.ToDate).
		Group("product_ads_report_data.`campaign_id`,campaigns.name,product_ads_report_data.ad_group_id,adg.name," +
			"product_ads_report_data.ad_id,asin").
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}

	return
}
func GetFullKeyword(profileId int64, sellerId, marketPlace, geo string, formRequest reportmodels.GetFullKeywordReportForm) (result []models.KeywordReportData, err error) {

	err = mysql.DB.Table("keyword_report_data").
		Select("keywords.`campaign_id` campaign_id,campaigns.name campaign_name,"+
			" keywords.ad_group_id ad_group_id,adg.name ad_group_name,keywords.match_type match_type,"+
			"keywords.keyword_id keyword_id,keywords.keyword_text keyword_name, "+
			"round(sum(`keyword_report_data`.`cost`),2) cost, "+
			" round((sum(`keyword_report_data`.`cost`)/SUM(keyword_report_data.sales) )*100,2) acos,"+
			"SUM(keyword_report_data.`impressions`) impressions,"+
			"SUM(keyword_report_data.clicks) clicks,SUM(`keyword_report_data`.orders) orders,round(SUM(keyword_report_data.sales),2) sales, "+
			"round((SUM(`keyword_report_data`.`cost`)/SUM(keyword_report_data.clicks)),2) cpc,"+
			"round((SUM(keyword_report_data.clicks)/SUM(keyword_report_data.`impressions`))*100,2) ctr  ").
		Joins("join ad_groups adg on adg.ad_group_id = keyword_report_data.ad_group_id ").
		Joins("join keywords on keywords.keyword_id = keyword_report_data.keyword_id").
		Joins("join campaigns on campaigns.campaign_id = keywords.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("keyword_report_data.report_date between  ? and ?", formRequest.FromDate, formRequest.ToDate).
		Group("keywords.`campaign_id`,campaigns.name ,keywords.ad_group_id,adg.name,keywords.keyword_id,keywords.keyword_text,keywords.match_type").
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}

	return
}
func GetAdGroupReportByDate(campaign models.CampaignReportData, fromDate, endDate time.Time) (adGroups []models.AdGroupReportData, err error) {

	//for i:=0;i<len(campaigns);i++{
	//	var
	err = mysql.DB.Table("ad_group_report_data").
		Select("ad_group_report_data.`campaign_id` campaign_id,ad_group_report_data.ad_group_id ad_group_id, "+
			"round(sum(`ad_group_report_data`.`cost`),2) cost, round(SUM(ad_group_report_data.sales),2) sales,"+
			" round((sum(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.sales))*100 ,2) acos,"+
			"SUM(ad_group_report_data.`impressions`) impressions,"+
			"SUM(ad_group_report_data.clicks) clicks,SUM(`ad_group_report_data`.orders) orders, "+
			"round((SUM(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.clicks)),2) cpc,"+
			"round((SUM(ad_group_report_data.clicks)/SUM(ad_group_report_data.`impressions`))*100,2) ctr  ").
		//Joins("join campaigns on campaigns.campaign_id = ad_group_report_data.campaign_id").
		//Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		//Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("ad_group_report_data.report_date between ? and ? and ad_group_report_data.campaign_id =?", fromDate, endDate, campaign.CampaignId).
		Group("ad_group_report_data.`campaign_id`,ad_group_report_data.ad_group_id").
		Find(&adGroups).Error
	if err != nil {
		log.Error(err)
		return
	}
	//campaign.AdGroupReport = adGroups

	return
}

func GetKeywordReportByDate(adgroup *models.AdGroupReportData, fromDate, endDate time.Time) (keyword []models.KeywordReportData, err error) {

	//for i:=0;i<len(campaigns);i++{
	//var keyword []models.KeywordReportData
	err = mysql.DB.Table("keyword_report_data").
		Select("keyword_report_data.`campaign_id` campaign_id,keyword_report_data.ad_group_id ad_group_id,"+
			"keyword_report_data.keyword_id keyword_id, "+
			"round(sum(`keyword_report_data`.`cost`),2) cost, round(SUM(keyword_report_data.sales),2) sales,"+
			" round((sum(`keyword_report_data`.`cost`)/SUM(keyword_report_data.sales) )*100,2) acos,"+
			"SUM(keyword_report_data.`impressions`) impressions,"+
			"SUM(keyword_report_data.clicks) clicks,SUM(`keyword_report_data`.orders) orders, "+
			"round((SUM(`keyword_report_data`.`cost`)/SUM(keyword_report_data.clicks)),2) cpc,"+
			"round((SUM(keyword_report_data.clicks)/SUM(keyword_report_data.`impressions`))*100,2) ctr  ").
		//Joins("join campaigns on campaigns.campaign_id = ad_group_report_data.campaign_id").
		//Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		//Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("keyword_report_data.report_date between ? and ? and keyword_report_data.campaign_id =? and keyword_report_data.ad_group_id=?", fromDate, endDate, adgroup.CampaignId, adgroup.AdGroupId).
		Group("keyword_report_data.`campaign_id`,keyword_report_data.ad_group_id,keyword_report_data.keyword_id").
		Find(&keyword).Error
	if err != nil {
		log.Error(err)
		return
	}
	//GetKeywordReport(adGroups)
	/*for i := 0; i < len(keyword); i++ {

		keyword[i].AttributedValues = GetAttributeTimeData(keyword[i])
	}*/
	//adgroup.KeywordReport = keyword
	return
}
func GetProductAdReportByDate(adgroup *models.AdGroupReportData, fromDate, endDate time.Time) (productAd []models.ProductAdsReportData, err error) {

	//for i:=0;i<len(campaigns);i++{

	err = mysql.DB.Table("product_ads_report_data").
		Select("product_ads_report_data.`campaign_id` campaign_id,product_ads_report_data.ad_group_id ad_group_id,"+
			"product_ads_report_data.ad_id ad_id, "+
			"round(sum(`product_ads_report_data`.`cost`),2) cost, round(SUM(product_ads_report_data.sales),2) sales,"+
			" round((sum(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.sales) )*100,2) acos,"+
			"SUM(product_ads_report_data.`impressions`) impressions,"+
			"SUM(product_ads_report_data.clicks) clicks,SUM(`product_ads_report_data`.orders) orders, "+
			"round((SUM(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.clicks)),2) cpc,"+
			"round((SUM(product_ads_report_data.clicks)/SUM(product_ads_report_data.`impressions`))*100,2) ctr  ").
		//Joins("join campaigns on campaigns.campaign_id = ad_group_report_data.campaign_id").
		//Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		//Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("product_ads_report_data.report_date between ? and ? and product_ads_report_data.campaign_id =? and product_ads_report_data.ad_group_id =?", fromDate, endDate, adgroup.CampaignId, adgroup.AdGroupId).
		Group("product_ads_report_data.`campaign_id`,product_ads_report_data.ad_group_id,product_ads_report_data.ad_id").
		Find(&productAd).Error
	if err != nil {
		log.Error(err)
		return
	}
	//GetKeywordReport(adGroups)

	//adgroup.ProductAdReport = productAd
	return
}
func FetchAsinList(marketPlace, geo, sellerId, profileId string) (keyword []reportmodels.QueryList, err error) {

	err = mysql.DB.Table("keyword_report_data").
		Select("keyword_report_data.query ").
		Joins("join campaigns on campaigns.campaign_id = keyword_report_data.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("campaigns.targeting_type = 'auto'").
		Find(&keyword).Error
	if err != nil {
		log.Error(err)
		return
	}
	return
}

func FetchKeywordDetails(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.TrendingReportForm, condition string) (keyword []models.KeywordReportData, err error) {

	err = mysql.DB.Table("keyword_report_data").
		Select("SUM(keyword_report_data.`impressions`) impressions,"+
			" SUM(keyword_report_data.clicks) clicks,"+
			" round(sum(`keyword_report_data`.`cost`),2) cost,"+
			" round((sum(`keyword_report_data`.`cost`)/SUM(keyword_report_data.sales))*100 ,2) acos,"+
			" round((SUM(`keyword_report_data`.`cost`)/SUM(keyword_report_data.clicks)),2) cpc,"+
			" round((SUM(keyword_report_data.clicks)/SUM(keyword_report_data.`impressions`))*100,2) ctr,"+
			" round(SUM(keyword_report_data.sales),2) sales,"+
			" SUM(`keyword_report_data`.orders) orders,campaigns.name as campaign_name,ad_groups.name as ad_group_name,keywords.match_type,keywords.keyword_text as keyword_name").
		Joins("join keywords on keywords.keyword_id = keyword_report_data.keyword_id").
		Joins("join campaigns on campaigns.campaign_id = keyword_report_data.campaign_id").
		Joins("join ad_groups on ad_groups.ad_group_id = keyword_report_data.ad_group_id").
		Where("keywords.marked_negative = 0 AND campaigns.profile_id=? AND campaigns.state='enabled' AND keyword_report_data.report_date BETWEEN ? AND ?", ppcCommon.ProfileID, form.FromDate, form.ToDate).
		Where(condition).
		Group("keywords.keyword_id,campaigns.name,ad_groups.name,keywords.match_type,keywords.keyword_text").
		Find(&keyword).Error
	if err != nil {
		log.Error(err)
		return
	}
	return
}

func FetchKeywordDetailsNew(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.TrendingReportForm, filter string) (result []models.KeywordReportData, err error) {

	rawBaseQuery := "select temp.* from (" + "SELECT keywords.`campaign_id`, " +
		" keywords.ad_group_id," +
		" keywords.keyword_id," +
		" campaigns.name campaign_name," +
		" campaigns.campaign_type," +
		" keywords.match_type," +
		" adg.name ad_group_name," +
		" keywords.keyword_text keyword_name," +
		" SUM(keyword_report_data.`impressions`) impressions," +
		" SUM(keyword_report_data.clicks) clicks," +
		" round(sum(`keyword_report_data`.`cost`),2) cost," +
		" round((sum(`keyword_report_data`.`cost`)/SUM(keyword_report_data.sales))*100 ,2) acos," +
		" round((SUM(`keyword_report_data`.`cost`)/SUM(keyword_report_data.clicks)),2) cpc," +
		" round((SUM(keyword_report_data.clicks)/SUM(keyword_report_data.`impressions`))*100,2) ctr," +
		" round(SUM(keyword_report_data.sales),2) sales," +
		" SUM(`keyword_report_data`.orders) orders" +
		" FROM `keyword_report_data` " +
		" join ad_groups adg on adg.ad_group_id = keyword_report_data.ad_group_id " +
		" join keywords on keywords.keyword_id = keyword_report_data.keyword_id " +
		" join campaigns on campaigns.campaign_id = keywords.campaign_id " +
		" WHERE  keyword_report_data.deleted_at IS NULL " +
		" AND (( campaigns.profile_id=? AND campaigns.state='enabled' " +
		" and keyword_report_data.report_date between  ? and ?)) " +
		" GROUP BY keywords.`campaign_id`,campaigns.name ,keywords.ad_group_id,adg.name,keywords.keyword_id,campaigns.campaign_type,keywords.match_type,keywords.keyword_text" + ") temp"
	if filter != "" {
		rawBaseQuery = rawBaseQuery + " where  " + filter
	}

	err = mysql.DB.Raw(rawBaseQuery, ppcCommon.ProfileID, form.FromDate, form.ToDate).Scan(&result).Error
	return
}

func FetchAsinTrendingData(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.AsinTrendingReportForm) (result []reportmodels.TrendingInfo, err error) {

	err = mysql.DB.Table("product_ads_report_data").
		Select("product_ads_report_data.report_date time,"+
			" round(`product_ads_report_data`.`cost`,2) cost, "+
			" round(product_ads_report_data.sales,2) sales,"+
			" round((sum(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.sales))*100 ,2) acos,"+
			" product_ads_report_data.`impressions` impressions,"+
			" product_ads_report_data.clicks clicks,"+
			" `product_ads_report_data`.orders orders,"+
			" round((SUM(`product_ads_report_data`.`cost`)/SUM(product_ads_report_data.clicks)),2) cpc,"+
			" round((SUM(product_ads_report_data.clicks)/SUM(product_ads_report_data.`impressions`))*100,2) ctr  ").
		Joins("join campaigns on campaigns.campaign_id = product_ads_report_data.campaign_id").
		Joins("join product_ads on product_ads.ad_id = product_ads_report_data.ad_id").
		Where("campaigns.profile_id=? AND report_date between ? and ? and product_ads.asin = ?", ppcCommon.ProfileID, form.FromDate, form.ToDate, form.Asin).
		//Group("product_ads_report_data.report_date ").
		Find(&result).Error

	return
}

func FilteredKeywordsData(ppcCommon ppcmanagementcommons.PPCManagementCommon, formRequest reportmodels.KeywordFilterForm, filter string) (result []models.KeywordReportData, err error) {

	rawBaseQuery := "select temp.* from (" + "SELECT keywords.`campaign_id`, " +
		" keywords.ad_group_id," +
		" keywords.keyword_id," +
		" campaigns.name campaign_name," +
		" campaigns.campaign_type," +
		" keywords.match_type," +
		" adg.name ad_group_name," +
		" keywords.keyword_text keyword_name," +
		" SUM(keyword_report_data.`impressions`) impressions," +
		" SUM(keyword_report_data.clicks) clicks," +
		" round(sum(`keyword_report_data`.`cost`),2) cost," +
		" round((sum(`keyword_report_data`.`cost`)/SUM(keyword_report_data.sales))*100 ,2) acos," +
		" round((SUM(`keyword_report_data`.`cost`)/SUM(keyword_report_data.clicks)),2) cpc," +
		" round((SUM(keyword_report_data.clicks)/SUM(keyword_report_data.`impressions`))*100,2) ctr," +
		" round(SUM(keyword_report_data.sales),2) sales," +
		" SUM(`keyword_report_data`.orders) orders" +
		" FROM `keyword_report_data` " +
		" join ad_groups adg on adg.ad_group_id = keyword_report_data.ad_group_id " +
		" join keywords on keywords.keyword_id = keyword_report_data.keyword_id " +
		" join campaigns on campaigns.campaign_id = keywords.campaign_id " +
		" WHERE  keyword_report_data.deleted_at IS NULL " +
		" AND (( campaigns.profile_id=? " +
		" AND keyword_report_data.campaign_id = ?  " +
		" and keyword_report_data.ad_group_id = ?  " +
		" and keyword_report_data.report_date between  ? and ?)) " +
		" GROUP BY keywords.`campaign_id`,campaigns.name ,keywords.ad_group_id,adg.name,keywords.keyword_id,campaigns.campaign_type,keywords.match_type,keywords.keyword_text" + ") temp"
	if filter != "" {
		rawBaseQuery = rawBaseQuery + " where  " + filter
	}

	err = mysql.DB.Raw(rawBaseQuery, ppcCommon.ProfileID, formRequest.CampaignID, formRequest.AdGroupID, formRequest.FromDate, formRequest.ToDate).Scan(&result).Error
	return
}

func TotalKeywordsCount(ppcCommon ppcmanagementcommons.PPCManagementCommon, state string) (count int, err error) {
	err = mysql.DB.Table("keywords").
		Joins("Join campaigns on campaigns.campaign_id=keywords.campaign_id").
		Where("campaigns.profile_id=? AND campaigns.state=?", ppcCommon.ProfileID, state).Count(&count).Error
	return
}

func FetchKeywordDetailsCount(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *reportmodels.TrendingReportForm, condition string) (count int, err error) {

	err = mysql.DB.Table("keyword_report_data").
		Select("keyword_report_data.*,campaigns.name as campaign_name,ad_groups.name as ad_group_name,keywords.match_type,keywords.keyword_text as keyword_name").
		Joins("join keywords on keywords.keyword_id = keyword_report_data.keyword_id").
		Joins("join campaigns on campaigns.campaign_id = keyword_report_data.campaign_id").
		Joins("join ad_groups on ad_groups.ad_group_id = keyword_report_data.ad_group_id").
		Where("keywords.marked_negative = 0 AND campaigns.profile_id=? AND campaigns.state='enabled' AND keyword_report_data.report_date BETWEEN ? AND ?", ppcCommon.ProfileID, form.FromDate, form.ToDate).
		Where(condition).
		Count(&count).Error
	if err != nil {
		log.Error(err)
		return
	}
	return
}
