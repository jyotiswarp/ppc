package reportresouces

import (
	"bitbucket.org/jyotiswarp/ppc/report/reportcontroller"
	"bitbucket.org/jyotiswarp/ppc/report/reportmodels"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
)

func ReportApis(router *gin.Engine) {

	ppcCampaign := router.Group("/report", middlewares.PPCRequestMiddleware())
	{
		//ppcCampaign.POST("/requestReport", middlewares.Validator(models.RequestReport{}), reportcontroller.RequestReport)
		//ppcCampaign.POST("/requestLastNReports", middlewares.Validator(models.RequestNReport{}), reportcontroller.RequestLastNReport)


		ppcCampaign.POST("/keyword/summary",middlewares.Validator(reportmodels.TrendingReportForm{}),reportcontroller.GetKeywordSummary)
		ppcCampaign.POST("/reports_by_date", middlewares.Validator(reportmodels.GetReportByDateForm{}), reportcontroller.GetReportByDate)
		ppcCampaign.GET("/asin_list", reportcontroller.GetAsinList)
		ppcCampaign.POST("/report_by_asin", middlewares.Validator(reportmodels.GetReportByAsinForm{}), reportcontroller.GetReportByAsin)
		ppcCampaign.POST("/trending_data", middlewares.Validator(reportmodels.TrendingReportForm{}), reportcontroller.GetTrendingData)
		ppcCampaign.POST("/ppc_summary", middlewares.Validator(reportmodels.TrendingReportForm{}), reportcontroller.GetPPCSummary)
		ppcCampaign.POST("/asin_trending_data", middlewares.Validator(reportmodels.AsinTrendingReportForm{}), reportcontroller.GetAsinTrendingData)

		ppcCampaign.POST("/campaign_report", middlewares.Validator(reportmodels.GetCampaignReportForm{}),middlewares.PaginationValidator(), reportcontroller.GetCampaignReport)
		ppcCampaign.POST("/adGroup_report", middlewares.Validator(reportmodels.GetAdGroupReportForm{}),middlewares.PaginationValidator(), reportcontroller.GetAdGroupReport)
		ppcCampaign.POST("/keyword_report", middlewares.Validator(reportmodels.GetKeywordReportForm{}),middlewares.PaginationValidator(), reportcontroller.GetKeywordReport)

		ppcCampaign.POST("/product_adReport", middlewares.Validator(reportmodels.GetProductAdReportForm{}), reportcontroller.GetProductAdReport)
		ppcCampaign.POST("/active_campaign_asin", middlewares.Validator(reportmodels.AsinTrendingReportForm{}), reportcontroller.CheckActiveCampaignForAsin)
		ppcCampaign.GET("/asin_suggestions", reportcontroller.GetAsinSuggestion)
		ppcCampaign.POST("/all_keywords", middlewares.Validator(reportmodels.GetFullKeywordReportForm{}), reportcontroller.GetAllKeywordReport)
		ppcCampaign.POST("/keyword_suggestions", middlewares.Validator(reportmodels.TrendingReportForm{}), reportcontroller.GetKeywordSuggestions)
		ppcCampaign.POST("/negative_keywords", middlewares.Validator(reportmodels.TrendingReportForm{}), reportcontroller.GetNegativeExactFromReport)
		ppcCampaign.GET("/test", reportcontroller.Test)

		ppcCampaign.POST("/filter_keywords", middlewares.Validator(reportmodels.KeywordFilterForm{}), reportcontroller.GetFilteredKeywords)

		// UI constants
		ppcCampaign.GET("/filter_types")
		ppcCampaign.GET("/filter_conditions")

	}

	//insights := router.Group("/insights/keyword", middlewares.PPCRequestMiddleware())
	//{
	//	insights.POST("/top_contributing", middlewares.Validator(ppcmanagementform.TimeRangeForm{}),reportcontroller.TopContributingKeywords)
	//}
}
