package reportuimodels

import (
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
	"time"
)

type CampaignUIListData struct {
	CampaignId           int64     `json:"campaignId"`
	CampaignName         string    `json:"campaignName"`
	DailyBudget          float64   `json:"dailyBudget"`
	PremiumBidAdjustment bool      `json:"premiumBidAdjustment"`
	State                string    `json:"state"`
	TargetingType        string    `json:"targetingType"`
	ServingStatus        string    `json:"servingStatus"`
	Impressions          int       `json:"impressions"`
	Acos                 float64   `json:"acos"`
	Clicks               int       `json:"clicks"`
	Cost                 float64   `json:"cost"`
	Cpc                  float64   `json:"cpc"`
	Ctr                  float64   `json:"ctr"`
	Sales                float64   `json:"sales"`
	Orders               int       `json:"orders"`
	StartDate            time.Time `json:"startDate"`
	EndDate              time.Time `json:"endDate"`
}

type AdGroupUIListData struct {
	CampaignId    int64   `json:"campaignId"`
	AdGroupId     int64   `json:"adGroupId"`
	CampaignName  string  `json:"campaignName"`
	AdGroupName   string  `json:"adGroupName"`
	Impressions   int     `json:"impressions"`
	Clicks        int     `json:"clicks"`
	Cost          float64 `json:"cost"`
	Acos          float64 `json:"acos"`
	Cpc           float64 `json:"cpc"`
	Ctr           float64 `json:"ctr"`
	Sales         float64 `json:"sales"`
	Orders        int     `json:"orders"`
	State         string  `json:"state"`
	ServingStatus string  `json:"servingStatus"`
	DefaultBid    float64 `json:"defaultBid"`
}

type KeywordReportData struct {
	CampaignId      int64                   `json:"campaignId"`
	AdGroupId       int64                   `json:"adGroupId"`
	KeywordId       int64                   `json:"keywordId"`
	ReportDate      *time.Time              `json:"reportDate"`
	CampaignName    string                  `json:"campaignName"`
	CampaignType    string                  `json:"campaignType"`
	AdGroupName     string                  `json:"adGroupName"`
	KeywordName     string                  `json:"keywordName"`
	MatchType       string                  `json:"matchType"`
	Query           string                  `json:"query"`
	Impressions     int                     `json:"impressions"`
	Clicks          int                     `json:"clicks"`
	Cost            float64                 `json:"cost"`
	Acos            float64                 `json:"acos"`
	Cpc             float64                 `json:"cpc"`
	Ctr             float64                 `json:"ctr"`
	Sales           float64                 `json:"sales"`
	Orders          int                     `json:"orders"`
	Bid             float64                 `json:"bid"`
	CreationDate    time.Time               `json:"creationDate"`
	LastUpdatedDate time.Time               `json:"lastUpdatedDate"`
	ServingStatus   string                  `json:"servingStatus"`
	State           string                  `json:"state"`
	SuggestedBid    keywordmodel.KeywordBid `json:"suggestedBid"`
}

type KeywordsSummary struct {
	Total    int `json:"total"`
	Positive int `json:"positive"`
	Negative int `json:"negative"`
}
