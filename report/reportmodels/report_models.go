package reportmodels

import (
	"time"
)

type RequestReport struct {
	ReportDate   int64  `json:"reportDate,omitempty"`
	CampaignType string `json:"campaignType,omitempty"`
	Metrics      string `json:"metrics,omitempty"`
	Segment      string `json:"segment,omitempty"`
	//Requester string `json:"Requester"`
}

type RequestReportRes struct {
	Status string `json:"status"`
	JobId  uint   `json:"job_id"`
}
type ReportUpdate struct {
	UpdatedAt time.Time `json:"updated_at"`
}

type OnDemandFilter struct {
	Filters map[string]interface{} `json:"filters"`
}

type GetReportByDateForm struct {
	FromDate   *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate     *time.Time `json:"toDate,omitempty" binding:"required"`
	ReportType string     `json:"reportType,omitempty" binding:"required"`
	//Requester string `json:"Requester"`
}
type GetReportByAsinForm struct {
	FromDate *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate   *time.Time `json:"toDate,omitempty" binding:"required"`
	Asin     string     `json:"asin,omitempty" binding:"required"`
	//Requester string `json:"Requester"`
}

type TrendingReportForm struct {
	FromDate *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate   *time.Time `json:"toDate,omitempty" binding:"required"`
	//Requester string `json:"Requester"`
}
type AsinTrendingReportForm struct {
	Asin     string     `json:"asin" binding:"required"`
	FromDate *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate   *time.Time `json:"toDate,omitempty" binding:"required"`
	//Requester string `json:"Requester"`
}
type TrendingInfo struct {
	CampaignId  int64      `json:"campaignId,omitempty"`
	Time        *time.Time `json:"time,omitempty"`
	Cost        float64    `json:"cost,omitempty"`
	Sales       float64    `json:"sales,omitempty"`
	Acos        float64    `json:"acos,omitempty"`
	Impressions int        `json:"impressions,omitempty"`
	Cpc         float64    `json:"cpc,omitempty"`
	Orders      int        `json:"orders,omitempty"`
	Ctr         float64    `json:"ctr,omitempty"`
}

type GetCampaignReportForm struct {
	FromDate *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate   *time.Time `json:"toDate,omitempty" binding:"required"`
	//CampaignId string     `json:"campaign_id,omitempty" binding:"required"`
	//Requester string `json:"Requester"`
}

type GetAdGroupReportForm struct {
	FromDate   *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate     *time.Time `json:"toDate,omitempty" binding:"required"`
	CampaignId int64      `json:"campaignId,omitempty" binding:"required"`
	//AdGroupId  string     `json:"adGroupId,omitempty"`
	//Requester string `json:"Requester"`
}

type GetKeywordReportForm struct {
	FromDate   *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate     *time.Time `json:"toDate,omitempty" binding:"required"`
	CampaignId int64      `json:"campaignId,omitempty" binding:"required"`
	AdGroupId  int64      `json:"adGroupId,omitempty" binding:"required"`
	//KeywordId  string     `json:"keywordId,omitempty"`
	//Requester string `json:"Requester"`
}

type GetProductAdReportForm struct {
	FromDate   *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate     *time.Time `json:"toDate,omitempty" binding:"required"`
	CampaignId int64      `json:"campaignId,omitempty" binding:"required"`
	AdGroupId  int64      `json:"adGroupId,omitempty"`
	//AdId       string     `json:"adId,omitempty"`
	//Requester string `json:"Requester"`
}

type GetFullKeywordReportForm struct {
	FromDate *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate   *time.Time `json:"toDate,omitempty" binding:"required"`
}

type QueryList struct {
	Query string `json:"query,omitempty"`
}
type CampaignActiveInfo struct {
	date           *time.Time `json:"date,omitempty"`
	CampaignStatus bool       `json:"campaignStatus,omitempty"`
}

type KeywordFilterForm struct {
	FromDate       *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate         *time.Time `json:"toDate,omitempty" binding:"required"`
	CampaignID     string     `json:"campaignId" binding:"required"`
	AdGroupID      string     `json:"adGroupId" binding:"required"`
	MinImpressions string     `json:"minImpressions"`
	MaxImpressions string     `json:"maxImpressions"`
	MaxAcos        string     `json:"maxAcos"`
	MinAcos        string     `json:"minAcos"`
	MaxClicks      string     `json:"maxClicks"`
	MinClicks      string     `json:"minClicks"`
	MinCtr         string     `json:"minCtr"`
	MaxCtr         string     `json:"maxCtr"`
	MaxSpend       string     `json:"maxSpend"`
	MinSpend       string     `json:"minSpend"`
	MinSales       string     `json:"minSales"`
	MaxSales       string     `json:"maxSales"`
	MatchType      string     `json:"matchType"`
}
