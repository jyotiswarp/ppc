package spprofileservices

import (
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofileContants"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofiledao"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofilemodels"
	"github.com/prometheus/common/log"
	"github.com/spf13/viper"
	"strconv"
)

func UpdateUserPreference(sellerId string, form []spprofilemodels.UserPreferenceData) (err error) {

	for _, data := range form {
		data := spprofilemodels.PPCUserPreference{
			SellerId:  sellerId,
			AttrKey:   data.AttrKey,
			AttrValue: data.AttrValue,
		}
		err = spprofiledao.UpdateUserPrefDb(data)
		if err != nil {
			log.Error(err)
			//return
		}
	}
	return
}

func GetDefaultPreference() (result spprofilemodels.UserPreference) {

	impression, _ := strconv.Atoi(viper.GetString("impressions"))
	clicks, _ := strconv.Atoi(viper.GetString("clicks"))
	order, _ := strconv.Atoi(viper.GetString("order"))
	cost, _ := strconv.ParseFloat(viper.GetString("cost"), 64)
	acos, _ := strconv.ParseFloat(viper.GetString("acos"), 64)
	conversion, _ := strconv.Atoi(viper.GetString("conversion"))

	result = spprofilemodels.UserPreference{Impressions: impression, Clicks: clicks, Order: order, Cost: cost, Acos: acos, Conversion: conversion}
	return
}

func GetUserOverview(sellerId string) (err error, userOverride spprofilemodels.OverviewDetails) {

	err, userOverride = spprofiledao.GetUserOverviewDb(sellerId)

	return

}

func GetUserGoals(marketPlace, geo, sellerId, profileId string, form spprofilemodels.GoalsData) (err error, userOverride spprofilemodels.GoalDetails) {

	err, userOverride = spprofiledao.GetUserGoalsFromDb(marketPlace, geo, sellerId, profileId, form)

	return

}

func GetUserPreference(sellerid string) (result spprofilemodels.UserPreference) {

	defaultPref := GetDefaultPreference()

	_, userOverride := spprofiledao.GetUserPrefFromDb(sellerid) // KV pair

	userPref := GetUserPrefModel(userOverride)

	return PatchUserPreference(defaultPref, userPref)

}

func GetUserPrefModel(data []spprofilemodels.PPCUserPreference) (result spprofilemodels.UserPreference) {

	for _, val := range data {
		switch val.AttrKey {
		case "acos":
			acos, _ := strconv.ParseFloat(val.AttrValue, 64)
			result.Acos = acos
		case "impressions":
			impression, _ := strconv.Atoi(val.AttrValue)
			result.Impressions = impression
		case "clicks":
			clicks, _ := strconv.Atoi(val.AttrValue)
			result.Clicks = clicks
		case "order":
			order, _ := strconv.Atoi(val.AttrValue)
			result.Order = order
		case "cost":
			cost, _ := strconv.ParseFloat(val.AttrValue, 64)
			result.Cost = cost
		case "conversion":
			conversion, _ := strconv.Atoi(val.AttrValue)
			result.Conversion = conversion

		}
	}

	return
}
func PatchUserPreference(defaultPref, userOverride spprofilemodels.UserPreference) spprofilemodels.UserPreference {

	if userOverride.Conversion == 0 {
		userOverride.Conversion = defaultPref.Conversion
	}
	if userOverride.Acos == 0 {
		userOverride.Acos = defaultPref.Acos
	}
	if userOverride.Clicks == 0 {
		userOverride.Clicks = defaultPref.Clicks
	}
	if userOverride.Order == 0 {
		userOverride.Order = defaultPref.Order
	}
	if userOverride.Impressions == 0 {
		userOverride.Impressions = defaultPref.Impressions
	}
	if userOverride.Cost == 0 {
		userOverride.Cost = defaultPref.Cost
	}

	return userOverride
}

func GetReportCount(marketPlace, geo, sellerId, profileId string) (err error, count int) {

	err, count = spprofiledao.GetSuccessJobCount(marketPlace, geo, sellerId, profileId)

	return

}

func CheckOnBoardingStatus(marketPlace, geo, sellerId, scope string) (result spprofilemodels.OnBoardingDetails) {

	err, attr := spprofiledao.FetchUserAttr(marketPlace, geo, sellerId, spprofileContants.Amazon_ppc_connected_on)
	if err != nil || attr.AttrValue == "" {
		log.Error(err)
		result.IsConnected = false
		return
	}
	result.IsConnected = true

	//err, attr = spprofiledao.FetchUserAttr(marketPlace, geo, sellerId, spprofileContants.Acos_variation)
	//if err != nil || attr.AttrValue == "" {
	//	log.Error(err)
	//	result.IsUserPrefSet = false
	//	return
	//}
	result.IsUserPrefSet = true

	//err, attr = spprofiledao.FetchUserAttr(marketPlace, geo, sellerId, spprofileContants.On_boarding_done)
	//if err != nil || attr.AttrValue == "" {
	//	log.Error(err)
	//	result.IsOnBoarded = false
	//	return
	//}
	result.IsOnBoarded = true

	err, attr = spprofiledao.FetchUserAttr(marketPlace, geo, sellerId, spprofileContants.PPC_report_fetching_requested)
	if err != nil || attr.AttrValue == "" {
		log.Error(err)
		result.IsReportRequested = false
		return
	}
	result.IsReportRequested = true

	err, result.NumberOfReports = spprofiledao.GetSuccessJobCount(marketPlace, geo, sellerId, scope)
	if err != nil {
		result.NumberOfReports = 0
		log.Error(err)
		return
	}
	if result.NumberOfReports >= 295 {
		result.IsDataSynced = true
	}

	return

}

func GetUserAttrValue(marketPlace, geo, sellerId string, form spprofilemodels.UserAttr) (err error, result spprofilemodels.PPCUserPreference) {

	err, result = spprofiledao.FetchUserAttr(marketPlace, geo, sellerId, form.AttrKey)

	return

}
