package spprofiledao

import (
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofileContants"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofilemodels"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	log "github.com/Sirupsen/logrus"
)

func MigrateSpProfileTables() {
	mysql.DB.AutoMigrate(&spprofilemodels.PPCUserPreference{})
}

func UpdateUserPrefDb(data spprofilemodels.PPCUserPreference) (err error) {

	var row spprofilemodels.PPCUserPreference

	mysql.DB.Where("seller_id = ? and attr_key = ?", data.SellerId, data.AttrKey).Find(&row)
	if row.AttrKey != "" {

		errDb := mysql.DB.Model(&data).Where("seller_id = ? and attr_key = ?", data.SellerId, data.AttrKey).Update(&data).Error
		if errDb != nil {
			log.Error("Unable to update data , error ", errDb)
		}
	} else {
		errDb := mysql.DB.Create(&data).Error
		if errDb != nil {
			log.Error("Unable to add data , error ", errDb)
		}

	}
	return

}

func GetUserPrefFromDb(sellerId string) (err error, result []spprofilemodels.PPCUserPreference) {

	err = mysql.DB.Find(&result).Where("seller_id = ?", sellerId).Error
	return

}

func GetUserOverviewDb(sellerId string) (err error, result spprofilemodels.OverviewDetails) {

	err = mysql.DB.Table("ppc_user_preferences").Select("attr_value added_on").
		Where("seller_id=? and attr_key = ? ", sellerId, spprofileContants.Amazon_ppc_connected_on).Find(&result).Error
	if err != nil {
		log.Error(err)
	}
	err = mysql.DB.Table("ppc_user_preferences").Select("attr_value average_daily_budget").
		Where("seller_id=? and attr_key = ? ", sellerId, spprofileContants.Average_daily_budget).Find(&result).Error
	if err != nil {
		log.Error(err)
	}
	err = mysql.DB.Table("ppc_user_preferences").Select("attr_value desired_average_acos").
		Where("seller_id=? and attr_key = ? ", sellerId, spprofileContants.Desired_average_acos).Find(&result).Error
	if err != nil {
		log.Error(err)
	}

	return

}

func GetUserGoalsFromDb(marketPlace, geo, sellerId, profileId string, form spprofilemodels.GoalsData) (err error, result spprofilemodels.GoalDetails) {

	err = mysql.DB.Table("ppc_user_preferences").Select("attr_value total_budget").
		Joins("join profiles prof on  prof.seller_string_id=ppc_user_preferences.seller_id").
		Where("attr_key = ? and market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?",
			spprofileContants.Total_budget, marketPlace, geo, sellerId, profileId).
		Find(&result).Error
	if err != nil {
		log.Error(err)
	}
	err = mysql.DB.Table("ppc_user_preferences").Select("attr_value acos_variation").
		Joins("join profiles prof on  prof.seller_string_id=ppc_user_preferences.seller_id").
		Where("attr_key = ? and market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?",
			spprofileContants.Desired_average_acos, marketPlace, geo, sellerId, profileId).Find(&result).Error
	if err != nil {
		log.Error(err)
	}
	err = mysql.DB.Table("campaign_report_data").
		Select(" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) current_acos,"+
			"round(SUM(`campaign_report_data`.`cost`),2) current_budget ").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", marketPlace, geo, sellerId, profileId).
		Where("report_date between ? and ?", form.FromDate, form.ToDate).
		//Group("campaign_report_data.`campaign_id`,campaigns.name ").
		Find(&result).Error
	return

}

func GetSuccessJobCount(marketPlace, geo, sellerId, profileId string) (err error, count int) {

	err = mysql.DB.Table("ppc_jobs_news").
		Where("profile_id = ? and seller_id=? and deleted_at is null and status= 'COMPLETED'",  profileId,sellerId).Count(&count).Error
	if err != nil {
		log.Error(err)
	}
	return

}
func FetchUserAttr(marketPlace, geo, sellerId, key string) (err error, result spprofilemodels.PPCUserPreference) {

	//err = mysql.DB.Where("seller_id = ? and attr_key = ? ", sellerId,form.AttrKey).Find(&result).Error

	err = mysql.DB.Model(spprofilemodels.PPCUserPreference{}).Select("ppc_user_preferences.*").
		//Joins("join profiles prof on  prof.seller_string_id=ppc_user_preferences.seller_id").
		Where(" seller_id = ? ", //market_place_name = ?  and geo = ? and
			/*marketPlace, geo,*/ sellerId).
		Where("attr_key = ?", key).
		Find(&result).Error
	if err != nil {
		log.Error(err)
	}
	return

}
