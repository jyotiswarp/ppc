package spprofilemodels

import (
	"github.com/jinzhu/gorm"
	"time"
)

type UserPreference struct {
	SellerId    string  `json:"sellerId,omitempty" binding:"required"`
	Impressions int     `json:"impressions,omitempty"`
	Clicks      int     `json:"clicks,omitempty"`
	Order       int     `json:"order,omitempty"`
	Cost        float64 `json:"cost,omitempty"`
	Acos        float64 `json:"acos,omitempty"`
	Conversion  int     `json:"conversion,omitempty"`
}

type UserPreferenceData struct {
	AttrKey   string `json:"attr_key,omitempty" binding:"required"`
	AttrValue string `json:"attr_value,omitempty" binding:"required"`
}

type UserAttr struct {
	AttrKey   string `json:"attr_key"`
	AttrValue string `json:"attr_value"`
}
type PPCUserPreference struct {
	gorm.Model
	SellerId  string `json:"sellerId,omitempty" gorm:"index:idx_seller_id_attr_key"`
	AttrKey   string `json:"attr_key" gorm:"index:idx_seller_id_attr_key"`
	AttrValue string `json:"attr_value"`
}

type OverviewDetails struct {
	AddedOn            string  `json:"added_on"`
	AverageDailyBudget float64 `json:"average_daily_budget"`
	DesiredAverageAcos float64 `json:"desired_average_acos"`
}
type GoalDetails struct {
	TotalBudget   float64 `json:"total_budget"`
	CurrentBudget float64 `json:"current_budget"`
	AcosVariation float64 `json:"acos_variation"`
	CurrentAcos   float64 `json:"current_acos"`
}
type GoalsData struct {
	FromDate *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate   *time.Time `json:"toDate,omitempty" binding:"required"`
}

type OnBoardingDetails struct {
	IsConnected       bool `json:"is_connected"`
	IsReportRequested bool `json:"is_report_requested"`
	IsUserPrefSet     bool `json:"is_user_pref_set"`
	IsOnBoarded       bool `json:"is_on_boarded"`
	NumberOfReports   int  `json:"number_of_reports"`
	IsDataSynced      bool `json:"is_data_synced"`
}
