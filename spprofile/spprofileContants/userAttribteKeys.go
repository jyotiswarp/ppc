package spprofileContants

const Amazon_ppc_connected_on = "amazon_ppc_connected_on"
const Average_daily_budget = "average_daily_budget"
const Desired_average_acos = "desired_average_acos"
const Total_budget = "total_budget"
const On_boarding_done = "on_boarding_done"
const PPC_report_fetching_requested = "ppc_report_fetching_requested"
const Daily_max_order = "Daily_max_order"
const Total_max_order = "Total_max_order"
const Daily_sales = "Daily_sales"
const Total_sales = "Total_sales"
const Acos_variation = "Acos_variation"
