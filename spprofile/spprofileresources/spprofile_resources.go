package spprofileresources

import (
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofilecontroller"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofilemodels"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
)

func SpProfileApi(router *gin.Engine) {

	ppcCampaign := router.Group("/spProfile")
	{
		ppcCampaign.POST("/updateUserPref", middlewares.Validator([]spprofilemodels.UserPreferenceData{}), spprofilecontroller.UpdateUserPreference)
		ppcCampaign.GET("/getOverview", spprofilecontroller.GetUserOverview)
		ppcCampaign.POST("/getGoals", middlewares.Validator(spprofilemodels.GoalsData{}), spprofilecontroller.GetUserGoals)
		ppcCampaign.GET("/checkReportCount", spprofilecontroller.GetReportCount)
		ppcCampaign.POST("/getAttrValue", middlewares.Validator(spprofilemodels.UserAttr{}), spprofilecontroller.GetUserAttrValue)

		ppcCampaign.GET("/OnBoardingStatus", spprofilecontroller.CheckOnBoarding)
	}
}
