package spprofilecontroller

import (
	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofileservices"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofilemodels"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofileservices"
	"github.com/gin-gonic/gin"
)

func UpdateUserPreference(c *gin.Context) {

	sellerId := c.GetHeader("sellerId")
	if sellerId == "" {
		c.JSON(400, gin.H{"error": "sellerId is missing"})
		return
	}
	formRequest := c.Keys["form_data"].(*[]spprofilemodels.UserPreferenceData)

	err := spprofileservices.UpdateUserPreference(sellerId, *formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Invalid request",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": "success",
	})
	return
}

func GetUserOverview(c *gin.Context) {

	//formRequest := c.Keys["form_data"].(*spprofilemodels.UserPreferenceData)
	sellerId := c.GetHeader("sellerId")
	if sellerId == "" {
		c.JSON(400, gin.H{"error": "sellerId is missing"})
		return
	}
	err, result := spprofileservices.GetUserOverview(sellerId)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Invalid request",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetUserGoals(c *gin.Context) {

	//formRequest := c.Keys["form_data"].(*spprofilemodels.UserPreferenceData)
	sellerId := c.GetHeader("sellerId")
	geo := c.GetHeader("geo")
	marketPlace := c.GetHeader("marketplace")
	err, scope := marketprofileservices.GetProfileId(marketPlace, geo, sellerId)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "invalid profile",
		})
		return
	}
	email, _ := c.GetQuery("email")
	if sellerId == "" || email == "" {
		c.JSON(400, gin.H{"error": "sellerId or email is missing"})
		return
	}

	if marketPlace == "" || geo == "" || scope == "" {
		c.JSON(400, gin.H{"error": "market_place or geo  is missing"})
		return
	}
	formRequest := c.Keys["form_data"].(*spprofilemodels.GoalsData)
	err, result := spprofileservices.GetUserGoals(marketPlace, geo, sellerId, scope, *formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Invalid request",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetReportCount(c *gin.Context) {

	//formRequest := c.Keys["form_data"].(*spprofilemodels.UserPreferenceData)
	sellerId := c.GetHeader("sellerId")
	geo := c.GetHeader("geo")
	marketPlace := c.GetHeader("marketplace")
	err, scope := marketprofileservices.GetProfileId(marketPlace, geo, sellerId)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "invalid profile",
		})
		return
	}
	email, _ := c.GetQuery("email")
	if sellerId == "" || email == "" {
		c.JSON(400, gin.H{"error": "sellerId or email is missing"})
		return
	}

	if marketPlace == "" || geo == "" || scope == "" {
		c.JSON(400, gin.H{"error": "market_place or geo  is missing"})
		return
	}
	err, result := spprofileservices.GetReportCount(marketPlace, geo, sellerId, scope)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "invalid request",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func CheckOnBoarding(c *gin.Context) {

	//formRequest := c.Keys["form_data"].(*spprofilemodels.UserPreferenceData)
	sellerId := c.GetHeader("sellerId")
	geo := c.GetHeader("geo")
	marketPlace := c.GetHeader("marketplace")
	err, scope := marketprofileservices.GetProfileId(marketPlace, geo, sellerId)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "invalid profile",
		})
		return
	}
	email, _ := c.GetQuery("email")
	if sellerId == "" || email == "" {
		c.JSON(400, gin.H{"error": "sellerId or email is missing"})
		return
	}

	if marketPlace == "" || geo == "" {
		c.JSON(400, gin.H{"error": "market_place or geo  is missing"})
		return
	}
	result := spprofileservices.CheckOnBoardingStatus(marketPlace, geo, sellerId, scope)
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}
func GetUserAttrValue(c *gin.Context) {

	//formRequest := c.Keys["form_data"].(*spprofilemodels.UserPreferenceData)
	sellerId := c.GetHeader("sellerId")
	geo := c.GetHeader("geo")
	marketPlace := c.GetHeader("marketplace")
	/*err, scope := marketprofileservices.GetProfileId(marketPlace, geo, sellerId)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "invalid profile",
		})
		return
	}*/
	email, _ := c.GetQuery("email")
	if sellerId == "" || email == "" {
		c.JSON(400, gin.H{"error": "sellerId or email is missing"})
		return
	}

	if marketPlace == "" || geo == "" {
		c.JSON(400, gin.H{"error": "market_place or geo  is missing"})
		return
	}
	formRequest := c.Keys["form_data"].(*spprofilemodels.UserAttr)
	err, result := spprofileservices.GetUserAttrValue(marketPlace, geo, sellerId, *formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Invalid attr_key",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}
