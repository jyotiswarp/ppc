package main

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupresources"
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationresources"
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignresources"
	"bitbucket.org/jyotiswarp/ppc/dao"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordresources"
	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofileresources"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetchermodels"
	"bitbucket.org/jyotiswarp/ppc/productad/productadresources"
	"bitbucket.org/jyotiswarp/ppc/report/reportresouces"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesdao"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesresources"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofiledao"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofileresources"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/fsnotify/fsnotify"
	"github.com/getsentry/raven-go"
	"github.com/gin-gonic/contrib/gzip"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"os"
	_ "time"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetcherservice"
)

var router *gin.Engine

func init() {
	log.Info("initializing ppc...")
	gin.SetMode(gin.ReleaseMode)
	router = gin.Default()
	router.Use(gzip.Gzip(gzip.DefaultCompression))

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.DebugLevel)

	// Config
	viper.SetConfigName("config")
	viper.AddConfigPath("$GOPATH/src/bitbucket.org/jyotiswarp/ppc/config")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("config file not found :", err)
	}

	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed")
	})
	raven.SetDSN("https://e64489104cbd43958c171f9c12c1fada@sentry.io/1234986")
	viper.WatchConfig()
	mysql.Init(false)
	dao.MigratePPCTables()
	rulesdao.MigrateRulesTables()
	ppcdatafetchermodels.MigrateJobTables()
	spprofiledao.MigrateSpProfileTables()
	authenticationresources.MarketAuthApis(router)
	marketprofileresources.MarketProfileApis(router)
	reportresouces.ReportApis(router)
	rulesresources.RulesApi(router)
	campaignresources.CampaignResources(router)
	adgroupresources.AdGroupResources(router)
	keywordresources.KeywordResources(router)
	productadresources.ProductAdResources(router)
	spprofileresources.SpProfileApi(router)
	backends.Initialize()
	//reportservices.Initialize()
	//rulesservices.Initialize()
	//rulesservices.RestartPendingRuleEvaluation()
	ppcdatafetcherservice.Initialize()
}

func main() {

	log.Info("Starting Ad Management Server..")
	router.Run(":9090")

}
