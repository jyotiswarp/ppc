package authenticationresources

import (
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationcontroller"
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationmodels"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
)

func MarketAuthApis(router *gin.Engine) {

	ppcCampaign := router.Group("/auth")
	{
		//ppcCampaign.GET("/connectAmazonPPCApis", authenticationcontroller.ConnectAmazonPPCApis)
		ppcCampaign.POST("/save_token", middlewares.Validator(authenticationmodels.TokenRequestForm{}), authenticationcontroller.SaveAPIToken)
		ppcCampaign.POST("/onBoardUserToPPC", middlewares.Validator(authenticationmodels.OnBoardUserFrom{}), authenticationcontroller.OnBoardUserToPPC)
		ppcCampaign.GET("/checkAuthAvailability", authenticationcontroller.CheckAuthAvailability)
		ppcCampaign.GET("/resycn", middlewares.PPCRequestMiddleware(), authenticationcontroller.ReSyncPPCData)

	}
}
