package authenticationmodels

import "github.com/jinzhu/gorm"

type PPCAPITokenDetails struct {
	gorm.Model
	Email        string `json:"email" gorm:"index:email_valid_access"`
	ValidAccess   bool `json:"valid_access" gorm:"index:email_valid_access"`
	AccessToken  string `json:"access_token" gorm:"type:TEXT"`
	RefreshToken string `json:"refresh_token" gorm:"type:TEXT"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"`
}

type TokenRequestForm struct {
	Code string `json:"code"`
}
type OnBoardUserFrom struct {
	AccessToken  string `json:"access_token" binding:"required"`
	RefreshToken string `json:"refresh_token" binding:"required"`
	TokenType    string `json:"token_type" binding:"required"`
	ExpiresIn    int    `json:"expires_in" binding:"required"`
}