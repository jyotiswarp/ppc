package authenticationcontroller

import (
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationdao"
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationmodels"
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationservices"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
	"strconv"
)

func SaveAPIToken(c *gin.Context) {

	//code := c.Query("code")
	sellerId := c.GetHeader("sellerId") /*
		geo := c.GetHeader("geo")
		marketPlace := c.GetHeader("market_place")*/
	email, _ := c.GetQuery("email")
	if sellerId == "" || email == "" {
		c.JSON(400, gin.H{"error": "email is missing"})
		return
	}
	//url := c.Request.URL.String()
	ppcManagerForm := c.Keys["form_data"].(*authenticationmodels.TokenRequestForm)
	err := authenticationservices.GetTheAccessToken(ppcManagerForm.Code /*geo,marketPlace,*/, email, sellerId) //}

	if err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}
	c.JSON(200, gin.H{"result": "Saved, Thanks!! - PPCv2 coming soon"})
	return
}

func OnBoardUserToPPC(c *gin.Context) {

	//code := c.Query("code")
	sellerId := c.GetHeader("sellerId")
	geo := c.GetHeader("geo")
	marketPlace := c.GetHeader("marketplace")
	email, _ := c.GetQuery("email")
	if sellerId == "" || email == "" {
		c.JSON(400, gin.H{"error": "email is missing"})
		return
	}
	if marketPlace == "" || geo == "" {
		c.JSON(400, gin.H{"error": "market_place or geo  is missing"})
		return
	}
	//url := c.Request.URL.String()
	ppcManagerForm := c.Keys["form_data"].(*authenticationmodels.OnBoardUserFrom)
	err := authenticationservices.OnBoardUserToPPCPlatform(*ppcManagerForm, geo, marketPlace, email, sellerId) //}

	if err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}
	c.JSON(200, gin.H{"result": "Saved, Thanks!!!"})
	return
}

func CheckAuthAvailability(c *gin.Context) {
	sellerId := c.GetHeader("sellerId")
	/*	geo := c.GetHeader("geo")
		marketPlace := c.GetHeader("market_place")*/
	email, _ := c.GetQuery("email")
	if sellerId == "" || email == "" {
		c.JSON(400, gin.H{"error": "email is missing"})
		return
	}
	//url := c.Request.URL.String()
	err, result := authenticationdao.GetAuthDetails(email) //}

	if err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}
	c.JSON(200, gin.H{"result": result})
	return
}

func ReSyncPPCData(c *gin.Context) {
	countStr, _ := c.GetQuery("count")
	count, _ := strconv.Atoi(countStr)
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	err := authenticationservices.ReSyncPPCData(ppcManagement, count)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": "done",
	})
	return
}
