package authenticationdao

import (
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationmodels"
	"bitbucket.org/jyotiswarp/ppc/dao"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
)

func GetAuthDetails(email string) (err error, result authenticationmodels.PPCAPITokenDetails) {

	err = mysql.DB.Model(&authenticationmodels.PPCAPITokenDetails{}).Where("email=? and deleted_at is null and valid_access = 1", email).Find(&result).Error
	return
}

func SavePPCToken(details authenticationmodels.PPCAPITokenDetails, sellerId string) (err error) {

	details.ValidAccess = true
	row := dao.CheckDataPresence(details).(authenticationmodels.PPCAPITokenDetails)
	if row.ID == 0 {
		//record not present
		err := mysql.DB.Create(&details).Error
		if err != nil {
			return err
		}
	} else {
		details.ID = row.ID
		details.CreatedAt = row.CreatedAt
		//record present
		err := mysql.DB.Save(&details).Error
		if err != nil {
			return err
		}
	}

	return
}
func SetAccessRevoked(email string) (err error) {
	var token authenticationmodels.PPCAPITokenDetails
	err = mysql.DB.Model(&token).Where("email = ?", email).Update("valid_access",0).Error
	return
}
