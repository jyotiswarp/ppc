package authenticationservices

import (
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationdao"
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationmodels"
	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofileservices"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetcherservice"
	"bitbucket.org/jyotiswarp/ppc/report/reportservices"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofiledao"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofilemodels"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

func GetAuthToken(code string) string {
	data := url.Values{}
	data.Set("grant_type", "authorization_code")
	data.Set("code", code)
	data.Set("redirect_uri", viper.GetString("redirect_uri"))
	data.Set("client_id", viper.GetString("client_id"))
	data.Set("client_secret", viper.GetString("client_secret"))

	client := &http.Client{}
	req, err := http.NewRequest("POST", "https://api.amazon.com/auth/o2/token", bytes.NewBufferString(data.Encode()))
	if err != nil {
		log.Error(err)
		return ""
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	res, err := client.Do(req)
	fmt.Println("res.StatusCode : ", res.StatusCode)
	if err != nil || res == nil || res.StatusCode == 200 {
		log.Debug("[Calling SendEmailFromDilipScript client]:", err)
		return ""
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	return string(body)
}

func GetTheAccessToken(code, email, sellerId string) (err error) {
	tokenPayload := GetAuthToken(code)
	fmt.Println("tokenpayload : ", string(tokenPayload))
	if tokenPayload == "" {
		return errors.New("Something went wrong")
	}

	var token authenticationmodels.PPCAPITokenDetails
	marshalErr := json.Unmarshal([]byte(tokenPayload), &token)
	if marshalErr != nil {
		log.Error(marshalErr)
		return
	}
	token.Email = email

	err = authenticationdao.SavePPCToken(token, sellerId)

	if err != nil {
		log.Error(err)
		return
	}
	/*result,err := marketprofileservices.GetProfileList(marketPlace, geo, sellerId, email)
	if err != nil || result.ProfileId == 0 {
		log.Error(err)
		return
	}*/

	now := time.Now()
	data := spprofilemodels.PPCUserPreference{SellerId: sellerId, AttrKey: "amazon_ppc_connected_on", AttrValue: now.Format(time.RFC3339)}
	err = spprofiledao.UpdateUserPrefDb(data)
	if err != nil {
		return err
	}

	return
}

func OnBoardUserToPPCPlatform(form authenticationmodels.OnBoardUserFrom, geo, marketPlace, email, sellerId string) (err error) {

	var token authenticationmodels.PPCAPITokenDetails
	token.ValidAccess = true
	token.AccessToken = form.AccessToken
	token.RefreshToken = form.RefreshToken
	token.ExpiresIn = form.ExpiresIn
	token.TokenType = form.TokenType
	token.Email = email
	err = authenticationdao.SavePPCToken(token, sellerId)

	if err != nil {
		log.Error(err)
		return
	}
	go GetUserProfileAndReport(geo, marketPlace, email, sellerId)

	now := time.Now()
	data := spprofilemodels.PPCUserPreference{SellerId: sellerId, AttrKey: "amazon_ppc_connected_on", AttrValue: now.Format(time.RFC3339)}
	err = spprofiledao.UpdateUserPrefDb(data)
	if err != nil {
		return err
	}

	return
}

func GetUserProfileAndReport(geo, marketPlace, email, sellerId string) (err error) {

	profile, err := marketprofileservices.GetProfileList(marketPlace, geo, sellerId, email)
	if err != nil {
		log.Error(err)
		data := spprofilemodels.PPCUserPreference{SellerId: sellerId, AttrKey: "ppc_report_fetching_failed", AttrValue: err.Error()}
		spprofiledao.UpdateUserPrefDb(data)
		return
	}
	log.Info("Profile data fetched - ", profile)

	profileId := strconv.Itoa(int(profile.ProfileId))
	ppcCommon := ppcmanagementcommons.PPCManagementCommon{
		Marketplace:  marketPlace,
		SellerID:     sellerId,
		Geo:          geo,
		ProfileID:    profile.ProfileId,
		Profile:      profile,
		ProfileIDStr: profileId,
	}
	reportservices.UpdateBasicDetails(profile, false)
	err = ppcdatafetcherservice.CreatePPCJobs(ppcCommon.Profile, true, 59)
	if err != nil {
		log.Error(err)
		data := spprofilemodels.PPCUserPreference{SellerId: sellerId, AttrKey: "ppc_report_fetching_failed", AttrValue: err.Error()}
		spprofiledao.UpdateUserPrefDb(data)
		return
	}
	now := time.Now()
	data := spprofilemodels.PPCUserPreference{SellerId: sellerId, AttrKey: "ppc_report_fetching_requested", AttrValue: now.Format(time.RFC3339)}
	err = spprofiledao.UpdateUserPrefDb(data)
	if err != nil {
		return err
	}

	return
}

func ReSyncPPCData(ppcCommon ppcmanagementcommons.PPCManagementCommon, numberOfDays int) (err error) {
	ppcdatafetcherservice.CreateBasicJobs(ppcCommon.Profile)
	err = ppcdatafetcherservice.CreatePPCJobs(ppcCommon.Profile, true, numberOfDays)
	if err != nil {
		log.Error(err)
		data := spprofilemodels.PPCUserPreference{SellerId: ppcCommon.SellerID, AttrKey: "ppc_report_fetching_failed", AttrValue: err.Error()}
		spprofiledao.UpdateUserPrefDb(data)
	}
	return
}
