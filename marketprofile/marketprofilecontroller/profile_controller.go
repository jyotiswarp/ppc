package marketprofilecontroller

import (
	"github.com/gin-gonic/gin"

	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofileservices"
)

func GetProfileList(c *gin.Context) {

	sellerId, _ := c.GetQuery("sellerId")
	geo := c.GetHeader("geo")
	marketPlace := c.GetHeader("market_place")
	email, _ := c.GetQuery("email")
	if sellerId == "" || email == "" {
		c.JSON(400, gin.H{"error": "sellerId or email is missing"})
		return
	}

	if marketPlace == "" || geo == "" {
		c.JSON(400, gin.H{"error": "market_place or geo Header is missing"})
		return
	}

	result, err := marketprofileservices.GetProfileList(marketPlace, geo, sellerId, email)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func GetSellerProfile(c *gin.Context) {

	sellerId, _ := c.GetQuery("sellerId")
	geo := c.GetHeader("geo")
	marketPlace := c.GetHeader("market_place")
	email, _ := c.GetQuery("email")
	if sellerId == "" || email == "" {
		c.JSON(400, gin.H{"error": "sellerId or email is missing"})
		return
	}

	if marketPlace == "" || geo == "" {
		c.JSON(400, gin.H{"error": "market_place or geo Header is missing"})
		return
	}

	result, err := marketprofileservices.GetSellerProfile(marketPlace, geo, sellerId, email)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}
