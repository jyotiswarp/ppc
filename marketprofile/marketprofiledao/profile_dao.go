package marketprofiledao

import (
	"bitbucket.org/jyotiswarp/ppc/dao"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"strings"
	"time"
)

func GetCampaignDetails(campaignId int64) (err error, result models.Profile) {

	err = mysql.DB.Where("campaign_id = ?", campaignId).Find(&result).Error
	return
}

func PushProfileToDb(result []models.Profile, marketPlace, geo, sellerId string) (err error) {

	fmt.Println("push data to profile")
	for _, data := range result {
		//if data.SellerStringId == sellerId {
		lastAnalysed := data.LastAnalyzedAt
		row := dao.CheckDataPresence(data).(models.Profile)
		data.MarketPlaceName = strings.ToLower(marketPlace)
		//data.Geo = strings.ToLower(geo)
		if row.ID == 0 {
			fmt.Println("create new entry")
			//record not present
			data.Geo = strings.ToLower(data.CountryCode)
			data.LastAnalyzedAt = time.Now()
			err := mysql.DB.Create(&data).Error
			if err != nil {
				return err
			}
		} else {
			//record present
			fmt.Println("update new entry")
			data.ID = row.ID
			data.CreatedAt = row.CreatedAt
			if lastAnalysed.IsZero() {
				data.LastAnalyzedAt = row.LastAnalyzedAt
			} else {
				data.LastAnalyzedAt = lastAnalysed
			}
			err := mysql.DB.Save(&data).Error
			if err != nil {
				return err
			}
		}

		//}

	}

	return nil
}

func GetProfileInformation(marketPlace, geo, sellerId string) (result models.Profile, err error) {

	//var row []models.Profile
	err = mysql.DB.
		Where("market_place_name = ?  and geo = ? and seller_string_id = ? ", marketPlace, geo, sellerId).
		Find(&result).Error

	if err != nil {
		log.Error(err)
		return result, err
	}

	return result, nil

}

func GetLastAnalyzedProfileDetailsByCondition(condition string) (profiles []models.Profile, err error) {
	err = mysql.DB.Where("is_access_revoked = 0 AND last_analyzed_at is not null and last_analyzed_at < now() - Interval " + condition).Find(&profiles).Error
	return profiles, err
}

func GetProfileDetails(sellerId, profileId string) (profiles models.Profile, err error) {
	err = mysql.DB.Where("profile_id = ? and seller_string_id = ? ", profileId, sellerId).Find(&profiles).Error
	return
}
func GetAllProfileDetails() (profiles []models.Profile, err error) {
	err = mysql.DB.Where("deleted_at is null ").Find(&profiles).Error
	return
}
