package marketprofileresources

import (
	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofilecontroller"
	"github.com/gin-gonic/gin"
)

func MarketProfileApis(router *gin.Engine) {

	ppcCampaign := router.Group("/profiles")
	{
		ppcCampaign.GET("/listProfiles", marketprofilecontroller.GetProfileList) //to get latest from amazon and this will update db also
		ppcCampaign.GET("/getProfile", marketprofilecontroller.GetSellerProfile) // get saved from our db

	}
}
