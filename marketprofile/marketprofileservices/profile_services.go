package marketprofileservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofiledao"
	"bitbucket.org/jyotiswarp/ppc/models"
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"strconv"
)

func UpdateProfileData(sellerId, email, geo, marketPlace string) (err error) {

	result, err := GetProfileFromBackend(sellerId, email, geo, marketPlace)
	if err != nil {
		log.Error(err)
		return err
	}
	err = marketprofiledao.PushProfileToDb(result, marketPlace, geo, sellerId)
	return err
}

func GetProfileFromBackend(sellerId, email, geo, marketPlace string) (result []models.Profile, err error) {
	fmt.Println("Getting Profiles :", sellerId, " --- ", email, "---- ", geo, "-----", marketPlace)
	market := backends.GetMarketPlace(marketPlace, geo)
	if market != nil {
		market.SetSellerId(sellerId)
		market.SetEmail(email)
		result, err = market.GetListOfProfiles()
		return
	}
	return result, errors.New("invalid market")
}
func GetSellerProfile(marketPlace, geo, sellerId, email string) (result models.Profile, err error) {

	result, err = marketprofiledao.GetProfileInformation(marketPlace, geo, sellerId)
	if err != nil {
		return
	}
	return
}

func GetProfileList(marketPlace, geo, sellerId, email string) (result models.Profile, err error) {
	//status, _ := dao.CheckLastUpdatedDate(sellerId, marketPlace, geo, "profile", 0)
	/*if time.Now().Sub(status).Hours() < 24 {
		result, err = marketprofiledao.GetProfileInformation(sellerId) // from DB
	} else {
	*/err = UpdateProfileData(sellerId, email, geo, marketPlace)
	if err != nil {
		log.Error(err)
		return
	}
	result, err = marketprofiledao.GetProfileInformation(marketPlace, geo, sellerId)

	//}
	if err != nil {
		return
	}

	return

}
func GetProfileId(marketPlace, geo, sellerId string) (err error, profileId string) {
	result, err := marketprofiledao.GetProfileInformation(marketPlace, geo, sellerId)

	//}
	if err != nil {
		return
	}

	profileId = strconv.Itoa(int(result.ProfileId))
	return
}
