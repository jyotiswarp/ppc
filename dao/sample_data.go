package dao

import (
	"bitbucket.org/jyotiswarp/ppc/models"
	"time"
)

func GetReportData() (result *models.CampaignReportData) {

	attribute1 := models.AttributedTimeData{AttributedConversions: 10, AttributedConversionsSameSKU: 3, AttributedSales: 234, AttributedSalesSameSKU: 23, NumberOfDays: 30}
	attribute2 := models.AttributedTimeData{AttributedConversions: 5, AttributedConversionsSameSKU: 2, AttributedSales: 544, AttributedSalesSameSKU: 13, NumberOfDays: 7}

	attribute3 := models.AttributedTimeData{AttributedConversions: 10, AttributedConversionsSameSKU: 3, AttributedSales: 234, AttributedSalesSameSKU: 23, NumberOfDays: 30}
	attribute4 := models.AttributedTimeData{AttributedConversions: 5, AttributedConversionsSameSKU: 2, AttributedSales: 544, AttributedSalesSameSKU: 13, NumberOfDays: 7}

	keyword1 := models.KeywordReportData{
		CampaignId:       250349224674867,
		AdGroupId:        2345,
		KeywordId:        11,
		Clicks:           20,
		Impressions:      1000,
		Ctr:              .2,
		Cost:             20000,
		Cpc:              .4,
		AttributedValues: []models.AttributedTimeData{attribute3, attribute4},
	}

	keyword2 := models.KeywordReportData{
		CampaignId:       250349224674867,
		AdGroupId:        2346,
		KeywordId:        12,
		Clicks:           20,
		Impressions:      1000,
		Ctr:              .2,
		Cost:             20000,
		Cpc:              .4,
		AttributedValues: []models.AttributedTimeData{attribute3, attribute4},
	}
	adgroup1 := models.AdGroupReportData{
		CampaignId:       250349224674867,
		AdGroupId:        2345,
		Clicks:           20,
		Impressions:      1000,
		Ctr:              .2,
		Cost:             20000,
		Cpc:              .4,
		AttributedValues: []models.AttributedTimeData{attribute3, attribute4},
		KeywordReport:    []models.KeywordReportData{keyword1}}

	adgroup2 := models.AdGroupReportData{
		CampaignId:       250349224674867,
		AdGroupId:        2346,
		Clicks:           20,
		Impressions:      1000,
		Ctr:              .2,
		Cost:             20000,
		Cpc:              .4,
		AttributedValues: []models.AttributedTimeData{attribute3, attribute4},
		KeywordReport:    []models.KeywordReportData{keyword2}}

	result = &models.CampaignReportData{CampaignId: 250349224674867,
		Clicks:           400,
		Impressions:      2000,
		Ctr:              .2,
		Cost:             20000,
		Cpc:              .4,
		AdGroupReport:    []models.AdGroupReportData{adgroup1, adgroup2},
		AttributedValues: []models.AttributedTimeData{attribute1, attribute2}}

	return result
}

func GetData() (result *models.Campaign) {

	/*attribute1 := models.AttributedTimeData{Acos: .2, AttributedConversions: 10, AttributedConversionsSameSKU: 3, AttributedSales: 234, AttributedSalesSameSKU: 23, Duration: 1}
	attribute2 := models.AttributedTimeData{Acos: .3, AttributedConversions: 5, AttributedConversionsSameSKU: 2, AttributedSales: 544, AttributedSalesSameSKU: 13, Duration: 7}

	attribute3 := models.AttributedTimeData{Acos: .4, AttributedConversions: 10, AttributedConversionsSameSKU: 3, AttributedSales: 234, AttributedSalesSameSKU: 23, Duration: 1}
	attribute4 := models.AttributedTimeData{Acos: .3, AttributedConversions: 5, AttributedConversionsSameSKU: 2, AttributedSales: 544, AttributedSalesSameSKU: 13, Duration: 7}

	keyword1 := models.KeywordReportData{
		CampaignId:       123455,
		AdGroupId:        2345,
		KeywordId:11,
		Clicks:           20,
		Impressions:      1000,
		Ctr:              .2,
		Cost:             20000,
		Cpc:              .4,
		AttributedValues: []models.AttributedTimeData{attribute3, attribute4},
	}

	keyword2 := models.KeywordReportData{
		CampaignId:       123455,
		AdGroupId:        2346,
		KeywordId:12,
		Clicks:           20,
		Impressions:      1000,
		Ctr:              .2,
		Cost:             20000,
		Cpc:              .4,
		AttributedValues: []models.AttributedTimeData{attribute3, attribute4},
	}
	adgroup1 := models.AdGroupReportData{
		CampaignId:       123455,
		AdGroupId:        2345,
		Clicks:           20,
		Impressions:      1000,
		Ctr:              .2,
		Cost:             20000,
		Cpc:              .4,
		AttributedValues: []models.AttributedTimeData{attribute3, attribute4},
		KeywordReport:[]models.KeywordReportData{keyword1}}

	adgroup2 := models.AdGroupReportData{
		CampaignId:       123455,
		AdGroupId:        2346,
		Clicks:           20,
		Impressions:      1000,
		Ctr:              .2,
		Cost:             20000,
		Cpc:              .4,
		AttributedValues: []models.AttributedTimeData{attribute3, attribute4},
		KeywordReport:[]models.KeywordReportData{keyword2}}
	*/

	header := models.Header{Marketplace: "amazon", Geo: "sandbox"}
	crete := time.Now().AddDate(0, 0, -20)
	endDate := time.Now()
	result = &models.Campaign{CampaignId: 250349224674867,
		State:           "active",
		ServingStatus:   "active",
		CreationDate:    &crete,
		LastUpdatedDate: &endDate,
		//SellerId:"ass112",
		Name:                 "Campaign 1",
		CampaignType:         "spProduct",
		ProfileId:            1750737210698083,
		EndDate:              &endDate,
		StartDate:            &crete,
		TargetingType:        "manual",
		PremiumBidAdjustment: true,
		DailyBudget:          10,
		Header:               header,
	}

	return result
}
