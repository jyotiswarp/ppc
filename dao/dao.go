package dao

import (
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationmodels"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/report/reportmodels"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	log "github.com/Sirupsen/logrus"
	"time"
)

func MigratePPCTables() {
	mysql.DB.AutoMigrate(&models.Profile{}, &models.Campaign{},&models.CampaignHistory{}, &models.AdGroup{},&models.AdGroupHistory{}, &models.Keyword{},&models.KeywordHistory{}, &models.NegativeKeyword{},
		&models.NegativeKeywordHistory{}, &models.CampaignReportData{}, &models.AdGroupReportData{}, &models.KeywordReportData{},
		&models.ProductAdsReportData{}, &models.AttributedTimeData{},
		&models.ProductAd{},&models.ProductAdHistory{}, &authenticationmodels.PPCAPITokenDetails{},&models.AsinReportData{},
	)
}
func CheckDataPresence(data interface{}) interface{} {

	var err error
	var result interface{}
	switch a := data.(type) {
	case models.Profile:
		row := data.(models.Profile)
		err = mysql.DB.Model(&models.Profile{}).Where("profile_id=? and deleted_at is null", row.ProfileId).Find(&row).Error
		result = row
	case models.Campaign:
		row := data.(models.Campaign)
		err = mysql.DB.Model(&models.Campaign{}).Where("campaign_id=? and deleted_at is null", row.CampaignId).Find(&row).Error
		result = row

	case models.CampaignHistory:
		row := data.(models.CampaignHistory)
		err = mysql.DB.Model(&models.CampaignHistory{}).Where("campaign_id=? and deleted_at is null and last_updated_date = ?", row.CampaignId,row.LastUpdatedDate).Find(&row).Error
		result = row

	case models.AdGroup:
		row := data.(models.AdGroup)
		err = mysql.DB.Model(&models.AdGroup{}).Where("campaign_id=? and ad_group_id = ? and deleted_at is null", row.CampaignId, row.AdGroupId).Find(&row).Error
		result = row

	case models.AdGroupHistory:
		row := data.(models.AdGroupHistory)
		err = mysql.DB.Model(&models.AdGroupHistory{}).Where("campaign_id=? and ad_group_id = ? and deleted_at is null and last_updated_date = ?", row.CampaignId, row.AdGroupId,row.LastUpdatedDate).Find(&row).Error
		result = row


	case models.Keyword:
		row := data.(models.Keyword)
		err = mysql.DB.Model(&models.Keyword{}).Where("campaign_id=? and ad_group_id = ? and keyword_id = ? and deleted_at is null", row.CampaignId, row.AdGroupId, row.KeywordId).Find(&row).Error
		result = row

	case models.KeywordHistory:
		row := data.(models.KeywordHistory)
		err = mysql.DB.Model(&models.KeywordHistory{}).Where("campaign_id=? and ad_group_id = ? and keyword_id = ? and deleted_at is null and last_updated_date = ?", row.CampaignId, row.AdGroupId, row.KeywordId,row.LastUpdatedDate).Find(&row).Error
		result = row

	case models.ProductAd:
		row := data.(models.ProductAd)
		err = mysql.DB.Model(&models.ProductAd{}).Where("campaign_id=? and ad_group_id = ? and ad_id = ? and deleted_at is null", row.CampaignId, row.AdGroupId, row.AdId).Find(&row).Error
		result = row

	case models.ProductAdHistory:
		row := data.(models.ProductAdHistory)
		err = mysql.DB.Model(&models.ProductAdHistory{}).Where("campaign_id=? and ad_group_id = ? and ad_id = ? and deleted_at is null and last_updated_date = ?", row.CampaignId, row.AdGroupId, row.AdId,row.LastUpdatedDate).Find(&row).Error
		result = row


	case models.NegativeKeyword:
		row := data.(models.NegativeKeyword)
		err = mysql.DB.Model(&models.NegativeKeyword{}).Where("campaign_id=? and ad_group_id = ? and keyword_id = ? and deleted_at is null", row.CampaignId, row.AdGroupId, row.KeywordId).Find(&row).Error
		result = row

	case models.NegativeKeywordHistory:
		row := data.(models.NegativeKeywordHistory)
		err = mysql.DB.Model(&models.NegativeKeywordHistory{}).Where("campaign_id=? and ad_group_id = ? and keyword_id = ? and deleted_at is null and last_updated_date = ?", row.CampaignId, row.AdGroupId, row.KeywordId,row.LastUpdatedDate).Find(&row).Error
		result = row

	case authenticationmodels.PPCAPITokenDetails:
		row := data.(authenticationmodels.PPCAPITokenDetails)
		err = mysql.DB.Model(&authenticationmodels.PPCAPITokenDetails{}).Where("email = ? and deleted_at is null", row.Email).Find(&row).Error
		result = row
	default:
		_ = a
	}

	if err != nil {
		log.Error(err)
	}
	return result
}

func CheckLastUpdatedDate(sellerId, marketPlace, geo, typeName string, scope int64) (date time.Time, err error) {

	switch typeName {
	case "profile":

		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("profiles profile").Select("Max(profile.updated_at) as updated_at").
			Where("profile.seller_string_id = ? and profile.market_place_name = ? and profile.geo = ?", sellerId, marketPlace, geo).
			Find(&row).Error
		date = row.UpdatedAt

	case "campaign":
		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("campaigns campaign").Select("Max(campaign.updated_at) as updated_at").
			Joins("join profiles as profile on profile.profile_id = campaign.profile_id ").
			Where("profile.profile_id = ? and profile.seller_string_id = ?", scope, sellerId).
			Find(&row).Error
		date = row.UpdatedAt

	case "adgroup":

		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("campaigns campaign").Select("Max(adGro.updated_at) as updated_at").
			Joins("join profiles as profile on profile.profile_id = campaign.profile_id ").
			Joins("join ad_groups as adGro on adGro.campaign_id = campaign.campaign_id").
			Where("profile.profile_id = ? and profile.seller_string_id = ?", scope, sellerId).
			Find(&row).Error
		date = row.UpdatedAt

	case "keyword":

		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("campaigns campaign").Select("Max(keywo.updated_at) as updated_at").
			Joins("join profiles as profile on profile.profile_id = campaign.profile_id ").
			Joins("join keywords as keywo on keywo.campaign_id = campaign.campaign_id").
			Where("profile.profile_id = ? and profile.seller_string_id = ?", scope, sellerId).
			Find(&row).Error
		date = row.UpdatedAt

	case "negativeKeyword":
		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("campaigns campaign").Select("Max(keywo.updated_at) as updated_at").
			Joins("join profiles as profile on profile.profile_id = campaign.profile_id ").
			Joins("join negative_keywords as keywo on keywo.campaign_id = campaign.campaign_id").
			Where("profile.profile_id = ? and profile.seller_string_id = ?", scope, sellerId).
			Find(&row).Error
		date = row.UpdatedAt

	case "biddableKeyword":
		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("campaigns campaign").Select("Max(keywo.updated_at) as updated_at").
			Joins("join profiles as profile on profile.profile_id = campaign.profile_id ").
			Joins("join biddable_keywords as keywo on keywo.campaign_id = campaign.campaign_id").
			Where("profile.profile_id = ? and profile.seller_string_id = ?", scope, sellerId).
			Find(&row).Error
		date = row.UpdatedAt

	case "productAd":

		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("campaigns campaign").Select("Max(proAd.updated_at) as updated_at").
			Joins("join profiles as profile on profile.profile_id = campaign.profile_id ").
			Joins("join product_ads as proAd on proAd.campaign_id = campaign.campaign_id").
			Where("profile.profile_id = ? and profile.seller_string_id = ?", scope, sellerId).
			Find(&row).Error
		date = row.UpdatedAt

	case "campaignsReport":
		//profileId, _ := strconv.ParseInt(scope, 10, 64)
		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("campaigns campaign").Select("Max(campReport.updated_at) updated_at").
			Joins("join profiles profile on profile.profile_id = campaign.profile_id ").
			Joins("join campaign_report_data campReport on campReport.campaign_id = campaign.campaign_id").
			Where("profile.profile_id = ? and profile.seller_string_id = ?", scope, sellerId).
			Find(&row).Error
		date = row.UpdatedAt

	case "adGroupsReport":
		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("campaigns campaign").Select("Max(adgroupReport.updated_at) updated_at").
			Joins("join profiles profile on profile.profile_id = campaign.profile_id ").
			Joins("join campaign_report_data campReport on campReport.campaign_id = campaign.campaign_id").
			Joins("join ad_group_report_data adgroupReport on adgroupReport.campaign_id = campaign.campaign_id").
			Where("profile.profile_id = ? and profile.seller_string_id = ?", scope, sellerId).
			Find(&row).Error
		date = row.UpdatedAt

	case "keywordsReport":

		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("campaigns campaign").Select("Max(keywordReport.updated_at) updated_at").
			Joins("join profiles profile on profile.profile_id = campaign.profile_id ").
			Joins("join campaign_report_data campReport on campReport.campaign_id = campaign.campaign_id").
			Joins("join keyword_report_data keywordReport on keywordReport.campaign_id = campaign.campaign_id").
			Where("profile.profile_id = ? and profile.seller_string_id = ?", scope, sellerId).
			Find(&row).Error
		date = row.UpdatedAt

	case "productAdsReport":
		var row reportmodels.ReportUpdate
		err = mysql.DB.Table("campaigns campaign").Select("Max(productAdReport.updated_at) updated_at").
			Joins("join profiles profile on profile.profile_id = campaign.profile_id ").
			Joins("join campaign_report_data campReport on campReport.campaign_id = campaign.campaign_id").
			Joins("join product_ads_report_data productAdReport on productAdReport.campaign_id = campaign.campaign_id").
			Where("profile.profile_id = ? and profile.seller_string_id = ?", scope, sellerId).
			Find(&row).Error
		date = row.UpdatedAt

	default:

	}

	if err != nil {
		//log.Error(err)
	}

	return date, err
}
