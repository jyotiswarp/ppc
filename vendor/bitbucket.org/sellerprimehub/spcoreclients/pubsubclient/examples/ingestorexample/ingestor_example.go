package ingestorexample

import (
	"bitbucket.org/sellerprimehub/spcoreclients/pubsubclient"
	"github.com/spf13/viper"
	"fmt"
	"time"
	"strconv"
)

var ingestor pubsubclient.PubSubIngestor

func Init() {
	initIngestor()
}

func initIngestor() {
	ingestor = pubsubclient.PubSubIngestor{}
	ingestor.TopicName = viper.GetString("trends_ingestor_topic")
	fmt.Println(ingestor.TopicName)
	ingestor.IsLogEnabled = true
	fmt.Println("Listener initiated")
	ingestor.Init()
}

func StartIngestion() {
	count := 1
	for {
		time.Sleep(250 * time.Millisecond)
		ingestor.Publish("hello " + strconv.Itoa(count))
		count++
	}
}