package pubsubclient

import (
	"cloud.google.com/go/pubsub"
	log "github.com/sirupsen/logrus"
	"encoding/json"
	"golang.org/x/net/context"
)

type PubSubIngestor struct {
	TopicName    string           //Pubsub Subscription Name
	messageChan  chan interface{} //Message Channel
	topic        *pubsub.Topic
	IsLogEnabled bool
}

func (ingestor *PubSubIngestor) logf(format string, a ...interface{}) {
	if ingestor.IsLogEnabled {
		log.Printf(format, a...)
	}
}

func (ingestor *PubSubIngestor) Init() {
	ingestor.messageChan = make(chan interface{}, 1000)
	client := GetPubSubClient()
	ingestor.topic = client.Topic(ingestor.TopicName)
	go ingestor.pushToPubSub()
}

func (ingestor *PubSubIngestor) Publish(data interface{}) bool {
	ingestor.messageChan <- data
	return true
}

func (ingestor *PubSubIngestor) pushToPubSub() {
	ctx := context.Background()
	for {
		msg, ok := <-ingestor.messageChan
		if ok == false {
			break
		}

		b, _ := json.Marshal(msg)
		ingestor.topic.Publish(ctx, &pubsub.Message{
			Data: b,
		})
	}
}
