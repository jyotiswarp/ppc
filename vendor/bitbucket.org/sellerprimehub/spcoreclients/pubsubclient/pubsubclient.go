package pubsubclient

import (
	"fmt"
	"os"
	"cloud.google.com/go/pubsub"
	"golang.org/x/net/context"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"sync"
)

var gclient *pubsub.Client
var once sync.Once

func GetPubSubClient() *pubsub.Client {

	once.Do(func(){
		createClient()
	})
	return gclient
}

func createClient() {
	ctx := context.Background()
	proj := viper.GetString("google_product_id")
	if proj == "" {
		fmt.Fprintf(os.Stderr, "GOOGLE_CLOUD_PROJECT environment variable must be set.\n")
		os.Exit(1)
	}
	c, err := pubsub.NewClient(ctx, proj)
	if err != nil {
		log.Fatalf("Could not create pubsub Client: %v", err)
	}
	gclient = c
}