package productaddao

import (
	"bitbucket.org/jyotiswarp/ppc/dao"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	log "github.com/Sirupsen/logrus"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
)

func GetProductAdDetails(adId int64) (err error, result models.ProductAd) {

	err = mysql.DB.Where("ad_id = ?", adId).Find(&result).Error
	return
}
func PushProductAdDataToDb(result []models.ProductAd, profileId int64) (err error) {

	for _, data := range result {
		row := dao.CheckDataPresence(data).(models.ProductAd)
		if row.ID == 0 {
			//record not present
			err := mysql.DB.Create(&data).Error
			if err != nil {
				return err
			}
		} else {
			//record present
			data.ID = row.ID
			data.CreatedAt = row.CreatedAt
			err := mysql.DB.Save(&data).Error
			if err != nil {
				return err
			}
		}
		history := models.ProductAdHistory{LastUpdatedDate:data.LastUpdatedDate,CreationDate:data.CreationDate,ServingStatus:data.ServingStatus,
		CampaignId:data.CampaignId,State:data.State,AdGroupId:data.AdGroupId, Asin:data.Asin,AdId:data.AdId,Sku:data.Sku,}
		rowHistory := dao.CheckDataPresence(history).(models.ProductAdHistory)
		if rowHistory.ID == 0 {
			err := mysql.DB.Create(&history).Error
			if err != nil {
				log.Error(err)
			}
		}
	}

	return nil
}

func GetProductAdInformation(ppcCommon ppcmanagementcommons.PPCManagementCommon, campaignId int64) (result []models.ProductAd, err error) {

	err = mysql.DB.Table("campaigns").
		Select("distinct proAd.*").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Joins("join ad_groups adg on adg.campaign_id = campaigns.campaign_id").
		Joins("join product_ads as proAd on proAd.campaign_id = campaigns.campaign_id and proAd.ad_group_id = adg.ad_group_id").
		Where("prof.seller_string_id = ? and prof.profile_id=?", ppcCommon.SellerID, ppcCommon.Profile.ProfileId).
		Find(&result).Error

	if err != nil {
		log.Error(err)
		return result, err
	}
	return result, nil

}
