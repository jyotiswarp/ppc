package productadmodels

type ProductAdForm struct {
	StartIndex       string  `json:"startIndex,omitempty"`
	Count            string  `json:"count,omitempty"`
	CampaignType     string  `json:"campaignType,omitempty"`
	AdGroupId        string  `json:"adGroupId,omitempty"`
	Sku              string  `json:"sku,omitempty"`
	Asin             string  `json:"asin,omitempty"`
	StateFilter      string  `json:"stateFilter,omitempty"`
	CampaignIdFilter []int64 `json:"campaignIdFilter,omitempty"`
	AdGroupIdFilter  []int64 `json:"ad_group_id_filter,omitempty"`
}
