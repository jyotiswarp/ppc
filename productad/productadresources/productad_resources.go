package productadresources

import (
	"bitbucket.org/jyotiswarp/ppc/productad/productadscontroller"
	"github.com/gin-gonic/gin"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
)

func ProductAdResources(router *gin.Engine) {
	managementProductAds := router.Group("/management/product_ads", middlewares.PPCRequestMiddleware())
	{
		managementProductAds.GET("/list", productadscontroller.GetProductAdList)

	}
}
