package productadservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/productad/productaddao"
	"bitbucket.org/jyotiswarp/ppc/productad/productadmodels"
	"errors"
	log "github.com/Sirupsen/logrus"
)

func UpdateProductAdDataModel(ppcCommon ppcmanagementcommons.PPCManagementCommon, form productadmodels.ProductAdForm) (err error) {

	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err := market.GetProductAdListExtended(form)
		if err != nil {
			log.Error(err)
			return err
		}
		err = productaddao.PushProductAdDataToDb(result, ppcCommon.Profile.ProfileId)
		return err
	}

	return errors.New("invalid market")
}

func GetProductAdList(ppcCommon ppcmanagementcommons.PPCManagementCommon, forceUpdate bool) (result []models.ProductAd, err error) {
	//status, err := dao.CheckLastUpdatedDate(sellerId, marketPlace, geo, "productAd", profileId)

	/*if !status.IsZero() && time.Now().Sub(status).Hours() < 4 {
		result, err = productaddao.GetProductAdInformation(sellerId, profileId, 0) // from DB
	} else {
	*/

	if forceUpdate {
		form := productadmodels.ProductAdForm{}
		err = UpdateProductAdDataModel(ppcCommon, form)
		if err != nil {
			log.Error(err)
			return
		}
	}

	result, err = productaddao.GetProductAdInformation(ppcCommon, 0)

	//}
	if err != nil {
		return result, err
	}

	return result, err

}
