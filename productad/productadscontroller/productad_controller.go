package productadscontroller

import (
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/productad/productadservices"
	"github.com/gin-gonic/gin"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
)

func GetProductAdList(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	result, err := productadservices.GetProductAdList(ppcManagement,false)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}
