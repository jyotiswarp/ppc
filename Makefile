.PHONY: build doc fmt lint run test clean  vet

#GOPATH=$(PWD)/vendor1:$(PWD)
#export GOPATH

TAGS = -tags 'newrelic_enabled'

default: build





build: fmt clean 
	go build -v -o ./ad_manager


doc:
	godoc -http=:6060 -index

# http://golang.org/cmd/go/#hdr-Run_gofmt_on_package_sources
fmt:
	go fmt ./src/...

# https://github.com/golang/lint
# go get github.com/golang/lint/golint
lint:
	golint ./src


clean:
	rm -rf `find ./vendor/src -type d -name .git` \
	&& rm -rf `find ./vendor/src -type d -name .hg` \
	&& rm -rf `find ./vendor/src -type d -name .bzr` \
	&& rm -rf `find ./vendor/src -type d -name .svn`
	rm -rf ./bin/*
	rm -rf ./pkg/*
	rm -rf ./vendor/bin/*
	rm -rf ./vendor/pkg/*



# http://godoc.org/code.google.com/p/go.tools/cmd/vet
# go get code.google.com/p/go.tools/cmd/vet
vet:
#	go vet ./src/...
