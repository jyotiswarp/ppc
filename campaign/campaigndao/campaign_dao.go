package campaigndao

import (
	//"bitbucket.org/jyotiswarp/ppc/backends/amazon/amazonmodel"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignmodels/campaignuimodels"
	"bitbucket.org/jyotiswarp/ppc/dao"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementform"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	"fmt"
	log "github.com/Sirupsen/logrus"
)

func PushCampaignDataToDb(result []models.Campaign, profileId int64) (err error) {

	for _, data := range result {
		row := dao.CheckDataPresence(data).(models.Campaign)
		data.ProfileId = profileId
		if row.ID == 0 {
			//record not present
			err := mysql.DB.Create(&data).Error
			if err != nil {
				return err
			}
		} else {
			data.ID = row.ID
			data.CreatedAt = row.CreatedAt
			//record present
			err := mysql.DB.Save(&data).Error
			if err != nil {
				return err
			}
		}
		history := models.CampaignHistory{DailyBudget: data.DailyBudget, State: data.State,
			ProfileId: data.ProfileId, CampaignId: data.CampaignId, SellerId: data.SellerId,
			CampaignType: data.CampaignType, Name: data.Name, ServingStatus: data.ServingStatus,
			CreationDate: data.CreationDate, LastUpdatedDate: data.LastUpdatedDate,
			PremiumBidAdjustment: data.PremiumBidAdjustment, TargetingType: data.TargetingType,
			StartDate: data.StartDate, EndDate: data.EndDate}

		rowHistory := dao.CheckDataPresence(history).(models.CampaignHistory)
		if rowHistory.ID == 0 {
			err := mysql.DB.Create(&history).Error
			if err != nil {
				log.Error(err)
			}
		}

	}

	return nil
}

func GetCampaignInformation(ppcCommon ppcmanagementcommons.PPCManagementCommon, pagination ppcmanagementcommons.Pagintion) (result []models.Campaign, err error) {
	fmt.Println("GetCampaignInformation1111")
	err = mysql.DB.Where("profile_id = ? AND state='enabled'", ppcCommon.ProfileID).
		Limit(pagination.Limit).
		Offset(pagination.Limit * pagination.Page).
		Find(&result).Error

	if err != nil {
		log.Error(err)
		return result, err
	}
	return result, nil

}

func GetAllCampaignsList(ppcCommon ppcmanagementcommons.PPCManagementCommon) (result []models.Campaign, err error) {
	fmt.Println("GetCampaignInformation1111")
	err = mysql.DB.Table("campaigns").
		Select("distinct campaigns.*").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?",
			ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.Profile.ProfileId).
		Find(&result).Error

	if err != nil {
		log.Error(err)
		return result, err
	}
	return result, nil

}

func GetCampaignDetails(campaignId int64) (result models.Campaign, err error) {

	err = mysql.DB.Table("campaigns").
		Select("campaigns.*").
		Where("campaign_id=?", campaignId).
		Find(&result).Error

	if err != nil {
		log.Error(err)
		return result, err
	}
	return result, nil

}

func GetCampaignSummaryByType(ppcCommon ppcmanagementcommons.PPCManagementCommon, timeRange *ppcmanagementform.TimeRangeForm) (summary []campaignuimodels.CampaignsSummary, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("ROUND(SUM(campaign_report_data.sales),2) as sales,SUM(campaign_report_data.orders) as orders,ROUND(SUM(campaign_report_data.cost)) as cost,"+
			" SUM(campaign_report_data.impressions) as impressions,"+
			" round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc,"+
			" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) acos, "+
			" SUM(campaign_report_data.clicks) as clicks,"+
			" round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr,"+
			" campaigns.targeting_type,"+
			" count(distinct campaigns.campaign_id) as count").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Where("campaigns.profile_id=? AND campaigns.state='enabled' AND campaign_report_data.report_date between ? AND ?", ppcCommon.Profile.ProfileId, timeRange.FromDate, timeRange.ToDate).
		Group("campaigns.targeting_type").
		Find(&summary).Error
	if err != nil {
		log.Error(err)
	}
	return

}

func GetAllCampaignReportSummary(ppcCommon ppcmanagementcommons.PPCManagementCommon, timeRange *ppcmanagementform.TimeRangeForm) (summary campaignuimodels.CampaignsSummary, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("ROUND(SUM(campaign_report_data.sales),2) as sales,"+
			" SUM(campaign_report_data.orders) as orders,"+
			" ROUND(SUM(campaign_report_data.cost)) as cost,"+
			" SUM(campaign_report_data.impressions) as impressions,"+
			" round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc, "+
			" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) acos, "+
			" SUM(campaign_report_data.clicks) as clicks,"+
			" round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr,"+
			" count(distinct campaigns.campaign_id) as count").
		Joins("right join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Where("campaigns.profile_id=? AND campaign_report_data.report_date between ? AND ?", ppcCommon.Profile.ProfileId, timeRange.FromDate, timeRange.ToDate).
		Find(&summary).Error
	if err != nil {
		log.Error(err)
	}
	return

}

func GetCampaignReportSummary(ppcCommon ppcmanagementcommons.PPCManagementCommon, timeRange *ppcmanagementform.TimeRangeForm, campaignID string) (summary campaignuimodels.CampaignsSummary, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("campaigns.name campaign_name,campaigns.campaign_id,"+
			" ROUND(SUM(campaign_report_data.sales),2) as sales,"+
			" SUM(campaign_report_data.orders) as orders,"+
			" ROUND(SUM(campaign_report_data.cost)) as cost,"+
			" SUM(campaign_report_data.impressions) as impressions,"+
			" round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc, "+
			" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) acos, "+
			" SUM(campaign_report_data.clicks) as clicks,"+
			" round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr").
		Joins("right join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Where("campaigns.profile_id=? AND campaigns.campaign_id=? AND campaign_report_data.report_date between ? AND ?", ppcCommon.Profile.ProfileId, campaignID, timeRange.FromDate, timeRange.ToDate).
		Group("campaigns.name,campaigns.campaign_id").
		Find(&summary).Error
	if err != nil {
		log.Error(err)
	}
	return

}

func GetAllCampaignReportTrends(ppcCommon ppcmanagementcommons.PPCManagementCommon, timeRange *ppcmanagementform.TimeRangeForm) (trends []campaignuimodels.CampaignsSummary, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("ROUND(SUM(campaign_report_data.sales),2) as sales,"+
			" SUM(campaign_report_data.orders) as orders,"+
			" ROUND(SUM(campaign_report_data.cost)) as cost,"+
			" SUM(campaign_report_data.impressions) as impressions,"+
			" round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc, "+
			" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) acos, "+
			" SUM(campaign_report_data.clicks) as clicks,"+
			" round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr,"+
			" DATE(campaign_report_data.report_date) as date").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Where("campaigns.profile_id=? AND campaign_report_data.report_date between ? AND ?", ppcCommon.Profile.ProfileId, timeRange.FromDate, timeRange.ToDate).
		Group("campaign_report_data.report_date").
		Order("campaign_report_data.report_date asc").
		Find(&trends).Error
	if err != nil {
		log.Error(err)
	}
	return

}

func GetCampaignReportTrends(ppcCommon ppcmanagementcommons.PPCManagementCommon, timeRange *ppcmanagementform.TimeRangeForm, campaignID string) (trends []campaignuimodels.CampaignsSummary, err error) {

	err = mysql.DB.Table("campaign_report_data").
		Select("ROUND(SUM(campaign_report_data.sales),2) as sales,"+
			" SUM(campaign_report_data.orders) as orders,"+
			" ROUND(SUM(campaign_report_data.cost)) as cost,"+
			" SUM(campaign_report_data.impressions) as impressions,"+
			" round((SUM(`campaign_report_data`.`cost`)/SUM(campaign_report_data.clicks)),2) cpc,"+
			" round((sum(`campaign_report_data`.`cost`)/SUM(campaign_report_data.sales))*100 ,2) acos, "+
			" SUM(campaign_report_data.clicks) as clicks,"+
			" round((SUM(campaign_report_data.clicks)/SUM(campaign_report_data.`impressions`))*100,2) ctr,"+
			" DATE(campaign_report_data.report_date) as date").
		Joins("join campaigns on campaigns.campaign_id = campaign_report_data.campaign_id").
		Where("campaigns.profile_id=? AND campaigns.campaign_id=? AND campaign_report_data.report_date between ? AND ?", ppcCommon.Profile.ProfileId, campaignID, timeRange.FromDate, timeRange.ToDate).
		Group("campaign_report_data.report_date").
		Order("campaign_report_data.report_date asc").
		Find(&trends).Error
	if err != nil {
		log.Error(err)
	}
	return

}
