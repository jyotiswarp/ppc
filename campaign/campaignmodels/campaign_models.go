package campaignmodels

type CampaignUpdateForm struct {
	CampaignId           int64   `json:"campaignId,omitempty"`
	Name                 string  `json:"name,omitempty"`
	State                string  `json:"state,omitempty"`
	DailyBudget          float64 `json:"dailyBudget,omitempty"`
	StartDate            string  `json:"startDate,omitempty"`
	EndDate              string  `json:"endDate,omitempty"`
	CampaignType         string  `json:"campaignType,omitempty"`
	PremiumBidAdjustment bool    `json:"premiumBidAdjustment"`
}
type CampaignForm struct {
	StartIndex       string  `json:"startIndex,omitempty"`
	Count            string  `json:"count,omitempty"`
	CampaignType     string  `json:"campaignType,omitempty"`
	StateFilter      string  `json:"stateFilter,omitempty"`
	Name             string  `json:"name,omitempty"`
	CampaignIdFilter []int64 `json:"campaignIdFilter,omitempty"`
}
