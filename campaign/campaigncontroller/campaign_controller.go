package campaigncontroller

import (
	"bitbucket.org/jyotiswarp/ppc/campaign/campaigndao"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignmodels"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignservices"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementform"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/common/log"
)

func GetCampaignList(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	pagination := c.Keys[middlewares.Pagination].(ppcmanagementcommons.Pagintion)
	result, err := campaignservices.GetCampaignList(ppcManagement, pagination, false)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func GetCampaigns(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	pagination := c.Keys[middlewares.Pagination].(ppcmanagementcommons.Pagintion)
	result, err := campaignservices.GetCampaignBySeller(ppcManagement, pagination)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func UpdateCampaign(c *gin.Context) {

	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*[]campaignmodels.CampaignUpdateForm)
	//temp code just for integration
	//c.JSON(200, gin.H{
	//	"result": "success",
	//})
	//return

	_, err := campaignservices.UpdateCampaign(ppcManagement, formRequest)
	if err != nil {
		log.Error(err)
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": "success",
	})
	return

}

func GetAllCampaignsSummary(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*ppcmanagementform.TimeRangeForm)
	result, err := campaigndao.GetAllCampaignReportSummary(ppcManagement, formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{"result": result})
}

func GetCampaignsSummaryByType(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*ppcmanagementform.TimeRangeForm)
	result, err := campaigndao.GetCampaignSummaryByType(ppcManagement, formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{"result": result})
}

func GetCampaignSummary(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*ppcmanagementform.TimeRangeForm)
	campaignID := c.Param("campaign_id")
	result, err := campaigndao.GetCampaignReportSummary(ppcManagement, formRequest, campaignID)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{"result": result})
}

func GetAllCampaignsTrends(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*ppcmanagementform.TimeRangeForm)
	result, err := campaigndao.GetAllCampaignReportTrends(ppcManagement, formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{"result": result})
}

func GetCampaignTrends(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*ppcmanagementform.TimeRangeForm)
	campaignID := c.Param("campaign_id")
	result, err := campaigndao.GetCampaignReportTrends(ppcManagement, formRequest, campaignID)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{"result": result})
}
