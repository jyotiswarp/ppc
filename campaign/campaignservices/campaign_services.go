package campaignservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaigndao"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignmodels"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
)

func GetCampaignList(ppcCommon ppcmanagementcommons.PPCManagementCommon, pagination ppcmanagementcommons.Pagintion, forceUpdate bool) (result []models.Campaign, err error) {

	//
	///*status, err := dao.CheckLastUpdatedDate(sellerId, marketPlace, geo, "campaign", profileId)
	//
	//if !status.IsZero() && time.Now().Sub(status).Hours() < 24 {
	//	result, err = campaigndao.GetCampaignInformation(sellerId, profileId) // from DB
	//} else {
	//*/

	if forceUpdate {
		form := campaignmodels.CampaignForm{}
		err = UpdateCampaignDataModel(ppcCommon, form)
		if err != nil {
			log.Error(err)
			return
		}
	}
	result, err = campaigndao.GetCampaignInformation(ppcCommon, pagination)

	if err != nil {
		return result, err
	}

	for i := 0; i < len(result); i++ {
		result[i].Header.Geo = ppcCommon.Geo
		result[i].Header.Marketplace = ppcCommon.Marketplace
		result[i].SellerId = ppcCommon.SellerID
	}
	return result, err

}

func GetAllCampaignsList(ppcCommon ppcmanagementcommons.PPCManagementCommon, forceUpdate bool) (result []models.Campaign, err error) {
	if forceUpdate {
		form := campaignmodels.CampaignForm{}
		err = UpdateCampaignDataModel(ppcCommon, form)
		if err != nil {
			log.Error(err)
			return
		}
	}
	result, err = campaigndao.GetAllCampaignsList(ppcCommon)

	if err != nil {
		return result, err
	}

	for i := 0; i < len(result); i++ {
		result[i].Header.Geo = ppcCommon.Geo
		result[i].Header.Marketplace = ppcCommon.Marketplace
		result[i].SellerId = ppcCommon.SellerID
	}
	return result, err

}

func GetCampaignBySeller(ppcCommon ppcmanagementcommons.PPCManagementCommon, pagination ppcmanagementcommons.Pagintion) (result []models.Campaign, err error) {
	fmt.Println("GetCampaignBySeller")
	if err != nil {
		log.Error(err)
		return
	}
	result, err = campaigndao.GetCampaignInformation(ppcCommon, pagination)

	if err != nil {
		return result, err
	}

	return result, err

}

func UpdateCampaignDataModel(ppcCommon ppcmanagementcommons.PPCManagementCommon, form campaignmodels.CampaignForm) (err error) {

	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		var errM error
		result, errM := market.GetCampaignsListExtended(form)
		err = errM
		if err != nil {
			log.Error(err)
			return
		}
		err = campaigndao.PushCampaignDataToDb(result, ppcCommon.Profile.ProfileId)
		return
	}
	return errors.New("invalid market")
}

func UpdateCampaign(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *[]campaignmodels.CampaignUpdateForm) ([]models.CampaignUpdateResponse, error) {
	//market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	//if market != nil {
	//	market.SetSellerId(ppcCommon.SellerID)
	//	market.SetProfileId(ppcCommon.Profile.ProfileId)
	//	market.SetEmail(ppcCommon.Profile.Email)
	//	result, err = market.UpdateCampaign(form)
	//	if err != nil {
	//		log.Error(err)
	//		return
	//	}
	//	syncReq := campaignmodels.CampaignForm{}
	//	for _, req := range *form {
	//		syncReq.CampaignIdFilter = append(syncReq.CampaignIdFilter, req.CampaignId)
	//	}
	//	err = UpdateCampaignDataModel(ppcCommon, syncReq)
	//	if err != nil {
	//		log.Error(err)
	//		return
	//	}
	//	return
	//}

	res, err := updateDataInCampaigns(ppcCommon, form)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	GetAllCampaignsList(ppcCommon, true)
	return res, nil

}

func updateDataInCampaigns(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *[]campaignmodels.CampaignUpdateForm) (result []models.CampaignUpdateResponse, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.UpdateCampaign(form)
		if err != nil {
			log.Error(err)
			return
		}
	}
	return

}
