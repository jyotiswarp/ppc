package campaignresources

import (
	"bitbucket.org/jyotiswarp/ppc/campaign/campaigncontroller"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignmodels"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementform"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
)

func CampaignResources(router *gin.Engine) {
	managementCampaignResources := router.Group("/management/campaigns", middlewares.PPCRequestMiddleware())
	{
		managementCampaignResources.GET("/list", middlewares.PaginationValidator(), campaigncontroller.GetCampaigns)
		managementCampaignResources.PUT("/update", middlewares.Validator([]campaignmodels.CampaignUpdateForm{}), campaigncontroller.UpdateCampaign)

	}

	insightsCampaignResources := router.Group("/campaigns", middlewares.PPCRequestMiddleware())
	{
		insightsCampaignResources.POST("/summary", middlewares.Validator(ppcmanagementform.TimeRangeForm{}), campaigncontroller.GetAllCampaignsSummary)
		insightsCampaignResources.POST("/summary_by_type", middlewares.Validator(ppcmanagementform.TimeRangeForm{}), campaigncontroller.GetCampaignsSummaryByType)
		insightsCampaignResources.POST("/trends", middlewares.Validator(ppcmanagementform.TimeRangeForm{}), campaigncontroller.GetAllCampaignsTrends)
	}

	perCampaignResources := router.Group("/campaign/:campaign_id", middlewares.PPCRequestMiddleware())
	{
		perCampaignResources.POST("/summary", middlewares.Validator(ppcmanagementform.TimeRangeForm{}), campaigncontroller.GetCampaignSummary)
		perCampaignResources.POST("/trends", middlewares.Validator(ppcmanagementform.TimeRangeForm{}), campaigncontroller.GetCampaignTrends)
	}
}
