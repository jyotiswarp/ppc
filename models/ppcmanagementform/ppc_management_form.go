package ppcmanagementform

import "time"

type TimeRangeForm struct {
	FromDate time.Time `json:"fromDate" binding:"required"`
	ToDate   time.Time `json:"toDate" binding:"required"`
}
