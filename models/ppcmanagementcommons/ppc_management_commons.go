package ppcmanagementcommons

import (
	"bitbucket.org/jyotiswarp/ppc/models"
	"strconv"
)

type PPCManagementCommon struct {
	SellerID     string
	Geo          string
	Marketplace  string
	Profile      models.Profile
	ProfileID    int64
	ProfileIDStr string
}


type Pagintion struct {
	Page		   int
	Limit		   int
}

func CreatePPCCommons(profile models.Profile) PPCManagementCommon {
	ppcCommons := PPCManagementCommon{
		Marketplace:  profile.MarketPlaceName,
		Geo:          profile.Geo,
		SellerID:     profile.SellerStringId,
		Profile:      profile,
		ProfileID:    profile.ProfileId,
		ProfileIDStr: strconv.FormatInt(profile.ProfileId, 10),
	}
	return ppcCommons
}
