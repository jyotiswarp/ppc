package models

type Header struct {
	Marketplace string `gorm:"-"`
	Geo         string `gorm:"-"`
}
