package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type AccountInfo struct {
	MarketplaceStringId string `json:"marketplaceStringId,omitempty"`
	SellerStringId      string `json:"sellerStringId,omitempty"`
}

// Profile
type Profile struct {
	gorm.Model
	MarketPlaceName     string    `json:"marketPlaceName,omitempty" gorm:"index:market_place_geo_seller_id_profile_id"`
	Geo                 string    `json:"geo,omitempty" gorm:"index:market_place_geo_seller_id_profile_id"`
	SellerStringId      string    `json:"sellerStringId,omitempty" gorm:"index:market_place_geo_seller_id_profile_id"`
	ProfileId           int64     `json:"profileId,omitempty" gorm:"index:market_place_geo_seller_id_profile_id"`
	Email               string    `json:"email,omitempty"`
	CountryCode         string    `json:"countryCode,omitempty"`
	CurrencyCode        string    `json:"currencyCode,omitempty"`
	DailyBudget         float64   `json:"dailyBudget,omitempty"`
	TimeZone            string    `json:"timeZone,omitempty"`
	MarketplaceStringId string    `json:"marketplaceStringId,omitempty"`
	LastAnalyzedAt      time.Time `json:"lastAnalyzedAt,omitempty"`
	IsAccessRevoked     bool      `json:"is_access_revoked,omitempty"`
}
