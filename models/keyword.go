package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type Keyword struct {
	gorm.Model
	CampaignId      int64      `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	AdGroupId       int64      `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	KeywordId       int64      `json:"keywordId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	KeywordText     string     `json:"keywordText,omitempty"`
	Bid             float64    `json:"bid,omitempty"`
	CreationDate    *time.Time `json:"creationDate,omitempty"` //  "epoch time in milliseconds"
	LastUpdatedDate *time.Time `json:"lastUpdatedDate,omitempty"`
	MatchType       string     `json:"matchType,omitempty"`
	ServingStatus   string     `json:"servingStatus,omitempty"`
	State           string     `json:"state,omitempty"`
	MarkedNegative  bool       `json:"marked_negative"`
}
type NegativeKeyword struct {
	gorm.Model
	CampaignId      int64      `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	AdGroupId       int64      `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	KeywordId       int64      `json:"keywordId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	KeywordText     string     `json:"keywordText,omitempty"`
	Bid             float64    `json:"bid,omitempty"`
	CreationDate    *time.Time `json:"creationDate,omitempty"` //  "epoch time in milliseconds"
	LastUpdatedDate *time.Time `json:"lastUpdatedDate,omitempty"`
	MatchType       string     `json:"matchType,omitempty"`
	ServingStatus   string     `json:"servingStatus,omitempty"`
	State           string     `json:"state,omitempty"`
}

type KeywordHistory struct {
	gorm.Model
	CampaignId      int64      `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	AdGroupId       int64      `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	KeywordId       int64      `json:"keywordId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	KeywordText     string     `json:"keywordText,omitempty"`
	Bid             float64    `json:"bid,omitempty"`
	CreationDate    *time.Time `json:"creationDate,omitempty"` //  "epoch time in milliseconds"
	LastUpdatedDate *time.Time `json:"lastUpdatedDate,omitempty"`
	MatchType       string     `json:"matchType,omitempty"`
	ServingStatus   string     `json:"servingStatus,omitempty"`
	State           string     `json:"state,omitempty"`
}
type NegativeKeywordHistory struct {
	gorm.Model
	CampaignId      int64      `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	AdGroupId       int64      `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	KeywordId       int64      `json:"keywordId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id"`
	KeywordText     string     `json:"keywordText,omitempty"`
	Bid             float64    `json:"bid,omitempty"`
	CreationDate    *time.Time `json:"creationDate,omitempty"` //  "epoch time in milliseconds"
	LastUpdatedDate *time.Time `json:"lastUpdatedDate,omitempty"`
	MatchType       string     `json:"matchType,omitempty"`
	ServingStatus   string     `json:"servingStatus,omitempty"`
	State           string     `json:"state,omitempty"`
}
