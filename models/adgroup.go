  package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type AdGroup struct {
	gorm.Model
	CampaignId      int64      `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id"`
	AdGroupId       int64      `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id"`
	CreationDate    *time.Time `json:"creationDate,omitempty"`
	DefaultBid      float64    `json:"defaultBid,omitempty"`
	LastUpdatedDate *time.Time `json:"lastUpdatedDate,omitempty"`
	Name            string     `json:"name,omitempty"`
	ServingStatus   string     `json:"servingStatus,omitempty"`
	State           string     `json:"state,omitempty"`
}
type AdGroupHistory struct {
	gorm.Model
	CampaignId      int64      `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id"`
	AdGroupId       int64      `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id"`
	CreationDate    *time.Time `json:"creationDate,omitempty"`
	DefaultBid      float64    `json:"defaultBid,omitempty"`
	LastUpdatedDate *time.Time `json:"lastUpdatedDate,omitempty"`
	Name            string     `json:"name,omitempty"`
	ServingStatus   string     `json:"servingStatus,omitempty"`
	State           string     `json:"state,omitempty"`
}

/*
type AdGroupDetails1 struct {
	gorm.Model
	AdGroupId       int64     `json:"adGroupId,omitempty"`
	CampaignId      int64     `json:"campaignId,omitempty"`
	CreationDate    *time.Time     `json:"creationDate,omitempty"` //  "epoch time in milliseconds"
	DefaultBid      float64 `json:"defaultBid,omitempty"`
	LastUpdatedDate *time.Time     `json:"lastUpdatedDate,omitempty"` //  "epoch time in milliseconds"
	Name            string  `json:"name,omitempty"`
	ServingStatus   string  `json:"servingStatus,omitempty"`
	State           string  `json:"state,omitempty"`
}*/
