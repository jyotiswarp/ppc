package models

import (
	//"bitbucket.org/jyotiswarp/ppc/models"
	"github.com/jinzhu/gorm"
	"time"
)

/*func GetCampaignDetails(Marketplace string, Geo string, UserId int, CampaignId int) *models.Campaign {
	// TODO  get next campaging  details from DB or backend
	var nextCampaign models.Campaign
	nextCampaign.CampaignId = CampaignId
	return &nextCampaign
}*/

type Campaign struct {
	Header
	gorm.Model
	SellerId             string     `gorm:"-"`
	ProfileId            int64      `json:"profileId,omitempty" gorm:"index:profile_id_campaign_id"`
	CampaignId           int64      `json:"campaignId,omitempty" gorm:"index:profile_id_campaign_id"`
	CampaignType         string     `json:"campaignType,omitempty"`
	DailyBudget          float64    `json:"dailyBudget,omitempty"`
	EndDate              *time.Time `json:"endDate,omitempty"`
	Name                 string     `json:"name,omitempty"`
	PremiumBidAdjustment bool       `json:"premiumBidAdjustment,omitempty"`
	StartDate            *time.Time `json:"startDate,omitempty"`
	State                string     `json:"state,omitempty"`
	TargetingType        string     `json:"targetingType,omitempty"`
	ServingStatus        string     `json:"servingStatus"`
	CreationDate         *time.Time `json:"creationDate"`
	LastUpdatedDate      *time.Time `json:"lastUpdatedDate"`
}

type CampaignHistory struct {
	Header
	gorm.Model
	SellerId             string     `gorm:"-"`
	ProfileId            int64      `json:"profileId,omitempty" gorm:"index:profile_id_campaign_id"`
	CampaignId           int64      `json:"campaignId,omitempty" gorm:"index:profile_id_campaign_id"`
	CampaignType         string     `json:"campaignType,omitempty"`
	DailyBudget          float64    `json:"dailyBudget,omitempty"`
	EndDate              *time.Time `json:"endDate,omitempty"`
	Name                 string     `json:"name,omitempty"`
	PremiumBidAdjustment bool       `json:"premiumBidAdjustment,omitempty"`
	StartDate            *time.Time `json:"startDate,omitempty"`
	State                string     `json:"state,omitempty"`
	TargetingType        string     `json:"targetingType,omitempty"`
	ServingStatus        string     `json:"servingStatus"`
	CreationDate         *time.Time `json:"creationDate"`
	LastUpdatedDate      *time.Time `json:"lastUpdatedDate"`
}

type CampaignUpdateResponse struct {
	Code        string `json:"code"`
	CampaignId  int64  `json:"campaignId,omitempty"`
	Description string `json:"description"`
}
