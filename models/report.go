package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type RequestReport struct {
	ReportDate   int64  `json:"reportDate,omitempty" binding:"required"`
	CampaignType string `json:"campaignType,omitempty" binding:"required"`
	Metrics      string `json:"metrics,omitempty" binding:"required"`
	Segment      string `json:"segment,omitempty"`
	//Requester string `json:"Requester"`
}

type RequestNReport struct {
	NumberOfDays int `json:"number_of_days" binding:"required"`
	//Requester string `json:"Requester"`
}

type Report struct {
	Status   string               `json:"status"`
	Campaign []CampaignReportData `json:"campaign"`
}
type CampaignReportData struct {
	gorm.Model
	CampaignId   int64      `json:"campaignId,omitempty" gorm:"index:campaign_id_report_date"`
	ReportDate   *time.Time `json:"reportDate,omitempty" gorm:"index:campaign_id_report_date"`
	CampaignName string     `gorm:"-" json:"campaignName"`
	Impressions  int        `json:"impressions,omitempty"`
	Acos         float64    `json:"acos"`
	Clicks       int        `json:"clicks,omitempty"`
	Cost         float64    `json:"cost,omitempty"`
	Cpc          float64    `json:"cpc"`
	Ctr          float64    `json:"ctr"`
	Sales        float64    `json:"sales"`
	Orders       int        `json:"orders"`
	//AdGroupReportData 		AdGroupReportData   `gorm:"ForeignKey:CampaignId"`
	AttributedValues []AttributedTimeData `json:"attributedValues,omitempty"`
	AdGroupReport    []AdGroupReportData  `json:"adGroupReportm,omitempty"`
}
type AdGroupReportData struct {
	gorm.Model
	CampaignId       int64                  `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id_report_date"`
	AdGroupId        int64                  `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id_report_date"`
	ReportDate       *time.Time             `json:"reportDate,omitempty" gorm:"index:campaign_id_ad_group_id_report_date"`
	CampaignName     string                 `gorm:"-" json:"campaignName"`
	AdGroupName      string                 `gorm:"-" json:"ad_groupName"`
	Impressions      int                    `json:"impressions,omitempty"`
	Clicks           int                    `json:"clicks,omitempty"`
	Cost             float64                `json:"cost,omitempty"`
	Acos             float64                `json:"acos"`
	Cpc              float64                `json:"cpc"`
	Ctr              float64                `json:"ctr"`
	Sales            float64                `json:"sales"`
	Orders           int                    `json:"orders"`
	AttributedValues []AttributedTimeData   `json:"attributedValues,omitempty"`
	KeywordReport    []KeywordReportData    `json:"keywordReport,omitempty"`
	ProductAdReport  []ProductAdsReportData `json:"productAdReport,omitempty"`
}

type KeywordReportData struct {
	gorm.Model
	CampaignId       int64                `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id_report_date"`
	AdGroupId        int64                `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id_report_date"`
	KeywordId        int64                `json:"keywordId,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id_report_date"`
	ReportDate       *time.Time           `json:"reportDate,omitempty" gorm:"index:campaign_id_ad_group_id_keyword_id_report_date"`
	CampaignName     string               `gorm:"-" json:"campaignName"`
	CampaignType     string               `gorm:"-" json:"campaignType"`
	AdGroupName      string               `gorm:"-" json:"adGroupName"`
	KeywordName      string               `gorm:"-" json:"keywordName"`
	MatchType        string               `gorm:"-" json:"matchType"`
	Query            string               `json:"query,omitempty"`
	Impressions      int                  `json:"impressions,omitempty"`
	Clicks           int                  `json:"clicks,omitempty"`
	Cost             float64              `json:"cost,omitempty"`
	Acos             float64              `json:"acos"`
	Cpc              float64              `json:"cpc"`
	Ctr              float64              `json:"ctr"`
	Sales            float64              `json:"sales"`
	Orders           int                  `json:"orders"`
	AttributedValues []AttributedTimeData `json:"attributedValues,omitempty"`
}

type ProductAdsReportData struct {
	gorm.Model
	CampaignId       int64                `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id_ad_id_report_date"`
	AdGroupId        int64                `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id_ad_id_report_date"`
	AdId             int64                `json:"adId,omitempty" gorm:"index:campaign_id_ad_group_id_ad_id_report_date"`
	ReportDate       *time.Time           `json:"reportDate,omitempty" gorm:"index:campaign_id_ad_group_id_ad_id_report_date"`
	Asin             string               `gorm:"-" json:"asin"`
	CampaignName     string               `gorm:"-" json:"campaignName"`
	AdGroupName      string               `gorm:"-" json:"ad_groupName"`
	Impressions      int                  `json:"impressions,omitempty"`
	Clicks           int                  `json:"clicks,omitempty"`
	Cost             float64              `json:"cost,omitempty"`
	Acos             float64              `json:"acos"`
	Cpc              float64              `json:"cpc"`
	Ctr              float64              `json:"ctr"`
	Sales            float64              `json:"sales"`
	Orders           int                  `json:"orders"`
	AttributedValues []AttributedTimeData `json:"attributedValues,omitempty"`
}

type AsinReportData struct {
	gorm.Model
	CampaignName string     `json:"campaignName"`
	AdGroupName  string     `json:"adGroupName"`
	CampaignId   int64      `json:"campaignId"`
	AdGroupId    int64      `json:"adGroupId"`
	ReportDate   *time.Time `json:"reportDate,omitempty" `
	Sku          string     `json:"sku"`
	Asin         string     `json:"asin"`
	Currency     string     `json:"currency"`
	KeywordText  string     `json:"keywordText"`
	MatchType    string     `json:"matchType"`
	OtherAsin    string     `json:"otherAsin"`
	//Order         int        `json:"order"`
	OtherSkuOrder int     `json:"other_sku_order,omitempty"`
	OtherSkuSales float64 `json:"other_sku_sales,omitempty"`
}

type AttributedTimeData struct {
	gorm.Model
	ReportId                     uint    `json:"report_id" gorm:"index:report_id_report_type"`
	ReportType                   string  `json:"report_type" gorm:"index:report_id_report_type"`
	NumberOfDays                 int     `json:"number_of_days,omitempty"` //in days
	AttributedConversionsSameSKU int     `json:"attributedConversionsSameSKU,omitempty"`
	AttributedConversions        int     `json:"attributedConversions,omitempty"`
	AttributedSalesSameSKU       float64 `json:"attributedSalesSameSKU,omitempty"`
	AttributedSales              float64 `json:"attributedSales,omitempty"`
}

type ReqReportResponse struct {
	ReportId      string `json:"reportId,omitempty"`
	RecordType    string `json:"recordType,omitempty"`
	Status        string `json:"status,omitempty"`
	StatusDetails string `json:"statusDetails,omitempty"`
}

type SnapshotFirstReq struct {
	SnapshotID string `json:"snapshotId"`
	RecordType string `json:"recordType"`
	Status     string `json:"status"`
}

type SnapShotResponse struct {
	SnapshotID    string `json:"snapshotId"`
	Status        string `json:"status"`
	StatusDetails string `json:"statusDetails"`
	Location      string `json:"location"`
	FileSize      int    `json:"fileSize"`
}

func GetLastNCampaignReports(CampaignId int64, markerplace string, geo string, n int) []*CampaignReportData {
	/*  TODO
	1) get from DB
	2) if last report is not very current, get from backend

	dummy below
	*/

	var toRet []*CampaignReportData

	for i := 0; i < n; i++ {
		c := CampaignReportData{}
		c.CampaignId = 100
		c.Impressions = i * 10
		c.Clicks = i
		toRet = append(toRet, &c)
	}

	return toRet
}
