package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type ProductAd struct {
	gorm.Model
	CampaignId      int64      `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id_ad_id"`
	AdGroupId       int64      `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id_ad_id"`
	AdId            int64      `json:"adId,omitempty" gorm:"index:campaign_id_ad_group_id_ad_id"`
	Asin            string     `json:"asin,omitempty"`
	CreationDate    *time.Time `json:"creationDate,omitempty"`
	LastUpdatedDate *time.Time `json:"lastUpdatedDate,omitempty"`
	ServingStatus   string     `json:"servingStatus,omitempty"`
	Sku             string     `json:"sku,omitempty"`
	State           string     `json:"state,omitempty"`
}
type ProductAdHistory struct {
	gorm.Model
	CampaignId      int64      `json:"campaignId,omitempty" gorm:"index:campaign_id_ad_group_id_ad_id"`
	AdGroupId       int64      `json:"adGroupId,omitempty" gorm:"index:campaign_id_ad_group_id_ad_id"`
	AdId            int64      `json:"adId,omitempty" gorm:"index:campaign_id_ad_group_id_ad_id"`
	Asin            string     `json:"asin,omitempty"`
	CreationDate    *time.Time `json:"creationDate,omitempty"`
	LastUpdatedDate *time.Time `json:"lastUpdatedDate,omitempty"`
	ServingStatus   string     `json:"servingStatus,omitempty"`
	Sku             string     `json:"sku,omitempty"`
	State           string     `json:"state,omitempty"`
}
