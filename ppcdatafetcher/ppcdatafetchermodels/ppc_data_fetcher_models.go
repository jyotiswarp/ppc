package ppcdatafetchermodels

import (
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	//"fmt"
	//log "github.com/Sirupsen/logrus"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetcherconstants"
	log "github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
	"time"
)

func MigrateJobTables() {
	mysql.DB.AutoMigrate(&PPCJobsNew{})
}

type ReportPubSubData struct {
	Profile    models.Profile
	ReportDate string
	PPCJob     PPCJobsNew
}

type PPCJobsNew struct {
	gorm.Model
	SellerId    string     `json:"seller_id,omitempty" gorm:"index:seller_id_profile_id_status"`
	ProfileId   string     `json:"profile_id,omitempty" gorm:"index:seller_id_profile_id_status"`
	RequestType string     `json:"request_type,omitempty"`
	Status      string     `json:"status,omitempty" gorm:"index:seller_id_profile_id_status"`
	Comments    string     `json:"comments,omitempty"`
	ReportDate  *time.Time `json:"report_date,omitempty"`
	ReportURL   string     `json:"report_url"`
	RetryCount  int        `json:"retry_count"`
	RequestHash string     `json:"request_hash"`
}

func FirstOrCreateJob(job PPCJobsNew) PPCJobsNew {
	var lJob PPCJobsNew
	err := mysql.DB.Where("request_hash=?", job.RequestHash).
		Find(&lJob).
		Error
	if err != nil {
		log.Error(err)
	}
	if lJob.ID != 0 {
		return lJob
	} else {
		err := mysql.DB.Create(&job).Error
		if err != nil {
			log.Error(err)
		}
		return job
	}
}

func GetFailedJobs(maxRetryCount int) (jobs []PPCJobsNew, err error) {
	err = mysql.DB.Where("status =? AND retry_count <=?", ppcdatafetcherconstants.Failed, maxRetryCount).Find(&jobs).Error
	return
}

func GetPendingJobs(timeDiffInHours, maxRetryCount int) (jobs []PPCJobsNew, err error) {
	err = mysql.DB.Where("status =? AND updated_at < ? and retry_count <=?", ppcdatafetcherconstants.Pending, time.Now().Add(-time.Hour*time.Duration(timeDiffInHours)), maxRetryCount).Find(&jobs).Error
	return
}
