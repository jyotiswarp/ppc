package ppcdatafetcherservice

import (
	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofiledao"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetcherconstants"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetchermodels"
	"bitbucket.org/jyotiswarp/ppc/pubsubclient"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/jasonlvhit/gocron"
	"github.com/spf13/viper"
	"sync"
)

var (
	s        *gocron.Scheduler
	ingestor *pubsubclient.PubSubIngestor
	listener *pubsubclient.PubSubReceive
	once     sync.Once
)

func Initialize() {
	go initIngestor()
	go listenTrendsSubs()
	s = gocron.NewScheduler()
	fmt.Println("interval : ", viper.GetInt64("report_update_interval_in_seconds"))
	s.Every(uint64(viper.GetInt64("report_update_interval_in_seconds"))).Seconds().Do(CheckAndTriggerReportUpdate)
	s.Every(uint64(viper.GetInt64("failed_report_update_interval_in_minutes"))).Minutes().Do(HandleFailedTasks)
	s.Every(uint64(viper.GetInt64("pending_report_update_interval_in_minutes"))).Minutes().Do(HandlePendingTasks)
	//s.Every(uint64(viper.GetInt64("ppc_access_check_interval_in_days"))).Day().Do(PPCUserAccessCheck)
	s.Start()
}

func GetProfileIngestor() *pubsubclient.PubSubIngestor {
	once.Do(func() {
		initIngestor()
	})
	return ingestor

}
func GetProfileListner() *pubsubclient.PubSubReceive {
	once.Do(func() {
		listenTrendsSubs()
	})
	return listener

}

func initIngestor() {
	ingestor = &pubsubclient.PubSubIngestor{}
	ingestor.TopicName = viper.GetString("ppc_profile_ingestor_topic")
	fmt.Println(ingestor.TopicName)
	ingestor.IsLogEnabled = true
	fmt.Println("Ingester initiated")
	ingestor.Init()
}
func listenTrendsSubs() {
	listener = &pubsubclient.PubSubReceive{}
	listener.Subscription = viper.GetString("ppc_profile_subs_queue")
	fmt.Println(listener.Subscription)
	listener.IsLogEnabled = true
	fmt.Println("Listener initiated")
	listener.Init()
	go StartProductListener()
}

func CheckAndTriggerReportUpdate() {
	fmt.Println("fetching profiles to update data")
	profiles, err := marketprofiledao.GetLastAnalyzedProfileDetailsByCondition("1 day")
	if err != nil {
		log.Error(err)
		return
	}
	if len(profiles) > 0 {
		for _, profile := range profiles {
			CreatePPCJobs(profile, false, 1)
		}

	} else {
		log.Print("No profiles found for report update")
	}
}

func HandleFailedTasks() {
	fmt.Println("fetching failed tasks")
	failedJobs, err := ppcdatafetchermodels.GetFailedJobs(viper.GetInt("max_retry_count"))
	if err != nil {
		log.Error(err)
		return
	}
	for _, job := range failedJobs {
		job.Status = ppcdatafetcherconstants.Created
		job.RetryCount += 1
		err := mysql.DB.Save(&job).Error
		if err != nil {
			log.Error(err)
			continue
		}
		CreateIndividualTask(job)
	}

}

func HandlePendingTasks() {
	fmt.Println("fetching pending tasks")
	pendingJobs, err := ppcdatafetchermodels.GetPendingJobs(viper.GetInt("restart_pending_jobs_after_in_hours"),viper.GetInt("max_retry_count"))
	if err != nil {
		log.Error(err)
		return
	}
	for _, job := range pendingJobs {
		job.Status = ppcdatafetcherconstants.Created
		job.RetryCount += 1
		err := mysql.DB.Save(&job).Error
		if err != nil {
			log.Error(err)
			continue
		}
		CreateIndividualTask(job)
	}

}

func PPCUserAccessCheck() {
	//profiles, err := marketprofiledao.GetAllProfileDetails()
	//if err != nil {
	//	log.Error(err)
	//	return
	//}
	//for _, prof := range profiles {
	//	go func(profile models.Profile) {
	//		counter := 0
	//		for {
	//
	//			result, err := marketprofileservices.GetProfileFromBackend(profile.SellerStringId, profile.Email, profile.Geo, profile.MarketPlaceName)
	//			fmt.Println("Email := ", profile.Email)
	//			if err == nil && len(result) > 0 {
	//				return
	//			} else {
	//				counter++
	//			}
	//			if counter > 5 {
	//				err = authenticationdao.SetAccessRevoked(profile.Email)
	//				return
	//			}
	//			fmt.Println("Counter :", counter)
	//			time.Sleep(2 * time.Second)
	//
	//		}
	//
	//	}(prof)
	//}

}
