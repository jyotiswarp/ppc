package ppcdatafetcherservice

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupservices"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignservices"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordservices"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetcherconstants"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetchermodels"
	"bitbucket.org/jyotiswarp/ppc/productad/productadservices"
	"bitbucket.org/jyotiswarp/ppc/report/reportmodels"
	"bitbucket.org/jyotiswarp/ppc/report/reportservices"
	"bitbucket.org/jyotiswarp/ppc/utils"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	"bitbucket.org/jyotiswarp/ppc/utils/stringutils"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/getsentry/raven-go"
	"github.com/spf13/viper"
	"strconv"
	"time"
)

func CreatePPCJobs(profile models.Profile, isFirstTimer bool, numberOfDays int) error {
	if isFirstTimer {
		return createFirstTimerTasks(profile, numberOfDays)
	} else {
		return createScheduledTasks(profile)
	}

	return nil
}

func createFirstTimerTasks(profile models.Profile, numberOfDays int) error {
	currentTime := time.Now().UTC()
	//createBasicJobs(profile)
	for i := 1; i <= numberOfDays; i++ {
		createReportJob(profile, i)
	}
	profile.LastAnalyzedAt = currentTime
	dbErr := mysql.DB.Save(profile).Error
	if dbErr != nil {
		log.Error(dbErr)
		return dbErr
	}
	return nil

}

func CreateBasicJobs(profile models.Profile) {
	currentTime := time.Now().UTC()
	for _, task := range ppcdatafetcherconstants.BasicJobTypes {
		ppcJob, err := createPPCJob(profile, currentTime, task)
		if err != nil {
			log.Error(err)
			continue
		}
		message := createPubSubMessage(ppcJob, getDateInYYYYMMDD(currentTime), profile)
		GetProfileIngestor().Publish(message)
	}

}

func createReportJob(profile models.Profile, index int) {
	currentTime := time.Now().UTC()
	prevDate := currentTime.AddDate(0, 0, -index)
	prevDateAsString := getDateInYYYYMMDD(prevDate)
	for _, reportTask := range ppcdatafetcherconstants.ReportJobTypes {
		ppcJob, err := createPPCJob(profile, prevDate, reportTask)
		if err != nil {
			log.Error(err)
			continue
		}
		message := createPubSubMessage(ppcJob, prevDateAsString, profile)
		GetProfileIngestor().Publish(message)
	}
}

func CreateIndividualTask(job ppcdatafetchermodels.PPCJobsNew) {
	prevDateAsString := getDateInYYYYMMDD(*job.ReportDate)
	var profile models.Profile
	err := mysql.DB.Where("profile_id=?", job.ProfileId).Find(&profile).Error
	if err != nil {
		log.Error(err)
	}
	message := createPubSubMessage(job, prevDateAsString, profile)
	GetProfileIngestor().Publish(message)
}

func createScheduledTasks(profile models.Profile) error {
	prevDateAsString := getDateInYYYYMMDD(profile.LastAnalyzedAt)
	for _, task := range ppcdatafetcherconstants.AllJobTypes {
		ppcJob, err := createPPCJob(profile, profile.LastAnalyzedAt, task)
		if err != nil {
			log.Error(err)
			return err
		}
		message := createPubSubMessage(ppcJob, prevDateAsString, profile)
		go GetProfileIngestor().Publish(message)
	}
	profile.LastAnalyzedAt = profile.LastAnalyzedAt.AddDate(0, 0, 1)
	dbErr := mysql.DB.Save(profile).Error
	if dbErr != nil {
		log.Error(dbErr)
	}
	return nil

}

func createPPCJob(profile models.Profile, reportDate time.Time, requestType string) (ppcdatafetchermodels.PPCJobsNew, error) {
	ppcJob := ppcdatafetchermodels.PPCJobsNew{
		ReportDate:  &reportDate,
		ProfileId:   strconv.FormatInt(profile.ProfileId, 10),
		SellerId:    profile.SellerStringId,
		Status:      ppcdatafetcherconstants.Created,
		RequestType: requestType,
	}
	ppcJob.RequestHash = generateHashString(ppcJob, profile)
	job := ppcdatafetchermodels.FirstOrCreateJob(ppcJob)
	return job, nil
}

func generateHashString(job ppcdatafetchermodels.PPCJobsNew, profile models.Profile) string {
	reqFilter := reportmodels.OnDemandFilter{}
	reqFilter.Filters = map[string]interface{}{}
	reqFilter.Filters["reportDate"] = job.ReportDate
	reqFilter.Filters["campaignType"] = "sponsoredProducts"
	reqFilter.Filters["sellerId"] = job.SellerId
	reqFilter.Filters["profileId"] = job.ProfileId
	reqFilter.Filters["market"] = profile.MarketPlaceName
	reqFilter.Filters["geo"] = profile.Geo
	reqFilter.Filters["jobType"] = job.RequestType

	newMap := utils.SortMap(reqFilter.Filters)
	hashKey := utils.Hash(utils.MapToString(newMap) + job.SellerId)
	return fmt.Sprint(hashKey)
}

func createPubSubMessage(ppcJob ppcdatafetchermodels.PPCJobsNew, reportDate string, profile models.Profile) ppcdatafetchermodels.ReportPubSubData {
	pubSubMessage := ppcdatafetchermodels.ReportPubSubData{Profile: profile, PPCJob: ppcJob, ReportDate: reportDate}
	return pubSubMessage
}

func getDateInYYYYMMDD(now time.Time) string {
	y, m, d := now.Date()
	month := strconv.Itoa(int(m))
	if len(month) == 1 {
		month = "0" + month
	}
	day := strconv.Itoa(d)
	if len(day) == 1 {
		day = "0" + day
	}
	reportDate := strconv.Itoa(y) + month + day
	return reportDate
}

func FetchPPCDataWrapper(request ppcdatafetchermodels.ReportPubSubData) error {

	var err error
	var fileName string

	date, _ := strconv.ParseInt(request.ReportDate, 10, 64)
	formRequest := models.RequestReport{ReportDate: date, Metrics: viper.GetString("report_metrics"), CampaignType: "sponsoredProducts"}
	var reportDate time.Time
	if formRequest.ReportDate != 0 {
		d := strconv.Itoa(int(formRequest.ReportDate))
		reportDate, _ = time.Parse("20060102", d)

	}
	if request.PPCJob.RequestType == ppcdatafetcherconstants.KeywordReport {
		formRequest.Segment = "query"
	}
	if request.PPCJob.RequestType == ppcdatafetcherconstants.AsinReport {
		formRequest.Metrics = viper.GetString("asin_report_metrics")
	}
	ppcCommon := ppcmanagementcommons.CreatePPCCommons(request.Profile)
	switch request.PPCJob.RequestType {

	case ppcdatafetcherconstants.GetCampaignList:
		_, err = campaignservices.GetAllCampaignsList(ppcCommon, true)
	case ppcdatafetcherconstants.GetAdGroupList:
		_, err = adgroupservices.GetAdGroupList(ppcCommon, true)
	case ppcdatafetcherconstants.GetKeywordsList:
		_, err = keywordservices.GetKeywordList(ppcCommon, true)
	case ppcdatafetcherconstants.GetNegativeKeys:
		_, err = keywordservices.GetNegativeKeywordList(ppcCommon, true)
	case ppcdatafetcherconstants.GetProdAdsList:
		_, err = productadservices.GetProductAdList(ppcCommon, true)
	case ppcdatafetcherconstants.CampaignReport:
		fileName, err = reportservices.FetchCampaignReportData(request, formRequest, reportDate)
	case ppcdatafetcherconstants.AdGroupReport:
		fileName, err = reportservices.FetchAdGroupReportData(request, formRequest, reportDate)
	case ppcdatafetcherconstants.KeywordReport:
		fileName, err = reportservices.FetchKeywordReportData(request, formRequest, reportDate)
	case ppcdatafetcherconstants.AsinReport:
		fileName, err = reportservices.FetchAsinReportData(request, formRequest, reportDate)
	case ppcdatafetcherconstants.ProdAdsReport:
		fileName, err = reportservices.FetchProductAdReportData(request, formRequest, reportDate)

	}
	if err != nil {
		raven.CaptureError(err, map[string]string{"Failed Job": stringutils.GetStringFromStruct(request.PPCJob)})
		return updateJobStatus(request.PPCJob, ppcdatafetcherconstants.Failed)
	}
	request.PPCJob.ReportURL = fileName
	return updateJobStatus(request.PPCJob, ppcdatafetcherconstants.Completed)
}

func updateJobStatus(job ppcdatafetchermodels.PPCJobsNew, status string) error {
	job.Status = status
	dbErr := mysql.DB.Save(&job).Error
	if dbErr != nil {
		log.Error(dbErr)
	}
	return nil
}
