package ppcdatafetcherservice

import (
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetcherconstants"
	"bitbucket.org/jyotiswarp/ppc/ppcdatafetcher/ppcdatafetchermodels"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	"cloud.google.com/go/pubsub"
	"encoding/json"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"time"
	"github.com/getsentry/raven-go"
	"bitbucket.org/jyotiswarp/ppc/utils/stringutils"
)

func StartProductListener() {
	count := 0
	for {
		if count < viper.GetInt("profile_max_request_count") {
			msg := GetProfileListner().Get()
			count++
			go processJobMessage(msg)
		} else {
			//log.Info("==Resetting count product==")
			count = 0
			time.Sleep(10 * time.Second)
		}
	}
}

func processJobMessage(msg *pubsub.Message) {
	var pubData ppcdatafetchermodels.ReportPubSubData
	marshalErr := json.Unmarshal(msg.Data, &pubData)
	if marshalErr != nil {
		raven.CaptureError(marshalErr, map[string]string{"pub sub message": stringutils.GetStringFromStruct(pubData)})
		log.Error(marshalErr)
		return
	}
	job := pubData.PPCJob
	fmt.Println("==============================================================================", job.ID)
	job.Status = ppcdatafetcherconstants.Pending
	err := mysql.DB.Save(&job).Error
	if err != nil {
		log.Error(err)
		return
	}
	msg.Ack()
	go handleJobRequest(pubData)

}

func handleJobRequest(request ppcdatafetchermodels.ReportPubSubData) {
	err := FetchPPCDataWrapper(request)
	if err != nil {
		log.Error(err)
	}
}
