package ppcdatafetcherconstants

const GetCampaignList = "GetCampaignList"
const GetAdGroupList = "GetAdGroupList"
const GetKeywordsList = "GetKeywordsList"
const GetProdAdsList = "GetProdAdsList"
const GetNegativeKeys = "GetNegativeKeys"

const CampaignReport = "CampaignReport"
const AdGroupReport = "AdGroupReport"
const KeywordReport = "KeywordReport"
const ProdAdsReport = "ProdAdsReport"
const AsinReport = "AsinReport"

const Created = "CREATED"
const Failed = "FAILED"
const Pending = "PENDING"
const Rejected = "REJECTED"
const Completed = "COMPLETED"

var ReportJobTypes []string
var BasicJobTypes []string
var AllJobTypes []string
var UrlMapping map[string]string

func init() {
	ReportJobTypes = append(ReportJobTypes, CampaignReport)
	ReportJobTypes = append(ReportJobTypes, AdGroupReport)
	ReportJobTypes = append(ReportJobTypes, KeywordReport)
	ReportJobTypes = append(ReportJobTypes, ProdAdsReport)
	ReportJobTypes = append(ReportJobTypes, AsinReport)

	BasicJobTypes = append(BasicJobTypes, GetCampaignList)
	BasicJobTypes = append(BasicJobTypes, GetAdGroupList)
	BasicJobTypes = append(BasicJobTypes, GetKeywordsList)
	BasicJobTypes = append(BasicJobTypes, GetProdAdsList)
	BasicJobTypes = append(BasicJobTypes, GetNegativeKeys)

	AllJobTypes = append(AllJobTypes, CampaignReport)
	AllJobTypes = append(AllJobTypes, AdGroupReport)
	AllJobTypes = append(AllJobTypes, KeywordReport)
	AllJobTypes = append(AllJobTypes, ProdAdsReport)
	AllJobTypes = append(AllJobTypes, AsinReport)
	AllJobTypes = append(AllJobTypes, GetCampaignList)
	AllJobTypes = append(AllJobTypes, GetAdGroupList)
	AllJobTypes = append(AllJobTypes, GetKeywordsList)
	AllJobTypes = append(AllJobTypes, GetProdAdsList)
	AllJobTypes = append(AllJobTypes, GetNegativeKeys)

	UrlMapping = make(map[string]string)
	UrlMapping[CampaignReport] = "campaigns"
	UrlMapping[AdGroupReport] = "adGroups"
	UrlMapping[KeywordReport] = "keywords"
	UrlMapping[ProdAdsReport] = "productAds"
	UrlMapping[AsinReport] = "asins"

}
