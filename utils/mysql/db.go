package mysql

import (
	_ "database/sql"
	_ "fmt"
	"log"
	_ "strings"
	_ "time"

	"database/sql"
	"fmt"
	"github.com/getsentry/raven-go"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
)

var (
	db *gorm.DB
	DB *gorm.DB
)

func DropAndRecreate() {
	db_name := viper.GetString("test_db_name")

	CONNECTION_STRING := viper.GetString("db_username") + ":" + viper.GetString("db_password") + "@tcp(" + viper.GetString("mysql_host") + ":3306)/"
	db, err := sql.Open("mysql", CONNECTION_STRING)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	_, err = db.Exec("DROP DATABASE " + db_name)
	if err != nil {
		//panic(err)
	}

	_, err = db.Exec("CREATE DATABASE " + db_name)
	if err != nil {
		//panic(err)
	}
}
func Init(isTest bool) {
	var db_name string
	if isTest == true {
		db_name = viper.GetString("test_db_name")
	} else {
		db_name = viper.GetString("db_name")
	}
	fmt.Println(" db details : ", viper.GetString("db_username")+":"+viper.GetString("db_password")+"@tcp("+viper.GetString("mysql_host")+":3306)/")
	CONNECTION_STRING := viper.GetString("db_username") + ":" + viper.GetString("db_password") + "@tcp(" + viper.GetString("mysql_host") + ":3306)/" + db_name + "?charset=utf8&parseTime=True&loc=Local"
	var err error
	db, err = gorm.Open("mysql", CONNECTION_STRING)
	if err != nil {
		raven.CaptureError(err, map[string]string{"[mysql connection]": CONNECTION_STRING})
		log.Fatal(err)
	}
	db.DB()
	db.DB().Ping()
	db.DB().SetMaxIdleConns(viper.GetInt("max_idle_conns"))
	db.DB().SetMaxOpenConns(viper.GetInt("max_open_conns"))
	db.LogMode(true)
	//bypassing abstraction ,need to remove this later
	DB = db
	//MigrateTables()
}
func setup() {
	// user_list_view_permission:=user.Permission{1,"user_list_view"}
	// admin_role:=user.Role{1,"admin_role",[]user.Permission{user_list_view_permission}}
	// admin_group:=user.Group{1,"admin_group",[]user.Role{admin_role}}
	// user:=user.User{
	//     sellerId :"a@a1.com",
	// 	Groups : []user.Group{admin_group},
	// }
	// db.Create(&user)
}
func MigrateTables() {
	//db.AutoMigrate(&amazonmodel.ProfileDetails{},&amazonmodel.CampaignDetails{},&amazonmodel.AdGroupDetails{})
}

func Create(entity interface{}) error {
	err := db.Create(entity).Error
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

// func FirstOrCreate(entity interface{}) error {
// 	err := db.FirstOrCreate(entity,&entity).Error
// 	if err != nil {
// 		log.Println(err)
// 		return err
// 	}
// 	return nil
// }

func ReadWithMap(filter map[string]interface{}, entity interface{}) (interface{}, error) {
	db.Where(filter).Find(&entity)
	return entity, nil
}

func Update(entity interface{}) error {
	if err := db.Save(entity).Error; err != nil {
		return err
	}
	return nil
}

func Delete(entity interface{}) error {

	if err := db.Delete(entity).Error; err != nil {
		return err
	}
	return nil

}

// func EditGroup(group user.Group) error {
// 	if err := db.Save(group).Error; err != nil {
// 		return err
// 	}
// 	return nil
// }
// func EditRole(role user.Role) error {
// 	if err := db.Save(role).Error; err != nil {
// 		return err
// 	}
// 	return nil
// }
// func EditPermission(permission user.Permission) error {
// 	if err := db.Save(permission).Error; err != nil {
// 		return err
// 	}
// 	return nil
// }
