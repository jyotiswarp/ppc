package stringutils

import (
	"encoding/json"
	"github.com/getsentry/raven-go"
	"github.com/prometheus/common/log"
)

func GetStringFromStruct(body interface{}) (string) {
	out, err := json.Marshal(body)
	if err != nil {
		log.Error(err)
		raven.CaptureError(err, nil)
		return ""
	}

	return string(out)
}
