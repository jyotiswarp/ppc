package google_storage

import (
	log "github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/storage/v1"
	"mime/multipart"
	"net/http"
	"sync"
	"time"
	"github.com/getsentry/raven-go"
)

const scope = storage.DevstorageFullControlScope

const (
	ppcBucketName = "ppc_amazon_reports"
)

var once sync.Once
var client *http.Client

func init() {
	GetStorageClient()
}

func GetStorageClient() *http.Client {
	once.Do(func() {
		createClient()
	})
	return client
}

func createClient() {
	client = &http.Client{Timeout: time.Second * 30}
}

func UploadToCloudPPCReport(file multipart.File, filename, contentType string) (err error) {

	object := &storage.Object{Name: filename, ContentType: contentType}

	client, err := google.DefaultClient(context.Background(), scope)
	if err != nil {
		raven.CaptureError(err, map[string]string{"[Google Storage: client creation]": filename})
		log.Error("Unable to get default client: %v", err)
		return
	}

	service, err := storage.New(client)
	if err != nil {
		raven.CaptureError(err, map[string]string{"[Google Storage:storage]": filename})
		log.Error("Unable to create storage service: %v", err)
		return
	}
	res, err := service.Objects.Insert(ppcBucketName, object).Media(file).PredefinedAcl("publicRead").Do()
	if err != nil {
		raven.CaptureError(err, map[string]string{"[Google Storage:inserting]": filename})
		log.Error(err)
		return
	}
	if res != nil {
		return nil
	} else {
		return
	}
}
