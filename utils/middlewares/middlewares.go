package middlewares

import (
	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofileservices"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	log "github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
	"reflect"
	"strconv"
)

var body string

const PpcManagementCommons = "ppc_management_commons"
const Pagination = "pagination"

func respondWithError(code int, message string, c *gin.Context) {
	resp := map[string]string{"error": message}
	c.JSON(code, resp)
	c.Abort()
}

func Validator(v interface{}) gin.HandlerFunc {
	return func(c *gin.Context) {
		a := reflect.New(reflect.TypeOf(v)).Interface()
		err := c.Bind(a)
		if err != nil {
			log.Error(err)
			respondWithError(401, err.Error(), c)
			return
		}
		c.Set("form_data", a)
		c.Next()
	}
}

func PPCRequestMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		sellerId := c.GetHeader("sellerId")
		geo := c.GetHeader("geo")
		marketPlace := c.GetHeader("marketplace")
		if geo == "" || sellerId == "" || marketPlace == "" {
			respondWithError(400, "sellerID/geo/marketplace is missing", c)
			return
		}

		profile, err := marketprofileservices.GetSellerProfile(marketPlace, geo, sellerId, "")
		if err != nil {
			respondWithError(400, "invalid profile details", c)
			return
		}
		profileIdStr := strconv.Itoa(int(profile.ProfileId))
		ppcCommons := ppcmanagementcommons.PPCManagementCommon{
			Marketplace:  marketPlace,
			Geo:          geo,
			SellerID:     sellerId,
			Profile:      profile,
			ProfileID:    profile.ProfileId,
			ProfileIDStr: profileIdStr,
		}
		c.Set(PpcManagementCommons, ppcCommons)
		c.Next()
	}
}

func PaginationValidator() gin.HandlerFunc {
	return func(c *gin.Context) {
		pageString := c.Query("page")
		limitString := c.Query("limit")
		var err error
		page := 0    //default
		limit := 100 //default
		if pageString != "" {
			page, err = strconv.Atoi(pageString)
			if err != nil {
				respondWithError(400, "Invalid page string", c)
				return
			}
			if page > 0 {
				page--
			}
		}
		if limitString != "" {
			limit, err = strconv.Atoi(limitString)
			if err != nil {
				respondWithError(400, "Invalid limit string", c)
				return
			}
		}
		pagination := ppcmanagementcommons.Pagintion{
			Page:  page,
			Limit: limit,
		}
		c.Set(Pagination, pagination)
		c.Next()
	}
}
