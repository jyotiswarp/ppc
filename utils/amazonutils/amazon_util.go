package amazonutils

import (
	"regexp"
	"strings"
)

var IsAlphaNumeric = regexp.MustCompile(`^[a-zA-Z0-9]+$`).MatchString
var IsNumeric = regexp.MustCompile(`^[0-9]+$`).MatchString

func IsAsin(str string) bool {
	str = strings.TrimSpace(str)
	in := strings.Index(str, " ")
	if (in >= 0 ) || len(strings.Split(str, "")) != 10 {
		return false
	}
	first := string([]rune(str)[0])
	if ! IsAlphaNumeric(str) {
		return false
	}
	if !IsNumeric(first) && first != "B" && first != "b" {
		return false
	}

	return true
}
