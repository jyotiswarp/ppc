package math

import (
	"crypto/rand"
	"fmt"
	"strconv"
)

var (
	alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
)

func Random(i int) string {
	bytes := make([]byte, i)
	for {
		rand.Read(bytes)
		for i, b := range bytes {
			bytes[i] = alphanum[b%byte(len(alphanum))]
		}
		return string(bytes)
	}
	return "ughwhy?!!!"
}

func ParseFloatFromString(value string, precisionDigits int) (float64, error) {
	v, _ := strconv.ParseFloat(value, 64)
	precision := "%." + strconv.Itoa(precisionDigits) + "f"
	return strconv.ParseFloat(fmt.Sprintf(precision, v), 64)
}

func RoundFloat(value float64, precisionDigits int) (float64, error) {
	precision := "%." + strconv.Itoa(precisionDigits) + "f"
	return strconv.ParseFloat(fmt.Sprintf(precision, value), 64)
}

func RoundFloatStringToPrecision(value string, precisionDigits int) string {
	v, _ := strconv.ParseFloat(value, 64)
	precision := "%." + strconv.Itoa(precisionDigits) + "f"
	return fmt.Sprintf(precision, v)
}

func Min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}

func MinFloat(a, b float64) float64 {
	if a <= b {
		return a
	}
	return b
}

func Max(a, b int) int {
	if a >= b {
		return a
	}
	return b
}

func MaxFloat(a, b float64) float64 {
	if a >= b {
		return a
	}
	return b
}
