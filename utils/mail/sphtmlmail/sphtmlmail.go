package sphtmlmail

import (
	"bitbucket.org/jyotiswarp/ppc/utils/mail"
	"bitbucket.org/jyotiswarp/ppc/utils/mail/themes"
	//"bitbucket.org/sellerprimehub/usermanagment/utils/request"
	"github.com/prometheus/common/log"
	"github.com/rockycoder/hermes"
)

type HtmlMailConfig struct {
	ReceiverName           string
	SenderName             string
	Subject                string
	To                     []string
	From                   string
	Bcc                    string
	Cc                     string
	IntroText              []string
	MiddleIntrosText       []string
	ButtonText             string
	ButtonLink             string
	ButtonInstruction      string
	Outros                 []string
	Signature              string
	TableData              [][]KeyValue
	Data                   interface{}
	AttachmentName         string
	DictionaryData         map[string]string
	Images                 []Image
	Theme                  hermes.Theme
	ProductInfo            []hermes.ProductInfo
	ProductInfoAfterIntros []hermes.ProductInfo
	Provider               string
	TopButton              []TopButton
	MailTitle              string
	TitleImage             string
	LearnMoreLink          string
}

type TopButton struct {
	Text  string
	Color string
	Link  string
}

type Image struct {
	ImageLink string
	HyperLink string
	Style     string
}

type KeyValue struct {
	Key   string
	Value string
}

func InitializeMail(theme hermes.Theme) hermes.Hermes {

	return hermes.Hermes{
		// Optional Theme
		Theme: theme,
		Product: hermes.Product{
			// Appears in header & footer of e-mails
			Name: "SellerPrime Team",
			Link: "https://sellerprime.com",
			// Optional product logo
			Logo:      "https://storage.googleapis.com/sellerprime_images/sp-logotype-white.png",
			Copyright: "Copyright © 2017 SellerPrime. All rights reserved.",
		},
	}

}

func creatEmailBody(mailConfig HtmlMailConfig) (string, string) {
	var hm hermes.Hermes
	if mailConfig.Theme == nil {
		var theme themes.SPActivityTheme
		hm = InitializeMail(&theme)
	} else {
		hm = InitializeMail(mailConfig.Theme)
	}

	hEmail := hermes.Email{
		Body: hermes.Body{
			Name:         mailConfig.ReceiverName,
			Intros:       mailConfig.IntroText,
			Outros:       mailConfig.Outros,
			Signature:    mailConfig.Signature,
			MiddleIntros: mailConfig.MiddleIntrosText,
			Greeting:     "Hey",
			Title:        mailConfig.MailTitle,
			//TitleImage:    mailConfig.TitleImage,
			//LearnMoreLink: mailConfig.LearnMoreLink,
		},
	}

	if len(mailConfig.Images) > 0 {
		images := []hermes.Image{}
		for _, image := range mailConfig.Images {
			image := hermes.Image{
				HyperLink: image.HyperLink,
				ImageLink: image.ImageLink,
				Style:     image.Style,
			}
			images = append(images, image)
		}
		hEmail.Body.Images = images
	}

	if mailConfig.ButtonText != "" {
		hEmail.Body.Actions = []hermes.Action{
			{
				Instructions: mailConfig.ButtonInstruction,
				Button: hermes.Button{
					Text:  mailConfig.ButtonText,
					Link:  mailConfig.ButtonLink,
					Color: "#FF9D3B",
				},
			},
		}
	}
	if len(mailConfig.TableData) > 0 {
		mailTable := hermes.Table{}
		tableData := [][]hermes.Entry{}
		for _, row := range mailConfig.TableData {
			var tableRow []hermes.Entry
			for _, val := range row {
				tableRow = append(tableRow, hermes.Entry{Key: val.Key, Value: val.Value})
			}
			tableData = append(tableData, tableRow)
		}
		mailTable.Data = tableData
		hEmail.Body.Table = mailTable
	}
	if len(mailConfig.DictionaryData) > 0 {
		var dicData []hermes.Entry
		for key, value := range mailConfig.DictionaryData {
			dicData = append(dicData, hermes.Entry{Key: key, Value: value})
		}
		hEmail.Body.Dictionary = dicData
	}
	if len(mailConfig.ProductInfo) > 0 {
		hEmail.Body.ProductInfoInTop = mailConfig.ProductInfo
	}
	if len(mailConfig.ProductInfoAfterIntros) > 0 {
		hEmail.Body.ProductInfoAfterIntros = mailConfig.ProductInfoAfterIntros
	}
	if len(mailConfig.TopButton) > 0 {
		button := hermes.Button{Text: mailConfig.TopButton[0].Text,
			Color: mailConfig.TopButton[0].Color,
			Link:  mailConfig.TopButton[0].Link,
		}
		action := hermes.Action{Button: button}
		hEmail.Body.TopButton = append(hEmail.Body.TopButton, action)
	}
	// Generate an HTML email with the provided contents (for modern clients)
	emailBody, err := hm.GenerateHTML(hEmail)
	if err != nil {
		log.Error(err) // Tip: Handle error with something else than a panic ;)
	}
	plainTextBody, err := hm.GeneratePlainText(hEmail)
	if err != nil {
		log.Error(err) // Tip: Handle error with something else than a panic ;)
	}
	return emailBody, plainTextBody
}

func CreateMailAndSend(mailConfig HtmlMailConfig) string {
	emailBody, plainTextBody := creatEmailBody(mailConfig)
	SendMail(mailConfig, emailBody, plainTextBody)
	return emailBody
}

func SendMail(mailConfig HtmlMailConfig, emailBody, plainText string) {
	for _, to := range mailConfig.To {
		var mailStruct mail.MailDtoV2
		mailStruct.To = to
		mailStruct.Subject = mailConfig.Subject
		mailStruct.From = mailConfig.From
		mailStruct.Text = emailBody
		mailStruct.Type = "activity"
		mailStruct.MailType = "html"
		mailStruct.Provider = mailConfig.Provider
		mailStruct.SenderName = mailConfig.SenderName
		mailStruct.AltBody = plainText

		//if !strings.Contains(to, "@sellerprime.com") {
		//	bccMap := make(map[string]string)
		//	bccMap["gaurav@sellerprime.com"] = "demo1"
		//	mailStruct.BCC = bccMap
		//}
		if mailConfig.Cc != "" {
			ccMap := make(map[string]string)
			ccMap[mailConfig.Cc] = "ankitha"
			mailStruct.CC = ccMap
		}

		mail.SendMailV2(mailStruct)
	}

}
