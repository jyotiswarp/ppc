package themes

// Default is the theme by default
type SPSimpleHtml struct{}

// Name returns the name of the default theme
func (dt *SPSimpleHtml) Name() string {
	return "ActivityTheme"
}

// HTMLTemplate returns a Golang template that will generate an HTML email.
func (dt *SPSimpleHtml) PlainTextTemplate() string {
	return `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css" rel="stylesheet" media="all">
        /* Base ------------------------------ */
        *:not(br):not(tr):not(html) {
            font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
        }
        blockquote {
            margin: 1.7rem 0;
            padding-left: 0.85rem;
            border-left: 10px solid #F0F2F4;
        }
        blockquote p {
            font-size: 1.1rem;
            color: #999;
        }
        blockquote cite {
            display: block;
            text-align: right;
            color: #666;
            font-size: 1.2rem;
        }
        cite {
            display: block;
            font-size: 0.925rem;
        }
        cite:before {
            content: "\2014 \0020";
        }
        /* Data table ------------------------------ */
        .data-wrapper {
            width: 100%;
            margin: 0;
            padding: 1px 0;
        }
        .data-table {
            width: 100%;
            margin: 0;
        }
        .data-table th {
            text-align: left;
            padding: 0px 5px;
            padding-bottom: 8px;
            border-bottom: 1px solid #EDEFF2;
        }
        .data-table th p {
            margin: 0;
            color: #9BA2AB;
            font-size: 12px;
        }
        .data-table td {
            padding: 10px 5px;
            color: #74787E;
            font-size: 15px;
            line-height: 5px;
        }
        /* Buttons ------------------------------ */
        /*Media Queries ------------------------------ */
        @media only screen and (max-width: 600px) {
            .email-body_inner,
            .email-footer {
                width: 100% !important;
            }
        }
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>
<body dir="{{.Hermes.TextDirection}}" style="height: 100%;margin: 0;line-height: 1.2em;background-color: #F2F4F6;color: #74787E;-webkit-text-size-adjust: none;">
<table class="email-wrapper"cellpadding="0" cellspacing="0" style="width: 60%;margin: auto;padding: 0;background-color: #F2F4F6;">
    <tr>
        <td class="content" style="align: center;padding: 0;color: #74787E;font-size: 15px;line-height: 1.2em;">
            <table class="email-content" width="100%" cellpadding="0" cellspacing="0" style="width: 100%;margin: 0;padding: 0;">
				<!-- Logo -->
                <tr>
                    <td class="email-masthead" style="background-color: white;height: 100px;padding: 1px 0;text-align: center;color: #74787E;font-size: 15px;line-height: 1.2em;">
                        <a class="email-masthead_name" href="{{.Hermes.Product.Link}}" target="_blank" style="padding-left: 25%;font-size: 18px;font-weight: bold;color: #2F3133;text-decoration: none;text-shadow: 0 1px 0 white;">
                            {{ if .Hermes.Product.Logo }}
                            <img src="{{.Hermes.Product.Logo | url }}" class="email-logo" style="margin: auto;max-height: 80px;float: left;padding-left:23px;" />
                            {{ else }}
                            {{ .Hermes.Product.Name }}
                            {{ end }}
                        </a>
						{{ with .Email.Body.TopButton }}
						{{ if gt (len .) 0 }}
						{{ range $action := . }}
							   <a href="{{ $action.Button.Link }}" target="_blank" style="float:right;margin-top: 5%;font-size: 18px;text-decoration: none;color: #3869D4;padding-right:23px">
									{{ $action.Button.Text }}
							   </a>
						{{ end }}
						{{ end }}
						{{ end }}
					</td>
                </tr>
	            <hr size="1" style="border-color:#cccccc">
                <!-- Email Body -->
                <tr>
                    <td class="email-body" width="100%" style="width: 100%;margin: 0;padding: 0;background-color: #FFF;color: #74787E;font-size: 15px;line-height: 1.2em;">
                        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" style="width: 570px;margin: 0 auto;padding: 0;">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell" style="padding: 35px;color: #74787E;font-size: 15px;line-height: 1.2em;">
                                    <p style="margin-top: 0;color: #74787E;font-size: 18px;line-height: 1.2em;">{{if .Email.Body.Title }}{{ .Email.Body.Title }}{{ else }}{{ .Email.Body.Greeting }},{{ end }}</p>
                                    {{ with .Email.Body.ProductInfoInTop }}
                                    <table  >
                                        <tbody>
                                        <tr>
                                            <td width=40% style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.2em;">
                                                {{ if gt (len .) 0 }}
                                                {{ range $line := . }}
                                                <img src={{ $line.Image }} width=130px height=130px/>
                                            </td>
                                            <td width=60% style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.2em;">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">{{ $line.Name }}</p>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">ASIN: {{ $line.ASIN }}</p>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">Brand: {{ $line.Brand }}</p>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">BSR: {{ $line.BSR }}</p>
                                                        {{ if $line.LQI }}
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">LQI: {{ $line.LQI }}</p>
                                                        {{ end }}
                                                        {{ if $line.RevenuePotential }}
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">Revenue Potential: {{ $line.RevenuePotential }}</p>
                                                        {{ end }}

                                                    </tr>
                                                </table>
                                                {{ end }}
                                                {{ end }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    {{ end }}

									{{ with .Email.Body.Intros }}
									{{ if gt (len .) 0 }}
									{{ range $line := . }}
											<table border="0" cellpadding="0" cellspacing="0" class="table-spacing" style="width: 100%;">
												<tbody>
												<tr>
													<td align="center" style="padding: 10px 5px;color: #74787E;font-size: 18px;line-height: 1.6em;">
														<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
															<tbody>
															<tr>
																<td align="left" style="color:#555c6a;font-family:Helvetica,Arial,sans-serif;font-size:18px;line-height:1.2em;font-weight:normal padding: 10px 5px;" width="480">{{ $line }}</td>
															</tr>
															</tbody>
															</table>
													</td>
												</tr>
												</tbody>
											</table>
									{{ end }}
									{{ end }}
									{{ end }}

                                    {{ with .Email.Body.ProductInfoAfterIntros }}
                                    <table style="width: 100%;">
                                        <tbody>
                                        <tr>
                                            <td width=40% style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.8em;">
                                                {{ if gt (len .) 0 }}
                                                {{ range $line := . }}
                                                <img src={{ $line.Image }} width=130px height=130px/>
                                            </td>
                                            <td width=60% style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.2em;">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">{{ $line.Name }}</p>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">ASIN: {{ $line.ASIN }}</p>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">Brand: {{ $line.Brand }}</p>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">BSR: {{ $line.BSR }}</p>
                                                        {{ if $line.LQI }}
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">LQI: {{ $line.LQI }}</p>
                                                        {{ end }}
                                                        {{ if $line.RevenuePotential }}
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 18px;line-height: 1.2em;">Revenue Potential: {{ $line.RevenuePotential }}</p>
                                                        {{ end }}
                                                    </tr>
                                                </table>
                                                {{ end }}
                                                {{ end }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    {{ end }}

									{{ with .Email.Body.MiddleIntros }}
									{{ if gt (len .) 0 }}
									{{ range $line := . }}
										<table border="0" cellpadding="0" cellspacing="0" class="table-spacing" style="width: 100%;">
											<tbody>
											<tr>
												<td align="center" style="padding: 10px 5px;color: #74787E;font-size: 18px;line-height: 1.6em;">
													<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
														<tbody>
														<tr>
															<td align="left" style="color:#555c6a;font-family:Helvetica,Arial,sans-serif;font-size:18px;line-height:1.2em;font-weight:normal padding: 10px 5px;" width="480">{{ $line }}</td>
														</tr>
														</tbody>
														</table>
												</td>
											</tr>
											</tbody>
									    </table>
									{{ end }}
									{{ end }}
									{{ end }}

                                    {{ with .Email.Body.Images }}
                                    {{ if gt (len .) 0 }}
                                    {{ range $line := . }}
									<table style="display: inline-block; width: 100%;">
									<tr>
									<td>
										<a href='{{ $line.HyperLink }}' target='_self' style="color: #3869D4;"><img src='{{ $line.ImageLink }}' style='display: block; margin: 0 auto;'></img></a>
									</td>
									</tr>
									</table>
                                    <table>
                                    <tr>
               							 <td colspan="2" height="10"></td>
           							</tr>
           							</table>
                                    {{ end }}
                                    {{ end }}
                                    {{ end }}
                                    {{ if (ne .Email.Body.FreeMarkdown "") }}
                                    {{ .Email.Body.FreeMarkdown.ToHTML }}
                                    {{ else }}

                                    {{ with .Email.Body.Dictionary }}
                                    {{ if gt (len .) 0 }}
                                    {{ range $entry := . }}
                                    <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="table-spacing" width="100%">
								    <tr>
               							 <td colspan="2" height="10"></td>
           							</tr>
									<td align="left"  height="40" valign="top" width="40%">
										<table border="0" cellpadding="0" cellspacing="0" class="table-spacing" width="50%">
											 <tr>
												<td width="30"></td>
												<td align="left" style="color:#2e3543;font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:1.2em;font-weight:bold;text-transform:uppercase">
													<table border="0" cellpadding="0" cellspacing="0" width="90%">
														<tr>
															<td colspan="2" height="37" style="line-height:1.2em;font-size:67px"></td>
														</tr>
														<tr>
															<td align="left" style="color:#2e3543;font-family:Arial,Helvetica,sans-serif;font-size:14px;line-height:1.2em;font-weight:bold;text-transform:uppercase;letter-spacing:1px">{{ $entry.Key }}</td>
														</tr>
														<tr>
															<td colspan="2" height="37" style="line-height:1.2em;font-size:67px"></td>
														</tr>
													</table>
												</td>
										</table>
									</td>
									<td align="right" bgcolor="#4CBAF6" class="right-box" height="40" valign="top" width="40%">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td bgcolor="#4CBAF6" colspan="2" height="37" style="line-height:1.2em;font-size:67px"></td>
											</tr>
											<tr>
												<td align="center" style="color:#fdfdfd;font-family:Helvetica,Arial,sans-serif;font-size:32px;line-height:1.2em;font-weight:bold">{{ $entry.Value }}</td>
											</tr>
											<tr>
												<td bgcolor="#4CBAF6" colspan="2" height="37" style="line-height:1.2em;font-size:67px"></td>
											</tr>

										</table>
									</td>
									<tr>
               							 <td colspan="2" height="10"></td>
           							</tr>
								</table>
									{{ end }}
									{{ end }}
									{{ end }}
                            <!-- Table -->
                            {{ with .Email.Body.Table }}
                            {{ $data := .Data }}
                            {{ $columns := .Columns }}
                            {{ if gt (len $data) 0 }}
									<table class="data-wrapper" width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td colspan="2" style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.2em;">
												<table class="data-table" width="100%" cellpadding="0" cellspacing="0">
													<tr>
														{{ $col := index $data 0 }}
														{{ range $entry := $col }}
														<th style=" padding: 0px 5px;padding-bottom: 8px;border-bottom: 1px solid #EDEFF2;"
																{{ with $columns }}
																{{ $width := index .CustomWidth $entry.Key }}
																{{ with $width }}
																width="{{ . }}"
																{{ end }}
																{{ $align := index .CustomAlignement $entry.Key }}
																{{ with $align }}
																style="text-align:{{ . }}"
																{{ end }}
																{{ end }}
														>
															<p style="margin-top: 0;color: #74787E;font-size: 18px;line-height: 1.2em;">{{ $entry.Key }}</p>
														</th>
														{{ end }}
													</tr>
													{{ range $row := $data }}
													<tr>
														{{ range $cell := $row }}
														<td style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.2em;"
																{{ with $columns }}
																{{ $align := index .CustomAlignement $cell.Key }}
																{{ with $align }}
																style="text-align:{{ . }}"
																{{ end }}
																{{ end }}
														>
															{{ $cell.Value }}
														</td>
														{{ end }}
													</tr>
													{{ end }}
												</table>
											</td>
										</tr>
									</table>
                            {{ end }}
                            {{ end }}
							<!-- Action -->

							{{ with .Email.Body.Actions }}
							{{ if gt (len .) 0 }}
							{{ range $action := . }}
								<p align="center" style="margin-top: 0;color: #74787E;font-size: 18px;line-height: 1.2em;">{{ $action.Instructions }}</p>
								<table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0" style="width: 100%;margin: 30px auto;padding: 0;text-align: left;">
								  <tr>
									<td colspan="2" height="5"></td>
								  </tr>
									<tr>
										<td align="center" style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.2em;">

												<a href="{{ $action.Button.Link }}" class="button" style="background-color: {{ $action.Button.Color }};display: inline-block;width: 100%;border-radius: 3px;color: #ffffff;font-size: 15px;line-height: 3.2em;text-align: center; text-decoration: none;-webkit-text-size-adjust: none;mso-hide: all;" target="_blank">
													{{ $action.Button.Text }}
												</a>

										</td>
									</tr>
								</table>
							{{ end }}
							{{ end }}
							{{ end }}
                            {{ end }}
                            {{ with .Email.Body.Outros }}
                            {{ if gt (len .) 0 }}
                            {{ range $line := . }}
                            <p style="margin-top: 0;color: #74787E;font-size: 18px;line-height: 1.2em;">{{ $line }}</p>
                            {{ end }}
                            {{ end }}
                            {{ end }}

                            <p style="margin-top: 0;color: #74787E;font-size: 18px;line-height: 1.2em;">
                                {{.Email.Body.Signature}},
                                <br />
                                {{.Hermes.Product.Name}}
                            </p>

                            {{ if (eq .Email.Body.FreeMarkdown "") }}
                            {{ with .Email.Body.Actions }}
                            <table class="body-sub" style="margin-top: 25px;padding-top: 25px;border-top: 1px solid #EDEFF2;table-layout: fixed;">
                                <tbody>
                                {{ range $action := . }}
                                <tr>
                                    <td style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.2em;">
                                        <p class="sub" style="margin-top: 0;color: #74787E;line-height: 1.2em;font-size: 14px;">{{$.Hermes.Product.TroubleText | replace "{ACTION}" $action.Button.Text}}</p>
                                        <p class="sub" style="margin-top: 0;color: #74787E;line-height: 1.2em;font-size: 14px;"><a style="color: #3869D4;word-break: break-all;" href="{{ $action.Button.Link }}">{{ $action.Button.Link }}</a></p>
                                    </td>
                                </tr>
                                {{ end }}
                                </tbody>
                            </table>
                            {{ end }}
                            {{ end }}
                            </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.2em;">
                        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" style="width: 570px;margin: 0 auto;padding: 0;text-align: center;">
                            <tr>
                                <td class="content-cell" style="padding: 10px;color: #74787E;font-size: 15px;line-height: 1.2em;">
									{{ with .Email.Body.TopButton }}
									{{ if gt (len .) 0 }}
									{{ range $action := . }}
                                    <p class="sub center" style="margin-top: 0;color: #AEAEAE;font-size: 14px;line-height: 1.2em;text-align: center;">
                                        If you wish to unsubscribe <a style="color:#AEAEAE;" href="{{ $action.Button.Link }}?unsubscribe=true">click here</a>
                                    </p>
									{{ end }}
									{{ end }}
									{{ end }}
									<p class="sub center" style="margin-top: 0;color: #AEAEAE;font-size: 14px;line-height: 1.2em;text-align: center;">
										03-2794, 513 Ang Mo Kio Ave 8, Singapore - 560513
                                    </p>
									<p class="sub center" style="margin-top: 0;color: #AEAEAE;font-size: 14px;line-height: 1.2em;text-align: center;">
										<a style="color:#3869D4;" href="https://sellerprime.com">www.sellerprime.com</a>
                                    </p>
									<p class="sub center" style="margin-top: 0;color: #AEAEAE;font-size: 14px;line-height: 1.2em;text-align: center;">
										View our <a style="color:#3869D4;" href="https://www.sellerprime.com/privacy.html">privacy policy</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
`
}

// PlainTextTemplate returns a Golang template that will generate an plain text email.
func (dt *SPSimpleHtml) HTMLTemplate() string {
	return `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css" rel="stylesheet" media="all">
        /* Base ------------------------------ */
        *:not(br):not(tr):not(html) {
            font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
        }
        blockquote {
            margin: 1.7rem 0;
            padding-left: 0.85rem;
            border-left: 10px solid #F0F2F4;
        }
        blockquote p {
            font-size: 1.1rem;
            color: #999;
        }
        blockquote cite {
            display: block;
            text-align: right;
            color: #666;
            font-size: 1.2rem;
        }
        cite {
            display: block;
            font-size: 0.925rem;
        }
        cite:before {
            content: "\2014 \0020";
        }
        /* Data table ------------------------------ */
        .data-wrapper {
            width: 100%;
            margin: 0;
            padding: 1px 0;
        }
        .data-table {
            width: 100%;
            margin: 0;
        }
        .data-table th {
            text-align: left;
            padding: 0px 5px;
            padding-bottom: 8px;
            border-bottom: 1px solid #EDEFF2;
        }
        .data-table th p {
            margin: 0;
            color: #9BA2AB;
            font-size: 12px;
        }
        .data-table td {
            padding: 10px 5px;
            color: #74787E;
            font-size: 15px;
            line-height: 5px;
        }
        /* Buttons ------------------------------ */
        /*Media Queries ------------------------------ */
        @media only screen and (max-width: 600px) {
            .email-body_inner,
            .email-footer {
                width: 100% !important;
            }
        }
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>
<body>
{{ with .Email.Body.Intros }}
  {{ range $line := . }}
     <p style="margin-top: 0;margin:6px;font-size: 16px;line-height: 1.2em;">{{ $line }}</p>
  {{ end }}
{{ end }}
{{ if (ne .Email.Body.FreeMarkdown "") }}
  {{ .Email.Body.FreeMarkdown.ToHTML }}
{{ else }}
  {{ with .Email.Body.Dictionary }}
    <ul>
    {{ range $entry := . }}
      <li>{{ $entry.Key }}: {{ $entry.Value }}</li>
    {{ end }}
    </ul>
  {{ end }}
  {{ with .Email.Body.Table }}
    {{ $data := .Data }}
    {{ $columns := .Columns }}
    {{ if gt (len $data) 0 }}
      <table class="data-table" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          {{ $col := index $data 0 }}
          {{ range $entry := $col }}
            <th>{{ $entry.Key }} </th>
          {{ end }}
        </tr>
        {{ range $row := $data }}
          <tr>
            {{ range $cell := $row }}
              <td>
                {{ $cell.Value }}
              </td>
            {{ end }}
          </tr>
        {{ end }}
      </table>
    {{ end }}
  {{ end }}
  {{ with .Email.Body.Actions }}
    {{ range $action := . }}
      <p>{{ $action.Instructions }} {{ $action.Button.Link }}</p>
    {{ end }}
  {{ end }}
{{ end }}
{{ with .Email.Body.Outros }}
  {{ range $line := . }}
    <p>{{ $line }}<p>
  {{ end }}
{{ end }}
<p style="margin-top: 0;margin:6px;font-size: 16px;line-height: 1.2em;">{{.Email.Body.Signature}}</p>
</body>
</html>
`
}
