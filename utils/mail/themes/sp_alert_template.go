package themes

// Default is the theme by default
type SPAlertTheme struct{}

// Name returns the name of the default theme
func (dt *SPAlertTheme) Name() string {
	return "ActivityTheme"
}

// HTMLTemplate returns a Golang template that will generate an HTML email.
func (dt *SPAlertTheme) HTMLTemplate() string {
	return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;height:100%;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
 <head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta name="x-apple-disable-message-reformatting">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="telephone=no" name="format-detection">
  <title></title>
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i" rel="stylesheet">
  <style>
@media only screen and (max-width: 800px) {p, ul li, ol li, a { font-size: 16px !important } h1 { font-size: 30px !important; text-align: center } h2 { font-size: 26px !important; text-align: center } h3 { font-size: 20px !important; text-align: center } h1 a { font-size: 30px !important } h2 a { font-size: 26px !important } h3 a { font-size: 20px !important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size: 12px !important } *[class="gmail-fix"] { display: none !important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align: center !important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align: right !important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align: left !important } .es-m-txt-r a img, .es-m-txt-c a img, .es-m-txt-l a img { display: inline !important } .es-button-border { width: 100% !important } .es-button { font-size: 20px !important; width: 100% !important; border-width: 10px 0px 10px 0px !important } .es-btn-fw { border-width: 10px 0px !important; text-align: center !important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width: 100% !important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width: 100% !important; max-width: 800px !important } .es-adapt-td { display: block !important; width: 100% !important } .adapt-img { width: 100% !important; height: auto !important } .es-m-p0 { padding: 0px !important } .es-m-p0r { padding-right: 0px !important } .es-m-p0l { padding-left: 0px !important } .es-m-p0t { } .es-m-p0b { padding-bottom: 0 !important } .es-m-p20b { padding-bottom: 20px !important } .es-hidden { display: none !important } table.es-table-not-adapt { width: auto !important } table.es-social { display: inline-block !important } table.es-social td { display: inline-block !important; padding-bottom: 10px } .logo { position: relative; right: 0px } .headerlogo { text-align: center } .mydashboard { text-align: center } .headerlogo img { display: inline-block } @media only screen and (min-width: 800px) {.md-left { width: 50% !important; float: left } .md-right { width: 50% !important; float: right } .mydashboard { text-align: right } .headerlogo { text-align: left } } }

</style>
  <style>
#outlook a { padding: 0 }
.ExternalClass { width: 100% }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100% }
a[x-apple-data-detectors="true"] { color: inherit !important; text-decoration: none !important }

</style>
 </head>
 <body style="width:100%;height:100%;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
  <div class="es-wrapper-color" style="background-color:rgb(243, 244, 244);">
   <!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" src="" color="#ffffff"></v:fill>
    </v:background>
<![endif]-->
   <table cellpadding="0" cellspacing="0" class="es-wrapper" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;">
    <tbody>
     <tr style="border-collapse:collapse;">
      <td valign="top" class="esd-email-paddings" style="padding:0;Margin:0;">

    <!-- header -->
       <table cellpadding="0" cellspacing="0" class="es-content esd-header-popover" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%;">
        <tbody>
         <tr style="border-collapse:collapse;">
          <td align="center" bgcolor="#3face2" style="padding:0;Margin:0;background-color:rgb(63, 172, 226);">
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="800" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:rgb(255, 255, 255);">
            <tbody>
             <tr style="border-collapse:collapse;">
              <td class="es-p40t es-p20b" align="left" esd-general-paddings-checked="true" bgcolor="#3face2" style="padding:0;Margin:0;padding-bottom:20px;padding-top:40px;background-color:rgb(63, 172, 226);">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tbody>
                 <tr style="border-collapse:collapse;">
                  <td width="800" align="center" valign="top" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                    <tbody>
					{{ if .Hermes.Product.Logo }}

                     <tr style="border-collapse:collapse;">
                      <td style="padding:0;Margin:0;"> <a target="_blank" href="http://www.sellerprime.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:rgb(19, 118, 200);"> <img src="{{.Hermes.Product.Logo | url }}" alt="sellerprime" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="150" title="sellerprime"> </a> </td>
						{{ with .Email.Body.TopButton }}
						{{ if gt (len .) 0 }}
						{{ range $action := . }}
						<td align="right" class="es-p30l es-m-txt-r" style="padding:0;Margin:0;padding-left:30px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:rgb(255, 255, 255);text-align:right;"><a style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:none;color:white;" target="_blank" href="{{ $action.Button.Link }}">{{ $action.Button.Text }}</a></p></td>
						{{ end }}
						{{ end }}
						{{ end }}
                     </tr>

					{{ end }}

                    </tbody>
                   </table> </td>
                 </tr>
                </tbody>
               </table> </td>
             </tr>
			{{ if .Email.Body.TitleImage }}
             <tr style="border-collapse:collapse;">
              <td class="es-p20t es-p20b" align="left" esd-general-paddings-checked="true" style="padding:0;Margin:0;padding-top:20px;padding-bottom:20px;background-color:rgb(255, 255, 255);" bgcolor="#ffffff">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tbody>
                 <tr style="border-collapse:collapse;">
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                    <tbody>
                     <tr style="border-collapse:collapse;">
                      <td align="center" style="padding:0;Margin:0;"> <a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:rgb(19, 118, 200);"> <img src="{{ .Email.Body.TitleImage }}" alt="" width="220" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"> </a> </td>
                     </tr>
                    </tbody>
                   </table> </td>
                 </tr>
                </tbody>
               </table> </td>
             </tr>
		 {{ end }}
            </tbody>
           </table> </td>
         </tr>
        </tbody>
       </table>

       <!-- header end -->

       <table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%;">
        <tbody>
         <tr style="border-collapse:collapse;">
          <td align="center" bgcolor="#f3f4f4" style="padding:0;Margin:0;background-color:rgb(243, 244, 244);">
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="800" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:rgb(255, 255, 255);">
            <tbody>
             <tr style="border-collapse:collapse;">
              <td class="es-p5b es-p10l" align="left" esd-general-paddings-checked="true" style="padding:0;Margin:0;padding-bottom:5px;padding-left:10px;">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tbody>
                 <tr style="border-collapse:collapse;">
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;">

                    <!-- header title -->
                   <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                    <tbody>
                     <tr style="border-collapse:collapse;">
                      <td align="center" style="padding:0;Margin:0;padding-bottom:15px"> <h1 style="Margin:0;line-height:120%;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:30px;font-style:normal;font-weight:normal;color:#646d8c;"><span style="font-size:26px;"><span style="line-height:150%;">{{if .Email.Body.Title }}{{ .Email.Body.Title }}{{end}}</span></span></h1></td>
                     </tr>
                     <!-- header title end -->

                     <!-- Intros  -->
					{{ with .Email.Body.Intros }}
					{{ if gt (len .) 0 }}
					{{ range $line := . }}

                     <tr style="border-collapse:collapse;">
                      <td align="left" class="es-m-txt-c es-p30t es-p15r es-p15l" style="padding:0;Margin:0;padding-right:15px;padding-top:15px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#646d8c;"><span style="font-size:16px;line-height:150%;">{{ $line }}</span></p></td>
                     </tr>
					{{end}}
					{{end}}
					{{end}}
                     <!-- Intros end -->

                    </tbody>
                   </table>
                 </td>
                 </tr>
                </tbody>
               </table> </td>
             </tr>
	         {{ with .Email.Body.ProductInfoInTop }}
                                    <table style="width: 85%;" align="center">
                                        <tbody>
                                        <tr>
                                            <td width=20% style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.8em;">
                                                {{ if gt (len .) 0 }}
                                                {{ range $line := . }}
                                                <img alt="product image" src={{ $line.Image }} width=130px height=130px/>
                                            </td>
                                            <td width=80% style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.2em;">
                                                <table style="width: 100%;">
                                                    <tr>
														<p style="margin-top: 0;margin:6px;color: #74787E;font-size: 16px;line-height: 1.2em;">{{ $line.Name }}</p>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 15px;line-height: 1.2em;">ASIN: {{ $line.ASIN }}</p>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 15px;line-height: 1.2em;">Brand: {{ $line.Brand }}</p>
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 15px;line-height: 1.2em;">BSR: {{ $line.BSR }}</p>
                                                        {{ if $line.LQI }}
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 15px;line-height: 1.2em;">LQI: {{ $line.LQI }}</p>
                                                        {{ end }}
                                                        {{ if $line.RevenuePotential }}
                                                        <p style="margin-top: 0;margin:6px;color: #74787E;font-size: 15px;line-height: 1.2em;">Revenue Potential: {{ $line.RevenuePotential }}</p>
                                                        {{ end }}
                                                    </tr>
                                                </table>
                                                {{ end }}
                                                {{ end }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
             {{ end }}

			{{ with .Email.Body.Dictionary }}
             <tr style="border-collapse:collapse;">
              <td align="left" esd-general-paddings-checked="true" style="padding:0;Margin:0;">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tbody>
                 <tr style="border-collapse:collapse;">
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                    <tbody>
                     <tr style="border-collapse:collapse;">
                      <td align="center" style="padding:0;Margin:0;">
                        <!-- Dictionary Data -->
                       <table border="0" cellspacing="1" cellpadding="1" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;height:0px;width:580px;margin-top:30px" class="cke_show_border">
                        <tbody>
                         <tr style="border-collapse:collapse;">
						{{ with .Email.Body.Dictionary }}
                        {{ if gt (len .) 0 }}
                        {{ range $entry := . }}
                          <td style="padding:0;Margin:0;text-align:center;color:#646d8c;width:25%;font-size:36px">{{ $entry.Value }}</td>
						{{end}}
						{{end}}
						{{end}}
                         </tr>
						 <tr style="border-collapse:collapse;">
						{{ with .Email.Body.Dictionary }}
                        {{ if gt (len .) 0 }}
                        {{ range $entry := . }}
                          <td style="padding:0;Margin:0;text-align:center;width:25%;vertical-align:top;padding:0 5px;"><span style="font-size:13px;color:#444961;">{{ $entry.Key }}</span></td>
						{{end}}
						{{end}}
						{{end}}
                         </tr>
                        </tbody>
                       </table>
                       <!-- Dictionary Data End-->
                     </tr>
                    </tbody>
                   </table> </td>
                 </tr>
                </tbody>
               </table> </td>
             </tr>
			{{ end }}
		{{ with .Email.Body.MiddleIntros }}
             <tr style="border-collapse:collapse;">
              <td align="left" esd-general-paddings-checked="true" style="padding:0;Margin:0;">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tbody>
                 <tr style="border-collapse:collapse;">
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;padding-top: 25px;">

                   <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                    <tbody>
					{{ with .Email.Body.MiddleIntros }}
					{{ if gt (len .) 0 }}
					{{ range $line := . }}
										<!-- Middle Intros text -->
                     <tr style="border-collapse:collapse;">
                      <td align="left"  class="es-p15r es-p15l" style="padding:0;Margin:0;padding-left:15px;padding-right:15px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#646d8c;"><span style="font-size:15px;line-height:150%;">{{ $line }}</span></p></td>
                     </tr>
					 {{end}}
					{{end}}
					{{end}}
										  <!-- Middle Intros text end -->
                    </tbody>
                   </table> </td>
                 </tr>
                </tbody>
               </table> </td>
             </tr>
			{{ end }}
			{{ with .Email.Body.Images }}
			{{ if gt (len .) 0 }}
			{{ range $line := . }}
			 <tr>
				<td class="esd-structure es-p40t es-p20b" align="left" esd-general-paddings-checked="true">
					<table cellpadding="0" cellspacing="0" width="100%" style="margin-top:20px">
						<tbody>
							<tr>
								<td width="800" class="esd-container-frame" align="center" valign="top">
									<table cellpadding="0" cellspacing="0" width="100%">
										<tbody>
											<tr>
												<td align="center" class="esd-block-image">
													<a href='{{ $line.HyperLink }}' target="_blank"> <img class="adapt-img" src='{{ $line.ImageLink }}' alt="Product Trend" width="500"> </a>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			{{ end }}
			{{ end }}
			{{ end }}

             <tr style="border-collapse:collapse;">
              <td class="es-p20t es-p20b" align="left" esd-general-paddings-checked="true" style="padding:0;Margin:0;padding-bottom:20px;">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tbody>
                 <tr style="border-collapse:collapse;">
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;border-radius:0px;">
                    <tbody>
                     <tr style="border-collapse:collapse;">
                      <td align="center" class="es-m-txt-l" style="padding:0;Margin:0;">

                      <!-- Table -->
                           <!-- Table -->
                            {{ with .Email.Body.Table }}
                            {{ $data := .Data }}
                            {{ $columns := .Columns }}
                            {{ if gt (len $data) 0 }}
									<table class="data-wrapper" width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td colspan="2" style="padding: 10px 5px;color: #74787E;font-size: 15px;line-height: 1.2em;">
												<table class="data-table" width="100%" cellpadding="0" cellspacing="0">
													<tr>
														{{ $col := index $data 0 }}
														{{ range $entry := $col }}
														<th style=" padding: 0px 20px;padding-top:10px;border-bottom: 1px solid #EDEFF2;background-color:#F0F7F7;text-align:left"
																{{ with $columns }}
																{{ $width := index .CustomWidth $entry.Key }}
																{{ with $width }}
																width="{{ . }}"
																{{ end }}
																{{ $align := index .CustomAlignement $entry.Key }}
																{{ with $align }}
																style="text-align:{{ . }}"
																{{ end }}
																{{ end }}
														>
															<p style="margin-top: 0;color: #74787E;font-size: 18px;line-height: 1.2em;margin-bottom:5px">{{ $entry.Key }}</p>
														</th>
														{{ end }}
													</tr>
													{{ range $row := $data }}
													<tr>
														{{ range $cell := $row }}
														<td style="padding: 10px 20px;color: #74787E;font-size: 15px;line-height: 1.2em;"
																{{ with $columns }}
																{{ $align := index .CustomAlignement $cell.Key }}
																{{ with $align }}
																style="text-align:{{ . }}"
																{{ end }}
																{{ end }}
														>
															{{ $cell.Value }}
														</td>
														{{ end }}
													</tr>
													{{ end }}
												</table>
											</td>
										</tr>
									</table>
                            {{ end }}
                            {{ end }}

                       <!-- table end -->

                       </tr>

                     <!-- button -->
					{{ with .Email.Body.Actions }}
					{{ if gt (len .) 0 }}
					{{ range $action := . }}
                     <tr style="border-collapse:collapse;">
                      <td align="center" class="es-p10t es-p10b es-p10r es-p10l" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px;"> <span class="es-button-border" style="border-style:solid;border-color:rgb(44, 181, 67);background:rgb(44, 181, 67);border-width:0px;display:inline-block;border-radius:10px;width:auto;"> <a href="{{ $action.Button.Link }}" class="es-button" target="_blank" style="mso-style-priority:100;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:rgb(255, 255, 255);border-style:solid;border-color:rgb(244, 167, 46);border-width:10px 70px;display:inline-block;background:rgb(244, 167, 46);border-radius:5px;font-weight:normal;font-style:normal;line-height:120%;width:auto;text-align:center;">{{ $action.Button.Text }}</a> </span> </td>
                     </tr>
					{{end}}
					{{end}}
					{{end}}
                     <!-- button end -->

                    </tbody>
                   </table> </td>
                 </tr>
                </tbody>
               </table> </td>
             </tr>

           <!-- outros -->


             <tr style="border-collapse:collapse;">
              <td class="es-p20t es-p20b" align="left" esd-general-paddings-checked="true" style="padding:0;Margin:0;padding-top:10px;padding-bottom:40px;">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tbody>
                 <tr style="border-collapse:collapse;">
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                    <tbody>
					{{ with .Email.Body.Outros }}
					{{ if gt (len .) 0 }}
					{{ range $line := . }}
                     <tr style="border-collapse:collapse;">
                      <td align="left" class="es-p15r es-p15l" style="padding:0;Margin:0;padding-left:15px;padding-right:15px;"> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#646d8c;"><span style="font-size:16px;line-height:150%;">{{ $line }}</span></p> </td>
                     </tr>
					  {{end}}
					  {{end}}
					  {{end}}
                    </tbody>
                   </table> </td>
                 </tr>
                </tbody>
               </table> </td>
             </tr>


             <!-- Outros End -->

            </tbody>
           </table> </td>
         </tr>
        </tbody>
       </table>

       <!-- footer -->

       <table cellpadding="0" cellspacing="0" class="es-content esd-footer-popover" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%;">
        <tbody>
         <tr style="border-collapse:collapse;">
          <td align="center" style="padding:0;Margin:0;">
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="800" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:rgb(255, 255, 255);">
            <tbody>
             <tr style="border-collapse:collapse;">
              <td class="es-p20t es-p20b" align="left" esd-general-paddings-checked="true" bgcolor="#f6f6f6" style="padding:0;Margin:0;padding-top:20px;padding-bottom:20px;background-color:rgb(243, 244, 244);">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tbody>
                 <tr style="border-collapse:collapse;">
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                    <tbody>
                     <tr style="border-collapse:collapse;">
                      <td align="center" style="padding:0;Margin:0;">
                       <table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                        <tbody>
                         <tr style="border-collapse:collapse;">
                          <td align="center" valign="top" class="es-p10r" style="padding:0;Margin:0;padding-right:10px;"> <a target="_blank" href="https://www.facebook.com/sellerprime/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:rgb(19, 118, 200);"><img title="Facebook" src="https://storage.googleapis.com/sellerprime_images/facebook-square-colored.png" alt="Fb" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a> </td>
                          <td align="center" valign="top" class="es-p10r" style="padding:0;Margin:0;padding-right:10px;"> <a target="_blank" href="https://www.youtube.com/channel/UCCTiMonx1aTgyDZ2Y6URBFw" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:rgb(19, 118, 200);"><img title="Youtube" src="https://storage.googleapis.com/sellerprime_images/youtube-square-colored.png" alt="Yt" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a> </td>
                          <td align="center" valign="top" class="es-p10r" style="padding:0;Margin:0;padding-right:10px;"> <a target="_blank" href="https://twitter.com/sellerprime?lang=en" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:rgb(19, 118, 200);"><img title="Twitter" src="https://storage.googleapis.com/sellerprime_images/twitter-square-colored.png" alt="Tw" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a> </td>
                          <td align="center" valign="top" class="es-p10r" style="padding:0;Margin:0;padding-right:10px;"> <a target="_blank" href="https://www.instagram.com/sellerprime/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:rgb(19, 118, 200);"><img title="Instagram" src="https://storage.googleapis.com/sellerprime_images/instagram-square-colored.png" alt="Ig" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a> </td>
                          <td align="center" valign="top" style="padding:0;Margin:0;"> <a target="_blank" href="https://in.linkedin.com/company/sellerprime" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:rgb(19, 118, 200);"><img title="Linkedin" src="https://storage.googleapis.com/sellerprime_images/linkedin-square-colored.png" alt="In" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a> </td>
                         </tr>
                        </tbody>
                       </table> </td>
                     </tr>
                    </tbody>
                   </table> </td>
                 </tr>
                </tbody>
               </table> </td>
             </tr>
             <tr style="border-collapse:collapse;">
              <td class="es-p20t es-p20b" align="left" esd-general-paddings-checked="true" bgcolor="#f6f6f6" style="padding:0;Margin:0;padding-top:20px;padding-bottom:20px;background-color:rgb(243, 244, 244);">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tbody>
                 <tr style="border-collapse:collapse;">
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;">
                   <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                    <tbody>
					{{ with .Email.Body.TopButton }}
					{{ if gt (len .) 0 }}
					{{ range $action := . }}
                     <tr style="border-collapse:collapse;">
                      <td align="center" class="es-p30b" style="padding:0;Margin:0;padding-bottom:30px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#646d8c;"><span style="font-size:13px;line-height:150%;">We designed this email to be beneficial to you, our user of SellerPrime - we hope you found it useful.</span></p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#646d8c;"><span style="font-size:13px;font-color:#444961">You can&nbsp;<a target="_blank"  href="{{ $action.Button.Link }}?unsubscribe=true" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:rgb(19, 118, 200);">unsubscribe</a>&nbsp;from our emails any time.</span></p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'source sans pro', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#646d8c;"><span style="font-size:13px;">Our portal address : 9 Temasek Boulevard Suntec Tower 2 #04-03, Singapore - 038989&nbsp;</span></p></td>
                     </tr>
					{{ end }}
					{{ end }}
					{{ end }}
                    </tbody>
                   </table> </td>
                 </tr>
                </tbody>
               </table> </td>
             </tr>
            </tbody>
           </table> </td>
         </tr>
        </tbody>
       </table> </td>
     </tr>
    </tbody>
   </table>
  </div>
 </body>
</html>`
}

// PlainTextTemplate returns a Golang template that will generate an plain text email.
func (dt *SPAlertTheme) PlainTextTemplate() string {
	return `<h2>{{if .Email.Body.Title }}{{ .Email.Body.Title }}{{ else }}{{ .Email.Body.Greeting }} {{ .Email.Body.Name }},{{ end }}</h2>

{{ with .Email.Body.ProductInfoInTop }}
<table  >
	<tbody>
	<tr>
		<td>
	{{ if gt (len .) 0 }}
    {{ range $line := . }}
			<table>
				<tr>
					<p>{{ $line.Name }}</p>
					<p>ASIN: {{ $line.ASIN }}</p>
					<p>Brand: {{ $line.Brand }}</p>
					<p>BSR: {{ $line.BSR }}</p>
					{{ if $line.LQI }}
					<p>LQI: {{ $line.LQI }}</p>
					{{ end }}
					{{ if $line.RevenuePotential }}
					<p>Revenue Potential: {{ $line.RevenuePotential }}</p>
					{{ end }}
				</tr>
			</table>
			{{ end }}
			{{ end }}
		</td>
	</tr>
	</tbody>
</table>
{{ end }}
{{ with .Email.Body.Intros }}
  {{ range $line := . }}
    <p>{{ $line }}</p>
  {{ end }}
{{ end }}
{{ with .Email.Body.ProductInfoAfterIntros }}
<table  >
	<tbody>
	<tr>
		<td>
	{{ if gt (len .) 0 }}
    {{ range $line := . }}
			<table>
				<tr>
					<p>{{ $line.Name }}</p>
					<p>ASIN: {{ $line.ASIN }}</p>
					<p>Brand: {{ $line.Brand }}</p>
					<p>BSR: {{ $line.BSR }}</p>
					{{ if $line.LQI }}
					<p>LQI: {{ $line.LQI }}</p>
					{{ end }}
					{{ if $line.RevenuePotential }}
					<p>Revenue Potential: {{ $line.RevenuePotential }}</p>
					{{ end }}
				</tr>
			</table>
			{{ end }}
			{{ end }}
		</td>
	</tr>
	</tbody>
</table>
{{ end }}
{{ with .Email.Body.MiddleIntros }}
  {{ range $line := . }}
    <p>{{ $line }}</p>
  {{ end }}
{{ end }}
{{ if (ne .Email.Body.FreeMarkdown "") }}
  {{ .Email.Body.FreeMarkdown.ToHTML }}
{{ else }}
  {{ with .Email.Body.Dictionary }}
    <ul>
    {{ range $entry := . }}
      <li>{{ $entry.Key }}: {{ $entry.Value }}</li>
    {{ end }}
    </ul>
  {{ end }}
  {{ with .Email.Body.Table }}
    {{ $data := .Data }}
    {{ $columns := .Columns }}
    {{ if gt (len $data) 0 }}
      <table class="data-table" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          {{ $col := index $data 0 }}
          {{ range $entry := $col }}
            <th>{{ $entry.Key }} </th>
          {{ end }}
        </tr>
        {{ range $row := $data }}
          <tr>
            {{ range $cell := $row }}
              <td>
                {{ $cell.Value }}
              </td>
            {{ end }}
          </tr>
        {{ end }}
      </table>
    {{ end }}
  {{ end }}
  {{ with .Email.Body.Actions }}
    {{ range $action := . }}
      <p>{{ $action.Instructions }} {{ $action.Button.Link }}</p>
    {{ end }}
  {{ end }}
{{ end }}
{{ with .Email.Body.Outros }}
  {{ range $line := . }}
    <p>{{ $line }}<p>
  {{ end }}
{{ end }}
<p>{{.Email.Body.Signature}},<br>{{.Hermes.Product.Name}} - {{.Hermes.Product.Link}}</p>

<p>{{.Hermes.Product.Copyright}}</p>
`
}
