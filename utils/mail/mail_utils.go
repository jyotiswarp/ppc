package mail

import (
	"bytes"
	"encoding/json"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
	"net/url"
	"strings"
)

type MailDto struct {
	From    string `json:"from" form:"from"`
	Text    string `json:"text" form:"text"`
	Subject string `json:"subject" form:"subject"`
	To      string `json:"to" form:"to"`
}

type MailDtoV2 struct {
	MailDto
	CC         map[string]string `json:"cc" form:"cc"`
	BCC        map[string]string `json:"bcc" form:"bcc"`
	ReplyTo    string            `json:"reply_to" form:"reply_to"`
	MailType   string            `json:"mail_type" form:"mail_type"`
	Type       string            `json:"type" form:"type"`
	Provider   string            `json:"provider"`
	SenderName string            `json:"sender_name"`
	AltBody    string            `json:"alt_body"`
}

type SmsDto struct {
	Number  string `json:"number" form:"number"`
	Message string `json:"message" form:"message"`
}

type IdentityStruct struct {
	Tags         []string          `json:"tags,omitempty"`
	FirstName    string            `json:"firstName,omitempty"`
	LastName     string            `json:"lastName,omitempty"`
	Email        string            `json:"email,omitempty"`
	Company      string            `json:"company,omitempty"`
	Birthday     string            `json:"birthday,omitempty"`
	CustomFields map[string]string `json:"customFields,omitempty"`
	NewEmail     string            `json:"newEmail,omitempty"`
}

type TrackStruct struct {
	AddTags    []string `json:"addTags,omitempty"`
	RemoveTags []string `json:"removeTags,omitempty"`
}

type TrackStructUIModel struct {
	Track TrackStruct `json:"track"`
	Email string      `json:"email"`
}

func AddUserToMailListWithTag_sendpulse(mailStruct IdentityStruct) bool {
	if !(strings.Contains(mailStruct.Email, "testsp") || strings.Contains(mailStruct.Email, "sptest")) {
		payload, err := json.Marshal(&mailStruct)
		if err != nil {
			log.Error(err)
			return false
		}

		client := &http.Client{}
		req, err := http.NewRequest("POST", viper.GetString("alertSystem")+"/add_email", bytes.NewBuffer(payload))
		if err != nil {
			log.Error(err)
			return false
		}
		req.Header.Set("Content-Type", "application/json")
		res, err := client.Do(req)
		if err != nil {
			log.Debug("[Calling Mail client]:", err)
			return false
		}
		defer res.Body.Close()
		if res.StatusCode == 200 {
			return true
		}
	}
	return false
}

func SendMail(from, subject, to, text string, mailType string) {
	mail := MailDto{From: from, Text: text, Subject: subject, To: to}

	payload, err := json.Marshal(&mail)
	if err != nil {
		log.Error(err)
		return
	}

	client := &http.Client{}
	endpoint := "/send_mail"
	if mailType == "html" {
		endpoint = "/send_html_mail"
	}
	req, err := http.NewRequest("POST", viper.GetString("mail_end_point")+endpoint, bytes.NewBuffer(payload))
	if err != nil {
		log.Error(err)
		return
	}
	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		log.Debug("[Calling Mail client]:", err)
		return
	}
	defer res.Body.Close()

}

func SendSMS(mobNo, message string) {

	sms := SmsDto{Number: mobNo, Message: message}
	payload, err := json.Marshal(&sms)
	if err != nil {
		log.Error(err)
		return
	}

	client := &http.Client{}
	req, err := http.NewRequest("POST", viper.GetString("alertSystem")+"/send_message", bytes.NewBuffer(payload))
	if err != nil {
		log.Error(err)
		return
	}
	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		log.Debug("[Calling SMS client]:", err)
		return
	}
	defer res.Body.Close()

}

func SendEmailFromDilipScript(from, to, subject, message string) int {
	data := url.Values{}
	data.Set("email_to", to)
	data.Set("subject", subject)
	data.Set("message", message)
	data.Set("email", from)

	client := &http.Client{}
	req, err := http.NewRequest("POST", "http://giftedstrings.com/sp_invite.php", bytes.NewBufferString(data.Encode()))
	if err != nil {
		log.Error(err)
		return 400
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	res, err := client.Do(req)
	if err != nil {
		log.Debug("[Calling SendEmailFromDilipScript client]:", err)
		return 400
	}
	defer res.Body.Close()
	return res.StatusCode
}

func SendSupportEmailFromDilipScript(from, to, subject, message string, category string) int {
	data := url.Values{}
	data.Set("email_to", to)
	data.Set("subject", subject)
	data.Set("message", message)
	data.Set("email", from)
	data.Set("category", category)

	client := &http.Client{}
	req, err := http.NewRequest("POST", "http://giftedstrings.com/sp_support.php", bytes.NewBufferString(data.Encode()))
	if err != nil {
		log.Error(err)
		return 400
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	res, err := client.Do(req)
	if err != nil {
		log.Debug("[Calling SendSupportEmailFromDilipScript client]:", err)
		return 400
	}
	defer res.Body.Close()
	return res.StatusCode
}

func SendMailV2(mail MailDtoV2) int {

	if mail.Provider == "" {
		mail.Provider = "sendpulse"
	}
	payload, err := json.Marshal(&mail)
	if err != nil {
		log.Error(err)
		return 400
	}

	client := &http.Client{}
	endpoint := "/send_mail_v2"
	req, err := http.NewRequest("POST", viper.GetString("alertSystem")+endpoint, bytes.NewBuffer(payload))
	if err != nil {
		log.Error(err)
		return 400
	}
	req.Header.Set("Content-Type", "application/json")
	res, err := client.Do(req)
	if err != nil {
		log.Error("[Calling Mail client]:", err)
		return 400
	}
	defer res.Body.Close()
	return res.StatusCode

}

type MailerLiteReq struct {
	Email string `json:"email"`
	Name  string `json:"name"`
}

func AddToMailerLite(name, email, groupID string) bool {
	mailStruct := MailerLiteReq{Email: email, Name: name}

	payload, err := json.Marshal(&mailStruct)
	if err != nil {
		log.Error(err)
		return false
	}

	client := &http.Client{}
	lUrl := "https://api.mailerlite.com/api/v2/groups/" + groupID + "/subscribers"
	fmt.Println(lUrl)
	req, err := http.NewRequest("POST", lUrl, bytes.NewBuffer(payload))
	if err != nil {
		log.Error(err)
		return false
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-MailerLite-ApiKey", "ef63efc421b4438a4c7066b7cc5960da")
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		log.Debug("[Calling Mail client]:", err)
		return false
	}
	defer res.Body.Close()
	if res.StatusCode == 200 {
		return true
	}
	return false
}

func RemoveFromMailerLite(email, groupID string) bool {
	client := &http.Client{}
	req, err := http.NewRequest("DELETE", "https://api.mailerlite.com/api/v2/groups/"+groupID+"/subscribers/"+email, nil)
	if err != nil {
		log.Error(err)
		return false
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-MailerLite-ApiKey", "ef63efc421b4438a4c7066b7cc5960da")
	res, err := client.Do(req)
	if err != nil {
		log.Debug("[Calling Mail client]:", err)
		return false
	}
	defer res.Body.Close()
	if res.StatusCode == 200 {
		return true
	}
	return false
}
