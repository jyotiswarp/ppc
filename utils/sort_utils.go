package utils

import (
	"encoding/json"
	"hash/fnv"
	"sort"
)

func sortValue(v interface{}) interface{} {
	array, ok := v.([]interface{})
	if ok {
		return sortArray(array)
	}
	mapValue, ok := v.(map[string]interface{})
	if ok {
		return SortMap(mapValue)
	}
	return v
}

func SortMap(req map[string]interface{}) interface{} {
	var keys []string
	for k := range req {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	newMap := make(map[string]interface{})

	for _, k := range keys {
		req[k] = sortValue(req[k])
		newMap[k] = req[k]
	}
	return newMap
}

func sortArray(array []interface{}) interface{} {
	switch t := array[0].(type) {
	case int:
		return sortIntArray(array)
	case float64:
		return sortFloatArray(array)
	case float32:
		return sortFloatArray(array)
	case string:
		return sortStringArray(array)
	default:
		_ = t
		return array
	}
}

func sortIntArray(array []interface{}) []int {
	var intArray []int
	for _, v := range array {
		intArray = append(intArray, v.(int))
	}
	sort.Ints(intArray)
	return intArray
}

func sortFloatArray(array []interface{}) []float64 {
	var floatArray []float64
	for _, v := range array {
		floatArray = append(floatArray, v.(float64))
	}
	sort.Float64s(floatArray)
	return floatArray
}

func sortStringArray(array []interface{}) []string {
	var stringArray []string
	for _, v := range array {
		stringArray = append(stringArray, v.(string))
	}
	sort.Strings(stringArray)
	return stringArray
}

func MapToString(req interface{}) string {
	str, _ := json.Marshal(req)
	return string(str)
}

func Hash(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}
