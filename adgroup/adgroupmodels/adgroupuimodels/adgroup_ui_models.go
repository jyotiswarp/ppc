package adgroupuimodels

import "time"

type AdGroupsSummary struct {
	Sales        float64   `json:"sales"`
	Orders       int       `json:"orders"`
	Cost         float64   `json:"cost"`
	Impressions  int       `json:"impressions"`
	CPC          float64   `json:"cpc"`
	ACOS         float64   `json:"acos"`
	Clicks       int       `json:"clicks"`
	Ctr          float64   `json:"ctr"`
	Date         time.Time `json:"date"`
	CampaignID   int64     `json:"campaignId"`
	CampaignName string    `json:"campaignName"`
	AdGroupID    int64     `json:"adGroupId"`
	AdGroupName  string    `json:"adGroupName"`
	Count        int       `json:"count"`
}
