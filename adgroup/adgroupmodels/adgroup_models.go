package adgroupmodels

type AdGroupUpdateForm struct {
	CampaignId int64   `json:"campaignId,omitempty" binding:"required"`
	AdGroupId  int64   `json:"adGroupId,omitempty" binding:"required"`
	Name       string  `json:"name,omitempty"`
	State      string  `json:"state,omitempty" `
	DefaultBid float64 `json:"defaultBid,omitempty"`
}
type AdGroupResponse struct {
	Code        string `json:"code,omitempty"`
	Description string `json:"description,omitempty"`
	AdGroupId   int64  `json:"adGroupId,omitempty"`
}

type AdGroupGetForm struct {
	CampaignId int64 `json:"campaignId,omitempty" binding:"required"`
}
type AdGroupForm struct {
	StartIndex       string  `json:"startIndex,omitempty"`
	Count            string  `json:"count,omitempty"`
	CampaignType     string  `json:"campaignType,omitempty"`
	StateFilter      string  `json:"stateFilter,omitempty"`
	Name             string  `json:"name,omitempty"`
	CampaignIdFilter []int64 `json:"campaignIdFilter,omitempty"`
	AdGroupIdFilter  []int64 `json:"adGroupIdFilter,omitempty"`
}

type AdGroupBidRecommendation struct {
	AdGroupID    int64        `json:"adGroupId"`
	SuggestedBid SuggestedBid `json:"suggestedBid"`
}

type SuggestedBid struct {
	RangeEnd   float64 `json:"rangeEnd"`
	RangeStart float64 `json:"rangeStart"`
	Suggested  float64 `json:"suggested"`
}
