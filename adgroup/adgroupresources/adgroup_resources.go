package adgroupresources

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupcontroller"
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupmodels"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementform"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
)

func AdGroupResources(router *gin.Engine) {
	managementAdGroupResources := router.Group("/management/adgroup", middlewares.PPCRequestMiddleware())
	{
		managementAdGroupResources.GET("/list", middlewares.PaginationValidator(), adgroupcontroller.GetAdGroupByCampaign)
		managementAdGroupResources.PUT("/update", middlewares.Validator([]adgroupmodels.AdGroupUpdateForm{}), adgroupcontroller.UpdateAdGroup)
	}

	insightsCampaignResources := router.Group("/adgroups/:campaign_id", middlewares.PPCRequestMiddleware())
	{
		insightsCampaignResources.POST("/summary", middlewares.Validator(ppcmanagementform.TimeRangeForm{}), adgroupcontroller.GetAllAdGroupsSummary)
		insightsCampaignResources.POST("/trends", middlewares.Validator(ppcmanagementform.TimeRangeForm{}), adgroupcontroller.GetAllAdGroupsTrends)
	}

	perCampaignResources := router.Group("/adgroup/:campaign_id/:adGroup_id", middlewares.PPCRequestMiddleware())
	{
		perCampaignResources.POST("/summary", middlewares.Validator(ppcmanagementform.TimeRangeForm{}), adgroupcontroller.GetAdGroupSummary)
		perCampaignResources.POST("/trends", middlewares.Validator(ppcmanagementform.TimeRangeForm{}), adgroupcontroller.GetAdGroupTrends)
		perCampaignResources.GET("/bid_recommendations", adgroupcontroller.GetAdGroupRecommendation)
	}
}
