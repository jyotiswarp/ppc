package adgroupcontroller

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupdao"
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupmodels"
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupservices"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementform"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
)

func GetAdGroupList(c *gin.Context) {

	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)

	result, err := adgroupservices.GetAdGroupList(ppcManagement, false)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}
func UpdateAdGroup(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*[]adgroupmodels.AdGroupUpdateForm)
	//temp code just for integration
	//c.JSON(200, gin.H{
	//	"result": "success",
	//})
	//return
	_, err := adgroupservices.UpdateAdGroup(ppcManagement, formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": "success",
	})
	return

}

func GetAdGroupByCampaign(c *gin.Context) {

	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	campaignID := c.Query("campaign_id")
	if campaignID == "" {
		c.JSON(400, gin.H{"error": "campaign id is missing"})
		return
	}
	result, err := adgroupservices.GetAdGroupByCampaign(ppcManagement, campaignID)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func GetAllAdGroupsSummary(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*ppcmanagementform.TimeRangeForm)
	campaignID := c.Param("campaign_id")
	result, err := adgroupdao.GetAllCampaignReportSummary(ppcManagement, formRequest, campaignID)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{"result": result})
}

func GetAllAdGroupsTrends(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*ppcmanagementform.TimeRangeForm)
	campaignID := c.Param("campaign_id")
	result, err := adgroupdao.GetAllAdGroupReportTrends(ppcManagement, formRequest, campaignID)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{"result": result})
}

func GetAdGroupSummary(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*ppcmanagementform.TimeRangeForm)
	campaignID := c.Param("campaign_id")
	adGroupID := c.Param("adGroup_id")
	result, err := adgroupdao.GetAdGroupReportSummary(ppcManagement, formRequest, campaignID, adGroupID)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{"result": result})
}

func GetAdGroupTrends(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*ppcmanagementform.TimeRangeForm)
	campaignID := c.Param("campaign_id")
	adGroupID := c.Param("adGroup_id")
	result, err := adgroupdao.GetAdGroupReportTrends(ppcManagement, formRequest, campaignID, adGroupID)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{"result": result})
}

func GetAdGroupRecommendation(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	campaignID := c.Param("campaign_id")
	adGroupID := c.Param("adGroup_id")
	result, err := adgroupservices.GetAdGroupBidRecommendation(ppcManagement, campaignID, adGroupID)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{"result": result})
}
