package adgroupservices

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupdao"
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupmodels"
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	log "github.com/Sirupsen/logrus"
)

func UpdateAdGroupDataModel(ppcCommon ppcmanagementcommons.PPCManagementCommon, formData adgroupmodels.AdGroupForm) error {
	var err error
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err := market.GetAdGroupListExtended(formData)
		if err != nil {
			log.Error(err)
			return err
		}
		err = adgroupdao.PushAdGroupDataToDb(result, ppcCommon.Profile.ProfileId)
	}
	return err
}

func GetAdGroupList(ppcCommon ppcmanagementcommons.PPCManagementCommon, forceUpdate bool) (result []models.AdGroup, err error) {

	//status, err := dao.CheckLastUpdatedDate(sellerId, marketPlace, geo, "adgroup", profileId)

	/*if !status.IsZero() && time.Now().Sub(status).Hours() < 24 {
		result, err = adgroupdao.GetAdGroupInformation(sellerId, profileId, 0)
	} else {*/

	if forceUpdate {
		form := adgroupmodels.AdGroupForm{}
		err = UpdateAdGroupDataModel(ppcCommon, form)
		if err != nil {
			log.Error(err)
			return
		}
	}

	result, err = adgroupdao.GetAdGroupInformation(ppcCommon, "")

	//}
	if err != nil {
		return result, err
	}

	return result, err

}

func GetAdGroupByCampaign(ppcCommon ppcmanagementcommons.PPCManagementCommon, campaignID string) (result []models.AdGroup, err error) {

	//id, err := strconv.ParseInt(campaignID, 10, 64)
	result, err = adgroupdao.GetAdGroupInformation(ppcCommon, campaignID)
	if err != nil {
		return result, err
	}

	return result, err

}
func UpdateAdGroup(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *[]adgroupmodels.AdGroupUpdateForm) ([]adgroupmodels.AdGroupResponse, error) {
	//formData := adgroupmodels.AdGroupForm{}
	//for _, adReq := range *form {
	//	formData.AdGroupIdFilter = append(formData.AdGroupIdFilter, adReq.AdGroupId)
	//}
	//err = UpdateAdGroupDataModel(ppcCommon, formData)
	//if err != nil {
	//	log.Error(err)
	//	return
	//}
	res, err := updateDataInAdGroup(ppcCommon, form)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	GetAdGroupList(ppcCommon, true)
	return res, nil

}

func GetAdGroupBidRecommendation(ppcCommon ppcmanagementcommons.PPCManagementCommon, campaignID, adGroupID string) (result adgroupmodels.AdGroupBidRecommendation, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.GetAdGroupBidRecommendation(adGroupID)
		if err != nil {
			log.Error(err)
			return
		}
	}
	return
}

func updateDataInAdGroup(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *[]adgroupmodels.AdGroupUpdateForm) (result []adgroupmodels.AdGroupResponse, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.UpdateAdGroupList(form)
		if err != nil {
			log.Error(err)
			return
		}
	}
	return

}
