package adgroupdao

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupmodels/adgroupuimodels"
	"bitbucket.org/jyotiswarp/ppc/dao"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementform"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	log "github.com/Sirupsen/logrus"
)

func GetAdGroupDetails(adGroupId int64) (err error, result models.AdGroup) {

	err = mysql.DB.Where("ad_group_id = ?", adGroupId).Find(&result).Error
	return
}
func PushAdGroupDataToDb(result []models.AdGroup, profileId int64) (err error) {

	for _, data := range result {

		row := dao.CheckDataPresence(data).(models.AdGroup)
		if row.ID == 0 {
			//record not present
			err := mysql.DB.Create(&data).Error
			if err != nil {
				return err
			}
		} else {
			//record present
			data.ID = row.ID
			data.CreatedAt = row.CreatedAt
			err := mysql.DB.Save(&data).Error
			if err != nil {
				return err
			}
		}
		history := models.AdGroupHistory{LastUpdatedDate: data.LastUpdatedDate, CreationDate: data.CreationDate, ServingStatus: data.ServingStatus,
			Name: data.Name, CampaignId: data.CampaignId, State: data.State, AdGroupId: data.AdGroupId, DefaultBid: data.DefaultBid}
		rowHistory := dao.CheckDataPresence(history).(models.AdGroupHistory)
		if rowHistory.ID == 0 {
			err := mysql.DB.Create(&history).Error
			if err != nil {
				log.Error(err)
			}
		}
	}

	return nil
}

func GetAdGroupInformation(ppcCommon ppcmanagementcommons.PPCManagementCommon, campaignID string) (result []models.AdGroup, err error) {

	if campaignID != "" {
		err = mysql.DB.Table("campaigns").
			Select("distinct adg.*").
			Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
			Joins("join ad_groups adg on adg.campaign_id = campaigns.campaign_id").
			Where(" campaigns.campaign_id =? AND campaigns.state='enabled'", campaignID).
			Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=?", ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.Profile.ProfileId).
			Find(&result).Error

		if err != nil {
			log.Error(err)
			return result, err
		}
	} else {
		err = mysql.DB.Table("campaigns").
			Select("distinct adg.*").
			Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
			Joins("join ad_groups adg on adg.campaign_id = campaigns.campaign_id").
			Where("prof.seller_string_id = ? and prof.profile_id=?", ppcCommon.SellerID, ppcCommon.Profile.ProfileId).
			Find(&result).Error

		if err != nil {
			log.Error(err)
			return result, err
		}
	}
	return result, nil

}

func GetAllCampaignReportSummary(ppcCommon ppcmanagementcommons.PPCManagementCommon, timeRange *ppcmanagementform.TimeRangeForm, campaignID string) (summary adgroupuimodels.AdGroupsSummary, err error) {

	err = mysql.DB.Table("ad_group_report_data").
		Select("campaigns.name as campaign_name,"+
			" campaigns.campaign_id,"+
			" ROUND(SUM(ad_group_report_data.sales),2) as sales,"+
			" SUM(ad_group_report_data.orders) as orders,"+
			" ROUND(SUM(ad_group_report_data.cost)) as cost,"+
			" SUM(ad_group_report_data.impressions) as impressions,"+
			" round((SUM(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.clicks)),2) cpc,"+
			" round((sum(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.sales))*100 ,2) acos, "+
			" SUM(ad_group_report_data.clicks) as clicks,"+
			" round((SUM(ad_group_report_data.clicks)/SUM(ad_group_report_data.`impressions`))*100,2) ctr,"+
			" count(distinct ad_groups.ad_group_id) as count").
		Joins("right join ad_groups on ad_groups.ad_group_id = ad_group_report_data.ad_group_id").
		Joins("right join campaigns on campaigns.campaign_id = ad_groups.campaign_id").
		Where("ad_groups.campaign_id = ? AND ad_group_report_data.report_date between ? AND ?", campaignID, timeRange.FromDate, timeRange.ToDate).
		Group("campaigns.name,campaigns.campaign_id").
		Find(&summary).Error
	if err != nil {
		log.Error(err)
	}
	return

}

func GetAdGroupReportSummary(ppcCommon ppcmanagementcommons.PPCManagementCommon, timeRange *ppcmanagementform.TimeRangeForm, campaignID, adGroupID string) (summary adgroupuimodels.AdGroupsSummary, err error) {

	err = mysql.DB.Table("ad_group_report_data").
		Select("campaigns.name as campaign_name,"+
			" campaigns.campaign_id,"+
			" ad_groups.ad_group_id,ad_groups.name as ad_group_name,"+
			" ROUND(SUM(ad_group_report_data.sales),2) as sales,"+
			" SUM(ad_group_report_data.orders) as orders,"+
			" ROUND(SUM(ad_group_report_data.cost)) as cost,"+
			" SUM(ad_group_report_data.impressions) as impressions,"+
			" round((SUM(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.clicks)),2) cpc, "+
			" round((sum(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.sales))*100 ,2) acos, "+
			" SUM(ad_group_report_data.clicks) as clicks,"+
			" round((SUM(ad_group_report_data.clicks)/SUM(ad_group_report_data.`impressions`))*100,2) ctr,"+
			" count(distinct ad_groups.ad_group_id) as count").
		Joins("right join ad_groups on ad_groups.ad_group_id = ad_group_report_data.ad_group_id").
		Joins("right join campaigns on campaigns.campaign_id = ad_groups.campaign_id").
		Where("ad_groups.ad_group_id=? AND ad_groups.campaign_id=? AND ad_group_report_data.report_date between ? AND ?", adGroupID, campaignID, timeRange.FromDate, timeRange.ToDate).
		Group("ad_groups.ad_group_id,ad_groups.name,campaigns.name,campaigns.campaign_id").
		Find(&summary).Error
	if err != nil {
		log.Error(err)
	}
	return

}

func GetAllAdGroupReportTrends(ppcCommon ppcmanagementcommons.PPCManagementCommon, timeRange *ppcmanagementform.TimeRangeForm, campaignID string) (trends []adgroupuimodels.AdGroupsSummary, err error) {

	err = mysql.DB.Table("ad_group_report_data").
		Select("ROUND(SUM(ad_group_report_data.sales),2) as sales,"+
			" SUM(ad_group_report_data.orders) as orders,"+
			" ROUND(SUM(ad_group_report_data.cost)) as cost,"+
			" SUM(ad_group_report_data.impressions) as impressions,"+
			" round((SUM(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.clicks)),2) cpc, "+
			" round((sum(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.sales))*100 ,2) acos, "+
			" SUM(ad_group_report_data.clicks) as clicks,"+
			" round((SUM(ad_group_report_data.clicks)/SUM(ad_group_report_data.`impressions`))*100,2) ctr,"+
			" DATE(ad_group_report_data.report_date) as date").
		Joins("join ad_groups on ad_groups.ad_group_id = ad_group_report_data.ad_group_id").
		Where("ad_groups.campaign_id=? AND ad_group_report_data.report_date between ? AND ?", campaignID, timeRange.FromDate, timeRange.ToDate).
		Group("ad_group_report_data.report_date").
		Order("ad_group_report_data.report_date asc").
		Find(&trends).Error
	if err != nil {
		log.Error(err)
	}
	return

}

func GetAdGroupReportTrends(ppcCommon ppcmanagementcommons.PPCManagementCommon, timeRange *ppcmanagementform.TimeRangeForm, campaignID, adGroupID string) (trends []adgroupuimodels.AdGroupsSummary, err error) {

	err = mysql.DB.Table("ad_group_report_data").
		Select("ROUND(SUM(ad_group_report_data.sales),2) as sales,"+
			" SUM(ad_group_report_data.orders) as orders,"+
			" ROUND(SUM(ad_group_report_data.cost)) as cost,"+
			" SUM(ad_group_report_data.impressions) as impressions,"+
			" round((SUM(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.clicks)),2) cpc, "+
			" round((sum(`ad_group_report_data`.`cost`)/SUM(ad_group_report_data.sales))*100 ,2) acos, "+
			" SUM(ad_group_report_data.clicks) as clicks,"+
			" round((SUM(ad_group_report_data.clicks)/SUM(ad_group_report_data.`impressions`))*100,2) ctr,"+
			" DATE(ad_group_report_data.report_date) as date").
		Joins("join ad_groups on ad_groups.ad_group_id = ad_group_report_data.ad_group_id").
		Where("ad_groups.ad_group_id=? AND ad_groups.campaign_id=? AND ad_group_report_data.report_date between ? AND ?", adGroupID, campaignID, timeRange.FromDate, timeRange.ToDate).
		Group("ad_group_report_data.report_date").
		Order("ad_group_report_data.report_date asc").
		Find(&trends).Error
	if err != nil {
		log.Error(err)
	}
	return

}
