package testutils

import (
	"fmt"
	"github.com/spf13/viper"
	//"bitbucket.org/sellerprimehub/usermanagment/persistance/mysql"
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupresources"
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationresources"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignresources"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordresources"
	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofileresources"
	"bitbucket.org/jyotiswarp/ppc/productad/productadresources"
	"bitbucket.org/jyotiswarp/ppc/report/reportresouces"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesresources"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofileresources"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	"bytes"
	"github.com/gin-gonic/contrib/gzip"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"testing"
	"bitbucket.org/jyotiswarp/ppc/backends"
)

var (
	testconfig string = "$GOPATH/src/bitbucket.org/jyotiswarp/ppc/config"
)

type LoginResponse struct {
	Auth     string `json:"auth"`
	SellerId string `json:"seller_id"`
	Email    string `json:"email"`
}

func InitWithApi() (server *httptest.Server, handlers *gin.Engine) {
	viper.SetConfigName("config_test")
	viper.AddConfigPath(testconfig)
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("config file not found")
		return nil, nil
	}
	viper.WatchConfig()
	handlers = gin.Default()
	handlers.Use(gzip.Gzip(gzip.DefaultCompression))
	authenticationresources.MarketAuthApis(handlers)
	marketprofileresources.MarketProfileApis(handlers)
	reportresouces.ReportApis(handlers)
	rulesresources.RulesApi(handlers)
	campaignresources.CampaignResources(handlers)
	adgroupresources.AdGroupResources(handlers)
	keywordresources.KeywordResources(handlers)
	productadresources.ProductAdResources(handlers)
	spprofileresources.SpProfileApi(handlers)
	backends.Initialize()

	server = httptest.NewServer(handlers)
	mysql.Init(false)
	return
}

func Request(handlers *gin.Engine, method string, url string, data string,headers map[string]string, urlEncoded bool) (resp *httptest.ResponseRecorder, err error) {
	request, err := http.NewRequest(method, url, bytes.NewBufferString(data))
	if err != nil {
		return
	}
	if urlEncoded {
		request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	} else {
		request.Header.Add("Content-Type", "application/json")
	}
	for key,value := range headers{
		request.Header.Add(key,value)
	}
	resp = httptest.NewRecorder()
	handlers.ServeHTTP(resp, request)
	return resp, err
}

func CheckIfResponse200(resp *httptest.ResponseRecorder, err error, t *testing.T) {
	if err != nil {
		t.Error(err)
		return
	}

	if resp.Code != 200 {
		t.Errorf("Error Response: %s", resp.Body.String())
		return
	}
	t.Logf("PASS : %s", resp.Body.String())
}
