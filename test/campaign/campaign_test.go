package campaign

import (
	"bitbucket.org/jyotiswarp/ppc/test/testutils"
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func getSandBoxHeaders() map[string]string {
	headers := make(map[string]string)
	headers["sellerId"] = "A3MV9OCIAOMFIM"
	headers["marketplace"] = "amazon"
	headers["geo"] = "sandbox"
	return headers

}

func getProductionHeaders() map[string]string {
	headers := make(map[string]string)
	headers["sellerId"] = "A3MV9OCIAOMFIM"
	headers["marketplace"] = "amazon"
	headers["geo"] = "us"
	return headers

}

func TestCampaignList(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/campaigns/list", server.URL)
	resp, err := testutils.Request(handlers, "GET", endpoint, "", getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestCampaignUpdate(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/campaigns/update", server.URL)
	data := make(map[string]interface{})

	data["campaignId"] = 50313246492316
	data["name"] = "Manual Target - Exact Match - Jay"
	list := []map[string]interface{}{data}

	dataBytes, _ := json.Marshal(list)
	resp, err := testutils.Request(handlers, "PUT", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAllCampaignTrends(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/campaigns/trends", server.URL)
	data := make(map[string]interface{})

	data["startDate"] = time.Now().AddDate(0, -2, 0)
	data["endDate"] = time.Now()

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAllAdGroupsSummary(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/campaigns/summary", server.URL)
	data := make(map[string]interface{})

	data["startDate"] = time.Now().AddDate(0, -2, 0)
	data["endDate"] = time.Now()

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAdGroupTrends(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/campaign/36385441958655/trends", server.URL)
	data := make(map[string]interface{})

	data["startDate"] = time.Now().AddDate(0, -2, 0)
	data["endDate"] = time.Now()

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAdGroupSummary(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/campaign/36385441958655/summary", server.URL)
	data := make(map[string]interface{})

	data["startDate"] = time.Now().AddDate(0, -2, 0)
	data["endDate"] = time.Now()

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAdGroupBidRecommendation(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/adgroup/36385441958655/145039278104465/bid_recommendations", server.URL)
	resp, err := testutils.Request(handlers, "GET", endpoint, "", getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}
