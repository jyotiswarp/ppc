package report

import (
	"testing"
	//"fmt"
	"bitbucket.org/jyotiswarp/ppc/test/testutils"
	"encoding/json"
	"fmt"
	"time"
)

func getSandBoxHeaders() map[string]string {
	headers := make(map[string]string)
	headers["sellerId"] = "A3MV9OCIAOMFIM"
	headers["marketplace"] = "amazon"
	headers["geo"] = "sandbox"
	return headers

}

func getProductionHeaders() map[string]string {
	headers := make(map[string]string)
	headers["sellerId"] = "A3MV9OCIAOMFIM"
	headers["marketplace"] = "amazon"
	headers["geo"] = "us"
	return headers

}

func getProduction2Headers() map[string]string {
	headers := make(map[string]string)
	headers["sellerId"] = "AVSZN1742UYKF"
	headers["marketplace"] = "amazon"
	headers["geo"] = "us"
	return headers

}

func TestFilteredKeywords(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/filter_keywords", server.URL)
	fromDate := time.Now().AddDate(0, -1, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["campaignId"] = "50313246492316"
	data["adGroupId"] = "26651230569540"
	data["fromDate"] = &fromDate
	data["toDate"] = &todate
	data["minImpressions"] = "1"
	data["maxImpressions"] = "10000"
	//data["maxAcos"]="100"
	//data["minAcos"]="1"
	data["maxClicks"] = "10000"
	data["minClicks"] = "1"

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestReportsByDate(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/reports_by_date", server.URL)
	fromDate := time.Now().AddDate(0, -1, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["reportType"] = "keyword"
	data["fromDate"] = &fromDate
	data["toDate"] = &todate

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestReportsByAsin(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/report_by_asin", server.URL)
	fromDate := time.Now().AddDate(0, -1, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["asin"] = "B01BXCBWUU"
	data["fromDate"] = &fromDate
	data["toDate"] = &todate

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestPPCSummary(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/ppc_summary", server.URL)
	fromDate := time.Now().AddDate(0, -1, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["fromDate"] = &fromDate
	data["toDate"] = &todate

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAllKeywords(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/all_keywords", server.URL)
	fromDate := time.Now().AddDate(0, -1, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["fromDate"] = &fromDate
	data["toDate"] = &todate

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestKeywordSuggestions(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/keyword_suggestions", server.URL)
	fromDate := time.Now().AddDate(0, -1, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["fromDate"] = &fromDate
	data["toDate"] = &todate

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestNegativeKeywords(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/negative_keywords", server.URL)
	fromDate := time.Now().AddDate(0, -6, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["fromDate"] = &fromDate
	data["toDate"] = &todate

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAsinList(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/asin_list", server.URL)
	resp, err := testutils.Request(handlers, "GET", endpoint, "", getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAsinSuggestions(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/asin_suggestions", server.URL)
	resp, err := testutils.Request(handlers, "GET", endpoint, "", getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestCampaignReport(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/campaign_report", server.URL)
	fromDate := time.Now().AddDate(0, -1, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["fromDate"] = &fromDate
	data["toDate"] = &todate
	data["campaignId"] = 50313246492316
	data["adGroupId"] = 26651230569540

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAdGroupReport(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/adGroup_report", server.URL)
	fromDate := time.Now().AddDate(0, -1, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["fromDate"] = &fromDate
	data["toDate"] = &todate
	data["campaignId"] = 50313246492316
	data["adGroupId"] = 26651230569540

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestKeywordReport(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/keyword_report", server.URL)
	fromDate := time.Now().AddDate(0, -1, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["fromDate"] = &fromDate
	data["toDate"] = &todate
	data["campaignId"] = 50313246492316
	data["adGroupId"] = 26651230569540

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestKeywordSummary(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/report/keyword/summary", server.URL)
	fromDate := time.Now().AddDate(0, -1, 0)
	todate := time.Now().AddDate(0, 0, -2)

	data := make(map[string]interface{})
	data["fromDate"] = &fromDate
	data["toDate"] = &todate

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}
