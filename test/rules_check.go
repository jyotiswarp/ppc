package main

import (
	"fmt"
	"github.com/Knetic/govaluate"
)

type CampaignReport struct {
	Clicks      int
	Impressions int
	Product     Product
	Keys        []Keyword
}

type Product struct {
	Orders int
}

type Keyword struct {
	What string
	cpc  float64
}

type UserPreference struct {
	OrderThreshold int
	CPCThreshhold  float64
}

func main() {

	functions := map[string]govaluate.ExpressionFunction{
		"totalKeywordCpc": func(args ...interface{}) (interface{}, error) {
			products := args[0].([]Keyword) // only one param
			id := args[1].(float64)
			fmt.Println("id : ", int64(id))
			var totalCpc float64 = 0.0

			for _, p := range products {
				totalCpc += p.cpc
			}
			return totalCpc, nil
		},
	}

	expression, err := govaluate.NewEvaluableExpressionWithFunctions("campaign.Product.Orders >= pref.OrderThreshold && totalKeywordCpc(campaign.Keys,16571972543607010) > pref.CPCThreshhold ", functions)
	fmt.Printf("exp %+v %+v\n", expression, err)

	c1 := CampaignReport{
		100,
		200,
		Product{99},
		[]Keyword{Keyword{"xx", 13.4}, Keyword{"yy", 4.0}},
	}

	parameters := make(map[string]interface{}, 8)
	parameters["campaign"] = c1

	userPreference := UserPreference{
		90,
		15.3,
	}

	parameters["pref"] = userPreference

	fmt.Println(expression.Evaluate(parameters))

}
