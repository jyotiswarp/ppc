package backends

import (
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/test/testutils"
	"testing"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
)

func getProductionMarketplace() backends.MarketClient {
	testutils.InitWithApi()
	market := backends.GetMarketPlace("amazon", "us")
	if market != nil {
		market.SetProfileId(2741830327201395)
		market.SetSellerId("A3MV9OCIAOMFIM")
		market.SetEmail("sghavami@moondaninaturals.com")
	}
	return market
}

func TestAccessToPPCAPIs(t *testing.T) {
	market := getProductionMarketplace()
	profiles, err := market.GetListOfProfiles()
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
	t.Log(profiles)
}

func TestExtendedKeywordList(t *testing.T) {
	market := getProductionMarketplace()
	keywordForm :=  keywordmodel.KeywordDetailsForm{}
	keys, err := market.GetKeywordListExtended(keywordForm)
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
	t.Log(keys)
}

func TestExtendedKeywordByID(t *testing.T) {
	market := getProductionMarketplace()
	keys, err := market.GetExtendedKeywordByID(16764975243817)
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
	t.Log(keys)
}
