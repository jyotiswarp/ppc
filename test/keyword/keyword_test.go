package keyword

import (
	"bitbucket.org/jyotiswarp/ppc/test/testutils"
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func getSandBoxHeaders() map[string]string {
	headers := make(map[string]string)
	headers["sellerId"] = "A3MV9OCIAOMFIM"
	headers["marketplace"] = "amazon"
	headers["geo"] = "sandbox"
	return headers

}

func getProductionHeaders() map[string]string {
	headers := make(map[string]string)
	headers["sellerId"] = "A3MV9OCIAOMFIM"
	headers["marketplace"] = "amazon"
	headers["geo"] = "us"
	return headers

}

func TestAllKeywordsList(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/keyword/all", server.URL)
	resp, err := testutils.Request(handlers, "GET", endpoint, "", getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestKeywordListByAdGroup(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/keyword/list", server.URL)
	data := make(map[string]interface{})

	data["campaignId"] = 36385441958655
	data["adGroupId"] = 145039278104465
	data["state"] = []string{"archived", "enabled", "paused"}

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAllNegativeKeywordsList(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/keyword/all_negative", server.URL)
	resp, err := testutils.Request(handlers, "GET", endpoint, "", getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAddNegativeKeyword(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/keyword/negative", server.URL)
	data := make(map[string]interface{})

	data["campaignId"] = 250349224674867
	data["adGroupId"] = 165719725436070
	data["keywordText"] = "test negative"
	data["matchType"] = "broad"
	data["state"] = "enabled"

	list := []map[string]interface{}{data}
	dataBytes, _ := json.Marshal(list)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getSandBoxHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAddBiddableKeyword(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/keyword/negative", server.URL)
	data := make(map[string]interface{})

	data["campaignId"] = 250349224674867
	data["adGroupId"] = 165719725436070
	data["keywordText"] = "test negative"
	data["matchType"] = "broad"
	data["state"] = "enabled"

	list := []map[string]interface{}{data}
	dataBytes, _ := json.Marshal(list)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getSandBoxHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestBidRecommendationSummary(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/keyword/bid_recommendation", server.URL)
	data := make(map[string]interface{})

	keywords := make(map[string]interface{})
	keywords["keyword"] = 276587758569551
	keywords["matchType"] = "exact"

	list := []map[string]interface{}{keywords}

	data["adGroupId"] = 26651230569540
	data["keywords"] = list

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestSuggestionsByAsin(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/keyword/suggestions_by_asin", server.URL)
	data := make(map[string]interface{})

	data["asins"] = []string{"B06XTKVZC7"}
	data["maxNumSuggestions"] = 50

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestSuggestionsByAdGroup(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/keyword/suggestions_by_adGroup", server.URL)
	data := make(map[string]interface{})

	data["adGroupId"] = "26651230569540"
	data["maxNumSuggestions"] = 50

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestMaxBidHistory(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/keyword/max_bid_history", server.URL)
	fromDate := time.Now().AddDate(0,-1,0)
	todate := time.Now().AddDate(0,0,-2)
	data := make(map[string]interface{})
	data["campaignId"]=50313246492316
	data["adGroupId"] = 26651230569540
	data["keywordId"] = 258627375756953
	data["fromDate"]=&fromDate
	data["toDate"]=&todate


	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}


func TestDailyBidHistory(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/keyword/daily_bid_history", server.URL)
	fromDate := time.Now().AddDate(0,-1,0)
	todate := time.Now().AddDate(0,0,-2)
	data := make(map[string]interface{})
	data["campaignId"]=50313246492316
	data["adGroupId"] = 26651230569540
	data["keywordId"] = 258627375756953
	data["fromDate"]=&fromDate
	data["toDate"]=&todate


	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}
