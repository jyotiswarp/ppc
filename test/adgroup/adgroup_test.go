package adgroup

import (
	"bitbucket.org/jyotiswarp/ppc/test/testutils"
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func getSandBoxHeaders() map[string]string {
	headers := make(map[string]string)
	headers["sellerId"] = "A3MV9OCIAOMFIM"
	headers["marketplace"] = "amazon"
	headers["geo"] = "sandbox"
	return headers

}

func getProductionHeaders() map[string]string {
	headers := make(map[string]string)
	headers["sellerId"] = "A3MV9OCIAOMFIM"
	headers["marketplace"] = "amazon"
	headers["geo"] = "us"
	return headers

}

func TestAdGroupListByCampaign(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/adgroup/list?campaign_id=%s", server.URL, "36385441958655")
	resp, err := testutils.Request(handlers, "GET", endpoint, "", getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAdGroupUpdate(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/management/adgroup/update", server.URL)
	data := make(map[string]interface{})

	data["campaignId"] = 26860701392333
	data["adGroupId"] = 165719725436070
	data["name"] = "Test Ad group"
	data["state"] = "enabled"
	data["defaultBid"] = 5.5
	list := []map[string]interface{}{data}

	dataBytes, _ := json.Marshal(list)
	resp, err := testutils.Request(handlers, "PUT", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAllAdGroupsTrends(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/adgroups/36385441958655/trends", server.URL)
	data := make(map[string]interface{})

	data["startDate"] = time.Now().AddDate(0, -2, 0)
	data["endDate"] = time.Now()

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAllAdGroupsSummary(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/adgroups/36385441958655/summary", server.URL)
	data := make(map[string]interface{})

	data["fromDate"] = time.Now().AddDate(0, -2, 0)
	data["toDate"] = time.Now()

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAdGroupTrends(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/adgroup/36385441958655/145039278104465/trends", server.URL)
	data := make(map[string]interface{})

	data["startDate"] = time.Now().AddDate(0, -2, 0)
	data["endDate"] = time.Now()

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAdGroupSummary(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/adgroup/36385441958655/145039278104465/summary", server.URL)
	data := make(map[string]interface{})

	data["fromDate"] = time.Now().AddDate(0, -2, 0)
	data["toDate"] = time.Now()

	dataBytes, _ := json.Marshal(data)
	resp, err := testutils.Request(handlers, "POST", endpoint, string(dataBytes), getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}

func TestAdGroupBidRecommendation(t *testing.T) {
	server, handlers := testutils.InitWithApi()
	endpoint := fmt.Sprintf("%s/adgroup/36385441958655/145039278104465/bid_recommendations", server.URL)
	resp, err := testutils.Request(handlers, "GET", endpoint, "", getProductionHeaders(), false)
	testutils.CheckIfResponse200(resp, err, t)
}
