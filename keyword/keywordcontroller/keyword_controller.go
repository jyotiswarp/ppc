package keywordcontroller

import (
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordservices"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
)

func GetKeywordList(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	result, err := keywordservices.GetKeywordList(ppcManagement, true)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func GetKeywordByCampAndAdGroup(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*keywordmodel.GetKeywordForm)
	result, err := keywordservices.GetKeyword(ppcManagement, *formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}
func GetNegativeKeywordList(c *gin.Context) {

	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	result, err := keywordservices.GetNegativeKeywordList(ppcManagement, false)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func GetNegativeExactKeywordList(c *gin.Context) {

	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	result, err := keywordservices.GetNegativeExactKeywordList(ppcManagement)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func GetBiddableRecommendation(c *gin.Context) {

	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*keywordmodel.BidRecommendationForm)
	result, err := keywordservices.GetBiddableRecommendation(ppcManagement, formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func GetAsinKeywordSuggestions(c *gin.Context) {

	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*keywordmodel.AsinSuggestionForm)

	result, err := keywordservices.GetAsinKeywordSuggestions(ppcManagement, formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func GetAdGroupKeywordSuggestions(c *gin.Context) {

	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*keywordmodel.AdGroupKeywordSuggestionForm)

	result, err := keywordservices.GetAdGroupKeywordSuggestions(ppcManagement, formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func AddNegativeKeyword(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*[]keywordmodel.KeywordForm)
	//temp code just for integration
	//c.JSON(200, gin.H{
	//	"result": "success",
	//})
	//return

	result, err := keywordservices.AddNegativeKeywordList(ppcManagement, *formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func AddKeyword(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*[]keywordmodel.KeywordForm)

	//temp code just for integration
	//c.JSON(200, gin.H{
	//	"result": "success",
	//})
	//return

	result, err := keywordservices.CreateAdGroupKeywordList(ppcManagement, formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}
func UpdateNegativeKeyword(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*[]keywordmodel.KeywordForm)
	//temp code just for integration
	//c.JSON(200, gin.H{
	//	"result": "success",
	//})
	//return

	result, err := keywordservices.UpdateNegativeKeyword(ppcManagement, formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func UpdateKeyword(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*[]keywordmodel.KeywordForm)
	//temp code just for integration
	//c.JSON(200, gin.H{
	//	"result": "success",
	//})
	//return

	result, err := keywordservices.UpdateKeyword(ppcManagement, formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func DeleteKeyword(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*[]keywordmodel.DeleteKeywordForm)
	//temp code just for integration
	//responseMap := make(map[int64]string)
	//responseMap[123] = "Success"
	//responseMap[1234] = "Failed"
	//c.JSON(200, gin.H{
	//	"result": responseMap,
	//})
	//return
	result, err := keywordservices.DeleteKeyword(ppcManagement, *formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}

func GetMaxKeywordBidHistory(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*keywordmodel.KeywordBidHistory)

	result, err := keywordservices.GetMaxKeywordBidHistory(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetDailyKeywordBidHistory(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*keywordmodel.KeywordBidHistory)

	//temp code just for integration
	//c.JSON(200, gin.H{
	//	"result": "success",
	//})
	//return

	result, err := keywordservices.GetDialyKeywordBidHistory(ppcManagement, formRequest)

	if err != nil {
		c.JSON(400, gin.H{
			"error": "Please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func MarkExistingKeywordAsNegative(c *gin.Context) {
	ppcManagement := c.Keys[middlewares.PpcManagementCommons].(ppcmanagementcommons.PPCManagementCommon)
	formRequest := c.Keys["form_data"].(*keywordmodel.MarkKeywordNegativeForm)
	//temp code just for integration
	c.JSON(200, gin.H{
		"result": "success",
	})
	return
	result, err := keywordservices.MarkKeywordAsNegative(ppcManagement, *formRequest)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "please try again",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return

}
