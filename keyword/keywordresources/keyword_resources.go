package keywordresources

import (
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordcontroller"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
)

func KeywordResources(router *gin.Engine) {
	managementKeywordResources := router.Group("/management/keyword", middlewares.PPCRequestMiddleware())
	{
		managementKeywordResources.POST("/list", middlewares.Validator(keywordmodel.GetKeywordForm{}), keywordcontroller.GetKeywordByCampAndAdGroup)
		managementKeywordResources.GET("/all", keywordcontroller.GetKeywordList)
		managementKeywordResources.GET("/all_negative", keywordcontroller.GetNegativeKeywordList)

		managementKeywordResources.POST("/negative", middlewares.Validator([]keywordmodel.KeywordForm{}), keywordcontroller.AddNegativeKeyword)
		managementKeywordResources.POST("/biddable", middlewares.Validator([]keywordmodel.KeywordForm{}), keywordcontroller.AddKeyword)

		managementKeywordResources.PUT("/negative", middlewares.Validator([]keywordmodel.KeywordForm{}), keywordcontroller.UpdateNegativeKeyword)
		managementKeywordResources.PUT("/biddable", middlewares.Validator([]keywordmodel.KeywordForm{}), keywordcontroller.UpdateKeyword)

		managementKeywordResources.PUT("/mark_negative", middlewares.Validator(keywordmodel.MarkKeywordNegativeForm{}), keywordcontroller.MarkExistingKeywordAsNegative)

		managementKeywordResources.DELETE("/list", middlewares.Validator([]keywordmodel.DeleteKeywordForm{}),keywordcontroller.DeleteKeyword)
		managementKeywordResources.GET("/negative_exact", keywordcontroller.GetNegativeExactKeywordList)



		managementKeywordResources.POST("/bid_recommendation", middlewares.Validator(keywordmodel.BidRecommendationForm{}), keywordcontroller.GetBiddableRecommendation)
		managementKeywordResources.POST("/suggestions_by_asin", middlewares.Validator(keywordmodel.AsinSuggestionForm{}), keywordcontroller.GetAsinKeywordSuggestions)
		managementKeywordResources.POST("/suggestions_by_adGroup", middlewares.Validator(keywordmodel.AdGroupKeywordSuggestionForm{}), keywordcontroller.GetAdGroupKeywordSuggestions)
		managementKeywordResources.POST("/max_bid_history", middlewares.Validator(keywordmodel.KeywordBidHistory{}), keywordcontroller.GetMaxKeywordBidHistory)
		managementKeywordResources.POST("/daily_bid_history", middlewares.Validator(keywordmodel.KeywordBidHistory{}), keywordcontroller.GetDailyKeywordBidHistory)
	}

}
