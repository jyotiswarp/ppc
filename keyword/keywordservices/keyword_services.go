package keywordservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/keyword/keyworddao"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"strconv"
)

func UpdateKeywordDataModel(ppcCommon ppcmanagementcommons.PPCManagementCommon, keywordForm keywordmodel.KeywordDetailsForm) (err error) {

	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err := market.GetKeywordListExtended(keywordForm)
		if err != nil {
			log.Error(err)
			return err
		}
		err = keyworddao.PushKeywordDataToDb(result, ppcCommon.Profile.ProfileId)
		return err
	}

	return errors.New("invalid market")
}

func GetKeywordList(ppcCommon ppcmanagementcommons.PPCManagementCommon, forceUpdate bool) (result []models.Keyword, err error) {

	if forceUpdate {
		keywordForm := keywordmodel.KeywordDetailsForm{}
		err = UpdateKeywordDataModel(ppcCommon, keywordForm)
		if err != nil {
			log.Error(err)
			return
		}
	}
	result, err = keyworddao.GetKeywordInformation(ppcCommon.SellerID, ppcCommon.Profile.ProfileId)
	if err != nil {
		return result, err
	}
	return result, err

}

func GetKeyword(ppcCommon ppcmanagementcommons.PPCManagementCommon, form keywordmodel.GetKeywordForm) (result []models.Keyword, err error) {
	if len(form.State) < 1 {
		form.State = append(form.State, "archived", "enabled", "paused")
	}
	result, err = keyworddao.GetKeywordInformationByCampAndAdGroup(ppcCommon.Marketplace, ppcCommon.Geo, ppcCommon.SellerID, ppcCommon.Profile.ProfileId, form)
	if err != nil {
		return result, err
	}
	return result, err

}
func AddNegativeKeywordList(ppcCommon ppcmanagementcommons.PPCManagementCommon, form []keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.CreateAdGroupNegativeKeywordList(&form)
		if err != nil {
			log.Error(err)
			return
		}
		updateNegativeKeys(ppcCommon, result)
		return
	}

	return result, errors.New("invalid market")

}

func CreateAdGroupKeywordList(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	finalForm := setDefaultValueAsEnabled(form)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.CreateAdGroupKeywordList(&finalForm)
		if err != nil {
			log.Error(err)
			return
		}
		updateBiddableKeys(ppcCommon, result)
		return
	}
	return result, errors.New("invalid market")

}

func setDefaultValueAsEnabled(form *[]keywordmodel.KeywordForm) []keywordmodel.KeywordForm {
	var finalForm []keywordmodel.KeywordForm
	for _, key := range *form {
		copyKey := key
		copyKey.State = "enabled"
		finalForm = append(finalForm, copyKey)
	}
	return finalForm
}

func UpdateNegativeKeyword(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.UpdateAdGroupNegativeKeywordList(form)
		if err != nil {
			log.Error(err)
			return
		}
		updateNegativeKeys(ppcCommon, result)
		return
	}

	return result, errors.New("invalid market")

}

func UpdateKeyword(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *[]keywordmodel.KeywordForm) (result []keywordmodel.KeywordResponse, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.UpdateAdGroupKeywordList(form)
		if err != nil {
			log.Error(err)
			return
		}
		updateBiddableKeys(ppcCommon, result)
		return
	}

	return result, errors.New("invalid market")

}

type DeleteKeywordResponse struct {
	KeywordID int64
	Result    keywordmodel.KeywordResponse
	Err       error
}

func DeleteKeyword(ppcCommon ppcmanagementcommons.PPCManagementCommon, keywords []keywordmodel.DeleteKeywordForm) (responseMap map[int64]string, err error) {
	responseMap = make(map[int64]string)
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		deleteKeyRespChan := make(chan DeleteKeywordResponse)
		for _, key := range keywords {
			go deleteKeyword(market, ppcCommon, key.KeywordId, deleteKeyRespChan)
		}
		for i := 0; i < len(keywords); i++ {
			deleteResp := <-deleteKeyRespChan
			if deleteResp.Err != nil {
				responseMap[deleteResp.KeywordID] = "Failed"
				continue
			}
			responseMap[deleteResp.KeywordID] = "Success"
		}
		return
	}

	return responseMap, errors.New("invalid market")

}

func deleteKeyword(market backends.MarketClient, ppcCommon ppcmanagementcommons.PPCManagementCommon, keywordID int64, deleteKeyRespChan chan DeleteKeywordResponse) {
	deleteResp := DeleteKeywordResponse{KeywordID: keywordID}
	result, err := market.DeleteKeyword(fmt.Sprint(keywordID))
	if err != nil {
		log.Error(err)
		deleteResp.Err = err
		deleteKeyRespChan <- deleteResp

	}
	deleteResp.Result = result
	keywordIds := []int64{result.KeywordId}
	SyncBiddableKeywords(ppcCommon, keywordIds)
	deleteKeyRespChan <- deleteResp
}

func GetNegativeKeywordList(ppcCommon ppcmanagementcommons.PPCManagementCommon, forceUpdate bool) (result []models.NegativeKeyword, err error) {
	if forceUpdate {
		form := keywordmodel.KeywordDetailsForm{}
		err = UpdateNegativeKeywordDataModel(ppcCommon, form)
		if err != nil {
			log.Error(err)
			return
		}
	}
	result, err = keyworddao.GetNegativeKeywordInformation(ppcCommon)
	if err != nil {
		return result, err
	}

	return result, err

}

func GetBiddableRecommendation(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *keywordmodel.BidRecommendationForm) (result keywordmodel.KeywordBidRecomendations, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.GetBiddableRecommendation(form)
		if err != nil {
			log.Error(err)
			return
		}
		return
	}

	return result, errors.New("invalid market")

}

func GetAsinKeywordSuggestions(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *keywordmodel.AsinSuggestionForm) (result []keywordmodel.SuggestedKeywords, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.GetKeywordSuggestionsByAsin(form)
		if err != nil {
			log.Error(err)
			return
		}
		return
	}

	return result, errors.New("invalid market")

}

func GetAdGroupKeywordSuggestions(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *keywordmodel.AdGroupKeywordSuggestionForm) (result []keywordmodel.SuggestedKeywords, err error) {
	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err = market.GetKeywordSuggestionsByAdGroup(form)
		if err != nil {
			log.Error(err)
			return
		}
		return
	}

	return result, errors.New("invalid market")

}

func UpdateNegativeKeywordDataModel(ppcCommon ppcmanagementcommons.PPCManagementCommon, form keywordmodel.KeywordDetailsForm) (err error) {

	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err := market.GetAdGroupNegativeKeywordListExtended(form)
		if err != nil {
			log.Error(err)
			return err
		}
		err = keyworddao.PushNegativeKeywordDataToDb(result, ppcCommon.Profile.ProfileId)
		return err
	}

	return errors.New("invalid market")
}

func GetMaxKeywordBidHistory(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *keywordmodel.KeywordBidHistory) (bidHistory []models.KeywordHistory, err error) {

	profileID := strconv.Itoa(int(ppcCommon.Profile.ProfileId))
	bidHistory, err = keyworddao.FetchMaxKeywordBidHistory(ppcCommon.SellerID, profileID, form)
	if err != nil {
		log.Error(err)
		return
	}

	return
}

func GetDialyKeywordBidHistory(ppcCommon ppcmanagementcommons.PPCManagementCommon, form *keywordmodel.KeywordBidHistory) (bidHistory []keywordmodel.KeywordDailyBid, err error) {

	profileID := strconv.Itoa(int(ppcCommon.Profile.ProfileId))
	bidHistory, err = keyworddao.FetchDailyKeywordBidHistory(ppcCommon.SellerID, profileID, form)
	if err != nil {
		log.Error(err)
		return
	}

	return
}

func GetNegativeExactKeywordList(ppcCommon ppcmanagementcommons.PPCManagementCommon) (bidHistory []models.NegativeKeyword, err error) {

	profileID := strconv.Itoa(int(ppcCommon.Profile.ProfileId))
	bidHistory, err = keyworddao.FetchNegativeExact(ppcCommon.SellerID, profileID)
	if err != nil {
		log.Error(err)
		return
	}

	return
}

func MarkKeywordAsNegative(ppcCommon ppcmanagementcommons.PPCManagementCommon, form keywordmodel.MarkKeywordNegativeForm) (keyword models.Keyword, err error) {

	keyword, err = keyworddao.GetKeyword(form)
	if err != nil {
		log.Error(err)
		return
	}
	negativeKeywordReq := keywordmodel.KeywordForm{CampaignId: form.CampaignId, AdGroupId: form.AdGroupId, KeywordText: keyword.KeywordText, MatchType: keyword.MatchType, State: keyword.State}
	_, err = AddNegativeKeywordList(ppcCommon, []keywordmodel.KeywordForm{negativeKeywordReq})
	if err != nil {
		log.Error(err)
		return
	}
	keyword.MarkedNegative = true
	err = keyworddao.GetDB().Save(&keyword).Error
	if err != nil {
		log.Error(err)
		return
	}
	return

}

func updateBiddableKeys(ppcCommon ppcmanagementcommons.PPCManagementCommon, result []keywordmodel.KeywordResponse) {
	var keywordIds []int64
	for _, res := range result {
		keywordIds = append(keywordIds, res.KeywordId)
	}
	SyncBiddableKeywords(ppcCommon, keywordIds)
}

func updateNegativeKeys(ppcCommon ppcmanagementcommons.PPCManagementCommon, result []keywordmodel.KeywordResponse) {
	var keywordIds []int64
	for _, res := range result {
		keywordIds = append(keywordIds, res.KeywordId)
	}
	SyncNegativeKeywords(ppcCommon, keywordIds)
}

func SyncBiddableKeywords(ppcCommon ppcmanagementcommons.PPCManagementCommon, keywordIDs []int64) {
	for _, id := range keywordIDs {
		UpdateKeywordDataByID(ppcCommon, id)
	}
}

func SyncNegativeKeywords(ppcCommon ppcmanagementcommons.PPCManagementCommon, keywordIDs []int64) {
	for _, id := range keywordIDs {
		UpdateNegativeKeywordDataByID(ppcCommon, id)
	}
}

func SyncSpDBWithUpdatedKeywords(ppcCommon ppcmanagementcommons.PPCManagementCommon) {
	GetKeywordList(ppcCommon, true)
	GetNegativeKeywordList(ppcCommon, true)
}

func UpdateKeywordDataByID(ppcCommon ppcmanagementcommons.PPCManagementCommon, keywordID int64) (err error) {

	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err := market.GetExtendedKeywordByID(keywordID)
		if err != nil {
			log.Error(err)
			return err
		}
		err = keyworddao.CreateOrUpdateKeywordData(result, ppcCommon.Profile.ProfileId)
		return err
	}

	return errors.New("invalid market")
}

func UpdateNegativeKeywordDataByID(ppcCommon ppcmanagementcommons.PPCManagementCommon, keywordID int64) (err error) {

	market := backends.GetMarketPlace(ppcCommon.Marketplace, ppcCommon.Geo)
	if market != nil {
		market.SetSellerId(ppcCommon.SellerID)
		market.SetProfileId(ppcCommon.Profile.ProfileId)
		market.SetEmail(ppcCommon.Profile.Email)
		result, err := market.GetExtendedNegativeKeywordByID(keywordID)
		if err != nil {
			log.Error(err)
			return err
		}
		err = keyworddao.CreateOrUpdateNegativeKeyword(result, ppcCommon.Profile.ProfileId)
		return err
	}

	return errors.New("invalid market")
}
