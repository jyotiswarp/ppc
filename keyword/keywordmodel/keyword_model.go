package keywordmodel

import "time"

type KeywordForm struct {
	CampaignId  int64   `json:"campaignId,omitempty" binding:"required"`
	AdGroupId   int64   `json:"adGroupId,omitempty" binding:"required"`
	KeywordText string  `json:"keywordText,omitempty"`
	MatchType   string  `json:"matchType,omitempty"`
	State       string  `json:"state,omitempty"`
	KeywordId   int64   `json:"keywordId,omitempty"`
	Bid         float64 `json:"bid,omitempty"`
}

type MarkKeywordNegativeForm struct {
	CampaignId int64 `json:"campaignId" binding:"required"`
	AdGroupId  int64 `json:"adGroupId" binding:"required"`
	KeywordId  int64 `json:"keywordId" binding:"required"`
}

type GetKeywordForm struct {
	CampaignId int64    `json:"campaignId,omitempty" binding:"required"`
	AdGroupId  int64    `json:"adGroupId,omitempty" binding:"required"`
	State      []string `json:"state"`
}

type BidRecommendationForm struct {
	AdGroupId int64            `json:"adGroupId,omitempty"`
	Keywords  []KeywordDetails `json:"keywords,omitempty" binding:"required"`
}

type AsinSuggestionForm struct {
	//AdGroupId int64            `json:"adGroupId,omitempty" binding:"required"`
	Asins             []string `json:"asins,omitempty" binding:"required"`
	MaxNumSuggestions int      `json:"maxNumSuggestions,omitempty"`
}

type AdGroupKeywordSuggestionForm struct {
	//AdGroupId int64            `json:"adGroupId,omitempty" binding:"required"`
	AdGroupId         int64 `json:"adGroupId,omitempty" binding:"required"`
	MaxNumSuggestions int   `json:"maxNumSuggestions,omitempty"`
}

type KeywordDetails struct {
	Keyword   string  `json:"keyword,omitempty"`
	MatchType string `json:"matchType,omitempty" binding:"required"`
}

type KeywordResponse struct {
	Code        string `json:"code,omitempty"`
	Description string `json:"description,omitempty"`
	KeywordId   int64  `json:"keywordId,omitempty"`
}

type KeywordBidRecomendations struct {
	AdGroupId       int64        `json:"adGroupId,omitempty"`
	Recommendations []KeywordBid `json:"recommendations,omitempty"`
}
type KeywordSuggestions struct {
	Asin              int64               `json:"asin,omitempty"`
	SuggestedKeywords []SuggestedKeywords `json:"suggested_keywords,omitempty"`
}

type SuggestedKeywords struct {
	KeywordText string `json:"keywordText"`
	MatchType   string `json:"matchType"`
}

type AdGroupKeywordSuggestion struct {
	AdGroupId         int64               `json:"adGroupId"`
	SuggestedKeywords []SuggestedKeywords `json:"suggestedKeywords"`
}

type KeywordBid struct {
	Code         string              `json:"code"`
	Keyword      string              `json:"keyword"`
	MatchType    string              `json:"matchType,omitempty"`
	SuggestedBid SuggestedBidDetails `json:"suggestedBid"`
}
type SuggestedBidDetails struct {
	RangeEnd   float64 `json:"rangeEnd"`
	RangeStart float64 `json:"rangeStart"`
	Suggested  float64 `json:"suggested"`
}
type KeywordDetailsForm struct {
	StartIndex       string  `json:"startIndex,omitempty"`
	Count            string  `json:"count,omitempty"`
	CampaignType     string  `json:"campaignType,omitempty"`
	StateFilter      string  `json:"stateFilter,omitempty"`
	Name             string  `json:"name,omitempty"`
	CampaignIdFilter []int64 `json:"campaignIdFilter,omitempty"`
	AdGroupIdFilter  []int64 `json:"adGroupIdFilter,omitempty"`
	KeywordText      string  `json:"keywordText,omitempty"`
}
type KeywordBidHistory struct {
	CampaignId int64      `json:"campaignId,omitempty" binding:"required"`
	AdGroupId  int64      `json:"adGroupId,omitempty" binding:"required"`
	KeywordId  int64      `json:"keywordId,omitempty" binding:"required"`
	FromDate   *time.Time `json:"fromDate,omitempty" binding:"required"`
	ToDate     *time.Time `json:"toDate,omitempty" binding:"required"`
}

type KeywordDailyBid struct {
	Date *time.Time `json:"date,omitempty"`
	Bid  float64    `json:"bid,omitempty"`
}

type DeleteKeywordForm struct {
	CampaignId int64 `json:"campaignId" binding:"required"`
	AdGroupId  int64 `json:"adGroupId" binding:"required"`
	KeywordId  int64 `json:"keywordId" binding:"required"`
}
