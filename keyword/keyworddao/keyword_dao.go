package keyworddao

import (
	"bitbucket.org/jyotiswarp/ppc/dao"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	log "github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
)

func GetDB() *gorm.DB {
	return mysql.DB
}

func GetKeywordDetails(keywordId int64) (err error, result models.Keyword) {

	err = mysql.DB.Where("keyword_id = ?", keywordId).Find(&result).Error
	return
}

func PushKeywordDataToDb(result []models.Keyword, profileId int64) (err error) {

	for _, data := range result {
		err = CreateOrUpdateKeywordData(data, profileId)
	}

	return nil
}

func CreateOrUpdateKeywordData(data models.Keyword, profileId int64) (err error) {
	row := dao.CheckDataPresence(data).(models.Keyword)
	if row.ID == 0 {
		//record not present
		err = mysql.DB.Create(&data).Error
		if err != nil {
			log.Error(err)
			return
		}
	} else {
		//record present
		data.ID = row.ID
		data.CreatedAt = row.CreatedAt
		data.MarkedNegative = row.MarkedNegative
		err = mysql.DB.Save(&data).Error
		if err != nil {
			log.Error(err)
			return
		}
	}

	history := models.KeywordHistory{AdGroupId: data.AdGroupId, State: data.State, CampaignId: data.CampaignId,
		ServingStatus: data.ServingStatus, CreationDate: data.CreationDate,
		LastUpdatedDate: data.LastUpdatedDate, MatchType: data.MatchType, KeywordId: data.KeywordId,
		KeywordText: data.KeywordText, Bid: data.Bid}
	rowHistory := dao.CheckDataPresence(history).(models.KeywordHistory)
	if rowHistory.ID == 0 {
		err = mysql.DB.Create(&history).Error
		if err != nil {
			log.Error(err)
			return
		}
	}
	return
}

func PushNegativeKeywordDataToDb(result []models.NegativeKeyword, profileId int64) (err error) {

	for _, data := range result {
		err = CreateOrUpdateNegativeKeyword(data, profileId)
	}

	return
}

func CreateOrUpdateNegativeKeyword(data models.NegativeKeyword, profileId int64) (err error) {
	row := dao.CheckDataPresence(data).(models.NegativeKeyword)
	if row.ID == 0 {
		//record not present
		err = mysql.DB.Create(&data).Error
		if err != nil {
			return err
		}
	} else {
		//record present
		data.ID = row.ID
		data.CreatedAt = row.CreatedAt
		err = mysql.DB.Save(&data).Error
		if err != nil {
			return err
		}
	}
	history := models.NegativeKeywordHistory{AdGroupId: data.AdGroupId, State: data.State, CampaignId: data.CampaignId,
		ServingStatus: data.ServingStatus, CreationDate: data.CreationDate,
		LastUpdatedDate: data.LastUpdatedDate, MatchType: data.MatchType, KeywordId: data.KeywordId,
		KeywordText: data.KeywordText, Bid: data.Bid}
	rowHistory := dao.CheckDataPresence(history).(models.NegativeKeywordHistory)
	if rowHistory.ID == 0 {
		err = mysql.DB.Create(&history).Error
		if err != nil {
			log.Error(err)
			return
		}
	}
	return nil
}

func GetKeywordInformation(sellerId string, profileId int64) (result []models.Keyword, err error) {

	err = mysql.DB.Table("campaigns").
		Select("distinct keywo.*").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Joins("join ad_groups adg on adg.campaign_id = campaigns.campaign_id").
		Joins("join keywords as keywo on keywo.campaign_id = campaigns.campaign_id and keywo.ad_group_id = adg.ad_group_id").
		Where("prof.seller_string_id = ? and prof.profile_id=?", sellerId, profileId).
		Find(&result).Error

	if err != nil {
		log.Error(err)
		return result, err
	}
	return result, nil

}
func GetKeywordInformationByCampAndAdGroup(marketPlace, geo, sellerId string, profileId int64, form keywordmodel.GetKeywordForm) (result []models.Keyword, err error) {

	err = mysql.DB.Table("campaigns").
		Select("distinct keywo.*").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Joins("join ad_groups adg on adg.campaign_id = campaigns.campaign_id").
		Joins("join keywords as keywo on keywo.campaign_id = campaigns.campaign_id and keywo.ad_group_id = adg.ad_group_id").
		Where("adg.campaign_id =? and adg.ad_group_id = ?", form.CampaignId, form.AdGroupId).
		Where("market_place_name = ?  and geo = ? and prof.seller_string_id = ? and prof.profile_id=? and keywo.state in (?) AND keywo.marked_negative=0 ", marketPlace, geo, sellerId, profileId, form.State).
		Find(&result).Error

	if err != nil {
		log.Error(err)
		return result, err
	}
	return result, nil

}
func GetNegativeKeywordInformation(ppcCommon ppcmanagementcommons.PPCManagementCommon) (result []models.NegativeKeyword, err error) {
	err = mysql.DB.Table("campaigns").
		Select("distinct negative_keywords.*").
		Joins("join negative_keywords on negative_keywords.campaign_id = campaigns.campaign_id").
		Where("campaigns.profile_id = ?", ppcCommon.ProfileID).
		Find(&result).Error

	if err != nil {
		log.Error(err)
		return result, err
	}
	return result, nil

}

func FetchMaxKeywordBidHistory(sellerId, profileId string, form *keywordmodel.KeywordBidHistory) (result []models.KeywordHistory, err error) {

	err = mysql.DB.Table("campaigns").
		Select("distinct keywo.bid bid, min(keywo.last_updated_date) last_updated_date").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Joins("join ad_groups adg on adg.campaign_id = campaigns.campaign_id").
		Joins("join keyword_histories as keywo on keywo.campaign_id = campaigns.campaign_id and keywo.ad_group_id = adg.ad_group_id").
		Where("prof.seller_string_id = ? and prof.profile_id=?", sellerId, profileId).
		Where("keywo.campaign_id = ? and keywo.ad_group_id = ? and keywo.keyword_id = ?", form.CampaignId, form.AdGroupId, form.KeywordId).
		Group("keywo.bid").
		Find(&result).Error

	if err != nil {
		log.Error(err)
		return result, err
	}
	return result, nil
	return
}

func FetchDailyKeywordBidHistory(sellerId, profileId string, form *keywordmodel.KeywordBidHistory) (result []keywordmodel.KeywordDailyBid, err error) {

	//keyword := models.KeywordReportData{}
	err = mysql.DB.Table("keyword_report_data").
		Select("keyword_report_data.report_date date, keyword_report_data.cpc bid").
		Joins("join campaigns on campaigns.campaign_id = keyword_report_data.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("prof.seller_string_id = ? and prof.profile_id=?", sellerId, profileId).
		Where("keyword_report_data.report_date between ? and ? and keyword_report_data.campaign_id =? and keyword_report_data.ad_group_id=? and keyword_report_data.keyword_id = ?", form.FromDate, form.ToDate, form.CampaignId, form.AdGroupId, form.KeywordId).
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}

	if err != nil {
		log.Error(err)
		return result, err
	}
	return result, nil
}

func FetchNegativeExact(sellerId, profileId string) (result []models.NegativeKeyword, err error) {

	//keyword := models.KeywordReportData{}
	err = mysql.DB.Table("keywords").
		Joins("join campaigns on campaigns.campaign_id = keywords.campaign_id").
		Joins("join profiles prof on  prof.profile_id=campaigns.profile_id").
		Where("prof.seller_string_id = ? and prof.profile_id=?", sellerId, profileId).
		Where("keywords.match_type = 'negativeExact'").
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}

	if err != nil {
		log.Error(err)
		return result, err
	}
	return result, nil
}

func GetKeyword(form keywordmodel.MarkKeywordNegativeForm) (result models.Keyword, err error) {

	err = mysql.DB.Where("campaign_id =? and ad_group_id=? and keyword_id = ?", form.CampaignId, form.AdGroupId, form.KeywordId).Find(&result).Error
	return
}
