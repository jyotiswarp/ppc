package rulesresources

import (
	"bitbucket.org/jyotiswarp/ppc/rules/rulescontroller"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesmodels"
	"bitbucket.org/jyotiswarp/ppc/utils/middlewares"
	"github.com/gin-gonic/gin"
)

func RulesApi(router *gin.Engine) {

	ppcCampaign := router.Group("/rule")
	{
		ppcCampaign.POST("/createRule", middlewares.Validator(rulesmodels.RulesForm{}), rulescontroller.CreateRule)
		ppcCampaign.GET("/getAllRules", rulescontroller.GetAllRules)
		ppcCampaign.GET("/getEventLogs", rulescontroller.GetEventLogs)
		ppcCampaign.GET("/getRules", rulescontroller.GetRulesBySellerId)
		ppcCampaign.POST("/changeRuleState", middlewares.Validator(rulesmodels.RulesStateForm{}), rulescontroller.ChangeRuleState)
		ppcCampaign.POST("/deleteRule", middlewares.Validator(rulesmodels.RulesDeleteForm{}), rulescontroller.DeleteRule)

	}
}
