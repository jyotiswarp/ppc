package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/campaign/campaignmodels"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/report/reportservices"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesdao"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesmodels"
	"bitbucket.org/jyotiswarp/ppc/utils/mail"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"strconv"
)

func pauseCampaign(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "paused")
	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)
	if market != nil {
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		form := campaignmodels.CampaignUpdateForm{CampaignId: c.CampaignId, State: "paused"}
		result, err := market.UpdateCampaign(&[]campaignmodels.CampaignUpdateForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and got failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "pauseCampaign"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d , campaign paused",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId)
		logMessage.IsError = false
		logMessage.Action = "pauseCampaign"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}

}

func enableCampaign(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled")
	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)
	if market != nil {
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		form := campaignmodels.CampaignUpdateForm{CampaignId: c.CampaignId, State: "enabled"}
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		result, err := market.UpdateCampaign(&[]campaignmodels.CampaignUpdateForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "enableCampaign"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d , campaign enabled",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId)
		logMessage.IsError = false
		logMessage.Action = "enableCampaign"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}
		if r.AlertRequired {
			mail.SendMail("ppc@sp.com", "testing", "subair.basheer777@gmail.com", "", "")
		}

	} else {
		log.Error("invalid market")
	}
}

func increaseCampaignBudgetByPercentage(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled")
	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)
	if market != nil {
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		err, bugetPercentage := rulesdao.GetRuleAttribute(r.RuleMapId, "budget_percentage")
		if err != nil {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "increaseCampaignBudgetByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		//fmt.Println("(100 - input.ActionValue)", (input.ActionValue))
		budgetPer, _ := strconv.ParseFloat(bugetPercentage, 64)
		maxBudgetLimit := 0.0
		budget := c.DailyBudget * (100 + budgetPer) / 100

		err, maxbudget := rulesdao.GetRuleAttribute(r.RuleMapId, "budget_max_limit")
		if err != nil {
			maxBudgetLimit = viper.GetFloat64("max_budget_limit")
		} else {
			maxBudgetLimit, _ = strconv.ParseFloat(maxbudget, 64)
		}
		if budget > maxBudgetLimit || budget <= 0 {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed since it reached maximum budget %f",
				r.RuleName, maxBudgetLimit)
			logMessage.IsError = true
			logMessage.Action = "increaseCampaignBudgetByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		//check for maximum bid
		//maxBid =
		form := campaignmodels.CampaignUpdateForm{CampaignId: c.CampaignId, DailyBudget: budget}

		result, err := market.UpdateCampaign(&[]campaignmodels.CampaignUpdateForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "increaseCampaignBudgetByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d , campaign budget increased to %f",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, budget)
		logMessage.IsError = false
		logMessage.Action = "increaseCampaignBudgetByPercentage"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}
}
func decreaseCampaignBudgetByPercentage(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled")
	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)
	if market != nil {
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		err, bugetPercentage := rulesdao.GetRuleAttribute(r.RuleMapId, "budget_percentage")
		if err != nil {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "decreaseCampaignBudgetByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		//fmt.Println("(100 - input.ActionValue)", (input.ActionValue))
		budgetPer, _ := strconv.ParseFloat(bugetPercentage, 64)
		//fmt.Println("(100 - input.ActionValue)", (input.ActionValue))
		budget := c.DailyBudget * (100 - budgetPer) / 100
		minBudget := 0.0
		err, maxbudget := rulesdao.GetRuleAttribute(r.RuleMapId, "budget_min_limit")
		if err != nil {
			minBudget = 0 //viper.GetFloat64("max_budget_limit")
		} else {
			minBudget, _ = strconv.ParseFloat(maxbudget, 64)
		}
		if budget < minBudget || budget <= 0 {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed since it reached minimum budget %f",
				r.RuleName, minBudget)
			logMessage.IsError = true
			logMessage.Action = "decreaseCampaignBudgetByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}

		form := campaignmodels.CampaignUpdateForm{CampaignId: c.CampaignId, DailyBudget: budget}
		result, err := market.UpdateCampaign(&[]campaignmodels.CampaignUpdateForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error",
				r.RuleName)
			logMessage.IsError = true
			logMessage.Action = "decreaseCampaignBudgetByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d , campaign budget decreased to %f",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, budget)
		logMessage.IsError = false
		logMessage.Action = "decreaseCampaignBudgetByPercentage"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}
}

func increaseCampaignBudgetByValue(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled")
	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)
	if market != nil {
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		err, bugetPercentage := rulesdao.GetRuleAttribute(r.RuleMapId, "budget_value")
		if err != nil {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err.Error())
			logMessage.IsError = true
			logMessage.Action = "increaseCampaignBudgetByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		//fmt.Println("(100 - input.ActionValue)", (input.ActionValue))
		budget, _ := strconv.ParseFloat(bugetPercentage, 64)
		//maxBudgetLimit := 0.0
		budget = c.DailyBudget + budget

		//err,maxbudget := rulesdao.GetRuleAttribute(r.RuleMapId,"budget_max_limit")
		/*if err != nil{
			maxBudgetLimit = viper.GetFloat64("max_budget_limit")
		}else{
			maxBudgetLimit,_ = strconv.ParseFloat(maxbudget,64)
		}*/
		if budget <= 0 {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed since it reached minimum zero budget %f",
				r.RuleName)
			logMessage.IsError = true
			logMessage.Action = "increaseCampaignBudgetByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		//check for maximum bid
		//maxBid =
		form := campaignmodels.CampaignUpdateForm{CampaignId: c.CampaignId, DailyBudget: budget}

		result, err := market.UpdateCampaign(&[]campaignmodels.CampaignUpdateForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "increaseCampaignBudgetByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d , campaign budget increased to %f",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, budget)
		logMessage.IsError = false
		logMessage.Action = "increaseCampaignBudgetByValue"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}
}
func decreaseCampaignBudgetByValue(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled")
	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)
	if market != nil {
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		err, bugetPercentage := rulesdao.GetRuleAttribute(r.RuleMapId, "budget_value")
		if err != nil {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err.Error())
			logMessage.IsError = true
			logMessage.Action = "decreaseCampaignBudgetByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		//fmt.Println("(100 - input.ActionValue)", (input.ActionValue))
		budget, _ := strconv.ParseFloat(bugetPercentage, 64)
		budget = c.DailyBudget - budget
		//fmt.Println("(100 - input.ActionValue)", (input.ActionValue))
		//budget := c.DailyBudget * (100 - budgetPer) / 100
		minBudget := 0.0
		/*err,maxbudget := rulesdao.GetRuleAttribute(r.RuleMapId,"budget_min_limit")
		if err != nil{
			minBudget = 0//viper.GetFloat64("max_budget_limit")
		}else{*/
		//minBudget,_ = strconv.ParseFloat(maxbudget,64)
		//}
		if budget <= 0 {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed since it reached minimum budget %f",
				r.RuleName, minBudget)
			logMessage.IsError = true
			logMessage.Action = "decreaseCampaignBudgetByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}

		form := campaignmodels.CampaignUpdateForm{CampaignId: c.CampaignId, DailyBudget: budget}
		result, err := market.UpdateCampaign(&[]campaignmodels.CampaignUpdateForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "decreaseCampaignBudgetByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d , campaign budget decreased to %f",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, budget)
		logMessage.IsError = false
		logMessage.Action = "decreaseCampaignBudgetByValue"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}
}
