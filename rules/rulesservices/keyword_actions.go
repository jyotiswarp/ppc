package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/keyword/keyworddao"
	"bitbucket.org/jyotiswarp/ppc/keyword/keywordmodel"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/report/reportservices"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesdao"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesmodels"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"strconv"
)

func decreaseKeywordBidByPercentage(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled : ", c)

	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)

	if market != nil {
		fmt.Println("c.ProfileId =", c.ProfileId, "c.SellerId = ", r.SellerId)
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		adGroupId, err := GetAdGroupId(r.RuleMapId)

		if err != nil {
			logMessage.IsError = true
			logMessage.Action = "decreaseKeywordBidByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		keywordId, err := GetKeywordId(r.RuleMapId)

		if err != nil {
			logMessage.IsError = true
			logMessage.Action = "decreaseKeywordBidByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		err, keywordDetails := keyworddao.GetKeywordDetails(keywordId)
		if err != nil {
			log.Error(err)
			return
		}
		err, bidPercentage := rulesdao.GetRuleAttribute(r.RuleMapId, "bid_percentage")
		if err != nil {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err.Error())
			logMessage.IsError = true
			logMessage.Action = "decreaseKeywordBidByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		bidPer, _ := strconv.ParseFloat(bidPercentage, 64)
		//fmt.Println("(100 - input.ActionValue)", (input.ActionValue))
		minBidLimit := 0.0
		bid := keywordDetails.Bid * (100 - bidPer) / 100

		err, minBid := rulesdao.GetRuleAttribute(r.RuleMapId, "bid_min_limit")
		if err != nil {
			minBidLimit = 0
		} else {
			minBidLimit, _ = strconv.ParseFloat(minBid, 64)
		}
		if bid < minBidLimit || bid <= 0 {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed since it reached minimum bid %f",
				r.RuleName, minBidLimit)
			logMessage.IsError = true
			logMessage.Action = "decreaseKeywordBidByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}

		form := keywordmodel.KeywordForm{CampaignId: c.CampaignId,
			MatchType: keywordDetails.MatchType,
			KeywordId: keywordDetails.KeywordId,
			AdGroupId: adGroupId,
			Bid: bid, State: keywordDetails.State, KeywordText: keywordDetails.KeywordText}

		result, err := market.UpdateAdGroupKeywordList(&[]keywordmodel.KeywordForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error  "+result[0].Code+"  ,and description "+result[0].Description,
				r.RuleName)
			logMessage.IsError = true
			logMessage.Action = "decreaseKeywordBidByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)
			log.Error(err)
			return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d ,adGroupId %d, keywordId %d, bid decreased to %f",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, adGroupId, keywordId, bid)
		logMessage.IsError = false
		logMessage.Action = "decreaseKeywordBidByPercentage"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}

	return

}
func increaseKeywordBidByPercentage(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled : ", c)

	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)

	if market != nil {
		fmt.Println("c.ProfileId =", c.ProfileId, "c.SellerId = ", r.SellerId)
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		adGroupId, err := GetAdGroupId(r.RuleMapId)

		if err != nil {
			logMessage.IsError = true
			logMessage.Action = "increaseKeywordBidByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		keywordId, err := GetKeywordId(r.RuleMapId)

		if err != nil {
			logMessage.IsError = true
			logMessage.Action = "increaseKeywordBidByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		err, keywordDetails := keyworddao.GetKeywordDetails(keywordId)
		if err != nil {
			log.Error(err)
			return
		}

		err, bidPercentage := rulesdao.GetRuleAttribute(r.RuleMapId, "bid_percentage")
		if err != nil {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err.Error())
			logMessage.IsError = true
			logMessage.Action = "increaseKeywordBidByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		bidPer, _ := strconv.ParseFloat(bidPercentage, 64)
		//fmt.Println("(100 - input.ActionValue)", (input.ActionValue))
		maxBidLimit := 0.0
		bid := keywordDetails.Bid * (100 + bidPer) / 100

		err, maxBid := rulesdao.GetRuleAttribute(r.RuleMapId, "bid_max_limit")
		if err != nil {
			maxBidLimit = viper.GetFloat64("bid_max_limit")
		} else {
			maxBidLimit, _ = strconv.ParseFloat(maxBid, 64)
		}
		if bid > maxBidLimit || bid <= 0 {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed since it reached maximum bid %f",
				r.RuleName, maxBidLimit)
			logMessage.IsError = true
			logMessage.Action = "increaseKeywordBidByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}

		//form := campaignmodels.CampaignUpdateForm{CampaignId:c.CampaignId,State:"enabled"}
		form := keywordmodel.KeywordForm{CampaignId: keywordDetails.CampaignId,
			MatchType: keywordDetails.MatchType,
			KeywordId: keywordDetails.KeywordId,
			AdGroupId: keywordDetails.AdGroupId,
			Bid: bid, State: keywordDetails.State, KeywordText: keywordDetails.KeywordText}
		result, err := market.UpdateAdGroupKeywordList(&[]keywordmodel.KeywordForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error  "+result[0].Code+"  ,and description "+result[0].Description,
				r.RuleName)
			logMessage.IsError = true
			logMessage.Action = "increaseKeywordBidByPercentage"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)
			log.Error(err)
			return
		}

		//todo LogTable Update
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d ,adGroupId %d, keywordId %d, bid increased to %f",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, adGroupId, keywordId, bid)
		logMessage.IsError = false
		logMessage.Action = "increaseKeywordBidByPercentage"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}

	return

}
func decreaseKeywordBidByValue(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled : ", c)

	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)

	if market != nil {
		fmt.Println("c.ProfileId =", c.ProfileId, "c.SellerId = ", r.SellerId)
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		adGroupId, err := GetAdGroupId(r.RuleMapId)

		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		keywordId, err := GetKeywordId(r.RuleMapId)

		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		err, keywordDetails := keyworddao.GetKeywordDetails(keywordId)
		if err != nil {
			log.Error(err)
			return
		}
		err, bidPercentage := rulesdao.GetRuleAttribute(r.RuleMapId, "bid_percentage")
		if err != nil {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err.Error())
			logMessage.IsError = true
			logMessage.Action = "decreaseKeywordBidByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		bid, _ := strconv.ParseFloat(bidPercentage, 64)
		//fmt.Println("(100 - input.ActionValue)", (input.ActionValue))
		//minBidLimit := 0.0
		//bid := keywordDetails.Bid * (100-bidPer) / 100

		//err, minBid := rulesdao.GetRuleAttribute(r.RuleMapId, "bid_min_limit")
		/*if err != nil {
			minBidLimit = 0
		} else {
			minBidLimit, _ = strconv.ParseFloat(minBid, 64)
		}*/
		if bid <= 0 {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed since it reached minimum bid %f",
				r.RuleName, 0.0)
			logMessage.IsError = true
			logMessage.Action = "decreaseKeywordBidByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}

		form := keywordmodel.KeywordForm{CampaignId: c.CampaignId,
			MatchType: keywordDetails.MatchType,
			KeywordId: keywordDetails.KeywordId,
			AdGroupId: adGroupId,
			Bid: bid, State: keywordDetails.State, KeywordText: keywordDetails.KeywordText}

		result, err := market.UpdateAdGroupKeywordList(&[]keywordmodel.KeywordForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error  "+result[0].Code+"  ,and description "+result[0].Description,
				r.RuleName)
			logMessage.IsError = true
			logMessage.Action = "decreaseKeywordBidByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)
			log.Error(err)
			return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d ,adGroupId %d, keywordId %d, bid decreased to %f",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, adGroupId, keywordId, bid)
		logMessage.IsError = false
		logMessage.Action = "decreaseKeywordBidByValue"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}

	return

}
func increaseKeywordBidByValue(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled : ", c)

	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)

	if market != nil {
		fmt.Println("c.ProfileId =", c.ProfileId, "c.SellerId = ", r.SellerId)
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		adGroupId, err := GetAdGroupId(r.RuleMapId)

		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		keywordId, err := GetKeywordId(r.RuleMapId)

		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		err, keywordDetails := keyworddao.GetKeywordDetails(keywordId)
		if err != nil {
			log.Error(err)
			return
		}

		err, bidPercentage := rulesdao.GetRuleAttribute(r.RuleMapId, "bid_value")
		if err != nil {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err.Error())
			logMessage.IsError = true
			logMessage.Action = "increaseKeywordBidByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		bid, _ := strconv.ParseFloat(bidPercentage, 64)
		//fmt.Println("(100 - input.ActionValue)", (input.ActionValue))
		//maxBidLimit := 0.0
		//bid := keywordDetails.Bid * (100 + bidPer) / 100

		//err, maxBid := rulesdao.GetRuleAttribute(r.RuleMapId, "bid_max_limit")
		/*if err != nil {
			maxBidLimit = viper.GetFloat64("bid_max_limit")
		} else {
			maxBidLimit, _ = strconv.ParseFloat(maxBid, 64)
		}*/
		if bid <= 0 {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed since it reached  bid value is zero",
				r.RuleName)
			logMessage.IsError = true
			logMessage.Action = "increaseKeywordBidByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}

		//form := campaignmodels.CampaignUpdateForm{CampaignId:c.CampaignId,State:"enabled"}
		form := keywordmodel.KeywordForm{CampaignId: keywordDetails.CampaignId,
			MatchType: keywordDetails.MatchType,
			KeywordId: keywordDetails.KeywordId,
			AdGroupId: keywordDetails.AdGroupId,
			Bid: bid, State: keywordDetails.State, KeywordText: keywordDetails.KeywordText}
		result, err := market.UpdateAdGroupKeywordList(&[]keywordmodel.KeywordForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error  "+result[0].Code+"  ,and description "+result[0].Description,
				r.RuleName)
			logMessage.IsError = true
			logMessage.Action = "increaseKeywordBidByValue"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)
			log.Error(err)
			return
		}

		//todo LogTable Update
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d ,adGroupId %d, keywordId %d, bid increased to %f",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, adGroupId, keywordId, bid)
		logMessage.IsError = false
		logMessage.Action = "increaseKeywordBidByValue"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}

	return

}

func removeKeyword(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled : ", c)

	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)

	if market != nil {
		fmt.Println("c.ProfileId =", c.ProfileId, "c.SellerId = ", r.SellerId)
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		adGroupId, err := GetAdGroupId(r.RuleMapId)

		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		keywordId, err := GetKeywordId(r.RuleMapId)

		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		err, keywordDetails := keyworddao.GetKeywordDetails(keywordId)
		if err != nil {
			log.Error(err)
			return
		}

		//form := campaignmodels.CampaignUpdateForm{CampaignId:c.CampaignId,State:"enabled"}
		result, err := market.DeleteKeyword(strconv.Itoa(int(keywordDetails.KeywordId)))
		if err != nil || result.Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error "+result.Code+"  ,and description "+result.Description,
				r.RuleName)
			logMessage.IsError = true
			logMessage.Action = "removeKeyword"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)
			log.Error(err)
			return
		}

		//todo LogTable Update
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d ,adGroupId %d, keywordId %d removed",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, adGroupId, keywordId)
		logMessage.IsError = false
		logMessage.Action = "removeKeyword"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}

	return

}

/*
func addKeyword(c *models.Campaign, r *UserRule, input *ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled : ", c)
	log.Println("input :", input)

	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)

	if input != nil && market != nil {
		fmt.Println("c.ProfileId =", c.ProfileId, "c.SellerId = ", r.SellerId)
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)		err, keywordDetails := keyworddao.GetKeywordDetails(input.KeywordId)
		if err != nil {
			log.Error(err)
			return
		}

		//form := campaignmodels.CampaignUpdateForm{CampaignId:c.CampaignId,State:"enabled"}
		form := keywordmodel.KeywordForm{}
		result, err := market.CreateAdGroupKeywordList(strconv.Itoa(int(keywordDetails.KeywordId)))
		if err != nil {
			log.Error(err)
			return
		}
		fmt.Println("result   =====", result)
		if result[0].Code == "SUCCESS" {
			//todo LogTable Update
			log.Println("Rule ? Executed. For sellerId ?, profileId ? ,campaignId ?, adGroupId ?, keywordId ? bid decreased to ?",
				r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, keywordDetails.AdGroupId, keywordDetails.KeywordText, bid)

			err = reportservices.UpdateBasicDetails(strings.ToLower(r.MarketPlace), strings.ToLower(r.Geo), r.SellerId, 0, strconv.Itoa(int(r.ProfileId)),true)
			if err != nil {
				log.Error(err)
				return
			}
		}

	} else {
		log.Error("invalid market")
	}

	return

}*/

func moveToNegativeKeyword(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled : ", c)

	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)

	if market != nil {
		fmt.Println("c.ProfileId =", c.ProfileId, "c.SellerId = ", r.SellerId)
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		adGroupId, err := GetAdGroupId(r.RuleMapId)

		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		keywordId, err := GetKeywordId(r.RuleMapId)

		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName)
			return
		}
		err, keywordDetails := keyworddao.GetKeywordDetails(keywordId)
		if err != nil {
			log.Error(err)
			return
		}

		//form := campaignmodels.CampaignUpdateForm{CampaignId:c.CampaignId,State:"enabled"}
		/*result, err := market.DeleteKeyword(strconv.Itoa(int(keywordDetails.KeywordId)))
		if err != nil || result.Code == "SUCCESS" {
			log.Error(err)
			return
		}*/
		fmt.Println("result   =====")
		form := keywordmodel.KeywordForm{CampaignId: c.CampaignId,
			MatchType: keywordDetails.MatchType,
			AdGroupId: adGroupId, State: keywordDetails.State, KeywordText: keywordDetails.KeywordText}
		resultCreate, err := market.CreateAdGroupNegativeKeywordList(&[]keywordmodel.KeywordForm{form})
		if err != nil || len(resultCreate) < 1 || resultCreate[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error code : "+resultCreate[0].Code+"  ,and description "+resultCreate[0].Description,
				r.RuleName)
			logMessage.IsError = true
			logMessage.Action = "moveToNegativeKeyword"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)
			log.Error(err)
			//return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d ,adGroupId %d, keywordId %d moved to negative keyword list",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, adGroupId, keywordId)
		logMessage.IsError = false
		logMessage.Action = "moveToNegativeKeyword"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}

	return

}

/*
func changeKeywordMatchType(c *models.Campaign, r *UserRule) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled : ", c)

	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)

	if market != nil {
		fmt.Println("c.ProfileId =", c.ProfileId, "c.SellerId = ", r.SellerId)
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		adGroupId, err := GetAdGroupId(r.RuleMapId)

		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName, )
			return
		}
		keywordId, err := GetKeywordId(r.RuleMapId)

		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since adgroupId not set for the rule",
				r.RuleName, )
			return
		}

		err, keywordDetails := keyworddao.GetKeywordDetails(keywordId)
		if err != nil {
			log.Error(err)
			return
		}

		//form := campaignmodels.CampaignUpdateForm{CampaignId:c.CampaignId,State:"enabled"}
		err, matchType := rulesdao.GetRuleAttribute(r.RuleMapId,"match_type")
		if err != nil {
			//log.Println("bid value cannot update since requested is less than zero")
			//log.Error("budget goes less than zero")
			logMessage.LogMessage = fmt.Sprintf("Rule %s failed, since match_type not set for the rule",
				r.RuleName, )
			return
		}
		form := keywordmodel.KeywordForm{CampaignId: keywordDetails.CampaignId,
			KeywordId: keywordDetails.KeywordId,
			AdGroupId: keywordDetails.AdGroupId, MatchType: matchType, KeywordText: keywordDetails.KeywordText,}
		result, err := market.UpdateAdGroupKeywordList(&[]keywordmodel.KeywordForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error "+ result[0].Code+"  , "+result[0].Description,
				r.RuleName)
			logMessage.IsError = true
			rulesdao.PushToRuleLog(logMessage)
			log.Error(err)
			return
		}
		fmt.Println("result   =====", result)

		//todo LogTable Update
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d ,adGroupId %d, keywordId %d changed match type to %s",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, adGroupId, keywordId, matchType)
		logMessage.IsError = false
		rulesdao.PushToRuleLog(logMessage)
		err = reportservices.UpdateBasicDetails(strings.ToLower(r.MarketPlace), strings.ToLower(r.Geo), r.SellerId, 0, strconv.Itoa(int(r.ProfileId)),true)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}

	return

}
*/

func GetKeywordId(ruleMapId uint) (keywordId int64, err error) {
	err, attrVal := rulesdao.GetRuleAttribute(ruleMapId, "keyword_id")
	if err != nil {
		return
	}
	keywordId, err = strconv.ParseInt(attrVal, 10, 64)
	return

}
