package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/adgroup/adgroupmodels"
	"bitbucket.org/jyotiswarp/ppc/backends"
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/report/reportservices"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesdao"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesmodels"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"strconv"
)

func pauseAdGroup(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "paused")
	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)
	if market != nil {
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}

		adGroupId, err := GetAdGroupId(r.RuleMapId)
		if err != nil {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "pauseAdGroup"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		form := adgroupmodels.AdGroupUpdateForm{AdGroupId: adGroupId, CampaignId: c.CampaignId, State: "paused"}
		result, err := market.UpdateAdGroupList(&[]adgroupmodels.AdGroupUpdateForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "pauseAdGroup"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d ,adGroupId %d AdGroup paused",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, adGroupId)
		logMessage.IsError = false
		logMessage.Action = "pauseAdGroup"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}

}

func enableAdGroup(c *models.Campaign, r *UserRule, input *[]ExpressionValResult) {
	log.Printf("For Rule %+v \n", r)
	log.Println("CampaignId ", c.CampaignId, "enabled")
	market := backends.GetMarketPlace(r.MarketPlace, r.Geo)
	if market != nil {
		market.SetProfileId(c.ProfileId)
		market.SetSellerId(r.SellerId)
		market.SetEmail(r.Email)
		logMessage := rulesmodels.RuleActionLog{SellerId: r.SellerId}
		adGroupId, err := GetAdGroupId(r.RuleMapId)
		if err != nil {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "enableAdGroup"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}

		form := adgroupmodels.AdGroupUpdateForm{AdGroupId: adGroupId, CampaignId: c.CampaignId, State: "enabled"}
		result, err := market.UpdateAdGroupList(&[]adgroupmodels.AdGroupUpdateForm{form})
		if err != nil || len(result) < 1 || result[0].Code != "SUCCESS" {
			logMessage.LogMessage = fmt.Sprintf("Rule %s Executed and failed with error %s",
				r.RuleName, err)
			logMessage.IsError = true
			logMessage.Action = "enableAdGroup"
			logMessage.RuleName = r.RuleName
			logMessage.RuleMapId = r.RuleMapId
			rulesdao.PushToRuleLog(logMessage)

			log.Error(err)
			return
		}
		logMessage.LogMessage = fmt.Sprintf("Rule %s Executed. For sellerId %s, profileId %d ,campaignId %d ,adGroupId %d AdGroup enabled",
			r.RuleName, r.SellerId, r.ProfileId, r.CampaignId, adGroupId)
		logMessage.IsError = false
		logMessage.Action = "enableAdGroup"
		logMessage.RuleName = r.RuleName
		logMessage.RuleMapId = r.RuleMapId
		rulesdao.PushToRuleLog(logMessage)
		ppcCommon := CreatePPCCommonFromUserRule(r)
		err = reportservices.UpdateBasicDetailsByCampaignId(ppcCommon, c.CampaignId)
		if err != nil {
			log.Error(err)
			return
		}

	} else {
		log.Error("invalid market")
	}
}

func GetAdGroupId(ruleMapId uint) (adGroupId int64, err error) {
	err, attrVal := rulesdao.GetRuleAttribute(ruleMapId, "ad_group_id")
	if err != nil {
		return
	}
	adGroupId, err = strconv.ParseInt(attrVal, 10, 64)
	return

}
