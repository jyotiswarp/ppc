package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/models"
)

// arg[0] = campaignReport
//  arg[1] = adGroupId
// arg[2] = keywordId
var CampaignImpressionCheck = func(args ...interface{}) (interface{}, error) {
	campaign := args[0].(models.CampaignReportData)
	/*result := args[1].(*[]ExpressionValResult)*/ /*
		result.CampaignId = campaign.CampaignId*/
	return float64(campaign.Impressions), nil

}

var campaignAcosCheck = func(args ...interface{}) (interface{}, error) {
	//result := args[0].(*ExpressionValResult)
	campaign := args[0].(models.CampaignReportData)
	//result.CampaignId = campaign.CampaignId

	return campaign.Acos, nil
}

var campaignOrderCheck = func(args ...interface{}) (interface{}, error) {
	//result := args[0].(*ExpressionValResult)
	campaign := args[0].(models.CampaignReportData)
	//result.CampaignId = campaign.CampaignId

	return float64(campaign.Orders), nil
}

var campaignRevenueCheck = func(args ...interface{}) (interface{}, error) {
	//result := args[0].(*ExpressionValResult)
	campaign := args[0].(models.CampaignReportData)
	//result.CampaignId = campaign.CampaignId

	return campaign.Sales, nil
}

var campaignClickCheck = func(args ...interface{}) (interface{}, error) {
	//result := args[0].(*ExpressionValResult)
	campaign := args[0].(models.CampaignReportData)
	//result.CampaignId = campaign.CampaignId
	return float64(campaign.Clicks), nil
}

var campaignCtrCheck = func(args ...interface{}) (interface{}, error) {
	//result := args[0].(*ExpressionValResult)
	campaign := args[0].(models.CampaignReportData)
	//result.CampaignId = campaign.CampaignId

	return campaign.Ctr, nil
}
var campaignSpendCheck = func(args ...interface{}) (interface{}, error) {
	//result := args[0].(*ExpressionValResult)
	campaign := args[0].(models.CampaignReportData)
	//result.CampaignId = campaign.CampaignId

	return campaign.Cost, nil
}
