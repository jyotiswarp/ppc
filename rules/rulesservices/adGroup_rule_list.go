package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/models"
	"github.com/pkg/errors"
)

// arg[0] = campaignReport
//  arg[1] = adGroupId
// arg[2] = keywordId
var adGroupImpressionCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			return float64(ad.Impressions), nil
		}

	}
	return 0.0, errors.New("not found")
}

var adGroupAcosCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			return ad.Acos, nil
		}

	}
	return 0.0, errors.New("not found")
}

var adGroupOrderCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			return float64(ad.Orders), nil
		}

	}
	return 0.0, errors.New("not found")
}

var adGroupRevenueCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			return ad.Sales, nil
		}

	}
	return 0.0, errors.New("not found")
}

var adGroupClickCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			return float64(ad.Clicks), nil
		}

	}
	return 0.0, errors.New("not found")
}

var adGroupCtrCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			return ad.Ctr, nil
		}

	}
	return 0.0, errors.New("not found")
}
var adGroupSpendCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			return ad.Cost, nil
		}

	}
	return 0.0, errors.New("not found")
}
