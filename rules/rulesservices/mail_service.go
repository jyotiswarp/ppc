package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationdao"
	"bitbucket.org/jyotiswarp/ppc/authentication/authenticationmodels"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesdao"
	"bitbucket.org/jyotiswarp/ppc/utils/mail/sphtmlmail"
	"bitbucket.org/jyotiswarp/ppc/utils/mail/themes"
	log "github.com/Sirupsen/logrus"
)

func CheckAlertSetOrNot(ruleMapId uint, sellerId, message string) (err error) {

	rules, err := rulesdao.GetRuleMapById(ruleMapId)
	if err != nil {
		log.Error(err)
		return
	}
	if rules.AlertRequired {
		//button := sphtmlmail.TopButton{Link: "https://app.sellerprime.com/dashboard/" + users.ActivationId, Color: "#FF9D3B", Text: "My Dashboard"}
		var auth authenticationmodels.PPCAPITokenDetails
		err, auth = authenticationdao.GetAuthDetails(sellerId)
		if err != nil {
			log.Error(err)
			return
		}
		mailConfig := sphtmlmail.HtmlMailConfig{
			MailTitle:  "PPC Management",
			TitleImage: "https://storage.googleapis.com/sellerprime_images/email-asset-keywordSearch01.png",
			To:         []string{auth.Email},
			Subject:    "PPC Management",
			From:       "ppc@sellerprime.com",
			IntroText:  []string{"APPC Management"},
			SenderName: "SellerPrime Info",
			Signature:  "Cheers",
			//	TableData:         tableData,
			Data:       message,
			ButtonText: "PPC Management",
			//ButtonLink:        "https://app.sellerprime.com/dashboard/" + users.ActivationId + "?redirect_url=business_alerts",
			ButtonInstruction: "PPC Management",
			Cc:                "",
			Theme:             &themes.SPAlertTheme{},
			//TopButton:         []sphtmlmail.TopButton{button},
		}
		//prevent sending mail if no data
		//if len(tableData) != 0 {
		sphtmlmail.CreateMailAndSend(mailConfig)
		//}
	}
	return
}
