package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/models"

	"bitbucket.org/jyotiswarp/ppc/campaign/campaigndao"
	log "github.com/Sirupsen/logrus"
	"github.com/jasonlvhit/gocron"
	"github.com/spf13/viper"
)

var (
	s *gocron.Scheduler
)

func Initialize() {
	s := gocron.NewScheduler()
	s.Every(uint64(viper.GetInt64("rule_execution_interval_in_seconds"))).Seconds().Do(ruleExecute)
	s.Start()
}

func ruleExecute() {
	log.Println("Rule execute")
	ruleList, err := GetAllRules("1 day")
	if err != nil {
		return
	}

	log.Println("rule List :", ruleList)
	for campaignId, _ := range ruleList.rules {
		campaign, err := campaigndao.GetCampaignDetails(campaignId)
		if err != nil {
			log.Error(err)
			return
		}
		log.Println("Campaign: ", campaign)
		ruleList.EvalRulesForCampaign(&campaign)

	}

	return

}

func getLastCampaingDetail(n int) []*models.CampaignReportData {
	// TODO get next campaign IF to scrape for, from GC PUBSUB

	// get last 2 campaign reports
	return models.GetLastNCampaignReports(1, "amazon", "us", 2)

}
