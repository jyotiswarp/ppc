package rulesservices

import (
	log "github.com/Sirupsen/logrus"
	"time"
)

type InsightsWriter struct {
}

func GetNewInsightsWriter() *InsightsWriter {
	return &InsightsWriter{}
}

func (iw *InsightsWriter) Write(rule *UserRule, campaignId int, insight string) {
	// TODO write into Influx
	// needs to be thread-safe

	log.Printf("Insight [%v] campaignId %d ruleId %s%s\n",
		time.Now(),
		insight,
		campaignId,
		rule.RuleName)

}
