package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/models"
	log "github.com/Sirupsen/logrus"
)

type action func(*models.Campaign, *UserRule, *[]ExpressionValResult)

var (
	actions        map[string]action
	insightsWriter *InsightsWriter
)

func init() {
	actions = make(map[string]action)
	actions["pause_campaign"] = pauseCampaign
	actions["enable_campaign"] = enableCampaign
	actions["decrease_campaign_budget_percentage"] = decreaseCampaignBudgetByPercentage
	actions["increase_campaign_budget_percentage"] = increaseCampaignBudgetByPercentage
	actions["decrease_campaign_budget_value"] = decreaseCampaignBudgetByValue
	actions["increase_campaign_budget_value"] = increaseCampaignBudgetByValue

	actions["pause_adGroup"] = pauseAdGroup
	actions["enable_adGroup"] = enableAdGroup

	actions["increase_keyword_bid_percentage"] = increaseKeywordBidByPercentage
	actions["decrease_keyword_bid_percentage"] = decreaseKeywordBidByPercentage
	actions["increase_keyword_bid_value"] = increaseKeywordBidByValue
	actions["decrease_keyword_bid_value"] = decreaseKeywordBidByValue
	actions["remove_keyword"] = removeKeyword
	//actions["add_keyword"] = addKeyword
	actions["move_to_negative_keyword"] = moveToNegativeKeyword
	//actions["change_keyword_match_type"] = changeKeywordMatchType
	//actions["move_to_phrase_match"] = moveToPhraseMatch
	//actions["move_manual_to_auto_campaign"] = enableCampaign
	//actions["move_auto_to_manual_campaign"] = enableCampaign

	insightsWriter = GetNewInsightsWriter()
}

func DoAction(c *models.Campaign, what string, r *UserRule, input *[]ExpressionValResult) {

	/*
		val := strings.Split(action, "-")
		what := ""
		if len(val) > 1 {
			v, _ := strconv.ParseFloat(val[1], 64)
			input.ActionValue = v
		}
		what = val[0]
		fmt.Println("action: ", val)
	*/

	if actions[what] != nil {
		actions[what](c, r, input)

	} else {
		log.Error("Action %s not found for rule %s", what, r.RuleName)
	}

}
