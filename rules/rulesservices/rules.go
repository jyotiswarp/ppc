package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesdao"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesmodels"
	"bitbucket.org/jyotiswarp/ppc/spprofile/spprofileservices"
	"fmt"
	"github.com/Knetic/govaluate"
	log "github.com/Sirupsen/logrus"
	"sync"
	"time"
)

type ExpressionValResult struct {
	CampaignId int64 `json:"campaign_id"`
	AdGroupId  int64 `json:"ad_group_id"`
	KeywordId  int64 `json:"keyword_id"`
}
type UserRule struct {
	MarketPlace string `json:"marketPlace"`
	Geo         string `json:"geo"`
	SellerId    string `json:"sellerId"`
	CampaignId  int64  `json:"campaign_id"`
	Condition   govaluate.EvaluableExpression
	Action      string
	RuleName    string
	RuleId      uint
	RuleMapId   uint
	ProfileId   int64  `json:"profile_id"`
	Email       string `json:"email"`
	AlertRequired bool
	/*AdGroupId  int64 `json:"ad_group_id"`
	KeywordId	int64 `json:"keyword_id"`
	Bid        float64 `json:"bid"`*/
}

type UserRules struct {
	mux   sync.RWMutex
	rules map[int64][]UserRule // CampaignId -> rules
}

func (u *UserRules) EvalRulesForCampaign(campaign *models.Campaign) {
	u.mux.RLock()
	defer u.mux.RUnlock()
	fmt.Println("EvalRulesForCampaign : ", campaign.CampaignId)
	for _, rule := range u.rules[campaign.CampaignId] {

		go func(r UserRule, camp models.Campaign) {
			fmt.Println("Excecuting ", r.RuleName, " for campaign ID : ", camp.CampaignId, "********", r.Condition)
			parameters := make(map[string]interface{}, 8)
			parameters["Campaign"] = camp

			userPref := spprofileservices.GetUserPreference(r.SellerId)

			parameters["UserPrefer"] = userPref
			input := &[]ExpressionValResult{}
			parameters["Input"] = input
			//fmt.Println("*******result :",result)
			campaignReport, err := GetReportForRuleExecution(camp.CampaignId, r.RuleMapId) //viper.GetInt("number_of_reports_for_rule"))
			// reportdao.GetReportByCampaign(campaign.ProfileId, campaign.CampaignId, campaign.SellerId, campaign.Marketplace, campaign.Geo)//GetNCampaignReport()// //dao.GetReportData() // //GetReportdata(u.SellerId,u.CampaignId)
			if err != nil {
				log.Error(err)
				return
			}
			parameters["CampaignReport"] = campaignReport
			parameters["Rules"] = r
			rulesData := rulesmodels.UserRuleMapping{
				CampaignId:     r.CampaignId,
				SellerId:       r.SellerId,
				Geo:            r.Geo,
				MarketPlace:    r.MarketPlace,
				RuleName:       r.RuleName,
				RuleId:         r.RuleId,
				ProfileId:      r.ProfileId}
			res, err := r.Condition.Evaluate(parameters)
			if err != nil {
				log.Error(err)
				lastAnalysed := time.Now().Add(-1*time.Hour)
				rulesData.LastAnalysedAt = &lastAnalysed
				rulesData.RunningStatus = "failed"
				rulesdao.UpdateRuleMapping(rulesData)
				return
			}
			if res == true {
				DoAction(&camp, r.Action, &r, input)
			}
			now := time.Now()
			rulesData.RunningStatus =   "ready"
			rulesData.LastAnalysedAt = &now
			rulesdao.UpdateRuleMapping(rulesData)
		}(rule, *campaign)

	}

}
