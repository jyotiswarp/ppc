package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/keyword/keyworddao"
	"bitbucket.org/jyotiswarp/ppc/models"
	"errors"
)

// arg[0] = campaignReport
//  arg[1] = adGroupId
// arg[2] = keywordId
var keywordImpressionCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	keywordId, err := GetKeywordId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			for _, key := range ad.KeywordReport {
				if key.KeywordId == keywordId {
					return float64(key.Impressions), nil
				}
			}
		}

	}
	return 0.0, errors.New("not found")
}

var keywordAcosCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	keywordId, err := GetKeywordId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			for _, key := range ad.KeywordReport {
				if key.KeywordId == keywordId {
					return key.Acos, nil
				}
			}
		}

	}
	return 0.0, errors.New("not found")
}

var keywordOrderCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	keywordId, err := GetKeywordId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			for _, key := range ad.KeywordReport {
				if key.KeywordId == keywordId {
					return float64(key.Orders), nil
				}
			}
		}

	}
	return 0.0, errors.New("not found")
}

var keywordRevenueCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	keywordId, err := GetKeywordId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			for _, key := range ad.KeywordReport {
				if key.KeywordId == keywordId {
					return key.Sales, nil
				}
			}
		}

	}
	return 0.0, errors.New("not found")
}

var keywordClickCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	keywordId, err := GetKeywordId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			for _, key := range ad.KeywordReport {
				if key.KeywordId == keywordId {
					return float64(key.Clicks), nil
				}
			}
		}

	}
	return 0.0, errors.New("not found")
}

var keywordCtrCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	keywordId, err := GetKeywordId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			for _, key := range ad.KeywordReport {
				if key.KeywordId == keywordId {
					return key.Ctr, nil
				}
			}
		}

	}
	return 0.0, errors.New("not found")
}
var keywordSpendCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	keywordId, err := GetKeywordId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			for _, key := range ad.KeywordReport {
				if key.KeywordId == keywordId {
					return key.Cost, nil
				}
			}
		}

	}
	return 0.0, errors.New("not found")
}

var keywordMatchCheck = func(args ...interface{}) (interface{}, error) {
	rules := args[1].(UserRule)
	campaign := args[0].(models.CampaignReportData)
	adGroup := campaign.AdGroupReport
	adGroupId, err := GetAdGroupId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	keywordId, err := GetKeywordId(rules.RuleMapId)
	if err != nil {
		return 0.0, errors.New("not found")
	}
	for _, ad := range adGroup {
		if ad.AdGroupId == adGroupId {
			for _, key := range ad.KeywordReport {
				if key.KeywordId == keywordId {
					err, keyword := keyworddao.GetKeywordDetails(keywordId)
					if err != nil {
						return false, err
					}
					return keyword.MatchType, nil

				}
			}
		}

	}
	return "", errors.New("not found")
}
