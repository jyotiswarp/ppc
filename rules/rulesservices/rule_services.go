package rulesservices

import (
	"bitbucket.org/jyotiswarp/ppc/models"
	"bitbucket.org/jyotiswarp/ppc/models/ppcmanagementcommons"
	"bitbucket.org/jyotiswarp/ppc/report/reportdao"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesdao"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesmodels"
	"fmt"
	"github.com/Knetic/govaluate"
	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"strconv"
	"time"
)

func CreateNewRule(form rulesmodels.RulesForm) (err error) {
	if form.Rule == "()" || form.Action == "" {
		return errors.New("Invalid rule")
	}
	err = rulesdao.UpdateUserRulesTable(form) //form.SellerId, strings.ToLower(form.MarketPlace), strings.ToLower(form.Geo), form.CampaignId, form.Rule, form.Action, form.RuleName,form.ProfileId)
	if err != nil {

	}
	return err

}

func GetExpressionFnMap() (functions map[string]govaluate.ExpressionFunction) {

	functions = map[string]govaluate.ExpressionFunction{

		"CampaignImpressionCheck": CampaignImpressionCheck,
		"campaignAcosCheck":       campaignAcosCheck,
		"campaignOrderCheck":      campaignOrderCheck,
		"campaignRevenueCheck":    campaignRevenueCheck,
		"campaignClickCheck":      campaignClickCheck,
		"campaignCtrCheck":        campaignCtrCheck,
		"campaignSpendCheck":      campaignSpendCheck,

		"adGroupImpressionCheck": adGroupImpressionCheck,
		"adGroupAcosCheck":       adGroupAcosCheck,
		"adGroupOrderCheck":      adGroupOrderCheck,
		"adGroupRevenueCheck":    adGroupRevenueCheck,
		"adGroupClickCheck":      adGroupClickCheck,
		"adGroupCtrCheck":        adGroupCtrCheck,
		"adGroupSpendCheck":      adGroupSpendCheck,

		"keywordImpressionCheck": keywordImpressionCheck,
		"keywordAcosCheck":       keywordAcosCheck,
		"keywordOrderCheck":      keywordOrderCheck,
		"keywordClickCheck":      keywordClickCheck,
		"keywordCtrCheck":        keywordCtrCheck,
		"keywordSpendCheck":      keywordSpendCheck,
		"keywordMatchCheck":      keywordMatchCheck,
	}

	return functions

}

func GetAllRules(duration string) (result UserRules, err error) {

	rulesRepo, err := rulesdao.GetRules(duration)
	if err != nil {
		log.Error(err)
		return
	}

	var idList []uint
	for _, rule := range rulesRepo {
		idList = append(idList, rule.RuleMapId)
	}
	if len(idList) < 1 {
		log.Error("no rules for evaluate")
		return
	}
	err = rulesdao.UpdateRuleRunningStatus(idList, "pending")
	if err != nil {
		log.Error(err)
		return
	}

	ruleMap := make(map[int64][]UserRule)

	for _, rules := range rulesRepo {

		//fmt.Println("GetExpressionFnMap() : ",GetExpressionFnMap())
		fmt.Println("rules :", rules)
		if rules.Condition != "" {
			expression, err := govaluate.NewEvaluableExpressionWithFunctions(rules.Condition, GetExpressionFnMap())
			if err != nil {
				log.Error(err)
				latAnalysed := time.Now().Add(-1 * time.Hour)
				rulesData := rulesmodels.UserRuleMapping{
					CampaignId:     rules.CampaignId,
					SellerId:       rules.SellerId,
					Geo:            rules.Geo,
					MarketPlace:    rules.MarketPlace,
					RuleName:       rules.RuleName,
					RuleId:         rules.RuleId,
					ProfileId:      rules.ProfileId,
					State:          rules.State,
					RunningStatus:  "failed",
					LastAnalysedAt: &latAnalysed,
					AlertRequired:  rules.AlertRequired}
				rulesdao.UpdateRuleMapping(rulesData)
				continue
			}
			rule := UserRule{CampaignId: rules.CampaignId, RuleName: rules.RuleName, SellerId: rules.SellerId, MarketPlace: rules.MarketPlace,
				Geo: rules.Geo, Condition: *expression, Action: rules.Action, RuleId: rules.RuleId, ProfileId: rules.ProfileId, RuleMapId: rules.RuleMapId,
				Email: rules.Email, AlertRequired: rules.AlertRequired}

			ruleMap[rules.CampaignId] = append(ruleMap[rules.CampaignId], rule)
		}

	}
	if len(ruleMap) > 0 {

		result = UserRules{rules: ruleMap}
	}

	return
}

func GetEventLog(sellerId string) (err error, result []rulesmodels.RuleActionLog) {

	err, result = rulesdao.FetchEventLogs(sellerId)
	return
}

func GetRulesBySellerId(sellerId string) (err error, result []rulesmodels.UserRuleMapping) {

	result, err = rulesdao.GetRulesBySellerId(sellerId)
	return
}

func ChangeRuleState(sellerId, state string, ruleMapId uint) (result rulesmodels.UserRuleMapping, err error) {

	result, err = rulesdao.ChangeRuleState(sellerId, state, ruleMapId)
	return
}

func DeleteRule(sellerId string, ruleMapId uint) (err error) {

	err = rulesdao.DeleteRule(sellerId, ruleMapId)
	return
}
func GetReportForRuleExecution(campaignId int64, ruleMapId uint) (result models.CampaignReportData, err error) {

	startDate, endDate := GetReportDateRangeForRule(ruleMapId)
	fmt.Println("startDate : ", startDate, "---endDate : ", endDate)
	result, err = reportdao.GetReportsByCampaignIdAndDateRange(campaignId, startDate, endDate)

	return
}

func GetReportDateRangeForRule(ruleMapId uint) (startDate, endDate time.Time) {
	days := viper.GetInt("number_of_reports_for_rule")
	hrDuration := strconv.Itoa(days * 24)
	var duration time.Duration
	duration, err := time.ParseDuration(hrDuration + "h")
	fmt.Println("duration :", duration)
	err, attrVal := rulesdao.GetRuleAttribute(ruleMapId, "start_date")
	if err != nil {
		return time.Now().Add(-duration), time.Now()
	}
	startDate, err = time.Parse("20060102", attrVal)
	if err != nil {
		return time.Now().Add(-duration), time.Now()
	}
	err, attrVal = rulesdao.GetRuleAttribute(ruleMapId, "end_date")
	if err != nil {
		return time.Now().Add(-duration), time.Now()
	}
	endDate, err = time.Parse("20060102", attrVal)
	if err != nil {
		return time.Now().Add(-duration), time.Now()
	}
	return
}

func RestartPendingRuleEvaluation() {
	log.Println("RestartPendingRuleEvaluation")
	err := rulesdao.UpdatePendingRulesAsFailed()
	if err != nil {
		log.Error(err)
		return
	}

}

func CreatePPCCommonFromUserRule(rule *UserRule) ppcmanagementcommons.PPCManagementCommon {

	profileIdStr := strconv.Itoa(int(rule.ProfileId))
	ppcCommon := ppcmanagementcommons.PPCManagementCommon{
		ProfileID:    rule.ProfileId,
		Geo:          rule.Geo,
		SellerID:     rule.SellerId,
		Marketplace:  rule.MarketPlace,
		ProfileIDStr: profileIdStr,
	}
	return ppcCommon

}
