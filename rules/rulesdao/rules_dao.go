package rulesdao

import (
	"bitbucket.org/jyotiswarp/ppc/marketprofile/marketprofiledao"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesmodels"
	"bitbucket.org/jyotiswarp/ppc/utils/mysql"
	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"strings"
	"time"
)

func MigrateRulesTables() {
	mysql.DB.AutoMigrate(&rulesmodels.UserRuleMapping{}, &rulesmodels.UserRules{}, &rulesmodels.UserRuleAttributes{}, &rulesmodels.RuleActionLog{})
}

func UpdateUserRulesTable(form rulesmodels.RulesForm) (err error) {

	sellerId := form.SellerId
	marketplace := strings.ToLower(form.MarketPlace)
	geo := strings.ToLower(form.Geo)
	campaignId := form.CampaignId
	exp := form.Rule
	action := form.Action
	ruleName := form.RuleName
	//profileId := form.ProfileId
	rule := rulesmodels.UserRules{Action: action, Condition: exp}
	err = mysql.DB.Where(&rule).Where("deleted_at is null").FirstOrCreate(&rule).Error
	if err != nil {
		log.Error(err)
		return err
	}
	profile, err := marketprofiledao.GetProfileInformation(marketplace, geo, sellerId)
	if err != nil {
		log.Error(err)
		return err
	}

	lastAnalysed := time.Now().Add(-24 * time.Hour)
	ruleMap := rulesmodels.UserRuleMapping{SellerId: sellerId, CampaignId: campaignId, Geo: geo, MarketPlace: marketplace, RuleName: ruleName, RuleId: rule.ID, LastAnalysedAt: &lastAnalysed,
		ProfileId: profile.ProfileId, State: "enabled", AlertRequired: form.AlertRequired, RunningStatus: "ready"}

	//ruleMapTmp := rulesmodels.UserRuleMapping{{}}
	_ = mysql.DB.Where("`user_rule_mappings`.`market_place` = ? and `user_rule_mappings`.`geo` =? and "+
		"`user_rule_mappings`.`seller_id` = ? and `user_rule_mappings`.`profile_id` = ? and `user_rule_mappings`.`campaign_id` = ? "+
		" and `user_rule_mappings`.`rule_name` =? and `user_rule_mappings`.`rule_id` = ? and deleted_at is null",
		ruleMap.MarketPlace, ruleMap.Geo, ruleMap.SellerId, ruleMap.ProfileId, ruleMap.CampaignId, ruleMap.RuleName, ruleMap.RuleId).Find(&ruleMap).Error

	if ruleMap.ID != 0 {
		log.Error("rule already exist")
		err = errors.New("Rule exist. please try different")
		return
	}
	err = mysql.DB.Create(&ruleMap).Error
	if err != nil {
		log.Error(err)
		return err
	}
	for _, attr := range form.Attributes {
		attrModels := rulesmodels.UserRuleAttributes{AttrValue: attr.AttrValue, AttrKey: attr.AttrKey, RuleMapId: ruleMap.ID}
		err = mysql.DB.Where(&attrModels).FirstOrCreate(&attrModels).Error

		if err != nil {
			log.Error(err)
			return err
		}
	}

	return err
}

func GetRules(duration string) (result []rulesmodels.UserRuleDbResult, err error) {

	err = mysql.DB.Table("user_rule_mappings").Select("user_rule_mappings.market_place,user_rule_mappings.geo,user_rule_mappings.seller_id,campaign_id,`condition`,`action`,`rule_name`, rule_id, user_rule_mappings.profile_id,user_rule_mappings.id rule_map_id," +
		"profiles.email,user_rule_mappings.state,alert_required").
		Joins("join user_rules on  user_rule_mappings.rule_id = user_rules.id").
		Joins("join profiles on profiles.profile_id = user_rule_mappings.profile_id ").
		Where("user_rule_mappings.deleted_at is null and last_analysed_at is not null and last_analysed_at < now() - Interval " + duration + " and user_rule_mappings.state = 'enabled' ").
		Where("user_rule_mappings.running_status != 'pending'").
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}
	return

}

func UpdateRuleRunningStatus(idList []uint, status string) (err error) {
	var ruleMap rulesmodels.UserRuleMapping
	err = mysql.DB.Model(&ruleMap).Where("id in (?)", idList).Update("running_status", status).Error
	return
}

func UpdateRuleMapping(data rulesmodels.UserRuleMapping) (err error) {
	err = mysql.DB.Model(&data).Where("market_place = ? and geo = ? and seller_id = ? and campaign_id =? and rule_id =? and deleted_at is null ", data.MarketPlace,
		data.Geo, data.SellerId, data.CampaignId, data.RuleId).Updates(&data).Error
	return

}

func PushToRuleLog(data rulesmodels.RuleActionLog) (err error) {
	err = mysql.DB.Create(&data).Error
	return
}

func GetRuleAttribute(ruleMapId uint, attrKey string) (err error, attrVal string) {
	result := rulesmodels.UserRuleAttributes{}
	err = mysql.DB.Where("rule_map_id  = ? and attr_key= ?", ruleMapId, attrKey).Find(&result).Error
	attrVal = result.AttrValue
	return
}

func FetchEventLogs(sellerId string) (err error, result []rulesmodels.RuleActionLog) {

	err = mysql.DB.Where("seller_id = ?", sellerId).Find(&result).Error
	return
}

func GetRuleMapById(id uint) (result rulesmodels.UserRuleMapping, err error) {

	err = mysql.DB.Table("user_rule_mappings").
		Where("id = ? and deleted_at is null ", id).
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}
	return

}

func GetRulesBySellerId(sellerId string) (result []rulesmodels.UserRuleMapping, err error) {

	err = mysql.DB.Table("user_rule_mappings").Select("user_rule_mappings.*").
		Where("seller_id = ? and deleted_at is null ", sellerId).
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}
	return

}

func ChangeRuleState(sellerId, state string, id uint) (result rulesmodels.UserRuleMapping, err error) {

	var mapTable rulesmodels.UserRuleMapping
	err = mysql.DB.Model(&mapTable).Where(" id = ? and seller_id = ? ", id, sellerId).Update("state", state).Error
	if err != nil {
		log.Error(err)
		return
	}
	err = mysql.DB.Table("user_rule_mappings").Select("user_rule_mappings.*").
		Where("seller_id = ? and id = ? and  deleted_at is null ", sellerId, id).
		Find(&result).Error
	if err != nil {
		log.Error(err)
		return
	}
	return

}

func DeleteRule(sellerId string, id uint) (err error) {

	var mapTable rulesmodels.UserRuleMapping
	err = mysql.DB.Where(" id = ? and seller_id = ? ", id, sellerId).Delete(&mapTable).Error
	if err != nil {
		log.Error(err)
		return
	}
	return

}
func UpdatePendingRulesAsFailed() (err error) {
	var rule rulesmodels.UserRuleMapping
	err = mysql.DB.Model(&rule).Where("running_status not in ('ready') and deleted_at is null").Update("running_status", "failed").Error
	return
}
