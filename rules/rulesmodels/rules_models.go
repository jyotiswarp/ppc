package rulesmodels

import (
	"bitbucket.org/jyotiswarp/ppc/models"
	"github.com/jinzhu/gorm"
	"time"
)

type RulesForm struct {
	SellerId    string `json:"sellerId" binding:"required"`
	MarketPlace string `json:"marketPlace" binding:"required"`
	Geo         string `json:"geo" binding:"required"`
	CampaignId  int64  `json:"campaignId" binding:"required"`
	//ProfileId     int64       `json:"profileId" binding:"required"`
	Rule          string     `json:"rule" binding:"required"`
	Action        string     `json:"action" binding:"required"`
	RuleName      string     `json:"ruleName" binding:"required"`
	Attributes    []RuleAttr `json:"attributes"`
	AlertRequired bool       `json:"alertRequired"`
}

type RulesStateForm struct {
	RuleMapId uint   `json:"ruleMapId" binding:"required"`
	State     string `json:"state" binding:"required"`
}
type RulesDeleteForm struct {
	RuleMapId uint `json:"ruleMapId" binding:"required"`
}
type RuleAttr struct {
	AttrKey   string `json:"attr_key"`
	AttrValue string `json:"attr_value"`
}

type UserRuleDbResult struct {
	MarketPlace string `json:"market_place"`
	Geo         string `json:"geo"`
	SellerId    string `json:"seller_id"`
	CampaignId  int64  `json:"campaign_id"`
	Condition   string `json:"condition"`
	Action      string `json:"action"`
	RuleName    string `json:"rule_name"`
	RuleId      uint   `json:"rule_id"`
	RuleMapId   uint   `json:"rule_map_id"`
	ProfileId   int64  `json:"profile_id"`
	Email       string `json:"email"`
	State       string `json:"state"`
	AlertRequired bool       `json:"alertRequired"`
}

type UserRules struct {
	gorm.Model
	Condition string `json:"condition"`
	Action    string `json:"action"`
}

type UserRuleMapping struct {
	gorm.Model
	MarketPlace    string          `json:"marketPlace" gorm:"index:idx_market_geo_seller_profile_campaign"`
	Geo            string          `json:"geo" gorm:"index:idx_market_geo_seller_profile_campaign"`
	SellerId       string          `json:"sellerId" gorm:"index:idx_market_geo_seller_profile_campaign"`
	ProfileId      int64           `json:"profileId" gorm:"index:idx_market_geo_seller_profile_campaign"`
	CampaignId     int64           `json:"campaignId" gorm:"index:idx_market_geo_seller_profile_campaign"`
	Campaign       models.Campaign `gorm:"ForeignKey:CampaignId" json:"-"`
	RuleName       string          `json:"ruleName"`
	RuleId         uint            `json:"ruleId"`
	UserRules      UserRules       `gorm:"ForeignKey:RuleId" json:"-"`
	LastAnalysedAt *time.Time      `json:"lastAnalysedAt"`
	State          string          `json:"state"`
	AlertRequired  bool            `json:"alertRequired"`
	RunningStatus  string          `json:"runningStatus"`
}

type UserRuleAttributes struct {
	gorm.Model
	RuleMapId uint            `json:"rule_map_id" gorm:"index:rule_map_id_attr_key"`
	RuleMap   UserRuleMapping `gorm:"ForeignKey:RuleMapId" json:"-"`
	AttrKey   string          `json:"attr_key" gorm:"index:rule_map_id_attr_key"`
	AttrValue string          `json:"attr_value"`
}
type RuleActionLog struct {
	gorm.Model
	SellerId   string `json:"sellerId" gorm:"index:seller_id_rule_map_id"`
	RuleMapId uint            `json:"rule_map_id" gorm:"index:seller_id_rule_map_id"`
	RuleMap   UserRuleMapping `gorm:"ForeignKey:RuleMapId" json:"-"`
	IsError    bool   `json:"is_error"`
	RuleName   string `json:"rule_name"`
	Action     string `json:"action"`
	LogMessage string `json:"log_message" gorm:"type:TEXT"`
}
