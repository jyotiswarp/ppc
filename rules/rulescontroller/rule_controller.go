package rulescontroller

import (
	"bitbucket.org/jyotiswarp/ppc/rules/rulesmodels"
	"bitbucket.org/jyotiswarp/ppc/rules/rulesservices"
	"github.com/gin-gonic/gin"
)

func CreateRule(c *gin.Context) {

	formRequest := c.Keys["form_data"].(*rulesmodels.RulesForm)

	err := rulesservices.CreateNewRule(*formRequest)

	if err != nil {
		if err.Error() == "Rule exist. please try different" {
			c.JSON(400, gin.H{
				"error": err.Error(),
			})
			return
		}
		c.JSON(400, gin.H{
			"error": "Invalid Rule",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": "success",
	})
	return
}

func GetAllRules(c *gin.Context) {

	result, err := rulesservices.GetAllRules("1 day")
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Invalid Rule",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetEventLogs(c *gin.Context) {

	sellerId, _ := c.GetQuery("sellerId")
	if sellerId == "" {
		c.JSON(400, gin.H{"error": "sellerId is missing"})
		return
	}
	err, result := rulesservices.GetEventLog(sellerId)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "No Logs",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func GetRulesBySellerId(c *gin.Context) {

	sellerId, _ := c.GetQuery("sellerId")
	if sellerId == "" {
		c.JSON(400, gin.H{"error": "sellerId is missing"})
		return
	}
	err, result := rulesservices.GetRulesBySellerId(sellerId)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "No rules",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func ChangeRuleState(c *gin.Context) {

	sellerId, _ := c.GetQuery("sellerId")
	if sellerId == "" {
		c.JSON(400, gin.H{"error": "sellerId is missing"})
		return
	}
	formRequest := c.Keys["form_data"].(*rulesmodels.RulesStateForm)
	result, err := rulesservices.ChangeRuleState(sellerId, formRequest.State, formRequest.RuleMapId)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Update error",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": result,
	})
	return
}

func DeleteRule(c *gin.Context) {

	sellerId, _ := c.GetQuery("sellerId")
	if sellerId == "" {
		c.JSON(400, gin.H{"error": "sellerId is missing"})
		return
	}
	formRequest := c.Keys["form_data"].(*rulesmodels.RulesDeleteForm)
	err := rulesservices.DeleteRule(sellerId, formRequest.RuleMapId)
	if err != nil {
		c.JSON(400, gin.H{
			"error": "Update error",
		})
		return
	}
	c.JSON(200, gin.H{
		"result": "Success",
	})
	return
}
