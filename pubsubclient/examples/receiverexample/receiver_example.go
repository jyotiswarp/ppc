package receiverexample

import (
	"bitbucket.org/sellerprimehub/spcoreclients/pubsubclient"
	"fmt"
	"github.com/spf13/viper"
)

var listener pubsubclient.PubSubReceive

func Init() {
	listenTrendsSubs()
}

func listenTrendsSubs() {
	listener = pubsubclient.PubSubReceive{}
	listener.Subscription = viper.GetString("trends_subs_queue")
	fmt.Println(listener.Subscription)
	listener.IsLogEnabled = true
	fmt.Println("Listener initiated")
	listener.Init()
}

func StartIngestion() {
	for {
		a := listener.Get()
		a.Ack()
	}
}
