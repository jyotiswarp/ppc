package examples

import (
	"bitbucket.org/sellerprimehub/spcoreclients/pubsubclient/examples/ingestorexample"
	"bitbucket.org/sellerprimehub/spcoreclients/pubsubclient/examples/receiverexample"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

var router *gin.Engine

func init() {

	viper.SetConfigName("app")
	viper.AddConfigPath("$GOPATH/src/bitbucket.org/sellerprimehub/spcoreclients/pubsubclient/config")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("config file not found")
		return
	}
	viper.WatchConfig()
	ingestorexample.Init()
	receiverexample.Init()
}

func main() {
	go ingestorexample.StartIngestion()
	go receiverexample.StartIngestion()
	router.Run(":" + viper.GetString("port"))
}
