package pubsubclient

import (
	"cloud.google.com/go/pubsub"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"golang.org/x/net/context"
)

type PubSubReceive struct {
	Subscription string                                                    //Pubsub Subscription Name
	Parse        func(msg *pubsub.Message) (map[string]interface{}, error) //To Parse the message and check it's validity
	messageChan  chan *pubsub.Message                                      //Message Channel
	receiveChan  chan bool                                                 //To signal receive
	sub          *pubsub.Subscription
	IsLogEnabled bool
}

func (receiver *PubSubReceive) receive() {
	<-receiver.receiveChan
	receiver.logf("%s Start.", receiver.Subscription)
	ctx := context.Background()
	cctx, _ := context.WithCancel(ctx)
	err := receiver.sub.Receive(cctx, func(c context.Context, message *pubsub.Message) {
		//receiver.logf("Got message from %s \n", receiver.Subscription)
		receiver.messageChan <- message
		<-receiver.receiveChan //block and wait for the signal
		return
	})

	if err != nil {
		receiver.logf("%s recieve Error : %v\n", receiver.Subscription, err)
	}
	fmt.Println("out of method")
}

//Consumes one message out of the pool.
func (receiver *PubSubReceive) Get() (obj *pubsub.Message) {
	receiver.receiveChan <- true
	received := <-receiver.messageChan
	return received
}

func (receiver *PubSubReceive) logf(format string, a ...interface{}) {
	if receiver.IsLogEnabled {
		log.Printf(format, a...)
	}
}

func (receiver *PubSubReceive) Init() {
	receiver.messageChan = make(chan *pubsub.Message)
	receiver.receiveChan = make(chan bool)
	client := GetPubSubClient()
	receiver.sub = client.Subscription(receiver.Subscription)
	receiver.sub.ReceiveSettings.NumGoroutines = viper.GetInt("pubsub_go_routines")
	//receiver.sub.ReceiveSettings.MaxOutstandingMessages = viper.GetInt("pubsub_no_of_nack_messages")
	go receiver.receive()
}
